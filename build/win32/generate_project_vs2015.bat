cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 2015" ..\..\
cd ..\..\
set IF_SOURCE_TOP=%CD%
cd %IF_SOURCE_TOP%\build\win32

IF EXIST "C:\Program Files\Microsoft Visual Studio 14.0\Common7\Tools\vsvars32.bat" (
    call "C:\Program Files\Microsoft Visual Studio 14.0\Common7\Tools\vsvars32.bat"
) ELSE (
    call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\vsvars32.bat"
)

devenv ProjectEuler.sln