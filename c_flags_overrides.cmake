#c_flag_overrides.cmake
if(MSVC)
    set(CMAKE_C_FLAGS_DEBUG_INIT "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1")
    set(CMAKE_C_FLAGS_MINSIZEREL_INIT     "/MT /O1 /Ob1 /D NDEBUG")
    set(CMAKE_C_FLAGS_RELEASE_INIT        "/MT /O2 /Ob2 /D NDEBUG")
    set(CMAKE_C_FLAGS_RELWITHDEBINFO_INIT "/MT /Zi /O2 /Ob1 /D NDEBUG")
    elseif (CMAKE_COMPILER_IS_GNUCC)
    message(STATUS "Gnu-Compiler detected, setting custom flags")
    set(CMAKE_C_FLAGS_DEBUG
        "-O2 -g -D_DEBUG" CACHE STRING "Debug options." FORCE
       )
    set(CMAKE_C_FLAGS_RELEASE
        "-O2 -fomit-frame-pointer -DNDEBUG" CACHE STRING "Release options." FORCE
       )
    set(CMAKE_C_FLAGS_RELWITHDEBINFO
        "${CMAKE_C_FLAGS_RELEASE} -g" CACHE STRING "Release with debug info options." FORCE
       )
    endif()
