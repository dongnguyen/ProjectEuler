// Prime triples and geometric sequences
// Problem 518
// Let S(n) = a+b+c over all triples (a,b,c) such that:

// a, b, and c are prime numbers.
// a < b < c < n.
// a+1, b+1, and c+1 form a geometric sequence.
// For example, S(100) = 1035 with the following triples:

// (2, 5, 11), (2, 11, 47), (5, 11, 23), (5, 17, 53), (7, 11, 17), (7, 23, 71), (11, 23, 47), (17, 23, 31), (17, 41, 97), (31, 47, 71), (71, 83, 97)

// Find S(108).

#include "template_class.h"
#include <thread>
#include <mutex>

extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);
static vector<uint64_t> prime_list;
static vector<uint64_t> prime_list_plus;
static vector<uint64_t> plus_list_square;
static set<uint64_t> set_prime_list;
static set<uint64_t> set_prime_list_plus;

#define N 1e8

bool checkGeoSeq(uint64_t a, uint64_t b, uint64_t* c_out, bool* should_break)
{
	//*should_break = false;
	//a++;
	//b++;
	//factor<uint64_t> r(b, a);
	//factor<uint64_t> c = factor<uint64_t>(b, 1)*r;
	factor<uint64_t> c(b*b, a);
	//if (c.denum != 1)
	{
		//c.reduce();
	}
	//c.reduce();
	if (c.denum == 1)
	{
		//if (c.num - 1 > N)
		if (c.num > N)
		{
			//*should_break = true;
			return false;
		}
		//if (set_prime_list_plus.find(c.num - 1) != set_prime_list_plus.end())
		if (set_prime_list_plus.find(c.num) != set_prime_list_plus.end())
		{
			//*c_out = c.num - 1;
			*c_out = c.num;
			/*r.reduce();
			if (r.denum != 1)
			{
				cout << "special" << endl;
			}*/
			return true;
		}
	}
	return false;
}

#define THREAD_NUM 8
uint64_t sumThread[THREAD_NUM] = { 0 };
static std::mutex coutlock;

void thread518(int id)
{
	uint64_t sum = 0;
	uint64_t size = prime_list.size();
	// faster faster
	for (int i = id; i < size - 2; i += THREAD_NUM)
	{
		uint64_t a = prime_list_plus[i];
		for (int j = i + 1; j < size - 1; j++)
		{
			uint64_t b = prime_list_plus[j];
			uint64_t b2 = plus_list_square[j];
			if (b2 % a == 0)
			{
				uint64_t c = b2 / a;
				if (c > N)
				{
					break;
				}
				if (set_prime_list_plus.find(c) != set_prime_list_plus.end())
				{
					sum += a + b + c - 3;
				}
			}
		}
	}
#if 0 // faster
	/*coutlock.lock();
	cout << "id: " << id << endl;
	coutlock.unlock();*/
	for (int i = id + 1; i < size - 1; i += THREAD_NUM)
	{
		if (i % 10000 == 0)
		{
			coutlock.lock();
			cout << i << endl;
			coutlock.unlock();
		}
		/*if (prime_list[i] == 83)
		{
			int debug = 0;
			debug++;
		}*/
		vector<uint64_t> divisors = divisors_of_n(plus_list_square[i], prime_list);
		int ss = divisors.size();
		for (int j = 0; j < ss / 2; j++)
		{
			uint64_t a = divisors[j] - 1;
			//uint64_t c = plus_list_square[i] / divisors[j] - 1;
			uint64_t c = divisors[ss - j - 1] - 1;
			//if (a & 1 == 0 || a == 2)
			if ((a != 2) && ((a & 1) == 0))
			{
				continue;
			}
			if ((c & 1) == 0)
			{
				continue;
			}
			if (a == c)
			{
				break;
			}
			if (c > N)
			{
				continue;
			}
			if (set_prime_list.find(a) != set_prime_list.end())
			{
				if (set_prime_list.find(c) != set_prime_list.end())
				{
					sum += a + c + prime_list[i];
					//(2, 5, 11)
					/*coutlock.lock();
					cout << "(" << a << ", " << prime_list[i] << ", " << c << ")" << endl;
					coutlock.unlock();*/
				}
			}
		}
	}
	//#else
	for (int i = id; i < size - 2; i += THREAD_NUM)
	{
		uint64_t a = prime_list_plus[i];
		uint64_t limit = sqrt(prime_list_plus[i] * prime_list_plus.back());
		bool should_break = false;
		for (int j = i + 1; j < size - 1; j++)
		{
			uint64_t b = prime_list_plus[j];
			uint64_t c = 0;
			if (b > limit)
			{
				break;
			}
			if (checkGeoSeq(a, b, &c, &should_break))
			{
				sum += a + b + c - 3;
			}
		}
	}
#endif
	sumThread[id] = sum;
}

uint64_t problem_518()
{
	create_prime_filter_method(&prime_list, 0, N);
	plus_list_square.resize(prime_list.size());
	prime_list_plus.resize(prime_list.size());
	for (int i = 0; i < prime_list.size(); i++)
	{
		//plus_list_square[i] = (prime_list[i] + 1) >> 1;
		plus_list_square[i] = prime_list[i] + 1;
		prime_list_plus[i] = prime_list[i] + 1;
		plus_list_square[i] *= plus_list_square[i];
	}
	set_prime_list.insert(prime_list.begin(), prime_list.end());
	set_prime_list_plus.insert(prime_list_plus.begin(), prime_list_plus.end());
	uint64_t sum = 0;
	cout << prime_list.size() << endl;

	std::thread threads[THREAD_NUM];
	for (int i = 0; i < THREAD_NUM; i++)
	{
		threads[i] = std::thread(thread518, i);
	}
	for (int i = 0; i < THREAD_NUM; i++)
	{
		threads[i].join();
	}
	for (int i = 0; i < THREAD_NUM; i++)
	{
		cout << "thread " << i << ": " << sumThread[i] << endl;
		sum += sumThread[i];
	}
	cout << sum << endl;
	return 0;
}
