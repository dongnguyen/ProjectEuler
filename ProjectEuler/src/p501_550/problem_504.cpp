// Square on the Inside
// Problem 504
// Let ABCD be a quadrilateral whose vertices are lattice points lying on the coordinate axes as follows:

// A(a, 0), B(0, b), C(−c, 0), D(0, −d), where 1 ≤ a, b, c, d ≤ m and a, b, c, d, m are integers.

// It can be shown that for m = 4 there are exactly 256 valid ways to construct ABCD. Of these 256 quadrilaterals, 42 of them strictly contain a square number of lattice points.

// How many quadrilaterals ABCD strictly contain a square number of lattice points for m = 100?

// best https://en.wikipedia.org/wiki/Pick%27s_theorem

#include "template_class.h"

#define M 100

int numLattice[M + 1][M + 1];

int calcReduce(int i, int j)
{
	if (i < 2)
	{
		return 0;
	}
	if (j < 2)
	{
		return 0;
	}
	return gcd(i, j) - 1;
}

void buildLattice()
{
	for (int i = 1; i <= M; i++)
	{
		for (int j = i; j <= M; j++)
		{
			int total = (i + 1)*(j + 1);
			int border = (i + j) * 2;
			int reduce = calcReduce(i, j);
			int left = (total - border - reduce) / 2;
			numLattice[i][j] = left;
			numLattice[j][i] = left;
		}
	}
}

extern bool is_square(int n);

uint64_t problem_504()
{
	uint64_t cnt = 0;
	buildLattice();
	for (int a = 1; a <= M; a++)
	{
		for (int b = 1; b <= M; b++)
		{
			for (int c = 1; c <= M; c++)
			{
				for (int d = 1; d <= M; d++)
				{
					int sum = 1;
					sum += numLattice[a][b];
					sum += numLattice[c][b];
					sum += numLattice[a][d];
					sum += numLattice[c][d];
					sum += a + b + c + d - 4;
					if (is_square(sum))
					{
						cnt++;
					}
				}
			}
		}
	}
	cout << cnt << endl;
	return cnt;
}
