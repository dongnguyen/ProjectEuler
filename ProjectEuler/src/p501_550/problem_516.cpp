//5-smooth totients
//Problem 516
//5-smooth numbers are numbers whose largest prime factor doesn't exceed 5.
//5-smooth numbers are also called Hamming numbers.
//Let S(L) be the sum of the numbers n not exceeding L such that Euler's totient function φ(n) is a Hamming number.
//S(100)=3728.
//
//Find S(1012). Give your answer modulo 232.

#include "template_class.h"

set<uint64_t> hamming;
set<uint64_t> hamming_totient;
//set<uint64_t> possibleN;
//map<uint64_t, vector<int>> possibleN;

//#define N ((uint64_t)1e12)
#define N 100
static const uint64_t MOD = ((uint64_t)(pow(2, 32) + 0.5));
extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);
static vector<uint64_t> prime_list;
static vector<uint64_t> possiblePrimes;
static set<uint64_t> set_prime_list;

static bool testPrime(uint64_t n)
{
	int size = prime_list.size();
	uint64_t limit = sqrt(n) + 0.5;
	for (int i = 0; i < size; i++)
	{
		if (limit < prime_list[i])
		{
			break;
		}
		if (n % prime_list[i] == 0)
		{
			return false;
		}
	}
	return true;
}

inline void calcSum(uint64_t &sum, uint64_t num, map<uint64_t, int> factor)
{
	uint64_t phi = num;
	for (auto ite : factor)
	{
		phi /= ite.first;
		phi *= ite.first - 1;
	}
	if (hamming_totient.find(phi) != hamming_totient.end())
	{
		sum += num;
		if (sum > MOD)
		{
			sum %= MOD;
		}
	}
}

uint64_t problem_516()
{
	create_prime_filter_method(&prime_list, 0, sqrt(N));
	set_prime_list.insert(prime_list.begin(), prime_list.end());
	// generate hamming set
	set<uint64_t> seeds;
	seeds.insert(1);
	while (!seeds.empty())
	{
		set<uint64_t> newseeds;
		for (auto s : seeds)
		{
			uint64_t next = s * 2;
			if (next <= N)
			{
				newseeds.insert(next);
				hamming.insert(next);
			}
			next = s * 3;
			if (next <= N)
			{
				newseeds.insert(next);
				hamming.insert(next);
			}
			next = s * 5;
			if (next <= N)
			{
				newseeds.insert(next);
				hamming.insert(next);
			}
		}
		seeds = newseeds;
	}
	cout << "hamming.size: " << hamming.size() << endl;
	hamming_totient.insert(1);
	for (auto h : hamming)
	{
		uint64_t p = h + 1;
		if (testPrime(p))
		{
			possiblePrimes.push_back(p);
		}
		if (h % 2 == 0)
		{
			hamming_totient.insert(h);
		}
	}
	cout << "possiblePrimes.size: " << possiblePrimes.size() << endl;
	cout << "hamming_totient.size: " << hamming_totient.size() << endl;
#if 0
	uint64_t maxsize = possiblePrimes.size();
	for (auto h : hamming_totient)
	{
		uint64_t m = h;
		// find all p: p - 1 | m
		vector<uint64_t> listp;
		for (int i = 0; i < maxsize; i++)
		{
			if ((possiblePrimes[i] - 1) <= m)
			{
				if ((m % (possiblePrimes[i] - 1)) == 0)
				{
					listp.push_back(possiblePrimes[i]);
				}
			}
			else
			{
				break;
			}
		}
		cout << m << " - listp.size: " << listp.size() << endl;
	}
#endif
#if 1
	uint64_t maxsize = possiblePrimes.size();
	//generate all possible N
	map<uint64_t, map<uint64_t, int>> seedsN;
	map<uint64_t, int> empty;
	seedsN[1] = empty;
	uint64_t sum = 1;
	while (!seedsN.empty())
	{
		map<uint64_t, map<uint64_t, int>> newseedsN;
		for (auto s : seedsN)
		{
			map<uint64_t, int> current_factor = s.second;
			uint64_t largestP = 0;
			if (!current_factor.empty())
			{
				largestP = current_factor.rbegin()->first;
			}
			uint64_t current_N = s.first;
			uint64_t next = 0;
			if (2 >= largestP)
			{
				next = current_N * 2;
				if (next <= N)
				{
					if (current_factor.find(2) == current_factor.end())
					{
						current_factor[2] = 1;
					}
					else
					{
						int newF = current_factor[2];
						current_factor[2] = ++newF;
					}
					newseedsN[next] = current_factor;
					calcSum(sum, next, current_factor);
				}
			}
			if (3 >= largestP)
			{
				next = current_N * 3;
				if (next <= N)
				{
					if (current_factor.find(3) == current_factor.end())
					{
						current_factor[3] = 1;
					}
					else
					{
						int newF = current_factor[3];
						current_factor[3] = ++newF;
					}
					newseedsN[next] = current_factor;
					calcSum(sum, next, current_factor);
				}
			}
			if (5 >= largestP)
			{
				next = current_N * 5;
				if (next <= N)
				{
					if (current_factor.find(5) == current_factor.end())
					{
						current_factor[5] = 1;
					}
					else
					{
						int newF = current_factor[5];
						current_factor[5] = ++newF;
					}
					newseedsN[next] = current_factor;
					calcSum(sum, next, current_factor);
				}
			}
			// all remain possible primes
			for (int i = 2; i < maxsize; i++)
			{
				uint64_t p = possiblePrimes[i];
				if (current_factor.find(p) == current_factor.end())
				{
					if (p >= largestP)
					{
						next = current_N * p;
						if (next <= N)
						{
							current_factor[p] = 1;
							newseedsN[next] = current_factor;
							calcSum(sum, next, current_factor);
						}
					}
				}
			}
		}
		seedsN = newseedsN;
	}
	cout << "sum: " << sum << endl;
#endif
	return 0;
}
