// Divisibility of factorials
// Problem 549
// The smallest number m such that 10 divides m! is m=5.
// The smallest number m such that 25 divides m! is m=10.

// Let s(n) be the smallest number m such that n divides m!.
// So s(10)=5 and s(25)=10.
// Let S(n) be ∑s(i) for 2 ≤ i ≤ n.
// S(100)=2012.

// Find S(108).

// s(i) -> https://en.wikipedia.org/wiki/Kempner_function

#include "template_class.h"
#include <string>

extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);

static vector<uint64_t> prime_list;
static set<uint64_t> set_prime_list;
static set<uint64_t> set_fact_list;
static vector<uint64_t> fact_list;
static vector<vector<int>> fact_nums;

//uint64_t Kempner[(unsigned int)1e8 + 1];
uint64_t *Kempner;

uint64_t sFactorials(uint64_t n)
{
	uint64_t keepn = n;
	//cout << "keepn = " << keepn << endl;
	if (Kempner[n] != -1)
	{
		return Kempner[n];
	}
	if (set_fact_list.find(n) != set_fact_list.end())
	{
		int findn = 2;
		for (; findn < fact_list.size(); findn++)
		{
			if (fact_list[findn] == n)
			{
				// cout << "N! = " << n << " - N = " << findn << endl;
				Kempner[n] = findn;
			}
		}
		return Kempner[n];
	}
	uint64_t max = 0;
	//uint64_t sqrtn = sqrt(n);
	for (int i = 0; i < prime_list.size(); i++)
	{
		if (prime_list[i] > n)
		{
			break;
		}
		uint64_t tmp = 1;
		int check = 0;
		while (n % prime_list[i] == 0)
		{
			tmp *= prime_list[i];
			n /= prime_list[i];
			check++;
		}
		if (check)
		{
			uint64_t k = 0;
			if (Kempner[tmp] == -1)
			{
				uint64_t cnt = 1;
				k = prime_list[i];
				uint64_t x = prime_list[i];
				while (x % tmp)
				{
					//cout << "x = " << x << endl;
					cnt++;
					k = cnt*prime_list[i];
					x *= k;
				}
				Kempner[tmp] = k;
			}
			else
			{
				k = Kempner[tmp];
			}
			if (max < k)
			{
				//cout << "k = " << k << endl;
				max = k;
			}
			if (Kempner[n] != -1)
			{
				if (max < Kempner[n])
				{
					max = Kempner[n];
				}
				break;
			}
		}
	}
	Kempner[keepn] = max;
	return max;
}

//uint64_t pick_one(int start_idx, uint64_t max_product, uint64_t cur_product, bool* result)
//{
//	uint64_t try_pick = prime_list[start_idx];
//}

uint64_t num_refined = 0;

void refine_2()
{
	for (int i = 0; i < prime_list.size() - 1; i++)
	{
		if (prime_list[i] > 1e4)
		{
			return;
		}
		uint64_t pd1 = 1;
		for (int e1 = 1; e1 <= prime_list[i]; e1++)
		{
			pd1 *= prime_list[i];
			if (pd1 > 1e8)
			{
				break;
			}
			uint64_t s1 = prime_list[i] * e1;
			for (int j = i + 1; j < prime_list.size(); j++)
			{
				uint64_t pd2 = pd1;
				int e2 = 1;
				for (; e2 <= prime_list[j]; e2++)
				{
					pd2 *= prime_list[j];
					uint64_t s2 = prime_list[j] * e2;
					if (pd2 > 1e8)
					{
						break;
					}
					num_refined++;
					Kempner[pd2] = max(s1, s2);
				}
				if (pd2 > 1e8 && e2 == 1)
				{
					break;
				}
			}
		}
	}
}

void refine_3()
{
	static const uint64_t l = pow(1e8, 1. / 3);
	for (int i = 0; i < prime_list.size() - 1; i++)
	{
		if (prime_list[i] > l)
		{
			return;
		}

		uint64_t pd1 = 1;
		for (int e1 = 1; e1 <= prime_list[i]; e1++)
		{
			pd1 *= prime_list[i];
			if (pd1 > 1e8)
			{
				break;
			}
			uint64_t s1 = prime_list[i] * e1;
			for (int j = i + 1; j < prime_list.size(); j++)
			{
				uint64_t pd2 = pd1;
				int e2 = 1;
				for (; e2 <= prime_list[j]; e2++)
				{
					pd2 *= prime_list[j];
					uint64_t s2 = prime_list[j] * e2;
					if (pd2 > 1e8)
					{
						break;
					}
					for (int k = j + 1; k < prime_list.size(); k++)
					{
						uint64_t pd3 = pd2;
						int e3 = 1;
						for (; e3 <= prime_list[k]; e3++)
						{
							pd3 *= prime_list[k];
							uint64_t s3 = prime_list[k] * e3;
							if (pd3 > 1e8)
							{
								break;
							}
							num_refined++;
							Kempner[pd3] = max(max(s1, s2), s3);
						}
						if (pd3 > 1e8 && e3 == 1)
						{
							break;
						}
					}
				}
				if (pd2 > 1e8 && e2 == 1)
				{
					break;
				}
			}
		}
	}
}

void refine_4()
{
	static const uint64_t li = pow(1e8, 1. / 4);
	for (int i = 0; i < prime_list.size() - 1; i++)
	{
		if (prime_list[i] > li)
		{
			return;
		}
		for (int j = i + 1; j < prime_list.size(); j++)
		{
			uint64_t pd = prime_list[i] * prime_list[j];
			if (pd > 1e8)
			{
				break;
			}
			for (int k = j + 1; k < prime_list.size(); k++)
			{
				uint64_t pd2 = pd * prime_list[k];
				if (pd2 > 1e8)
				{
					break;
				}
				for (int l = k + 1; l < prime_list.size(); l++)
				{
					uint64_t pd3 = pd2 * prime_list[l];
					if (pd3 > 1e8)
					{
						break;
					}
					num_refined++;
					Kempner[pd3] = k;
				}
			}
		}
	}
}

uint64_t problem_549()
{
	Kempner = new uint64_t[(unsigned int)1e8 + 1];
	uint64_t ret = 0;
	create_prime_filter_method(&prime_list, 2, 1e8);
	set_prime_list.insert(prime_list.begin(), prime_list.end());
	uint64_t fact = 1;
	uint64_t n = 1;
	fact_list.resize(14);
	//fact_nums.resize(1e8);
	while (fact < 1e8)
	{
		fact_list[n] = fact;
		set_fact_list.insert(fact);
		fact *= ++n;
	}
	for (uint64_t i = 0; i <= 1e8; i++)
	{
		Kempner[i] = -1;
	}
	// refinement
	for (int i = 0; i < prime_list.size(); i++)
	{
		uint64_t A = prime_list[i];
		uint64_t p = prime_list[i];
		uint64_t e = 1; // A = p^e
		num_refined++;
		Kempner[p] = p;
		for (e = 2; e <= p; e++)
		{
			A *= p;
			if (A > 1e8)
			{
				break;
			}
			num_refined++;
			Kempner[A] = p*e;
		}
		// ignore e > p
	}
	cout << "prime_list.size(): " << prime_list.size() << endl;
	//refine_2();
	cout << (int)pow(1e8, 1. / 3) << endl;
	//refine_3();
	cout << (int)pow(1e8, 1. / 4) << endl;
	//refine_4();
	cout << sFactorials(8) << endl;
	cout << sFactorials(10) << endl;
	cout << sFactorials(25) << endl;
	for (uint64_t i = 2; i <= 1e8; i++)
	{
		if (i % 1000000 == 0)
		{
			cout << i << endl;
		}
		uint64_t s = sFactorials(i);
		//cout << "n: " << i << " A: " << s << endl;
		ret += s;
	}
	/*cout << num_refined << endl;
	ret = 1e8 - num_refined;*/
	delete Kempner;
	return ret;
}
