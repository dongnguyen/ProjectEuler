
#include "template_class.h"
#include "mpirxx.h"

mpz_class problem_303()
{
    int n = 10000;
    mpz_class sum = 0;
    for(int i = 1; i <= n-2; i++)
        //for(int i = 1; i <= n; i++)
    {
        vector<uint64_t> candidate;
        candidate.resize(2);
        candidate[0] = 1;
        candidate[1] = 2;

        do
        {
            int size = candidate.size();
            uint64_t found = ULLONG_MAX;
            for(int j = 0; j < size; j++)
            {
                if(candidate[j] % i == 0)
                {
                    if(found > candidate[j])
                    {
                        found = candidate[j];
                    }
                    //sum += candidate[j];
#ifdef _DEBUG
                    //int d = candidate[j];
#endif
                    //break;
                }
            }
            if(found != ULLONG_MAX)
            {
                /*if(i == 3333)
                {
                    cout << found << endl;
                }*/
                stringstream ss;
                ss << found/i;
                mpz_class tmp(ss.str());
                sum += tmp;
                break;
            }
            candidate.resize(size * 3);
            for(int j = size; j < 2*size; j++)
            {
                candidate[j] = candidate[j - size] * 10 + 1;
            }
            for(int j = 2*size; j < 3*size; j++)
            {
                candidate[j] = candidate[j - 2*size] * 10 + 2;
            }
            for(int j = 0; j < size; j++)
            {
                candidate[j] = candidate[j] * 10; // add 0;
            }
        }
        while(1);
    }
    //return sum;


    uint64_t found9999 = ULLONG_MAX;
    {
        vector<uint64_t> candidate;
        candidate.resize(2);
        candidate[0] = 1;
        candidate[1] = 2;

        int count = 1;
        do
        {
            int size = candidate.size();
            uint64_t found = ULLONG_MAX;
            candidate.resize(size * 2);
            for(int j = size; j < 2*size; j++)
            {
                candidate[j] = candidate[j - size] * 10 + 2;
            }
            /*for(int j = 2*size; j < 3*size; j++)
            {
                candidate[j] = candidate[j - 2*size] * 10 + 2;
            }*/
            for(int j = 0; j < size; j++)
            {
                candidate[j] = candidate[j] * 10 + 1; // add 1;
            }
            count++;
            if(count == 18)
            {
                //cout << i << " - " << count << endl;
                //exit(-1);
                break;
            }
        }
        while(1);
        int size = candidate.size();

        //cout << size << endl;

        for(int i = 0; i < size; i++)
        {
            uint64_t tmp;
            /*uint64_t tmp = candidate[i] * 100;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }
            tmp = candidate[i] * 100 + 1;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }
            tmp = candidate[i] * 100 + 2;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }*/
            /*===========================*/
            /*tmp = candidate[i] * 100 + 10;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }*/
            tmp = candidate[i] * 100 + 11;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }
            tmp = candidate[i] * 100 + 12;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }
            /*===========================*/
            /*tmp = candidate[i] * 100 + 20;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }*/
            tmp = candidate[i] * 100 + 21;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }
            tmp = candidate[i] * 100 + 22;
            if(tmp % 9999 == 0)
            {
                if(found9999 > tmp)
                {
                    found9999 = tmp;
                }
            }
        }
    }
    //cout << found9999 << endl;
    stringstream ss;
    ss << found9999/9999;
    mpz_class tmp(ss.str());
    sum += tmp;
    return sum + 1;
}
