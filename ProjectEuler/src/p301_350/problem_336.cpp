// Maximix Arrangements
// Problem 336
// A train is used to transport four carriages in the order: ABCD. However, sometimes when the train arrives to collect the carriages they are not in the correct order.
// To rearrange the carriages they are all shunted on to a large rotating turntable. After the carriages are uncoupled at a specific point the train moves off the turntable pulling the carriages still attached with it. The remaining carriages are rotated 180 degrees. All of the carriages are then rejoined and this process is repeated as often as necessary in order to obtain the least number of uses of the turntable.
// Some arrangements, such as ADCB, can be solved easily: the carriages are separated between A and D, and after DCB are rotated the correct order has been achieved.

// However, Simple Simon, the train driver, is not known for his efficiency, so he always solves the problem by initially getting carriage A in the correct place, then carriage B, and so on.

// Using four carriages, the worst possible arrangements for Simon, which we shall call maximix arrangements, are DACB and DBAC; each requiring him five rotations (although, using the most efficient approach, they could be solved using just three rotations). The process he uses for DACB is shown below.

// p336_maximix.gif
// It can be verified that there are 24 maximix arrangements for six carriages, of which the tenth lexicographic maximix arrangement is DFAECB.

// Find the 2011th lexicographic maximix arrangement for eleven carriages.

#include "template_class.h"

#define SIZEN 11
#define TOTAL 39916800
#define OUTPUT 2011

//#define SIZEN 6
//#define TOTAL (2*3*4*5*6)
//#define OUTPUT 10

//#define SIZEN 4
//#define TOTAL (2*3*4)

typedef struct _params
{
	vector<char> set;
	int size;
	_params()
	{
		size = 0;
	}
	_params(vector<char> in_set, int start = 0)
	{
		init(in_set, start);
	}
	void init(vector<char> in_set, int start = 0)
	{
		size = in_set.size() - start;
		set.resize(size);
		for (int i = 0; i < size; i++)
		{
			set[i] = in_set[start + i];
		}
	}
} params;

struct ClassParamCompare
{
	bool operator() (const params& lhs, const params& rhs) const
	{
		if (lhs.size < rhs.size)
		{
			return true;
		}
		if (lhs.size > rhs.size)
		{
			return false;
		}
		for (int i = 0; i < lhs.size; i++)
		{
			if (lhs.set[i] < rhs.set[i])
			{
				return true;
			}
			else if (lhs.set[i] > rhs.set[i])
			{
				return false;
			}
		}
		return false;
	}
};

vector<vector<char>> list_all_comb;
map < params, int, ClassParamCompare> store_rotation;

static int verify(vector<char>& set, int start = 0)
{
	for (int i = start; i < SIZEN; i++)
	{
		if (i != set[i])
		{
			return i;
		}
	}
	return -1;
}

static void rotate(vector<char>& set, int idx)
{
	int middle = (idx + SIZEN - 1) / 2;
	for (int i = idx, j = SIZEN - 1; i <= middle; i++, j--)
	{
		char tmp = set[i];
		set[i] = set[j];
		set[j] = tmp;
	}
}

int calcSimonRotation(vector<char> set, int start = 0)
{
	if (start == SIZEN - 1)
	{
		return 0;
	}
	int invalid = -1;
	invalid = verify(set, start);
	if (invalid == -1)
	{
		return 0;
	}
	params para(set, invalid);
	if (store_rotation.find(para) != store_rotation.end())
	{
		return store_rotation[para];
	}
	int rotation = 0;
	// first rotate
	int find_index = 0;
	for (int i = invalid; i < SIZEN; i++)
	{
		if (set[i] == invalid)
		{
			find_index = i;
			break;
		}
	}
	if (find_index != SIZEN - 1)
	{
		rotation++;
		// rotate from find_index to end
		rotate(set, find_index);
	}
	// rotate from invalid to end
	rotation++;
	rotate(set, invalid);
	int ret = rotation + calcSimonRotation(set, invalid + 1);
	if (invalid >= 2)
	{
		store_rotation[para] = ret;
	}
	return ret;
}

set<vector<char>> max_list;

std::string problem_336()
{
	std::string ret;
	//list_all_comb.resize(TOTAL);
	vector<char> genset;
	genset.resize(SIZEN);
	for (int i = 0; i < SIZEN; i++)
	{
		genset[i] = i;
	}
	int idx = 0;
	int max = 0;
	do
	{
		//list_all_comb[idx++] = genset;
		vector<char> set = genset;
		int needStep = calcSimonRotation(set);
		if (max < needStep)
		{
			max_list.clear();
			max_list.insert(set);
			max = needStep;
		}
		else if (max == needStep)
		{
			max_list.insert(set);
		}
		idx++;
	} while (next_permutation(genset.begin(), genset.end()));

	//cout << "idx: " << idx << endl;
	cout << "max: " << max << endl;
	cout << "num: " << max_list.size() << endl;
	idx = 0;
	for (auto ite : max_list)
	{
		idx++;
		if (idx == OUTPUT)
		{
			cout << idx++ << ": ";
			for (int i = 0; i < SIZEN; i++)
			{
				cout << char('A' + ite[i]);
			}
			cout << endl;
			break;
		}
	}
	return ret;
}
