// Digital root clocks
// Problem 315
// p315_clocks.gif
// Sam and Max are asked to transform two digital clocks into two "digital root" clocks.
// A digital root clock is a digital clock that calculates digital roots step by step.

// When a clock is fed a number, it will show it and then it will start the calculation, showing all the intermediate values until it gets to the result.
// For example, if the clock is fed the number 137, it will show: "137" → "11" → "2" and then it will go black, waiting for the next number.

// Every digital number consists of some light segments: three horizontal (top, middle, bottom) and four vertical (top-left, top-right, bottom-left, bottom-right).
// Number "1" is made of vertical top-right and bottom-right, number "4" is made by middle horizontal and vertical top-left, top-right and bottom-right. Number "8" lights them all.

// The clocks consume energy only when segments are turned on/off.
// To turn on a "2" will cost 5 transitions, while a "7" will cost only 4 transitions.

// Sam and Max built two different clocks.

// Sam's clock is fed e.g. number 137: the clock shows "137", then the panel is turned off, then the next number ("11") is turned on, then the panel is turned off again and finally the last number ("2") is turned on and, after some time, off.
// For the example, with number 137, Sam's clock requires:

// "137"	:	(2 + 5 + 4) × 2 = 22 transitions ("137" on/off).
// "11"	:	(2 + 2) × 2 = 8 transitions ("11" on/off).
// "2"	:	(5) × 2 = 10 transitions ("2" on/off).
// For a grand total of 40 transitions.
// Max's clock works differently. Instead of turning off the whole panel, it is smart enough to turn off only those segments that won't be needed for the next number.
// For number 137, Max's clock requires:

// "137"

// :

// 2 + 5 + 4 = 11 transitions ("137" on)
// 7 transitions (to turn off the segments that are not needed for number "11").
// "11"

// :

// 0 transitions (number "11" is already turned on correctly)
// 3 transitions (to turn off the first "1" and the bottom part of the second "1";
// the top part is common with number "2").
// "2"

// :

// 4 transitions (to turn on the remaining segments in order to get a "2")
// 5 transitions (to turn off number "2").
// For a grand total of 30 transitions.
// Of course, Max's clock consumes less power than Sam's one.
// The two clocks are fed all the prime numbers between A = 107 and B = 2×107.
// Find the difference between the total number of transitions needed by Sam's clock and that needed by Max's one.

#include "template_class.h"
#include <map>

extern uint64_t sum_digits(uint64_t n);
extern uint8_t count_bit_1(uint8_t n);
extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);
static vector<uint64_t> prime_list;

uint64_t digitRoot(uint64_t n)
{
	return n - (n - 1) / 9;
}

typedef enum _digitClock {
	NUM_0 = 0b01111110,
	NUM_1 = 0b00110000,
	NUM_2 = 0b01101101,
	NUM_3 = 0b01111001,
	NUM_4 = 0b00110011,
	NUM_5 = 0b01011011,
	NUM_6 = 0b01011111,
	NUM_7 = 0b01110010,
	NUM_8 = 0b01111111,
	NUM_9 = 0b01111011,
	OFF = 0,
} digitClock;

uint8_t transMap[11][11];
uint8_t digitMap[11];

uint64_t numTransactions(vector<uint8_t> a, vector<uint8_t> b)
{
	uint64_t sum = 0;
	int size1 = a.size();
	int size2 = b.size();
	vector<uint8_t> tmpa = a;
	vector<uint8_t> tmpb = b;
	int minsize = 0;
	int maxsize = 0;
	if (size1 >= size2)
	{
		tmpa = b;
		tmpb = a;
	}
	minsize = tmpa.size();
	maxsize = tmpb.size();
	int i = 0;
	for (; i < minsize; i++)
	{
		sum += transMap[tmpa[i]][tmpb[i]];
	}
	for (; i < maxsize; i++)
	{
		sum += transMap[tmpb[i]][10];
	}
	return sum;
}

std::map<uint64_t, uint64_t> storeSam;
std::map<uint64_t, uint64_t> storeMax;

vector<uint8_t> numToDigits(uint64_t n)
{
	vector<uint8_t> digits;
	while (n)
	{
		uint8_t tmp = n % 10;
		digits.push_back(tmp);
		n /= 10;
	}
	return digits;
}

uint64_t numTransactionsPerFeedSam(uint64_t N)
{
	if (N < 10)
	{
		return 2 * transMap[N][10];
	}
	if (storeSam.find(N) != storeSam.end())
	{
		return storeSam[N];
	}
	uint64_t sum = 0;
	// from 0 to N * 2
	vector<uint8_t> digitsN = numToDigits(N);
	vector<uint8_t> digits0;
	sum = 2 * numTransactions(digitsN, digits0);
	sum += numTransactionsPerFeedSam(sum_digits(N));
	if (N < 9 * 8)
	{
		storeSam[N] = sum;
	}
	return sum;
}

uint64_t numTransactionsPerFeedMax(uint64_t N, bool start = true)
{
	if (N < 10)
	{
		// N --> OFF
		if (start)
			return 2 * transMap[N][10];
		else
			return transMap[N][10];
	}
	if (storeMax.find(N) != storeMax.end())
	{
		return storeMax[N];
	}
	uint64_t sum = 0;
	vector<uint8_t> digitsN = numToDigits(N);
	if (start)
	{
		// from 0 to N
		vector<uint8_t> digits0;
		sum += numTransactions(digitsN, digits0);
	}
	// from N to next
	uint64_t sumDigit = sum_digits(N);
	vector<uint8_t> digitsNext = numToDigits(sumDigit);
	sum += numTransactions(digitsN, digitsNext);
	sum += numTransactionsPerFeedMax(sumDigit, false);
	if (N < 9 * 8)
	{
		storeMax[N] = sum;
	}
	return sum;
}

uint64_t problem_315()
{
	digitMap[0] = NUM_0;
	digitMap[1] = NUM_1;
	digitMap[2] = NUM_2;
	digitMap[3] = NUM_3;
	digitMap[4] = NUM_4;
	digitMap[5] = NUM_5;
	digitMap[6] = NUM_6;
	digitMap[7] = NUM_7;
	digitMap[8] = NUM_8;
	digitMap[9] = NUM_9;
	digitMap[10] = OFF;
	for (int i = 0; i < 11; i++)
	{
		for (int j = i; j < 11; j++)
		{
			uint8_t cost = count_bit_1(digitMap[i] ^ digitMap[j]);
			transMap[i][j] = cost;
			transMap[j][i] = cost;
		}
	}
	uint64_t ret = 0;
	uint64_t sum1 = 0;
	uint64_t sum2 = 0;
	create_prime_filter_method(&prime_list, 1e7, 2e7);
	int i = 0;
	for (; i < prime_list.size(); i++)
	{
		sum1 += numTransactionsPerFeedSam(prime_list[i]);
		sum2 += numTransactionsPerFeedMax(prime_list[i]);
	}

	cout << sum1 << endl;
	cout << sum2 << endl;
	cout << sum1 - sum2 << endl;

	return ret;
}
