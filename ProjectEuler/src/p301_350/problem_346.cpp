// Strong Repunits
// Problem 346
// The number 7 is special, because 7 is 111 written in base 2, and 11 written in base 6
// (i.e. 710 = 116 = 1112). In other words, 7 is a repunit in at least two bases b > 1.

// We shall call a positive integer with this property a strong repunit. It can be verified that there are 8 strong repunits below 50: {1,7,13,15,21,31,40,43}.
// Furthermore, the sum of all strong repunits below 1000 equals 15864.

// Find the sum of all strong repunits below 1012.

#include "template_class.h"

//set<uint64_t> one_base;
set<uint64_t> strong_repunit;

uint64_t problem_346()
{
	uint64_t sum = 1;
	const uint64_t N = (uint64_t)1e12;
	uint64_t b = 2;
	//one_base.insert(1);
	//strong_repunit.insert(1);
	while (b < sqrt(N))
	{
		uint64_t digitVal = b*b;
		uint64_t testN = b*b + b + 1;
		while (testN < N)
		{
			/*if (one_base.find(testN) != one_base.end())
			{
				strong_repunit.insert(testN);
			}
			one_base.insert(testN);*/
			if (strong_repunit.find(testN) == strong_repunit.end())
			{
				sum += testN;
				strong_repunit.insert(testN);
			}

			digitVal *= b;
			testN += digitVal;
		}
		b++;
		if (b % 10000 == 0)
		{
			cout << b << endl;
			cout << "strong_repunit size: " << strong_repunit.size() << endl;
		}
	}
	/*for (auto it : strong_repunit)
	{
		sum += it;
	}*/
	return sum;
}
