// Bitwise-OR operations on random integers
// Problem 323
// Let y0, y1, y2,... be a sequence of random unsigned 32 bit integers
// (i.e. 0 ≤ yi < 232, every value equally likely).

// For the sequence xi the following recursion is given:

// x0 = 0 and
// xi = xi-1| yi-1, for i > 0. ( | is the bitwise-OR operator)
// It can be seen that eventually there will be an index N such that xi = 232 -1 (a bit-pattern of all ones) for all i ≥ N.

// Find the expected value of N.
// Give your answer rounded to 10 digits after the decimal point.

//P(All 1s on turn N) = (1 - 0.5^N) ^ 32
//
//So,
//P(Finishing on turn N) = P(All 1s on turn N) - P(All 1s on turn N - 1)

#include "template_class.h"
#include <iomanip>
#include <cstdlib>
#include <thread>

using namespace std;

#define NUM_RUN (1e8)
#define NUM_THREAD 8

uint64_t result323[NUM_THREAD];

double run323(int id)
{
	uint64_t ret = 0;
	//cout << "thread run" << endl;
	for (int i = 0; i < NUM_RUN; i++)
	{
		uint32_t x = 0;
		uint64_t N = 0;
		while (x != uint32_t(-1))
		{
			uint32_t r = (uint32_t)(rand()) << 30
				| (uint32_t)(rand()) << 15
				| (uint32_t)(rand());
			x = r | x;
			N++;
		}
		ret += N;
	}
	result323[id] = ret;
	return ret;
}

void buildPow2(BigInteger pow_2_big[], int size)
{
	pow_2_big[0] = 1;
	for (int i = 1; i < size; i++)
	{
		pow_2_big[i] = pow_2_big[i - 1] * 2;
	}
}

factor<BigInteger> pow_big(factor<BigInteger> f, int n = 32)
{
	if (n == 0)
	{
		return factor<BigInteger>(1, 1);
	}
	factor<BigInteger> ret = f;
	for (int i = 1; i < n; i++)
	{
		ret *= f;
	}
	return ret;
}

template <typename T>
std::string divide_with_precision(T n, T d, int precise)
{
	T mag = n / d;
	std::stringstream out;
	out << mag << ".";
	n -= mag*d;
	for (int i = 0; i < precise; i++)
	{
		n *= 10;
		T t = n / d;
		n -= t*d;
		out << t;
	}
	return out.str();
}

#define LIMIT 50
const int run = LIMIT;
BigInteger pow_2_big[LIMIT];
factor<BigInteger> prob[LIMIT + 1];

factor<BigInteger> prob_pick(int need, int remain, int N)
{
	if (N > LIMIT)
	{
		return factor<BigInteger>(0, 1);
	}
	if (remain < need)
	{
		return factor<BigInteger>(0, 1);
	}
	factor<BigInteger> p(0, 1);
	for (int pick = 0; pick <= need; pick++)
	{
		p += factor<BigInteger>(1, pow_2_big[pick]) *
			factor<BigInteger>(1, pow_2_big[remain - pick]) *
			prob_pick(need - pick, remain - pick, N + 1);
	}
	prob[N] += p;
	return p;
}

factor<BigInteger> prob_cnt[33];
extern uint64_t numCombination(int k, int n);

extern BigInteger factorial_bigint(int n);

BigInteger numCombination_bigint(int k, int n)
{
	if (k == 0)
	{
		return 1;
	}
	return(factorial_bigint(n) / factorial_bigint(k) / factorial_bigint(n - k));
	return 0;
}

vector<factor<BigInteger>> eachPick(int bits)
{
	vector<factor<BigInteger>> ret;
	ret.resize(bits + 1);
	for (int i = 0; i <= bits; i++)
	{
		BigInteger numCom(numCombination_bigint(i, bits));
		ret[i] = factor<BigInteger>(numCom, 1) * factor<BigInteger>(1, bits);
	}
	return ret;
}

double problem_323()
{
	//cout << divide_with_precision(2, 1, 20) << endl;
	buildPow2(pow_2_big, LIMIT);
	//prob_pick(32, 32, 1);
	// run N time
	vector<factor<BigInteger>> prob_old;
	vector<factor<BigInteger>> prob_new;
	prob_old.resize(33);
	prob_new.resize(33);
	for (int idx = 0; idx < 33; idx++)
	{
		prob_old[idx] = factor<BigInteger>(0, 1);
		prob_new[idx] = factor<BigInteger>(0, 1);
	}
	prob_old[0] = factor<BigInteger>(1, 1);
	factor<BigInteger> totalN;
	factor<BigInteger> totalp;
	factor<BigInteger> res;
	for (int i = 1; i < run; i++)
	{
		factor<BigInteger> testsum;
		// reset new array
		for (int idx = 0; idx < 33; idx++)
		{
			prob_new[idx] = factor<BigInteger>(0, 1);
		}
		for (int pre_bits = 0; pre_bits < 32; pre_bits++)
		{
			for (int new_bits = 0; new_bits <= 32 - pre_bits; new_bits++)
			{
				BigInteger numCom(numCombination_bigint(new_bits, 32 - pre_bits));
				prob_new[pre_bits + new_bits] += prob_old[pre_bits] *
					factor<BigInteger>(numCom, 1)*
					factor<BigInteger>(1, pow_2_big[32 - pre_bits]);
			}
		}
		totalN += factor<BigInteger>(i, 1)*prob_new[32];
		totalp += prob_new[32];
		prob[i] = prob_new[32];
		// update old array
		for (int idx = 0; idx < 33; idx++)
		{
			testsum += prob_new[idx];
			prob_old[idx] = prob_new[idx];
		}
		//factor<BigInteger> oneBits = factor<BigInteger>(1, 1) - factor<BigInteger>(1, pow_2_big[i]);

		cout << "N: " << i << " : " << endl;
		//cout << divide_with_precision(prob[i].num, prob[i].denum, 20) << endl;
		//cout << divide_with_precision(testsum.num, testsum.denum, 20) << endl;
	}
	totalp.swap();
	res = totalN * totalp;
	cout << divide_with_precision(res.num, res.denum, 10) << endl;

#if 0
	for (int i = 1; i < run; i++)
	{
		// have at least 1 at a place: 1 - non_1_in_N_times
		factor<BigInteger> one = factor<BigInteger>(1, 1) - factor<BigInteger>(1, pow_2_big[i]);
		// have at least 1 at 32 places: one^32
		factor<BigInteger> all = pow_big(one);
		cout << "N: " << i << " : ";
		//cout << all.num << " / " << all.denum << endl;
		//cout << all.num << endl;
		cout << divide_with_precision(all.num, all.denum, 20) << endl;
		//cout << setprecision(13) << all << endl;
		//ret += (1. - all)*i;
		prob += (factor<BigInteger>(1, 1) - all)*factor<BigInteger>(i, 1);
	}
	cout << divide_with_precision(prob.num, prob.denum, 20) << endl;
	//cout << setprecision(13) << ret << endl;
#endif
#if 0
	thread t[NUM_THREAD];
	srand(time(NULL));
	for (int i = 0; i < NUM_THREAD; i++)
	{
		t[i] = thread(run323, i);
	}
	for (int i = 0; i < NUM_THREAD; i++)
	{
		t[i].join();
	}
	for (int i = 0; i < NUM_THREAD; i++)
	{
		//cout << setprecision(13) << result323[i] << endl;
		cout << result323[i] << endl;
	}
#endif
	return 0;
}
