//Primonacci
//Problem 304
//For any positive integer n the function next_prime(n) returns the smallest prime p
//such that p>n.
//
//The sequence a(n) is defined by:
//a(1)=next_prime(1014) and a(n)=next_prime(a(n-1)) for n>1.
//
//The fibonacci sequence f(n) is defined by: f(0)=0, f(1)=1 and f(n)=f(n-1)+f(n-2) for n>1.
//
//The sequence b(n) is defined as f(a(n)).
//
//Find ?b(n) for 1?n?100 000. Give your answer mod 1234567891011.

#include "template_class.h"

#ifdef _MSC_VER
    #if _MSC_VER < 1900
        #include "ParallelPrimeSieve.h"
    #else
        #include "primesieve.hpp"
        using namespace primesieve;
    #endif
#else
    #include "primesieve.hpp"
    using namespace primesieve;
#endif

uint64_t problem_304()
{
    vector<uint64_t> p;
    //ParallelPrimeSieve genP;
    //genP.setNumThreads(4);
#ifdef _MSC_VER
#if _MSC_VER < 1900
    PrimeSieve genP;
    genP.generate_N_Primes((uint64_t)1e14, (uint64_t)1e5, &p);
#else
    generate_n_primes((uint64_t)1e14, (uint64_t)1e5, &p);
#endif
#else
    generate_n_primes((uint64_t)1e14, (uint64_t)1e5, &p);
#endif

    uint64_t period = 900788112;
    //FILE* fp = fopen("fibo.txt", "w");
    for(int i = 0; i < 1e5; i++)
    {
        p[i] %= period;
        //fprintf(fp, "%ld\n", p[i]);
    }
    uint64_t f_2 = 0;
    uint64_t f_1 = 1;
    uint64_t mod = 1234567891011;
    int count = 2;
    int idx = 0;
    uint64_t sum = 0;
    while(1)
    {
        uint64_t tmp = (f_2 + f_1) % mod;
        f_2 = f_1;
        f_1 = tmp;
        if(count == p[idx])
        {
            sum += tmp;
            sum %= mod;
            idx++;
            if(idx == 100000)
            {
                break;
            }
        }
        count++;
    }
    //fclose(fp);
    /*uint64_t f_2 = 0;
    uint64_t f_1 = 1;
    uint64_t mod = 1234567891011;
    while(1)
    {
        uint64_t tmp = (f_2 + f_1) % mod;
        f_2 = f_1;
        f_1 = tmp;
        period++;
        if(f_2 == 0 && f_1 == 1)
        {
            break;
        }
    }*/

    return sum;
}
