//Matrix Sum
//Problem 345
//We define the Matrix Sum of a matrix as the maximum sum of matrix elements with each element being the only one in his row and column.For example, the Matrix Sum of the matrix below equals 3315 (= 863 + 383 + 343 + 959 + 767) :
//
//7  53 183 439 863
//497 383 563  79 973
//287  63 343 169 583
//627 343 773 959 943
//767 473 103 699 303
//Find the Matrix Sum of :
//
//7  53 183 439 863 497 383 563  79 973 287  63 343 169 583
//627 343 773 959 943 767 473 103 699 303 957 703 583 639 913
//447 283 463  29  23 487 463 993 119 883 327 493 423 159 743
//217 623   3 399 853 407 103 983  89 463 290 516 212 462 350
//960 376 682 962 300 780 486 502 912 800 250 346 172 812 350
//870 456 192 162 593 473 915  45 989 873 823 965 425 329 803
//973 965 905 919 133 673 665 235 509 613 673 815 165 992 326
//322 148 972 962 286 255 941 541 265 323 925 281 601  95 973
//445 721  11 525 473  65 511 164 138 672  18 428 154 448 848
//414 456 310 312 798 104 566 520 302 248 694 976 430 392 198
//184 829 373 181 631 101 969 613 840 740 778 458 284 760 390
//821 461 843 513  17 901 711 993 293 157 274  94 192 156 574
//34 124   4 878 450 476 712 914 838 669 875 299 823 329 699
//815 559 813 459 522 788 168 586 966 232 308 833 251 631 107
//813 883 451 509 615  77 281 613 459 205 380 274 302  35 805

// best algorithm https://en.wikipedia.org/wiki/Hungarian_algorithm

#include "template_class.h"
#if 1
#define DIMS 15
int input_345[DIMS][DIMS] =
{
	{ 7, 53, 183, 439, 863, 497, 383, 563, 79, 973, 287, 63, 343, 169, 583 },
	{ 627, 343, 773, 959, 943, 767, 473, 103, 699, 303, 957, 703, 583, 639, 913 },
	{ 447, 283, 463, 29, 23, 487, 463, 993, 119, 883, 327, 493, 423, 159, 743 },
	{ 217, 623, 3, 399, 853, 407, 103, 983, 89, 463, 290, 516, 212, 462, 350 },
	{ 960, 376, 682, 962, 300, 780, 486, 502, 912, 800, 250, 346, 172, 812, 350 },
	{ 870, 456, 192, 162, 593, 473, 915, 45, 989, 873, 823, 965, 425, 329, 803 },
	{ 973, 965, 905, 919, 133, 673, 665, 235, 509, 613, 673, 815, 165, 992, 326 },
	{ 322, 148, 972, 962, 286, 255, 941, 541, 265, 323, 925, 281, 601, 95, 973 },
	{ 445, 721, 11, 525, 473, 65, 511, 164, 138, 672, 18, 428, 154, 448, 848 },
	{ 414, 456, 310, 312, 798, 104, 566, 520, 302, 248, 694, 976, 430, 392, 198 },
	{ 184, 829, 373, 181, 631, 101, 969, 613, 840, 740, 778, 458, 284, 760, 390 },
	{ 821, 461, 843, 513, 17, 901, 711, 993, 293, 157, 274, 94, 192, 156, 574 },
	{ 34, 124, 4, 878, 450, 476, 712, 914, 838, 669, 875, 299, 823, 329, 699 },
	{ 815, 559, 813, 459, 522, 788, 168, 586, 966, 232, 308, 833, 251, 631, 107 },
	{ 813, 883, 451, 509, 615, 77, 281, 613, 459, 205, 380, 274, 302, 35, 805 }
};
#else
#define DIMS 5
int input_345[DIMS][DIMS] =
{
	{ 7, 53, 183, 439, 863 },
	{ 497, 383, 563, 79, 973 },
	{ 287, 63, 343, 169, 583 },
	{ 627, 343, 773, 959, 943 },
	{ 767, 473, 103, 699, 303 }
};
#endif

#include <map>

using std::map;

struct data_345
{
	int sum;
	set<int> pos;
	data_345()
	{
		sum = 0;
	}
};

typedef struct _params345
{
	vector<int> set;
	int size;
	_params345(vector<int> in_set)
	{
		size = in_set.size();
		set.resize(size);
		for (int i = 0; i < size; i++)
		{
			set[i] = in_set[i];
		}
	}
} params345;

struct ClassParamCompare345
{
	bool operator() (const params345& lhs, const params345& rhs) const
	{
		if (lhs.size < rhs.size)
		{
			return true;
		}
		else if (lhs.size > rhs.size)
		{
			return false;
		}
		for (int i = 0; i < lhs.size; i++)
		{
			if (lhs.set[i] < rhs.set[i])
			{
				return true;
			}
			else if (lhs.set[i] > rhs.set[i])
			{
				return false;
			}
		}
		return false;
	}
};

vector<int> removePos(vector<int> set, int row, int col)
{
	vector<int> ret;
	int dims = (int)(sqrt(set.size() - 0.5));
	int size = dims * dims;
	ret.resize(size);
	int idx = 0;
	int n = set.size();
	int olddims = dims + 1;
	for (int i = 0; i < n; i++)
	{
		int r = i / olddims;
		int c = i % olddims;
		if (r != row && c != col)
		{
			ret[idx++] = set[i];
		}
	}
	return ret;
}

map < params345, int, ClassParamCompare345> store345;

int maxSum(vector<int> set)
{
	int sum = 0;
	if (set.size() == 0)
	{
		return 0;
	}

	params345 para(set);
	std::map<params345, int, ClassParamCompare345>::iterator it = store345.find(para);
	if (it != store345.end())
	{
		return it->second;
	}

	int size = (int)(sqrt(set.size() + 0.5));
	for (int i = 0; i < size; i++)
	{
		vector<int> newset = removePos(set, 0, i);
		sum = max(sum, set[i] + maxSum(newset));
	}
	store345[para] = sum;
	return sum;
}

int problem_345()
{
#if 0
	data_345 memo[DIMS][DIMS];
	// init
	for (int i = 0; i < DIMS; i++)
	{
		memo[0][i].pos.insert(i);
		memo[0][i].sum = input_345[0][i];
	}
	for (int i = 1; i < DIMS; i++)
	{
		for (int j = 0; j < DIMS; j++)
		{
			data_345 tmp[DIMS - 1];
			int idx = 0;
			for (int pos = 0; pos < DIMS; pos++)
			{
				if (memo[i - 1][j].pos.find(pos) == memo[i - 1][j].pos.end())
				{
					tmp[idx] = memo[i - 1][j];
					tmp[idx].sum += input_345[i][j];
					tmp[idx].pos.insert(pos);
					idx++;
				}
			}
			idx = 0;
			int max = tmp[0].sum;
			for (int k = 1; k < DIMS - 1; k++)
			{
				if (max < tmp[k].sum)
				{
					max = tmp[k].sum;
					idx = k;
				}
			}
			memo[i][j] = tmp[idx];
		}
	}
#endif
	vector<int> set;
	set.resize(DIMS*DIMS);
	for (int i = 0; i < set.size(); i++)
	{
		set[i] = input_345[i / (DIMS)][i % (DIMS)];
	}
	int sum = maxSum(set);
	cout << sum << endl;
	cout << store345.size() << endl;
	return 0;
}
