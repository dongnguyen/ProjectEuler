// Largest integer divisible by two primes
// Problem 347
// The largest integer ≤ 100 that is only divisible by both the primes 2 and 3 is 96, as 96=32*3=25*3. For two distinct primes p and q let M(p,q,N) be the largest positive integer ≤N only divisible by both p and q and M(p,q,N)=0 if such a positive integer does not exist.

// E.g. M(2,3,100)=96.
// M(3,5,100)=75 and not 90 because 90 is divisible by 2 ,3 and 5.
// Also M(2,73,100)=0 because there does not exist a positive integer ≤ 100 that is divisible by both 2 and 73.

// Let S(N) be the sum of all distinct M(p,q,N). S(100)=2262.

// Find S(10 000 000).

#include "template_class.h"

extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);

uint64_t problem_347()
{
	uint64_t sum = 0;
	const uint64_t N = (uint64_t)1e7;
	vector<uint64_t> prime_list;
	create_prime_filter_method(&prime_list, 0, N / 2);
	for (int i = 0; i < prime_list.size() - 1; i++)
	{
		uint64_t p = prime_list[i];
		if (p*p > N)
		{
			break;
		}
		for (int j = i + 1; j < prime_list.size(); j++)
		{
			uint64_t q = prime_list[j];
			if (p * q > N)
			{
				break;
			}
			uint64_t max = 0;
			uint64_t p1 = p;
			for (int e1 = 1;; e1++)
			{
				uint64_t p2 = q;
				if (p1 * p2 > N)
				{
					break;
				}
				for (int e2 = 1;; e2++)
				{
					if (p1 * p2 > N)
					{
						break;
					}
					if (max < p1*p2)
					{
						max = p1*p2;
					}
					p2 *= q;
				}
				p1 *= p;
			}
			sum += max;
		}
	}
	return sum;
}
