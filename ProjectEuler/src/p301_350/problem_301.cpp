
#include "template_class.h"

uint32_t nim_sum(uint32_t n)
{
    return n ^ (n << 1) ^ ((n << 1) + n);
    //return n ^ (n * 2) ^ (n * 3);
}

int problem_301()
{
    int count = 0;
    for(uint32_t i = 1; i <= 1073741824; i++)
    {
        if(nim_sum(i) == 0)
        {
            count++;
        }
    }
    return count;
}
