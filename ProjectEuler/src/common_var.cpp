#include "template_class.h"

using std::set;

vector<int> triangle_list;
vector<uint64_t> fact_list;
vector<BigInteger> fact_bigint_list;
vector<int> prime_list;
set<int> set_prime_list;
vector<int> abundant_list;
vector<int> sum_two_abundant_list;
