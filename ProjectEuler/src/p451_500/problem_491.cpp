// Double pandigital number divisible by 11
// Problem 491
// We call a positive integer double pandigital if it uses all the digits 0 to 9 exactly twice (with no leading zero). For example, 40561817703823564929 is one such number.

// How many double pandigital numbers are divisible by 11?

#include "template_class.h"
#include <string>
#include <map>

using std::map;

extern uint64_t factorial(int n);
extern uint64_t numCombination(int k, int n);

typedef class _params_491
{
public:
	int currentSum;
	int numPosEven;
	int numPosOdd;
	set<int> setCoupleNum;
	int singleNum;
	_params_491(
		int currentSum,
		int numPosEven,
		int numPosOdd,
		set<int> setCoupleNum,
		int singleNum)
	{
		this->currentSum = currentSum;
		this->numPosEven = numPosEven;
		this->numPosOdd = numPosOdd;
		this->singleNum = singleNum;
		this->setCoupleNum = setCoupleNum;
	}
#if 0
	_params_491(const _params_491& other)
	{
		this->currentSum = other.currentSum;
		this->numPosEven = other.numPosEven;
		this->numPosOdd = other.numPosOdd;
		this->singleNum = other.singleNum;
		for (auto i : other.setCoupleNum)
		{
			this->setCoupleNum.insert(i);
		}
	}
	~_params_491()
	{
	}
#endif
} params_491;

class ClassParamCompare
{
public:
	bool operator() (const params_491& lhs, const params_491& rhs) const
	{
		if (lhs.currentSum < rhs.currentSum)
			return true;
		else if (lhs.currentSum > rhs.currentSum)
			return false;

		if (lhs.numPosEven < rhs.numPosEven)
			return true;
		else if (lhs.numPosEven > rhs.numPosEven)
			return false;

		if (lhs.numPosOdd < rhs.numPosOdd)
			return true;
		else if (lhs.numPosOdd > rhs.numPosOdd)
			return false;

		if (lhs.singleNum < rhs.singleNum)
			return true;
		else if (lhs.singleNum > rhs.singleNum)
			return false;

		if (lhs.setCoupleNum.size() < rhs.setCoupleNum.size())
			return true;
		else if (lhs.setCoupleNum.size() > rhs.setCoupleNum.size())
			return false;

		auto left = lhs.setCoupleNum.begin();
		auto right = rhs.setCoupleNum.begin();

		for (int i = 0; i < lhs.setCoupleNum.size(); i++, left++, right++)
		{
			if (*left < *right)
				return true;
			else if (*left > *right)
				return false;
		}

		return false;
	}
};

static map < params_491, uint64_t, ClassParamCompare> store_result;
const int NO_SINGLE = 10;

uint64_t count_491(params_491 in, bool considerZero = true)
{
	// break condition
	if (in.numPosEven + in.numPosOdd == 0)
	{
		return 0;
	}
	// even = +
	// odd = -
	if (in.numPosEven == 0 && in.numPosOdd == 1)
	{
		// check sum
		int sum = in.currentSum - in.singleNum;
		if (sum % 11 == 0)
		{
			return 1;
		}
		return 0;
	}
	if (in.numPosEven == 1 && in.numPosOdd == 0)
	{
		// check sum
		int sum = in.currentSum + in.singleNum;
		if (sum % 11 == 0)
		{
			return 1;
		}
		return 0;
	}
	if (in.numPosEven == 0 && in.numPosOdd == 2)
	{
		// check sum
		int sum = in.currentSum - (*in.setCoupleNum.begin() << 1);
		if (sum % 11 == 0)
		{
			return 1;
		}
		return 0;
	}
	if (in.numPosEven == 2 && in.numPosOdd == 0)
	{
		// check sum
		int sum = in.currentSum + (*in.setCoupleNum.begin() << 1);
		if (sum % 11 == 0)
		{
			return 1;
		}
		return 0;
	}
	if (in.numPosEven == 1 && in.numPosOdd == 1)
	{
		// check sum
		int sum = in.currentSum;
		if (sum % 11 == 0)
		{
			return 1;
		}
		return 0;
	}

	static map < params_491, uint64_t, ClassParamCompare> ::iterator it = store_result.find(in);
	if (it != store_result.end())
	{
		// debug
		cout << "short-cut" << endl;
		return it->second;
	}

	// main logic
	if (considerZero)
	{
		uint64_t cnt = 0;
		for (int pick = 1; pick <= 9; pick++)
		{
			params_491 newIn = in;
			newIn.numPosEven--;
			newIn.setCoupleNum.erase(pick);
			newIn.singleNum = pick;
			newIn.currentSum = pick;
			cnt += count_491(newIn, false);
		}
		return cnt;
	}
	else
	{
		if (in.singleNum == NO_SINGLE)
		{
			// start to store result here
			uint64_t cnt = 0;
			for (auto pick : in.setCoupleNum)
			{
				// two even pos
				if (in.numPosEven >= 2)
				{
					params_491 newIn = in;
					newIn.numPosEven -= 2;
					newIn.setCoupleNum.erase(pick);
					newIn.currentSum += pick << 1;
					if (newIn.currentSum > 11)
					{
						newIn.currentSum %= 11;
					}
					else if (newIn.currentSum < -11)
					{
						int sum = -newIn.currentSum;
						sum %= 11;
						newIn.currentSum = -sum;
					}
					cnt += numCombination(2, in.numPosEven) * count_491(newIn, false);
				}
				// two odd pos
				if (in.numPosOdd >= 2)
				{
					params_491 newIn = in;
					newIn.numPosOdd -= 2;
					newIn.setCoupleNum.erase(pick);
					newIn.currentSum -= pick << 1;
					if (newIn.currentSum > 11)
					{
						newIn.currentSum %= 11;
					}
					else if (newIn.currentSum < -11)
					{
						int sum = -newIn.currentSum;
						sum %= 11;
						newIn.currentSum = -sum;
					}
					cnt += numCombination(2, in.numPosOdd) * count_491(newIn, false);
				}
				// one even pos and one odd pos
				if (in.numPosEven >= 1 && in.numPosOdd >= 1)
				{
					params_491 newIn = in;
					newIn.numPosEven--;
					newIn.numPosOdd--;
					newIn.setCoupleNum.erase(pick);
					cnt += in.numPosEven * in.numPosOdd * count_491(newIn, false);
				}
			}
			store_result[in] = cnt;
			return cnt;
		}
		else
		{
			// do not store result
			uint64_t cnt = 0;
			// pick even pos
			if (in.numPosEven >= 1)
			{
				int pick = in.singleNum;
				params_491 newIn = in;
				newIn.numPosEven--;
				newIn.singleNum = NO_SINGLE;
				newIn.currentSum += pick;
				if (newIn.currentSum > 11)
				{
					newIn.currentSum %= 11;
				}
				else if (newIn.currentSum < -11)
				{
					int sum = -newIn.currentSum;
					sum %= 11;
					newIn.currentSum = -sum;
				}
				cnt += in.numPosEven * count_491(newIn, false);
			}
			// pick odd pos
			if (in.numPosOdd >= 1)
			{
				int pick = in.singleNum;
				params_491 newIn = in;
				newIn.numPosOdd--;
				newIn.singleNum = NO_SINGLE;
				newIn.currentSum -= pick;
				if (newIn.currentSum > 11)
				{
					newIn.currentSum %= 11;
				}
				else if (newIn.currentSum < -11)
				{
					int sum = -newIn.currentSum;
					sum %= 11;
					newIn.currentSum = -sum;
				}
				cnt += in.numPosOdd * count_491(newIn, false);
			}
			return cnt;
		}
	}
}

uint64_t problem_491()
{
	uint64_t ret = 0;
	set<int> setCoupleNum;
	setCoupleNum.insert(0);
	setCoupleNum.insert(1);
	setCoupleNum.insert(2);
	setCoupleNum.insert(3);
	setCoupleNum.insert(4);
	setCoupleNum.insert(5);
	setCoupleNum.insert(6);
	setCoupleNum.insert(7);
	setCoupleNum.insert(8);
	setCoupleNum.insert(9);
	params_491 init(
		0, // currentSum
		10, // numPosEven
		10, // numPosOdd
		setCoupleNum, // setCoupleNum
		NO_SINGLE // singleNum
	);
	ret = count_491(init);
	return ret;
}
