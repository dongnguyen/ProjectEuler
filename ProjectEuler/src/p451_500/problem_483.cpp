// Repeated permutation
// Problem 483
// We define a permutation as an operation that rearranges the order of the elements {1, 2, 3, ..., n}. There are n! such permutations, one of which leaves the elements in their initial order. For n = 3 we have 3! = 6 permutations:
// - P1 = keep the initial order
// - P2 = exchange the 1st and 2nd elements
// - P3 = exchange the 1st and 3rd elements
// - P4 = exchange the 2nd and 3rd elements
// - P5 = rotate the elements to the right
// - P6 = rotate the elements to the left

// If we select one of these permutations, and we re-apply the same permutation repeatedly, we eventually restore the initial order.
// For a permutation Pi, let f(Pi) be the number of steps required to restore the initial order by applying the permutation Pi repeatedly.
// For n = 3, we obtain:
// - f(P1) = 1 : (1,2,3) ? (1,2,3)
// - f(P2) = 2 : (1,2,3) ? (2,1,3) ? (1,2,3)
// - f(P3) = 2 : (1,2,3) ? (3,2,1) ? (1,2,3)
// - f(P4) = 2 : (1,2,3) ? (1,3,2) ? (1,2,3)
// - f(P5) = 3 : (1,2,3) ? (3,1,2) ? (2,3,1) ? (1,2,3)
// - f(P6) = 3 : (1,2,3) ? (2,3,1) ? (3,1,2) ? (1,2,3)

// Let g(n) be the average value of f2(Pi) over all permutations Pi of length n.
// g(3) = (12 + 22 + 22 + 22 + 32 + 32)/3! = 31/6 � 5.166666667e0
// g(5) = 2081/120 � 1.734166667e1
// g(20) = 12422728886023769167301/2432902008176640000 � 5.106136147e3

// Find g(350) and write the answer in scientific notation rounded to 10 significant digits, using a lowercase e to separate mantissa and exponent, as in the examples above.

#include "template_class.h"
#include <string>

#define UNDEFINED -1
#define MAX_N 351

BigInteger buffer_bigint_483[MAX_N];

extern BigInteger factorial_bigint(int n);
extern vector<BigInteger> fact_bigint_list;

BigInteger num_of_permutation_all_state_change(int n)
{
	if (buffer_bigint_483[n] != UNDEFINED)
	{
		return buffer_bigint_483[n];
	}
	if (n < 2)
	{
		buffer_bigint_483[n] = 0;
		return 0;
	}
	BigInteger N = n;
	BigInteger numNotAllStateChange = 0;
	for (int i = 1; i < n; i++)
	{
		BigInteger combination = fact_bigint_list[n] / fact_bigint_list[i] / fact_bigint_list[n - i];
		numNotAllStateChange += combination*num_of_permutation_all_state_change(n - i);
	}
	buffer_bigint_483[n] = fact_bigint_list[n] - numNotAllStateChange - 1;
	return buffer_bigint_483[n];
}

std::string problem_483()
{
	std::string ret("problem_483");
	for (int i = 0; i < MAX_N; i++)
	{
		buffer_bigint_483[i] = UNDEFINED;
	}
	factorial_bigint(MAX_N);
	int n = 20;
	BigInteger sum = 0;
	sum = 1;
	int k = 2;
	for (; k <= n; k++)
	{
		//combination(k, n) * num_of_permutation_all_state_change(k) * k
		BigInteger combination = fact_bigint_list[n] / fact_bigint_list[k] / fact_bigint_list[n - k];
		sum += combination*num_of_permutation_all_state_change(k)*k*k;
	}
	//std::string bigIntegerToString(const BigInteger &x);
	cout << "sum: " << bigIntegerToString(sum) << endl;
	return ret;
}
