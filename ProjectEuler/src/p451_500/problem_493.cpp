//Under The Rainbow
//Problem 493
//70 colored balls are placed in an urn, 10 for each of the seven rainbow colors.
//
//What is the expected number of distinct colors in 20 randomly picked balls ?
//
//Give your answer with nine digits after the decimal point(a.bcdefghij).

#include "template_class.h"
#include <string>
#include <map>

using std::map;

static factor<BigInteger> num_colors[8];

extern uint64_t factorial(int n);
extern uint64_t numCombination(int k, int n);

#if 1
typedef struct _params
{
	vector<int> set;
	int size;
	int pickNum;
	int remain;
	int target;
	_params(vector<int> in_set, int _pickNum, int _remain, int _target = 0)
	{
		size = in_set.size();
		//set.resize(size + 3);
		set.resize(size);
		for (int i = 0; i < size; i++)
		{
			set[i] = in_set[i];
		}
		//size += 3;
		pickNum = _pickNum;
		remain = _remain;
		target = _target;
		sort();
	}
	void sort()
	{
		for (int i = 0; i < size - 1; i++)
		{
			for (int j = i + 1; j < size; j++)
			{
				if (set[i] > set[j])
				{
					int tmp = set[i];
					set[i] = set[j];
					set[j] = tmp;
				}
			}
		}
	}
#if 0
	bool checkEqual(const _params& ref) const
	{
		for (int i = 0; i < size; i++)
		{
			if (set[i] != ref.set[i])
			{
				return false;
			}
		}
		if (pickNum != ref.pickNum) return false;
		if (remain != ref.remain) return false;
		if (target != ref.target) return false;
		return true;
	}
	bool operator< (const _params& ref) const
	{
		if (size < ref.size)
		{
			return true;
		}
		if (size > ref.size)
		{
			return false;
		}
		if (checkEqual(ref))
		{
			return false;
		}
		for (int i = 0; i < size; i++)
		{
			if (set[i] < ref.set[i])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	bool operator> (const _params& ref) const
	{
		if (size > ref.size)
		{
			return true;
		}
		if (size < ref.size)
		{
			return false;
		}
		if (checkEqual(ref))
		{
			return false;
		}
		for (int i = 0; i < size; i++)
		{
			if (set[i] > ref.set[i])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	bool operator== (const _params& ref) const
	{
		if (size != ref.size)
		{
			return false;
		}
		return checkEqual(ref);
	}
#endif
} params;

struct ClassParamCompare
{
	bool operator() (const params& lhs, const params& rhs) const
	{
		if (lhs.remain < rhs.remain)
			return true;
		else if (lhs.remain > rhs.remain)
			return false;
		if (lhs.pickNum < rhs.pickNum)
			return true;
		else if (lhs.pickNum > rhs.pickNum)
			return false;
		if (lhs.target < rhs.target)
			return true;
		else if (lhs.target > rhs.target)
			return false;
		for (int i = 0; i < lhs.size; i++)
		{
			if (lhs.set[i] < rhs.set[i])
			{
				return true;
			}
			else if (lhs.set[i] > rhs.set[i])
			{
				return false;
			}
		}
		return false;
	}
};

static map < params, factor<BigInteger>, ClassParamCompare> store_pick_same;
static map < params, factor<BigInteger>, ClassParamCompare> store_pick_not_same;
static map < params, factor<BigInteger>, ClassParamCompare> store_pick_prob;

factor<BigInteger> prob_pick_same(vector<int> in_set, vector<int>& out_set, int pickNum, int remain)
{
	if (pickNum > 20)
	{
		return factor<BigInteger>(0, 1);
	}
	params para(in_set, pickNum, remain);
	if (store_pick_same.find(para) != store_pick_same.end())
	{
		return store_pick_same[para];
	}
	int sum_same = 0;
	for (int i = 0; i < 7; i++)
	{
		if (in_set[i] < 10)
		{
			sum_same += in_set[i];
		}
	}
	return factor<BigInteger>(sum_same, remain);
}

factor<BigInteger> prob_pick_not_same(vector<int> in_set, vector<int>& out_set, int pickNum, int remain)
{
	if (pickNum > 20)
	{
		return factor<BigInteger>(0, 1);
	}
	params para(in_set, pickNum, remain);
	if (store_pick_not_same.find(para) != store_pick_same.end())
	{
		return store_pick_not_same[para];
	}
	int sum_not_same = 0;
	for (int i = 0; i < 7; i++)
	{
		if (in_set[i] == 10)
		{
			sum_not_same += in_set[i];
		}
	}
	return factor<BigInteger>(sum_not_same, remain);
}

factor<BigInteger> prob_pick_target(vector<int> in_set, int pickNum, int remain, int target)
{
	// end condition
	if (pickNum == 20)
	{
		// check meet target
		if (0 == target)
		{
			// meet
			return factor<BigInteger>(1, 1);
		}
		else
		{
			// not meet
			return factor<BigInteger>(0, 1);
		}
	}
	params para(in_set, pickNum, remain, target);
	std::map<params, factor<BigInteger>, ClassParamCompare>::iterator it = store_pick_prob.find(para);
	//if (it != store_pick_prob.end())
	if (it != store_pick_prob.end())
	{
		return it->second;
	}
	//still not meet target
	if (target > 0)
	{
		factor<BigInteger> prob(0, 1);
		// pick same color
		for (int i = 0; i < 7; i++)
		{
			if (in_set[i] < 10 && in_set[i] > 0)
			{
				factor<BigInteger> tmpp(in_set[i], remain);
				in_set[i]--;
				prob += tmpp * prob_pick_target(in_set, pickNum + 1, remain - 1, target);
				in_set[i]++;
			}
		}
		// pick not same color
		for (int i = 0; i < 7; i++)
		{
			if (in_set[i] == 10)
			{
				factor<BigInteger> tmpp(in_set[i], remain);
				in_set[i]--;
				prob += tmpp * prob_pick_target(in_set, pickNum + 1, remain - 1, target - 1);
				in_set[i]++;
			}
		}
		//store_pick_prob.insert(std::make_pair(para, prob));
		store_pick_prob[para] = prob;
		return prob;
	}
	// meet target already
	else // target = 0
	{
		factor<BigInteger> prob(0, 1);
		// pick same color
		for (int i = 0; i < 7; i++)
		{
			if (in_set[i] < 10 && in_set[i] > 0)
			{
				factor<BigInteger> tmpp(in_set[i], remain);
				in_set[i]--;
				prob += tmpp * prob_pick_target(in_set, pickNum + 1, remain - 1, target);
				in_set[i]++;
			}
		}
		//store_pick_prob.insert(std::make_pair(para, prob));
		store_pick_prob[para] = prob;
		return prob;
	}
}

void picking()
{
	// 2 color
	vector<int> color_set;
	color_set.resize(7);
	color_set[0] = 10;
	color_set[1] = 10;
	color_set[2] = 10;
	color_set[3] = 10;
	color_set[4] = 10;
	color_set[5] = 10;
	color_set[6] = 10;
	int remain = 69;
	for (int pick = 2; pick <= 20; pick++)
	{
		for (int color = 0; color < 7; color++)
		{
			//num_colors[2] += prob_pick_same(color_set, color_set, pick, remain)*prob_pick_not_same;
		}
		remain--;
	}
}

#endif

std::string problem_493()
{
	uint64_t numcom = numCombination(2, 7);
	factor<BigInteger> prob = factor<BigInteger>(20, 70) *
		factor<BigInteger>(19, 69)*
		factor<BigInteger>(18, 68)*
		factor<BigInteger>(17, 67)*
		factor<BigInteger>(16, 66)*
		factor<BigInteger>(15, 65)*
		factor<BigInteger>(14, 64)*
		factor<BigInteger>(13, 63)*
		factor<BigInteger>(12, 62)*
		factor<BigInteger>(11, 61)*
		factor<BigInteger>(10, 60) *
		factor<BigInteger>(9, 59)*
		factor<BigInteger>(8, 58)*
		factor<BigInteger>(7, 57)*
		factor<BigInteger>(6, 56)*
		factor<BigInteger>(5, 55)*
		factor<BigInteger>(4, 54)*
		factor<BigInteger>(3, 53)*
		factor<BigInteger>(2, 52)*
		factor<BigInteger>(1, 51);
	num_colors[2] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	cout << numcom << endl;
	cout << bigIntegerToString(num_colors[2].num) << endl;
	cout << bigIntegerToString(num_colors[2].denum) << endl;

	for (int i = 1; i <= 7; i++)
	{
		vector<int> color_set;
		color_set.resize(7);
		color_set[0] = 10;
		color_set[1] = 10;
		color_set[2] = 10;
		color_set[3] = 10;
		color_set[4] = 10;
		color_set[5] = 10;
		color_set[6] = 10;
		//store_pick_prob.clear();
		int pickNum = 0;
		int remain = 70;
		int target = i;
		factor<BigInteger> p = prob_pick_target(color_set, pickNum, remain, target);
		cout << "numColor: " << i << endl;
		cout << bigIntegerToString(p.num) << endl;
		cout << bigIntegerToString(p.denum) << endl;
		cout << "store_pick_prob.size(): " << store_pick_prob.size() << endl;
		cout << endl;
	}
	return "";
#if 0
	// problem 106
	int n = 6;
	vector<int> S;
	vector<int> D;
	vector<int> T;
	S.resize(20);
	T.resize(20);
	D.resize(20);
	for (int i = 0; i < 20; i++)
	{
		S[i] = i + 1;
		D[i] = 2 * (i + 1);
		T[i] = 3 * (i + 1);
	}
	// n = 6
	int sum = 0
	{
		if (25 < n)
		{
		}
	// checkout
	}
	return "";
	std::string ret("problem_493");
	num_colors[0] = factor<BigInteger>(0, 1);
	num_colors[1] = factor<BigInteger>(0, 1);
	num_colors[2] = factor<BigInteger>(0, 1);
	num_colors[3] = factor<BigInteger>(0, 1);
	num_colors[4] = factor<BigInteger>(0, 1);
	num_colors[5] = factor<BigInteger>(0, 1);
	num_colors[6] = factor<BigInteger>(0, 1);
	num_colors[8] = factor<BigInteger>(0, 1);
	//2 color
	uint64_t numcom = numCombination(2, 7);
	factor<BigInteger> prob = factor<BigInteger>(10, 70) *
		factor<BigInteger>(10, 69)*
		factor<BigInteger>(18, 68)*
		factor<BigInteger>(17, 67)*
		factor<BigInteger>(16, 66)*
		factor<BigInteger>(15, 65)*
		factor<BigInteger>(14, 64)*
		factor<BigInteger>(13, 63)*
		factor<BigInteger>(12, 62)*
		factor<BigInteger>(11, 61)*
		factor<BigInteger>(10, 60) *
		factor<BigInteger>(9, 59)*
		factor<BigInteger>(8, 58)*
		factor<BigInteger>(7, 57)*
		factor<BigInteger>(6, 56)*
		factor<BigInteger>(5, 55)*
		factor<BigInteger>(4, 54)*
		factor<BigInteger>(3, 53)*
		factor<BigInteger>(2, 52)*
		factor<BigInteger>(1, 51);
	num_colors[2] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	/*cout << numcom << endl;
	cout << bigIntegerToString(num_colors[2].num) << endl;
	cout << bigIntegerToString(num_colors[2].denum) << endl;*/
	//3 color
	numcom = numCombination(3, 7);
	prob = factor<BigInteger>(10, 70) *
		factor<BigInteger>(10, 69)*
		factor<BigInteger>(10, 68)*
		factor<BigInteger>(27, 67)*
		factor<BigInteger>(26, 66)*
		factor<BigInteger>(25, 65)*
		factor<BigInteger>(24, 64)*
		factor<BigInteger>(23, 63)*
		factor<BigInteger>(22, 62)*
		factor<BigInteger>(21, 61)*
		factor<BigInteger>(20, 60) *
		factor<BigInteger>(19, 59)*
		factor<BigInteger>(18, 58)*
		factor<BigInteger>(17, 57)*
		factor<BigInteger>(16, 56)*
		factor<BigInteger>(15, 55)*
		factor<BigInteger>(14, 54)*
		factor<BigInteger>(13, 53)*
		factor<BigInteger>(12, 52)*
		factor<BigInteger>(11, 51);
	num_colors[3] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	/*cout << numcom << endl;
	cout << bigIntegerToString(num_colors[3].num) << endl;
	cout << bigIntegerToString(num_colors[3].denum) << endl;*/
	//4 color
	numcom = numCombination(4, 7);
	prob = factor<BigInteger>(10, 70) *
		factor<BigInteger>(10, 69)*
		factor<BigInteger>(10, 68)*
		factor<BigInteger>(10, 67)*
		factor<BigInteger>(36, 66)*
		factor<BigInteger>(35, 65)*
		factor<BigInteger>(34, 64)*
		factor<BigInteger>(33, 63)*
		factor<BigInteger>(32, 62)*
		factor<BigInteger>(31, 61)*
		factor<BigInteger>(30, 60) *
		factor<BigInteger>(29, 59)*
		factor<BigInteger>(28, 58)*
		factor<BigInteger>(27, 57)*
		factor<BigInteger>(26, 56)*
		factor<BigInteger>(25, 55)*
		factor<BigInteger>(24, 54)*
		factor<BigInteger>(23, 53)*
		factor<BigInteger>(22, 52)*
		factor<BigInteger>(21, 51);
	num_colors[4] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	//5 color
	numcom = numCombination(5, 7);
	prob = factor<BigInteger>(10, 70) *
		factor<BigInteger>(10, 69)*
		factor<BigInteger>(10, 68)*
		factor<BigInteger>(10, 67)*
		factor<BigInteger>(10, 66)*
		factor<BigInteger>(45, 65)*
		factor<BigInteger>(44, 64)*
		factor<BigInteger>(43, 63)*
		factor<BigInteger>(42, 62)*
		factor<BigInteger>(41, 61)*
		factor<BigInteger>(40, 60) *
		factor<BigInteger>(39, 59)*
		factor<BigInteger>(38, 58)*
		factor<BigInteger>(37, 57)*
		factor<BigInteger>(36, 56)*
		factor<BigInteger>(35, 55)*
		factor<BigInteger>(34, 54)*
		factor<BigInteger>(33, 53)*
		factor<BigInteger>(32, 52)*
		factor<BigInteger>(31, 51);
	num_colors[5] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	//6 color
	numcom = numCombination(6, 7);
	prob = factor<BigInteger>(10, 70) *
		factor<BigInteger>(10, 69)*
		factor<BigInteger>(10, 68)*
		factor<BigInteger>(10, 67)*
		factor<BigInteger>(10, 66)*
		factor<BigInteger>(10, 65)*
		factor<BigInteger>(54, 64)*
		factor<BigInteger>(53, 63)*
		factor<BigInteger>(52, 62)*
		factor<BigInteger>(51, 61)*
		factor<BigInteger>(50, 60) *
		factor<BigInteger>(49, 59)*
		factor<BigInteger>(48, 58)*
		factor<BigInteger>(47, 57)*
		factor<BigInteger>(46, 56)*
		factor<BigInteger>(45, 55)*
		factor<BigInteger>(44, 54)*
		factor<BigInteger>(43, 53)*
		factor<BigInteger>(42, 52)*
		factor<BigInteger>(41, 51);
	num_colors[6] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	//7 color
	numcom = 1;
	prob = factor<BigInteger>(10, 70) *
		factor<BigInteger>(10, 69)*
		factor<BigInteger>(10, 68)*
		factor<BigInteger>(10, 67)*
		factor<BigInteger>(10, 66)*
		factor<BigInteger>(10, 65)*
		factor<BigInteger>(10, 64);
	num_colors[7] = prob * factor<BigInteger>((unsigned long)numcom, 1);
	prob = factor<BigInteger>(0, 1);
	for (int i = 2; i <= 7; i++)
	{
		cout << "Color : " << i << endl;
		cout << bigIntegerToString(num_colors[i].num) << endl;
		cout << bigIntegerToString(num_colors[i].denum) << endl;
		cout << "=========" << endl;
		prob += factor<BigInteger>(i, 1) * num_colors[i];
	}
	cout << bigIntegerToString(prob.num) << endl;
	cout << bigIntegerToString(prob.denum) << endl;
	return ret;
#endif
}
