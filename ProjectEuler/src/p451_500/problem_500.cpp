// Problem 500!!!
// Problem 500
// The number of divisors of 120 is 16.
// In fact 120 is the smallest number having 16 divisors.

// Find the smallest number with 2500500 divisors.
// Give your answer modulo 500500507.

#include "template_class.h"
#include <string>
#include <map>

#define N 1e8

extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);
static vector<uint64_t> prime_list;
static vector<uint64_t> exp_list;
static vector<uint64_t> next_exp_list;
static vector<uint64_t> next_increaseN_list;
static vector<uint64_t> next_reduceN_list;

uint64_t problem_500()
{
	const uint64_t mod = 500500507;
	uint64_t ret = 0;
	create_prime_filter_method(&prime_list, 0, N);
	int size = prime_list.size();
	const int exp_num_devisor = 500500;
	//const int exp_num_devisor = 4;
#if 0
	uint64_t cnt = 0;
	for (int i = 0; i < exp_num_devisor; i++)
	{
		for (int j = 0; j < exp_num_devisor; j++)
		{
			cnt++;
			if (cnt % 1000000000 == 0)
			{
				cout << cnt << endl;
			}
		}
	}
	return 0;
#endif
	int num_p = exp_num_devisor;
	exp_list.resize(exp_num_devisor);
	next_exp_list.resize(exp_num_devisor);
	next_increaseN_list.resize(exp_num_devisor);
	next_reduceN_list.resize(exp_num_devisor);
	for (int i = 0; i < exp_num_devisor; i++)
	{
		exp_list[i] = 1;
		next_exp_list[i] = 3;
		next_increaseN_list[i] = (uint64_t)(pow(prime_list[i], 2) + 0.5);
		next_reduceN_list[i] = prime_list[i];
	}
	// reduce num
	int cnt = 0;
	int target_reduce_idx = num_p - 1;
	bool start_cnt = false;
	while (target_reduce_idx > 0)
	{
		int reduce_idx = -1;
		bool isFinish = false;
		while (exp_list[target_reduce_idx])
		{
			// val N reduce
			uint64_t reduceN = next_reduceN_list[target_reduce_idx];
			uint64_t reduceNumDivs = exp_list[target_reduce_idx] + 1;
			for (int i = 0; i < target_reduce_idx; i++)
			{
				uint64_t increaseN = next_increaseN_list[i];
				if (increaseN < reduceN)
				{
					reduce_idx = i;
					exp_list[target_reduce_idx] = (exp_list[target_reduce_idx] + 1) / 2 - 1;
					if (exp_list[target_reduce_idx])
					{
						next_reduceN_list[target_reduce_idx] =
							(uint64_t)(pow(prime_list[reduce_idx], (exp_list[target_reduce_idx] - 1) / 2) + 0.5);
					}
					else
					{
						next_reduceN_list[target_reduce_idx] = 0;
					}
					exp_list[reduce_idx] = next_exp_list[reduce_idx];
					next_increaseN_list[reduce_idx] = (uint64_t)(pow(prime_list[reduce_idx], next_exp_list[reduce_idx] + 1) + 0.5);
					next_exp_list[reduce_idx] = next_exp_list[reduce_idx] * 2 + 1;
					break;
				}
			}
			// reduce
			if (reduce_idx == -1)
			{
				// finish
				isFinish = true;
				//start_cnt = true;
				break;
			}
		}
		if (isFinish)
		{
			break;
		}
		target_reduce_idx--;
	}
	uint64_t smallest = 1;
#if 1
	for (int i = 0; i < exp_num_devisor; i++)
	{
		if (exp_list[i])
		{
			//cout << exp_list[i] << " ";
			for (int e = 0; e < exp_list[i]; e++)
			{
				smallest *= prime_list[i];
				if (smallest > mod)
				{
					smallest = smallest % mod;
				}
			}
		}
		else
		{
			break;
		}
	}
	//cout << "smallest: " << smallest << endl;
	cout << "smallest: ";
#endif
	return smallest;
}
