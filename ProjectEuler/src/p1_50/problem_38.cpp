//Pandigital multiples
//Problem 38
//Take the number 192 and multiply it by each of 1, 2, and 3:
//
//192 � 1 = 192
//192 � 2 = 384
//192 � 3 = 576
//By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)
//
//The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).
//
//What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?

#include "template_class.h"

bool CheckPandigital(int x, vector<int>& digit)
{
    int store_size = digit.size();
    while(x)
    {
        int tmp = x % 10;
        for(int i = 0; i < digit.size(); i++)
        {
            if(tmp == digit[i] || tmp == 0)
            {
                return false;
            }
        }
        digit.push_back(tmp);
        x /= 10;
    }

    for(int i = store_size; i < (digit.size()-store_size)/2 + store_size; i++)
    {
        swap(digit[i], digit[digit.size() - i - 1 + store_size]);
    }
    return true;
}

bool CheckPandigitalMultiples(int x, vector<int>& digit)
{
    if(!CheckPandigital(x, digit))
    {
        return false;
    }
    int n= 2;
    while(digit.size() < 9)
    {
        if(!CheckPandigital(x*n, digit))
        {
            return false;
        }
        n++;
    }
    if(digit.size() != 9)
    {
        return false;
    }
    return true;
}

int problem_38()
{
    vector<int> result;
    for(int i = 1; i < 10; i++)
    {
        for(int j = 1; j < 10; j++)
        {
            if(j == i)
            {
                continue;
            }
            for(int k = 1; k < 10; k++)
            {
                if(k == j || k == i)
                {
                    continue;
                }
                for(int l = 1; l < 10; l++)
                {
                    if(l == k || l == j || l == i)
                    {
                        continue;
                    }
                    // 4 digits
                    vector<int> digit;
                    int x = i*1000 + j*100 + k*10 + l;
                    if(CheckPandigitalMultiples(x, digit))
                    {
                        result.push_back(
                            digit[0] * 1e8 +
                            digit[1] * 1e7 +
                            digit[2] * 1e6 +
                            digit[3] * 1e5 +
                            digit[4] * 1e4 +
                            digit[5] * 1e3 +
                            digit[6] * 1e2 +
                            digit[7] * 1e1 +
                            digit[8]
                        );
                    }
                }
                // 3 digits
                int x = i*100 + j*10 + k;
                vector<int> digit;
                if(CheckPandigitalMultiples(x, digit))
                {
                    result.push_back(
                        digit[0] * 1e8 +
                        digit[1] * 1e7 +
                        digit[2] * 1e6 +
                        digit[3] * 1e5 +
                        digit[4] * 1e4 +
                        digit[5] * 1e3 +
                        digit[6] * 1e2 +
                        digit[7] * 1e1 +
                        digit[8]
                    );
                }
            }
            // 2 digits
            vector<int> digit;
            int x = i*10 + j;
            if(CheckPandigitalMultiples(x, digit))
            {
                result.push_back(
                    digit[0] * 1e8 +
                    digit[1] * 1e7 +
                    digit[2] * 1e6 +
                    digit[3] * 1e5 +
                    digit[4] * 1e4 +
                    digit[5] * 1e3 +
                    digit[6] * 1e2 +
                    digit[7] * 1e1 +
                    digit[8]
                );
            }
        }
        // 1 digits
        vector<int> digit;
        if(CheckPandigitalMultiples(i, digit))
        {
            result.push_back(
                digit[0] * 1e8 +
                digit[1] * 1e7 +
                digit[2] * 1e6 +
                digit[3] * 1e5 +
                digit[4] * 1e4 +
                digit[5] * 1e3 +
                digit[6] * 1e2 +
                digit[7] * 1e1 +
                digit[8]
            );
        }
    }
    int max = 0;
    for(int i = 0; i < result.size(); i++)
    {
        if(max < result[i])
        {
            max = result[i];
        }
    }
    return max;
}
