//Goldbach's other conjecture
//Problem 46
//It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.
//
//9 = 7 + 2�12
//15 = 7 + 2�22
//21 = 3 + 2�32
//25 = 7 + 2�32
//27 = 19 + 2�22
//33 = 31 + 2�12
//
//It turns out that the conjecture was false.
//
//What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?

#include "template_class.h"

extern bool prime_test(int x);
extern bool check_square(uint32_t n);
extern vector<int> prime_list;

bool check_twice_square(uint32_t n)
{
    if(n == 0)
    {
        return false;
    }
    if(n & 1)
    {
        return false;
    }
    return check_square(n >> 1);
}

bool test_problem_46(uint32_t n)
{
    int prime_idx = 0;
    while(1)
    {
        if(prime_idx == prime_list.size())
        {
            return false;
        }
        if(prime_list[prime_idx] >= n)
        {
            return false;
        }
        if(check_twice_square(n - prime_list[prime_idx]))
        {
            return true;
        }
        prime_idx++;
    }
    return false;
}

int problem_46()
{
    for(int i = 5; ; i += 2)
    {
        if(!prime_test(i))
        {
            if(!test_problem_46(i))
            {
                return i;
            }
        }
    }
    return 0;
}
