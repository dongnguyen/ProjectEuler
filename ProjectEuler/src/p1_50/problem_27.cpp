//Quadratic primes
//Problem 27
//Euler discovered the remarkable quadratic formula:
//
//n� + n + 41
//
//It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when n = 41, 41� + 41 + 41 is clearly divisible by 41.
//
//The incredible formula  n� ? 79n + 1601 was discovered, which produces 80 primes for the consecutive values n = 0 to 79. The product of the coefficients, ?79 and 1601, is ?126479.
//
//Considering quadratics of the form:
//
//n� + an + b, where |a| < 1000 and |b| < 1000
//
//where |n| is the modulus/absolute value of n
//e.g. |11| = 11 and |?4| = 4
//Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.

#include "template_class.h"

extern bool prime_test(int x);
extern vector<int> prime_list;

couple_int problem_27()
{
    // S = n� + an + b
    // n = 0 --> S = b
    // --> b prime and positive
    // n = 1 --> S = 1 + a + b
    // a = 0, and odd
    // create list b (prime)
    for(int i = 5; i <= 1000; i+=2)
    {
        prime_test(i);
    }
    vector<int>& b = prime_list;

    couple_int ret;

    int max_len = 0;
    for(int a = -999; a < 0; a += 2)
    {
        for(int j = 0; j < b.size(); j++)
        {
            int count = 0;
            for(int n = 0;; n++)
            {
                if(false == prime_test(n*n + a*n + b[j]))
                {
                    break;
                }
                count++;
            }
            if(max_len < count)
            {
                max_len = count;
                ret = couple_int(a,b[j]);
            }
        }
    }

    int a = 0;
    for(int j = 0; j < b.size(); j++)
    {
        int count = 0;
        for(int n = 0;; n++)
        {
            if(false == prime_test(n*n + a*n + b[j]))
            {
                break;
            }
            count++;
        }
        if(max_len < count)
        {
            max_len = count;
            ret = couple_int(a,b[j]);
        }
    }

    for(a = 1; a < 1000; a += 2)
    {
        for(int j = 0; j < b.size(); j++)
        {
            int count = 0;
            for(int n = 0;; n++)
            {
                if(false == prime_test(n*n + a*n + b[j]))
                {
                    break;
                }
                count++;
            }
            if(max_len < count)
            {
                max_len = count;
                ret = couple_int(a,b[j]);
            }
        }
    }

    return couple_int(ret.m_a*ret.m_b,0);
}
