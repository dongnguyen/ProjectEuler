//Circular primes
//Problem 35
//The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
//
//There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
//
//How many circular primes are there below one million?

#include "template_class.h"

extern bool prime_test(int x);

void test_problem_35(int x, vector<int>& result)
{
    for(int i = 0; i < result.size(); i++)
    {
        if(result[i] == x)
        {
            return;
        }
    }
    if(!prime_test(x))
    {
        return;
    }
    vector<int> digits;
    digits.reserve(5);
    int tmp = x;
    while(tmp)
    {
        digits.push_back(tmp % 10);
        tmp /= 10;
    }
    for(int i = 0; i < digits.size(); i++)
    {
        if((digits[i] & 1 ) == 0 || digits[i] == 5)
        {
            return;
        }
    }
    for(int i = 0; i < digits.size()/2; i++)
    {
        swap(digits[i], digits[digits.size() - i - 1]);
    }
    vector<int> candidates;
    candidates.resize(digits.size() - 1);
    for(int i = 0; i < candidates.size(); i++)
    {
        int tmp = digits[0];
        int new_cand = 0;
        for(int j = 0; j < digits.size() - 1; j++)
        {
            digits[j] = digits[j+1];
            new_cand = new_cand*10 + digits[j];
        }
        digits.back() = tmp;
        new_cand = new_cand*10 + tmp;
        candidates[i] = new_cand;
        if(!prime_test(new_cand))
        {
            return;
        }
    }
    result.push_back(x);
    for(int i = 0; i < candidates.size(); i++)
    {
        result.push_back(candidates[i]);
    }
}

int problem_35()
{
    for(int tmp = 5; tmp < 1e6; tmp += 2)
    {
        prime_test(tmp);
    }
    vector<int> result;
    vector<int> digits;
    digits.resize(4);
    digits[0] = 1;
    digits[1] = 3;
    digits[2] = 7;
    digits[3] = 9;
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            for(int k = 0; k < 4; k++)
            {
                for(int l = 0; l < 4; l++)
                {
                    for(int m = 0; m < 4; m++)
                    {
                        for(int n = 0; n < 4; n++)
                        {
                            int tmp = digits[i]*100000 + digits[j]* 10000 + digits[k]* 1000 + digits[l] * 100 + digits[m] * 10 + digits[n];
                            test_problem_35(tmp, result);
                        }
                        int tmp = digits[i]*10000 + digits[j]* 1000 + digits[k]* 100 + digits[l] * 10 + digits[m];
                        test_problem_35(tmp, result);
                    }
                    int tmp = digits[i]*1000 + digits[j]* 100 + digits[k]* 10 + digits[l];
                    test_problem_35(tmp, result);
                }
                int tmp = digits[i]*100 + digits[j]* 10 + digits[k];
                test_problem_35(tmp, result);
            }
        }
    }
    return result.size() + 13;
}
