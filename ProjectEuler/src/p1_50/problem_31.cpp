//Coin sums
//Problem 31
//In England the currency is made up of pound, �, and pence, p, and there are eight coins in general circulation:
//
//1p, 2p, 5p, 10p, 20p, 50p, �1 (100p) and �2 (200p).
//It is possible to make �2 in the following way:
//
//1ף1 + 1�50p + 2�20p + 1�5p + 1�2p + 3�1p
//How many different ways can �2 be made using any number of coins?

#include "template_class.h"

int input_coin[8] = {1, 2, 5, 10, 20, 50, 100, 200};

int problem_31(int x)
{
    //return count_way(x, input_coin, 8);
    int count = 0;
    for(int i = 0; i <= x/input_coin[7]; i++)
    {
        int tmp1 = x - i*input_coin[7];
        if(!tmp1)
        {
            count++;
            continue;
        }
        for(int j = 0; j <= tmp1/input_coin[6]; j++)
        {
            int tmp2 = tmp1 - j*input_coin[6];
            if(!tmp2)
            {
                count++;
                break;
            }
            for(int k = 0; k <= tmp2/input_coin[5]; k++)
            {
                int tmp3 = tmp2 - k*input_coin[5];
                if(!tmp3)
                {
                    count++;
                    break;
                }
                for(int l = 0; l <= tmp3/input_coin[4]; l++)
                {
                    int tmp4 = tmp3 - l*input_coin[4];
                    if(!tmp4)
                    {
                        count++;
                        break;
                    }
                    for(int m = 0; m <= tmp4/input_coin[3]; m++)
                    {
                        int tmp5 = tmp4 - m*input_coin[3];
                        if(!tmp5)
                        {
                            count++;
                            break;
                        }
                        for(int n = 0; n <= tmp5/input_coin[2]; n++)
                        {
                            int tmp6 = tmp5 - n*input_coin[2];
                            if(!tmp6)
                            {
                                count++;
                                break;
                            }
                            for(int o = 0; o <= tmp6/input_coin[1]; o++)
                            {
                                int tmp7 = tmp6 - o*input_coin[1];
                                if(!tmp7)
                                {
                                    count++;
                                    break;
                                }
                                count++;
                            }
                        }
                    }
                }
            }
        }
    }
    return count;
}