//Consecutive prime sum
//Problem 50
//The prime 41, can be written as the sum of six consecutive primes:
//
//41 = 2 + 3 + 5 + 7 + 11 + 13
//This is the longest sum of consecutive primes that adds to a prime below one-hundred.
//
//The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.
//
//Which prime, below one-million, can be written as the sum of the most consecutive primes?

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_list(int x);

int problem_50(int limit)
{
    create_prime_list(limit);
    int ret = 0;
    int max = 0;
    int start_idx = 0;
    while(1)
    {
        int sum = 0;
        int idx = start_idx;
        for(int i = 0; i < max && idx < prime_list.size(); i++)
        {
            sum += prime_list[idx++];
        }
        while(sum < limit && idx < prime_list.size())
        {
            sum += prime_list[idx++];
            bool found = false;
            for(int i = idx + 1; i < prime_list.size(); i++)
            {
                if(sum < prime_list[i])
                {
                    break;
                }
                if(sum == prime_list[i])
                {
                    found = true;
                    break;
                }
            }
            if(found)
            {
                ret = sum;
                max = idx - start_idx + 1;
            }
        }
        start_idx++;
        if(start_idx == prime_list.size() - 1)
        {
            break;
        }
    }
    return ret;
}
