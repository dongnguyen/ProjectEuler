//Digit canceling fractions
//Problem 33
//The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
//
//We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
//
//There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.
//
//If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

#include "template_class.h"

bool check_curious_fraction(fraction f)
{
    if(f.numerator >= f.denominator)
    {
        return false;
    }
    vector<int> n;
    vector<int> d;
    n.resize(2);
    d.resize(2);

    n[0] = f.numerator / 10;
    n[1] = f.numerator % 10;

    d[0] = f.denominator / 10;
    d[1] = f.denominator % 10;

    int same = -1;
    int pos[2];
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            if(n[i] == d[j])
            {
                same = n[i];
                pos[0] = i;
                pos[1] = j;
                break;
            }
        }
        if(same != -1)
        {
            break;
        }
    }
    if(same != -1)
    {
        n.erase(n.begin() + pos[0]);
        d.erase(d.begin() + pos[1]);
    }

    int gcd1 = gcd(n[0], d[0]);
    int gcd2 = gcd(f.numerator, f.denominator);

    if(fraction(n[0] / gcd1, d[0] / gcd1) == fraction(f.numerator / gcd2, f.denominator / gcd2))
    {
        return true;
    }

    return false;
}

int problem_33()
{
    vector<fraction> list;

    for(int i = 1; i < 10; i++)
    {
        for(int j = 1; j < 10; j++)
        {
            for(int k = 1; k < 10; k++)
            {
                int a = i*10 + j;
                int b = i*10 + k;
                fraction f(a, b);
                if(check_curious_fraction(f))
                {
                    f.reduce_form();
                    list.push_back(f);
                }

                a = i*10 + j;
                b = k*10 + i;
                f = fraction(a, b);
                if(check_curious_fraction(f))
                {
                    f.reduce_form();
                    list.push_back(f);
                }

                a = j*10 + i;
                b = k*10 + i;
                f = fraction(a, b);
                if(check_curious_fraction(f))
                {
                    f.reduce_form();
                    list.push_back(f);
                }

                a = j*10 + i;
                b = i*10 + k;
                f = fraction(a, b);
                if(check_curious_fraction(f))
                {
                    f.reduce_form();
                    list.push_back(f);
                }
            }
        }
    }

    int p1 = 1;
    int p2 = 1;
    for(int i = 0; i < list.size(); i++)
    {
        p1 *= list[i].numerator;
        p2 *= list[i].denominator;
    }
    fraction f(p1, p2);
    f.reduce_form();

    return f.denominator;
}
