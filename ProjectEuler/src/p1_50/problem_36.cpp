//Double-base palindromes
//Problem 36
//The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
//
//Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
//
//(Please note that the palindromic number, in either base, may not include leading zeros.)

#include "template_class.h"


extern bool check_palindromic(int x);
extern int BitReverse32(int x);
extern int32_t FoundNumLeadingZero(int32_t x);

bool check_palindromic_36(int x)
{
    if(!check_palindromic(x))
    {
        return false;
    }
    int num_shift = FoundNumLeadingZero(x);
    int y = BitReverse32(x);
    int mask = BitReverse32(-1 << num_shift);
    y >>= num_shift;
    y &= mask;
    if(x == y)
    {
        return true;
    }
    return false;
}

int problem_36(int limit)
{
    int sum = 0;
    for(int i = 0; i < limit; i++)
    {
        if(check_palindromic_36(i))
        {
            sum += i;
        }
    }
    return sum;
}
