//Prime permutations
//Problem 49
//The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.
//
//There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.
//
//What 12-digit number do you form by concatenating the three terms in this sequence?

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_list(int x);

bool check_permutation(vector<int>& in)
{
    vector<vector<int>> digits;
    digits.resize(in.size());
    for(int i = 0; i < digits.size(); i++)
    {
        int tmp = in[i];
        while(tmp)
        {
            digits[i].push_back(tmp % 10);
            tmp /= 10;
        }
        sort(digits[i].begin(), digits[i].end());
    }
    for(int i = 1; i < digits.size(); i++)
    {
        if(digits[i].size() != digits[0].size())
        {
            return false;
        }
        for(int j = 0; j < digits[0].size(); j++)
        {
            if(digits[i][j] != digits[0][j])
            {
                return false;
            }
        }
    }
    return true;
}

uint64_t problem_49()
{
    uint64_t ret = 0;
    create_prime_list(1e4);
    int found_4_digits = 0;
    for(; found_4_digits < prime_list.size(); found_4_digits++)
    {
        if(prime_list[found_4_digits] > 1e3)
        {
            break;
        }
    }
    vector<int> prime_list_4_digits;
    prime_list_4_digits.assign(prime_list.begin() + found_4_digits, prime_list.end());
    vector<vector<int>> list_consequence;
    for(int i = 0; i < prime_list_4_digits.size() - 2; i++)
    {
        for(int j = i + 1; j < prime_list_4_digits.size() - 1; j++)
        {
            int diff = prime_list_4_digits[j] - prime_list_4_digits[i];
            int candidate = prime_list_4_digits[j] + diff;
            bool found = false;
            for(int k = j + 1; k < prime_list_4_digits.size(); k++)
            {
                if(candidate == prime_list_4_digits[k])
                {
                    found = true;
                    break;
                }
            }
            if(found)
            {
                vector<int> element;
                element.resize(3);
                element[0] = prime_list_4_digits[i];
                element[1] = prime_list_4_digits[j];
                element[2] = candidate;
                if(check_permutation(element))
                {
                    if(1487 == element[0])
                    {
                        continue;
                    }
                    list_consequence.push_back(element);
                }
            }
        }
    }
    ret = list_consequence[0][0] * 1e8 +
          list_consequence[0][1] * 1e4 +
          list_consequence[0][2];
    return ret;
}
