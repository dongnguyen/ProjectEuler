//Truncatable primes
//Problem 37
//Published on Saturday, 15th February 2003, 01:00 am; Solved by 32678
//The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.
//
//Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
//
//NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

#include "template_class.h"

extern bool prime_test(int x);
extern vector<int> prime_list;

bool CheckTruncatablePrimes(int x)
{
    if(!prime_test(x))
    {
        return false;
    }
    int ite = 0;
    int den = 10;
    do
    {
        ite = x % den;
        if(!prime_test(ite))
        {
            return false;
        }
        den *= 10;
    }
    while(ite != x);

    int tmp = x/10;
    while(tmp)
    {
        if(!prime_test(tmp))
        {
            return false;
        }
        tmp /= 10;
    }

    return true;
}

int problem_37()
{
    prime_list.push_back(5);
    prime_list.push_back(7);
    int count = 0;
    int start = 11;
    int sum = 0;
    while(count != 11)
    {
        if(CheckTruncatablePrimes(start))
        {
            sum += start;
            count++;
        }
        start += 2;
    }
    return sum;
}