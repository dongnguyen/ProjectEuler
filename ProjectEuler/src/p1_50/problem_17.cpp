//Number letter counts
//Problem 17
//If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
//
//If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
//
//
//NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

#include "template_class.h"

int one_digit[10] =
{
    0,
    3, //one
    3, //two
    5, //three
    4, //four
    4, //five
    3, //six
    5, //seven
    5, //eight
    4  //nine
};

int two_10[]=
{
    0,
    3, //ten
    6, //twenty
    6, //thirty
    5, //forty
    5, //fifty
    5, //sixty
    7, //seventy
    6, //eighty
    6  //ninety
};

int teen[] =
{
    0,
    6, //eleven
    6, //twelve
    8, //thirteen
    8, //fourteen
    7, //fifteen
    7, //sixteen
    9, //seventeen
    8, //eighteen
    8  //nineteen
};

int _and = 3;
int hundred = 7;
int thousand = 8;

int count_letter(int x)
{
    if(x < 10)
    {
        return one_digit[x];
    }

    if(x < 100)
    {
        if(x % 10 == 0)
        {
            return two_10[x/10];
        }
        if(x < 20)
        {
            return teen[x - 10];
        }

        return two_10[x / 10] + one_digit[x % 10];
    }

    if(x < 1000)
    {
        if(x % 100 == 0)
        {
            return one_digit[x/100] + hundred;
        }

        return one_digit[x/100] + hundred + _and + count_letter(x % 100);
    }

    if(x == 1000)
    {
        return one_digit[1] + thousand;
    }

    return -1;
}

int problem_17(int x)
{
    int sum = 0;
    for(int i = 1; i <= x; i++)
    {
        sum += count_letter(i);
    }
    return sum;
}
