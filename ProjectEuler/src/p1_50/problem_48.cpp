//Self powers
//Problem 48
//The series, 11 + 22 + 33 + ... + 1010 = 10405071317.
//
//Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.

#include "template_class.h"

extern void sum_num_string(vector<int>& a, vector<int>& b, vector<int>& s);

void mul_string_with_n_last_10_digit(int n, vector<int>& result)
{
    mul_string_with_n(n, result);
    if(result.size() > 10)
    {
        result.resize(10);
    }
}

void last_10_digit_of_n_power_n(int n, vector<int>& result)
{
    result.resize(1);
    result[0] = 1;
    for(int i = 0; i < n; i++)
    {
        mul_string_with_n_last_10_digit(n, result);
    }
}

uint64_t problem_48()
{
    vector<int> result;
    for(int i = 1; i <= 1000; i++)
    {
        if(i % 10 == 0)
        {
            continue;
        }
        vector<int> tmp;
        last_10_digit_of_n_power_n(i, tmp);
        sum_num_string(result, tmp, result);
        result.resize(11);
    }
    uint64_t ret = result[9]*1e9 +
                   result[8]*1e8 +
                   result[7]*1e7 +
                   result[6]*1e6 +
                   result[5]*1e5 +
                   result[4]*1e4 +
                   result[3]*1e3 +
                   result[2]*1e2 +
                   result[1]*1e1 +
                   result[0];
    return ret;
}
