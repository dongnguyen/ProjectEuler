//Lexicographic permutations
//Problem 24
//A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
//
//012   021   102   120   201   210
//
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

#include "template_class.h"

extern int first_num(vector<int>& list, int& order);

int problem_24()
{
	vector<int> list;
	vector<int> new_list;
	int size = 10;
	int order = (int)1e6 - 1;
	list.reserve(size);
	new_list.reserve(size);
	for (int i = 0; i < size; i++)
	{
		list.push_back(i);
	}
	int64_t result = 0;
	for (int i = 0; i < size; i++)
	{
		new_list.push_back(first_num(list, order));
		result = result * 10 + new_list.back();
	}
	return (int)result;
}
