//Number spiral diagonals
//Problem 28
//Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
//
//21 22 23 24 25
//20  7  8  9 10
//19  6  1  2 11
//18  5  4  3 12
//17 16 15 14 13
//
//It can be verified that the sum of the numbers on the diagonals is 101.
//
//What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?


#include "template_class.h"

int64_t problem_28(int n)
{
    int n_1 = 1;
    int n_0 = 0;
    int64_t sum = 1;
    int inc = 2;
    for(int idx = 3; idx <= n; idx += 2)
    {
        for(int i = 0; i < 4; i++)
        {
            n_0 = n_1 + inc;
            n_1 = n_0;
            sum += n_0;
        }
        inc += 2;
    }
    return sum;
}
