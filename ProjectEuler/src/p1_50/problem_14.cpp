//Longest Collatz sequence
//Problem 14
//The following iterative sequence is defined for the set of positive integers:
//
//n ? n/2 (n is even)
//n ? 3n + 1 (n is odd)
//
//Using the rule above and starting with 13, we generate the following sequence:
//
//13 ? 40 ? 20 ? 10 ? 5 ? 16 ? 8 ? 4 ? 2 ? 1
//It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
//
//Which starting number, under one million, produces the longest chain?
//
//NOTE: Once the chain starts the terms are allowed to go above one million.

#include "template_class.h"

extern uint64_t count_collatz_sequence(uint64_t x);

uint64_t problem_14(uint32_t n)
{
	bool *evaluated = new bool[n + 1];
	uint64_t *count_all = new uint64_t[n + 1];
	memset(evaluated, 0, (n + 1) * sizeof(bool));
	memset(count_all, 0, (n + 1) * sizeof(uint64_t));
	evaluated[0] = true;
	count_all[0] = 0;
	uint64_t max = 0;
	uint64_t ret = 0;
	for (uint64_t i = n; i >= 1; i--)
	{
		if (!evaluated[i])
		{
			count_all[i] = count_collatz_sequence(i);
			evaluated[i] = true;
		}
		if (!(i & 1))
		{
			evaluated[i >> 1] = true;
			count_all[i >> 1] = count_all[i] - 1;
		}
		if (max < count_all[i])
		{
			max = count_all[i];
			ret = i;
		}
	}

	delete[]evaluated;
	delete[]count_all;
	return ret;
}
