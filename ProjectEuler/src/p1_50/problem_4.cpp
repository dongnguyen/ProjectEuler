//Largest palindrome product
//Problem 4
//A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.
//
//Find the largest palindrome made from the product of two 3-digit numbers.

#include "template_class.h"

extern bool check_palindromic(int x);

int problem_4()
{
    int x = 999;
    int y = 999;
    int tmp = 0;
    int ret = 0;
    bool result = false;
    do
    {
        do
        {
            tmp = x * y;
            result = check_palindromic(tmp);
            if(result)
            {
                if(ret < tmp)
                {
                    ret = tmp;
                }
            }
            y--;
        }
        while(y >= 100);
        x--;
        y = x;
    }
    while(x >= 100);
    return ret;
}