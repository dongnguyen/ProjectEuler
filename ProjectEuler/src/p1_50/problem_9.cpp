//Special Pythagorean triplet
//Problem 9
//A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
//
//a2 + b2 = c2
//For example, 32 + 42 = 9 + 16 = 25 = 52.
//
//There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//Find the product abc.

#include "template_class.h"

struct elements
{
    int val;
    int sqr;
    /*elements & operator=(const elements &rhs)
    {
        val = rhs.val;
        sqr = rhs.sqr;
        return *this;
    }*/
};

int problem_9(int x)
{
    vector<elements> square;
    square.reserve(x);
    for(int i = 1; i <= x; i++)
    {
        elements tmp;
        tmp.val = i;
        tmp.sqr = i*i;
        square.push_back(tmp);
    }
    for(int i = x - 2; i >= 2; i--)
    {
        int c  = square[i].val;
        int c2 = square[i].sqr;
        for(int j = min(x - c - 3, i - 1); j >= 1; j--)
        {
            int b  = square[j].val;
            int b2 = square[j].sqr;

            int a  = square[x - c - b - 1].val;
            int a2 = square[x - c - b - 1].sqr;
            if(c2 == (a2+b2))
            {
                return a*b*c;
            }
        }
    }
    return -1;
}