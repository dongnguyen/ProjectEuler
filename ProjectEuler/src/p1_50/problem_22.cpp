//Names scores
//Problem 22
//Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.
//
//For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 � 53 = 49714.
//
//What is the total of all the name scores in the file?

#include "template_class.h"

void read_input_words(char* path, vector<vector<char>>& words_list)
{
    FILE* fp = fopen(path, "r");
    char c;
    bool read_word = false;
    int num_blink_count = 0;
    while(!feof(fp))
    {
        fscanf(fp, "%c", &c);
        if(c == '"')
        {
            num_blink_count++;
            if(num_blink_count == 1)
            {
                vector<char> tmp;
                words_list.push_back(tmp);
                read_word = true;
            }
            else if(num_blink_count == 2)
            {
                read_word = false;
                num_blink_count = 0;
                words_list.back().push_back(0); // --> put the NULL terminate.
            }
            continue;
        }
        if(read_word)
        {
            words_list.back().push_back(c-64);
        }
    }
    words_list.pop_back();
    fclose(fp);
}

struct less_than_key
{
    inline bool operator() (const vector<char>& str1, const vector<char>& str2)
    {
        return (strcmp(str1.data(), str2.data()) < 0);
        /*int i = 0;
        while (i < str1.size() && i < str2.size()) {
            if (str1[i] == str2[i]) {
                i++;
            } else {
                return str1[i] < str2[i];
            }
        }
        return str1.size() <= str2.size();*/
    }
};

int problem_22(char* path)
{
    /*char a[] = "test";
    char b[] = "test2";
    int dd = strcmp(a, b);*/
    vector<vector<char>> words_list;
    read_input_words(path, words_list);
    std::sort(words_list.begin(), words_list.end(), less_than_key());

    vector<int> scores;
    scores.resize(words_list.size());
    for(int i = 0; i < scores.size(); i++)
    {
        int worth = 0;
        for(int j = 0; j < words_list[i].size()-1; j++)
        {
            worth += words_list[i][j];
        }
        scores[i] = (i+1)*worth;
    }
    int count = 0;
    for(int i = 0; i < scores.size(); i++)
    {
        count += scores[i];
    }
    /*sort(scores.begin(), scores.end());
    int count = 1;
    int pre = scores[0];
    int idx = 1;
    while(1)
    {
        if(scores[idx] != pre)
        {
            count++;
            pre = scores[idx];
        }
        idx++;
        if(idx == scores.size())
        {
            break;
        }
    }*/
    return count;
}
