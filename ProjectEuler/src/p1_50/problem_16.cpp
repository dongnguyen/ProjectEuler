//Power digit sum
//Problem 16
//215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
//
//What is the sum of the digits of the number 21000?


#include "template_class.h"

extern void compute_2_pow_n(int n, vector<char>& result);

int problem_16(int n)
{
    vector<char> result;
    compute_2_pow_n(n, result);
    int sum = 0;
    for(int i = 0; i < result.size(); i++)
    {
        sum += result[i];
    }
    return sum;
}