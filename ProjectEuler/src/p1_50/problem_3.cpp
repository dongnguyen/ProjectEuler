//Largest prime factor
//Problem 3
//The prime factors of 13195 are 5, 7, 13 and 29.
//
//What is the largest prime factor of the number 600851475143 ?

#include "template_class.h"

int64_t problem_3(int64_t x)
{
    //int64_t sqr = (int64_t)sqrt((float)x);
    int64_t ret = 2;
    if((x & 1) == 0)
    {
        x >>= 1;
    }
    int64_t loop = 3;
    int count = 0;
    while(x != 1)
    {
        while(x % loop == 0)
        {
            x /= loop;
        }
        ret = loop;
        loop += 2;
        count++;
    }
    return ret;
}