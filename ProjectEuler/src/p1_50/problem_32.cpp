//Pandigital products
//Problem 32
//We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.
//
//The product 7254 is unusual, as the identity, 39 � 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.
//
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//
//HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.

#include "template_class.h"

bool check_pandigital(int a, int b)
{
    vector<int> digit;
    int p = a*b;
    while(a)
    {
        digit.push_back(a % 10);
        a /= 10;
    }
    while(b)
    {
        digit.push_back(b % 10);
        b /= 10;
    }
    while(p)
    {
        digit.push_back(p % 10);
        p /= 10;
    }
    if(digit.size() != 9)
    {
        return false;
    }
    sort(digit.begin(), digit.end());
    //int count = 0;
    int pre = digit[0];
    if(pre == 0)
    {
        return false;
    }
    for(int i = 1; i < 9; i++)
    {
        if(digit[i] == pre)
        {
            return false;
        }
        pre = digit[i];
    }
    return true;
}

int problem_32()
{
    vector<int> result;
    for(int i = 1; i < 10; i++)
    {
        for(int j = 1; j < 10; j++)
        {
            if(j == i)
            {
                continue;
            }
            for(int k = 1; k < 10; k++)
            {
                if(k == j || k == i)
                {
                    continue;
                }
                for(int l = 1; l < 10; l++)
                {
                    if(l == i || l == j || l == k)
                    {
                        continue;
                    }
                    for(int m = 1; m < 10; m++)
                    {
                        if(m == i || m == j || m == k || m == l)
                        {
                            continue;
                        }
                        if(check_pandigital(i, j*1000 + k*100 + l*10 + m))
                        {
                            int tmp = i * (j*1000 + k*100 + l*10 + m);
                            bool have = false;
                            for(int x = 0; x < result.size(); x++)
                            {
                                if(result[x] == tmp)
                                {
                                    have = true;
                                    break;
                                }
                            }
                            if(!have)
                            {
                                result.push_back(tmp);
                            }
                        }
                        if(check_pandigital(i*10 + j, k*100 + l*10 + m))
                        {
                            int tmp = (i*10 + j) * (k*100 + l*10 + m);
                            bool have = false;
                            for(int x = 0; x < result.size(); x++)
                            {
                                if(result[x] == tmp)
                                {
                                    have = true;
                                    break;
                                }
                            }
                            if(!have)
                            {
                                result.push_back(tmp);
                            }
                        }
                    }
                }
            }
        }
    }
    int sum = 0;
    for(int i = 0; i < result.size(); i++)
    {
        sum += result[i];
    }
    return sum;
}
