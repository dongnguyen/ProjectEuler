//Integer right triangles
//Problem 39
//If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.
//
//{20,48,52}, {24,45,51}, {30,40,50}
//
//For which value of p ? 1000, is the number of solutions maximised?

#include "template_class.h"


int problem_39(int limit)
{
    vector<int> pow2;
    pow2.resize(limit);
    vector<int> p;
    p.resize(limit+1);
    for(int i = 0; i < limit; i++)
    {
        pow2[i] = i*i;
    }
    for(int i = 0; i < limit; i++)
    {
        for(int j = i; j < limit; j++)
        {
            int sum = pow2[i] + pow2[j];
            if(sum < limit*limit)
            {
                int idx = (int)sqrt(sum);
                if(sum == pow2[idx])
                {
                    if(i + j + idx <= limit)
                    {
                        p[i + j + idx]++;
                    }
                }
            }
        }
    }
    int max = 0;
    int pos = 0;
    for(int i = 0; i <= limit; i++)
    {
        if(max < p[i])
        {
            max = p[i];
            pos = i;
        }
    }
    return pos;
}
