//Distinct powers
//Problem 29
//Consider all integer combinations of ab for 2 ? a ? 5 and 2 ? b ? 5:
//
//22=4, 23=8, 24=16, 25=32
//32=9, 33=27, 34=81, 35=243
//42=16, 43=64, 44=256, 45=1024
//52=25, 53=125, 54=625, 55=3125
//If they are then placed in numerical order, with any repeats removed, we get the following sequence of 15 distinct terms:
//
//4, 8, 9, 16, 25, 27, 32, 64, 81, 125, 243, 256, 625, 1024, 3125
//
//How many distinct terms are in the sequence generated by ab for 2 ? a ? 100 and 2 ? b ? 100?

#include "template_class.h"

int problem_29(int n)
{
	vector<int_3> list;
	for (int i = 2; i <= sqrt(n); i++)
	{
		for (int j = 2; j <= n; j++)
		{
			int tmp = (int)pow(i, j);
			if (tmp <= n)
			{
				int_3 a(i, j, tmp);
				bool insert = true;
				for (int k = 0; k < list.size(); k++)
				{
					if (a == list[k])
					{
						insert = false;
						break;
					}
				}
				if (insert)
				{
					list.push_back(a);
				}
			}
			else
			{
				break;
			}
		}
	}
	int total = (n - 1)*(n - 1);
	int count_redundant = 0;

	for (int i = 0; i < list.size(); i++)
	{
		count_redundant += (n - 2 * list[i].b) / list[i].b + 1;
	}

	vector<vector<int>> a_b_list;
	vector<int> tt1;
	a_b_list.push_back(tt1);
	int pre = list[0].a;
	a_b_list[0].push_back(list[0].b);
	for (int i = 1; i < list.size(); i++)
	{
		if (list[i].a != pre)
		{
			vector<int> tt;
			tt.push_back(list[i].b);
			a_b_list.push_back(tt);
			pre = list[i].a;
		}
		else
		{
			a_b_list.back().push_back(list[i].b);
		}
	}

	for (int i = 0; i < a_b_list.size(); i++)
	{
		if (a_b_list[i].size() > 1)
		{
			int b = 0;
			if (n % a_b_list[i][0] == 0)
			{
				b = n + a_b_list[i][0];
			}
			else
			{
				b = (n / a_b_list[i][0] + 1) * a_b_list[i][0];
			}
			vector<int> tmp;
			for (; b <= a_b_list[i][0] * n; b += a_b_list[i][0])
			{
				tmp.push_back(b);
			}
			for (int j = 1; j < a_b_list[i].size(); j++)
			{
				int b = 0;
				if (n % a_b_list[i][j] == 0)
				{
					b = n + a_b_list[i][j];
				}
				else
				{
					b = (n / a_b_list[i][j] + 1) * a_b_list[i][j];
				}
				for (; b <= a_b_list[i][j] * n; b += a_b_list[i][j])
				{
					bool have = false;
					for (int k = 0; k < tmp.size(); k++)
					{
						if (b == tmp[k])
						{
							have = true;
							count_redundant++;
							break;
						}
					}
					if (!have)
					{
						tmp.push_back(b);
					}
				}
			}
		}
	}

	return total - count_redundant;
	//int l = 10;
	/*vector<int_3> tmp;
	vector<int_3> tmp2;
	for(int a = 2; a <= l; a++)
	{
		for(int b = 2; b <= l; b++)
		{
			int_3 aa(a,b,pow(a,b));
			tmp.push_back(aa);
			tmp2.push_back(aa);
		}
	}
	sort(tmp.begin(), tmp.end());
	int count = 1;
	uint64_t pre = tmp[0].a_pow_b;
	for(int i = 1; i < tmp.size(); i++)
	{
		if(tmp[i].a_pow_b != pre)
		{
			count++;
			pre = tmp[i].a_pow_b;
		}
	}*/
}
