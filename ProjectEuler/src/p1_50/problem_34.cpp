//Digit factorials
//Problem 34
//145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
//
//Find the sum of all numbers which are equal to the sum of the factorial of their digits.
//
//Note: as 1! = 1 and 2! = 2 are not sums they are not included.

#include "template_class.h"

extern bool test_problem_30(vector<int>& list, int val);
extern uint64_t factorial(int n);

int problem_34()
{
    /*__int64 n = 1;
    while(n*factorial(9) > pow(10, n))
    {
        n++;
    }*/
    //n --> maximum = 7

    vector<int> result;

    for(int i = 0; i < 10; i++)
    {
        for(int j = i; j < 10; j++)
        {
            for(int k = j; k < 10; k++)
            {
                for(int l = k; l < 10; l++)
                {
                    for(int m = l; m < 10; m++)
                    {
                        for(int n = m; n < 10; n++)
                        {
                            for(int o = n; o < 10; o++)
                            {
                                vector<int> list;
                                list.resize(7);
                                list[0] = i;
                                list[1] = j;
                                list[2] = k;
                                list[3] = l;
                                list[4] = m;
                                list[5] = n;
                                list[6] = o;
                                if(test_problem_30(list,
                                                   factorial(i) +
                                                   factorial(j) +
                                                   factorial(k) +
                                                   factorial(l) +
                                                   factorial(m) +
                                                   factorial(n) +
                                                   factorial(o)))
                                {
                                    result.push_back(factorial(i) +
                                                     factorial(j) +
                                                     factorial(k) +
                                                     factorial(l) +
                                                     factorial(m) +
                                                     factorial(n) +
                                                     factorial(o));
                                }
                            }
                            vector<int> list;
                            list.resize(6);
                            list[0] = i;
                            list[1] = j;
                            list[2] = k;
                            list[3] = l;
                            list[4] = m;
                            list[5] = n;
                            if(test_problem_30(list,
                                               factorial(i) +
                                               factorial(j) +
                                               factorial(k) +
                                               factorial(l) +
                                               factorial(m) +
                                               factorial(n)))
                            {
                                result.push_back(factorial(i) +
                                                 factorial(j) +
                                                 factorial(k) +
                                                 factorial(l) +
                                                 factorial(m) +
                                                 factorial(n));
                            }
                        }
                        vector<int> list;
                        list.resize(5);
                        list[0] = i;
                        list[1] = j;
                        list[2] = k;
                        list[3] = l;
                        list[4] = m;
                        if(test_problem_30(list,
                                           factorial(i) +
                                           factorial(j) +
                                           factorial(k) +
                                           factorial(l) +
                                           factorial(m)))
                        {
                            result.push_back(factorial(i) +
                                             factorial(j) +
                                             factorial(k) +
                                             factorial(l) +
                                             factorial(m));
                        }
                    }
                    vector<int> list;
                    list.resize(4);
                    list[0] = i;
                    list[1] = j;
                    list[2] = k;
                    list[3] = l;
                    if(test_problem_30(list,
                                       factorial(i) +
                                       factorial(j) +
                                       factorial(k) +
                                       factorial(l)))
                    {
                        result.push_back(factorial(i) +
                                         factorial(j) +
                                         factorial(k) +
                                         factorial(l));
                    }
                }
                vector<int> list;
                list.resize(3);
                list[0] = i;
                list[1] = j;
                list[2] = k;
                if(test_problem_30(list,
                                   factorial(i) +
                                   factorial(j) +
                                   factorial(k)))
                {
                    result.push_back(factorial(i) +
                                     factorial(j) +
                                     factorial(k));
                }
            }
            vector<int> list;
            list.resize(2);
            list[0] = i;
            list[1] = j;
            if(test_problem_30(list,
                               factorial(i) +
                               factorial(j)))
            {
                result.push_back(factorial(i) +
                                 factorial(j));
            }
        }
    }
    return 0;
}
