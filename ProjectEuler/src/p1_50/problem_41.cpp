//Pandigital prime
//Problem 41
//We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.
//
//What is the largest n-digit pandigital prime that exists?

#include "template_class.h"

extern vector<int> prime_list;
extern bool prime_test3(int x);
extern void create_prime_list(int x);

int problem_41()
{
    create_prime_list((int)sqrt(1e9));
    vector<int> pandigital_prime;
    for(int i = 9; i >= 0; i--)
    {
        bool found = false;
        vector<int> digits;
        digits.resize(i);
        for(int j = 1; j <= i ; j++)
        {
            digits[j-1] = j;
        }
        do
        {
            int tmp = 0;
            for(int j = 0; j < i ; j++)
            {
                tmp = tmp * 10 + digits[j];
            }
            if(prime_test3(tmp))
            {
                pandigital_prime.push_back(tmp);
                found = true;
            }
        }
        while(next_permutation(digits.begin(), digits.end()));
        if(found)
        {
            break;
        }
    }
    return pandigital_prime.back();
}
