//Sub-string divisibility
//Problem 43
//The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.
//
//Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:
//
//d2d3d4=406 is divisible by 2
//d3d4d5=063 is divisible by 3
//d4d5d6=635 is divisible by 5
//d5d6d7=357 is divisible by 7
//d6d7d8=572 is divisible by 11
//d7d8d9=728 is divisible by 13
//d8d9d10=289 is divisible by 17
//Find the sum of all 0 to 9 pandigital numbers with this property.

#include "template_class.h"

uint64_t problem_43()
{
    uint64_t sum = 0;
    vector<int> digits;
    digits.resize(10);
    for(int i = 0; i < 10; i++)
    {
        digits[i] = i;
    }
    do
    {
        if(digits[0] == 0)
        {
            continue;
        }
        if(digits[3] & 1)
        {
            continue;
        }
        if(digits[5] != 0 && digits[5] != 5)
        {
            continue;
        }
        if((digits[2] + digits[3] + digits[4]) % 3)
        {
            continue;
        }
        if((digits[4]*100 + digits[5]*10 + digits[6]) % 7)
        {
            continue;
        }
        if((digits[5]*100 + digits[6]*10 + digits[7]) % 11)
        {
            continue;
        }
        if((digits[6]*100 + digits[7]*10 + digits[8]) % 13)
        {
            continue;
        }
        if((digits[7]*100 + digits[8]*10 + digits[9]) % 17)
        {
            continue;
        }
        sum += digits[0]*1e9 +
               digits[1]*1e8 +
               digits[2]*1e7 +
               digits[3]*1e6 +
               digits[4]*1e5 +
               digits[5]*1e4 +
               digits[6]*1e3 +
               digits[7]*1e2 +
               digits[8]*1e1 +
               digits[9];
    }
    while(next_permutation(digits.begin(), digits.end()));
    return sum;
}
