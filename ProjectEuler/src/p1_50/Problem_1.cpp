//Multiples of 3 and 5
//    Problem 1
//    If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
//    The sum of these multiples is 23.
//
//    Find the sum of all the multiples of 3 or 5 below 1000.

#include "template_class.h"

int problem_1(int limit)
{
    // 3*n*(n+1)/2 + 5*m*(m+1)/2 - 15*k*(k+1)/2
    int n = (limit - 1) / 3;
    int m = (limit - 1) / 5;
    int k = (limit - 1) / 15;
    int result = 3 * n*(n + 1) / 2 +
                 5 * m*(m + 1) / 2 -
                 15 * k*(k + 1) / 2;
    return result;
}