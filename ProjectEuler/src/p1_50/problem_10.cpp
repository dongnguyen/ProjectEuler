//Summation of primes
//Problem 10
//The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//
//Find the sum of all the primes below two million.

#include "template_class.h"

extern vector<int> prime_list;
extern bool prime_test(int x);

int64_t problem_10(int n)
{
	prime_list.reserve((uint32_t)(n / log(n)));
	prime_test(2);
	for (int i = 3; i <= n; i += 2)
	{
		prime_test(i);
	}
	int64_t sum = 0;
	for (int i = 0; i < prime_list.size(); i++)
	{
		sum += prime_list[i];
	}
	return sum;
}
