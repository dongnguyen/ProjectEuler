//Distinct primes factors
//Problem 47
//The first two consecutive numbers to have two distinct prime factors are:
//
//14 = 2 � 7
//15 = 3 � 5
//
//The first three consecutive numbers to have three distinct prime factors are:
//
//644 = 2� � 7 � 23
//645 = 3 � 5 � 43
//646 = 2 � 17 � 19.
//
//Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_list(int x);
extern bool prime_test(int x);

class prime_factors
{
public:
    vector<int> num_each_prime;
    vector<int> divisor_prime;
    int mn;
    prime_factors()
    {
        mn = 0;
    }
    prime_factors(int n)
    {
        init(n);
    }
    void init(int n)
    {
        mn = n;
        num_each_prime.resize(0);
        divisor_prime.resize(0);
        int tmp_n = n;
        for(int i = 0; i < prime_list.size(); i++)
        {
            if(tmp_n == 1)
            {
                break;
            }
            int tmp = 0;
            while(tmp_n % prime_list[i] == 0)
            {
                tmp_n /= prime_list[i];
                tmp++;
            }
            if(tmp)
            {
                num_each_prime.push_back(tmp);
                divisor_prime.push_back(prime_list[i]);
            }
        }
    }
    /*bool operator!=(const prime_factors &other) const
    {
        for(int i = 0; i < num_each_prime.size(); i++)
        {
            for(int j = i; j <
            if(num_each_prime[i] == other.num_each_prime[i] &&
        }
    }*/
    int num_distinct_prime_factors()
    {
        return num_each_prime.size();
    }
};

int problem_47()
{
    //create_prime_list(1e6);
    /*vector<int> integer;
    vector<int> num_factor;
    integer.resize(4);
    num_factor.resize(4);
    for(int i = 0; i < 4; i++)
    {
        integer[i] = i + 2;
        prime_factors pf(integer[i]);
        num_factor[i] = pf.num_distinct_prime_factors();
    }*/
    int trace = 5;
    int pre = 3;
    int num = 4;
    while(1)
    {
        if(trace >= 644)
        {
            int d = 0;
            d++;
        }
        for(int i = pre + 2; i <= trace; i += 2)
        {
            if(prime_test(i))
            {
                pre = i;
            }
        }
        bool cont = false;
        for(int i = 0; i < num; i++)
        {
            prime_factors pf(trace + i);
            if(pf.num_distinct_prime_factors() != num)
            {
                trace += i+1;
                cont = true;
                break;
            }
        }
        if(cont)
        {
            continue;
        }
        return trace;
    }

    return 0;
}
