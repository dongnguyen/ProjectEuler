//Coded triangle numbers
//Problem 42
//The nth term of the sequence of triangle numbers is given by, tn = �n(n+1); so the first ten triangle numbers are:
//
//1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
//
//By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.
//
//Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?

#include "template_class.h"

extern void read_input_words(char* path, vector<vector<char>>& words_list);
extern bool check_triangle_number(int num);

void sum_char_position(vector<vector<char>>& words_list, vector<int>& sum)
{
    sum.resize(words_list.size());
    for(int i = 0; i < sum.size(); i++)
    {
        int sum_tmp = 0;
        for(int j = 0; j < words_list[i].size() - 1; j++)
        {
            sum_tmp += words_list[i][j];
        }
        sum[i] = sum_tmp;
    }
}

int problem_42(char* path)
{
    vector<vector<char>> words_list;
    read_input_words(path, words_list);
    vector<int> sum;
    sum_char_position(words_list, sum);
    int count = 0;
    for(int i = 0; i < sum.size(); i++)
    {
        if(check_triangle_number(sum[i]))
        {
            count++;
        }
    }
    return count;
}
