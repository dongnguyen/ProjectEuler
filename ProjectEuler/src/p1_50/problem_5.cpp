//Smallest multiple
//Problem 5
//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

#include "template_class.h"

int prime[] = {2, 3, 5, 7, 11, 13, 17, 19};

void count_func(int x, int range, int* c)
{
    for(int i = 0; prime[i] <= range; i++)
    {
        while(x % prime[i] == 0)
        {
            c[prime[i]]++;
            x /= prime[i];
        }
        if(x == 1)
        {
            break;
        }
    }
}

int problem_5(int range)
{
    int p = 1;
    int *count = new int[range+1];
    int *tmp = new int[range+1];
    memset(count, 0, (range+1)*sizeof(int));
    for(int i = 2; i <= range; i++)
    {
        memset(tmp, 0, (range+1)*sizeof(int));
        count_func(i, range, tmp);
        for(int j = 2; j <= range; j++)
        {
            if(count[j] < tmp[j])
            {
                count[j] = tmp[j];
            }
        }
    }
    for(int i = 2; i <= range; i++)
    {
        for(int j = 0; j < count[i]; j++)
        {
            p *= i;
        }
    }
    delete []count;
    delete []tmp;
    return p;
}