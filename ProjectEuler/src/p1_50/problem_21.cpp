//Amicable numbers
//Problem 21
//Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
//If d(a) = b and d(b) = a, where a ? b, then a and b are an amicable pair and each of a and b are called amicable numbers.
//
//For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
//
//Evaluate the sum of all the amicable numbers under 10000.

#include "template_class.h"

extern int sum_of_divisors(int n);

int problem_21(int n)
{
    //int a = sum_of_divisors(220);
    //int b = sum_of_divisors(284);
    vector<char> amicable_numbers;
    amicable_numbers.resize(n+1);
    for(int i = 1; i <= n; i++)
    {
        if(!amicable_numbers[i])
        {
            int a = sum_of_divisors(i);
            if(a <= n && a != i)
            {
                if(i == sum_of_divisors(a))
                {
                    amicable_numbers[i] = 1;
                    amicable_numbers[a] = 1;
                }
            }
        }
    }
    int sum = 0;
    for(int i = 1; i <=n; i++)
    {
        if(amicable_numbers[i])
        {
            sum += i;
        }
    }
    return sum;
}