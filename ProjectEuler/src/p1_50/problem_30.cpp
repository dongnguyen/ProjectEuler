//Digit fifth powers
//Problem 30
//Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
//
//1634 = 14 + 64 + 34 + 44
//8208 = 84 + 24 + 04 + 84
//9474 = 94 + 44 + 74 + 44
//As 1 = 14 is not a sum it is not included.
//
//The sum of these numbers is 1634 + 8208 + 9474 = 19316.
//
//Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.

#include "template_class.h"

extern bool test_problem_30(vector<int>& list, int val);

int pow_5[] =
{
    0,      //0
    1,      //1
    32,     //2
    243,    //3
    1024,   //4
    3125,   //5
    7776,   //6
    16807,  //7
    32768,  //8
    59049   //9
};

int problem_30()
{
    //2 digits
    vector<int> list;
    vector<int> result;
    list.resize(2);
    for(int i = 0; i < 10; i++)
    {
        for(int j = i; j < 10; j++)
        {
            list[0] = i;
            list[1] = j;
            if(test_problem_30(list, pow_5[i] + pow_5[j]))
            {
                result.push_back(pow_5[i] + pow_5[j]);
            }
        }
    }
    //3 digits
    list.resize(3);
    for(int i = 0; i < 10; i++)
    {
        for(int j = i; j < 10; j++)
        {
            for(int k = j; k < 10; k++)
            {
                list[0] = i;
                list[1] = j;
                list[2] = k;
                if(test_problem_30(list, pow_5[i] + pow_5[j] + pow_5[k]))
                {
                    result.push_back(pow_5[i] + pow_5[j] + pow_5[k]);
                }
            }
        }
    }
    //4 digits
    list.resize(4);
    for(int i = 0; i < 10; i++)
    {
        for(int j = i; j < 10; j++)
        {
            for(int k = j; k < 10; k++)
            {
                for(int l = k; l < 10; l++)
                {
                    list[0] = i;
                    list[1] = j;
                    list[2] = k;
                    list[3] = l;
                    if(test_problem_30(list, pow_5[i] + pow_5[j] + pow_5[k] + pow_5[l]))
                    {
                        result.push_back(pow_5[i] + pow_5[j] + pow_5[k] + pow_5[l]);
                    }
                }
            }
        }
    }
    //5 digits
    list.resize(5);
    for(int i = 0; i < 10; i++)
    {
        for(int j = i; j < 10; j++)
        {
            for(int k = j; k < 10; k++)
            {
                for(int l = k; l < 10; l++)
                {
                    for(int m = l; m < 10; m++)
                    {
                        list[0] = i;
                        list[1] = j;
                        list[2] = k;
                        list[3] = l;
                        list[4] = m;
                        if(test_problem_30(list, pow_5[i] + pow_5[j] + pow_5[k] + pow_5[l] + pow_5[m]))
                        {
                            result.push_back(pow_5[i] + pow_5[j] + pow_5[k] + pow_5[l] + pow_5[m]);
                        }
                    }
                }
            }
        }
    }
    //6 digits
    list.resize(6);
    for(int i = 0; i < 10; i++)
    {
        for(int j = i; j < 10; j++)
        {
            for(int k = j; k < 10; k++)
            {
                for(int l = k; l < 10; l++)
                {
                    for(int m = l; m < 10; m++)
                    {
                        for(int n = m; n < 10; n++)
                        {
                            list[0] = i;
                            list[1] = j;
                            list[2] = k;
                            list[3] = l;
                            list[4] = m;
                            list[5] = n;
                            if(test_problem_30(list, pow_5[i] + pow_5[j] + pow_5[k] + pow_5[l] + pow_5[m] + pow_5[n]))
                            {
                                result.push_back(pow_5[i] + pow_5[j] + pow_5[k] + pow_5[l] + pow_5[m] + pow_5[n]);
                            }
                        }
                    }
                }
            }
        }
    }
    int sum = 0;
    for(int i = 0; i < result.size(); i++)
    {
        sum += result[i];
    }

    return sum;
}