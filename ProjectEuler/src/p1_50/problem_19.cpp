//Counting Sundays
//Problem 19
//You are given the following information, but you may prefer to do some research for yourself.
//
//1 Jan 1900 was a Monday.
//Thirty days has September,
//April, June and November.
//All the rest have thirty-one,
//Saving February alone,
//Which has twenty-eight, rain or shine.
//And on leap years, twenty-nine.
//A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
//How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

int month_days[] =
{
    0,
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
};

int problem_19()
{
    int year = 1900;
    int month = 1;
    int day = 1;
    int count = 0;
    do
    {
        if(month==13)
        {
            month = 1;
            year++;
        }
        bool leap = true;
        if(year % 4 != 0)
        {
            leap = false;
        }
        else
        {
            if(year % 100 == 0 && year % 400 != 0)
            {
                leap = false;
            }
        }
        int day_to_next_month = month_days[month];
        if(month == 2 && leap)
        {
            day_to_next_month++;
        }
        day = (day + day_to_next_month) % 7;
        if(day == 0 && year > 1900)
        {
            count++;
        }
        month++;
    }
    while(year != 2000 || month!=13);
    return count;
}
