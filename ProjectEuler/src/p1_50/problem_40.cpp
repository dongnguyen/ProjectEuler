//Champernowne's constant
//Problem 40
//An irrational decimal fraction is created by concatenating the positive integers:
//
//0.123456789101112131415161718192021...
//
//It can be seen that the 12th digit of the fractional part is 1.
//
//If dn represents the nth digit of the fractional part, find the value of the following expression.
//
//d1 � d10 � d100 � d1000 � d10000 � d100000 � d1000000

#include "template_class.h"

int ChampernowneConstant(int order)
{
    int idx = order - 1;
    const int const_1 = 10;
    if(order < const_1)
    {
        return order;
    }
    const int const_2 = (99 - 10 + 1)*2 + const_1;
    if(order < const_2)
    {
        idx -= const_1 - 1;
        int num = idx / 2 + 10;
        int digit_order = idx % 2;
        vector<int> digit;
        while(num)
        {
            int tmp = num % 10;
            digit.push_back(tmp);
            num /= 10;
        }
        return digit[digit.size() - digit_order - 1];
    }
    const int const_3 = (999 - 100 + 1)*3 + const_2;
    if(order < const_3)
    {
        idx -= const_2 - 1;
        int num = idx / 3 + 100;
        int digit_order = idx % 3;
        vector<int> digit;
        while(num)
        {
            int tmp = num % 10;
            digit.push_back(tmp);
            num /= 10;
        }
        return digit[digit.size() - digit_order - 1];
    }
    const int const_4 = (9999 - 1000 + 1)*4 + const_3;
    if(order < const_4)
    {
        idx -= const_3 - 1;
        int num = idx / 4 + 1000;
        int digit_order = idx % 4;
        vector<int> digit;
        while(num)
        {
            int tmp = num % 10;
            digit.push_back(tmp);
            num /= 10;
        }
        return digit[digit.size() - digit_order - 1];
    }
    const int const_5 = (99999 - 10000 + 1)*5 + const_4;
    if(order < const_5)
    {
        idx -= const_4 - 1;
        int num = idx / 5 + 10000;
        int digit_order = idx % 5;
        vector<int> digit;
        while(num)
        {
            int tmp = num % 10;
            digit.push_back(tmp);
            num /= 10;
        }
        return digit[digit.size() - digit_order - 1];
    }
    const int const_6 = (999999 - 100000 + 1)*6 + const_5;
    if(order < const_6)
    {
        idx -= const_5 - 1;
        int num = idx / 6 + 100000;
        int digit_order = idx % 6;
        vector<int> digit;
        while(num)
        {
            int tmp = num % 10;
            digit.push_back(tmp);
            num /= 10;
        }
        return digit[digit.size() - digit_order - 1];
    }
    return -1;
}

int problem_40()
{
    int ret = ChampernowneConstant(1      ) *
              ChampernowneConstant(10     ) *
              ChampernowneConstant(100    ) *
              ChampernowneConstant(1000   ) *
              ChampernowneConstant(10000  ) *
              ChampernowneConstant(100000 ) *
              ChampernowneConstant(1000000);
    return ret;
}
