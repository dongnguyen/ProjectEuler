//10001st prime
//Problem 7
//By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//
//What is the 10001st prime number?

#include "template_class.h"

using namespace std;

extern vector<int> prime_list;
extern bool prime_test(int x);

int problem_7(int order)
{
    prime_list.reserve(order);
    int x = 3;
    prime_test(2);
    while(prime_list.size() != (unsigned int)order)
    {
        prime_test(x);
        x += 2;
    }
    return prime_list.back();
}
