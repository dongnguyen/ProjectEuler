//Using up to one million tiles how many different "hollow" square laminae can be formed?
//Problem 173
//We shall define a square lamina to be a square outline with a square "hole" so that the shape possesses vertical and horizontal symmetry. For example, using exactly thirty-two square tiles we can form two different square laminae:
//
//
//With one-hundred tiles, and not necessarily using all of the tiles at one time, it is possible to form forty-one different square laminae.
//
//Using up to one million tiles how many different square laminae can be formed?

#include "template_class.h"

uint64_t problem_173(int limit)
{
    //limit = 100;
    int max_n = (limit + 1) / 2;
    int center_n = sqrt(limit);
    uint64_t sum = 0;
    for(uint64_t n = max_n; n > center_n; n--)
    {
        int k = 0;
        double sqrt_delta = sqrt(n*n - limit);
        k = n - sqrt_delta;
        k >>= 1;
        sum += k;
    }
    for(uint64_t n = center_n; n > 2; n--)
    {
        int k = (n - 3) / 2 + 1;
        sum += k;
    }
    return sum;
}
