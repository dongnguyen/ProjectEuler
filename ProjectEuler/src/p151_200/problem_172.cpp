//Investigating numbers with few repeated digits.
//Problem 172
//How many 18 - digit numbers n(without leading zeros) are there such that no digit occurs more than three times in n ?

#include "template_class.h"

uint64_t count_172(int num_remain, int len, bool not_first)
{
    /*------------------------------------------*/
    // exit condition
    if (num_remain * 3 < len)
    {
        return 0;
    }
    if (len == 0)
    {
        return 1;
    }
    if (len < 0)
    {
        return 0;
    }
    /*------------------------------------------*/
    uint64_t sum = 0;
    sum += count_172(num_remain - 1, len, true);
    sum += len*count_172(num_remain - 1, len - 1, true);
    sum += len*(len - 1)*count_172(num_remain - 1, len - 2, true);
    if (not_first)
    {
        sum += len*(len - 1)*(len - 2)*count_172(num_remain - 1, len - 3, true);
    }
    return sum;
    /*------------------------------------------*/
}

uint64_t problem_172()
{
    uint64_t d = count_172(10, 2, false);
    return 0;
}