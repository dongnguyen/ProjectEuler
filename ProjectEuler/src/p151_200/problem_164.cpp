//Numbers for which no three consecutive digits have a sum greater than a given value.
//Problem 164
//How many 20 digit numbers n(without any leading zero) exist such that no three consecutive digits of n have a sum greater than 9 ?

#include "template_class.h"

uint64_t problem_164(int n)
{
    uint64_t memo[1000][4];
    for (int i = 0; i < 1000; i++)
    {
        memo[i][0] = i / 100;
        memo[i][1] = (i % 100) / 10;
        memo[i][2] = i % 10;
        if (memo[i][0] && (memo[i][0] + memo[i][1] + memo[i][2] <= 9))
        {
            memo[i][3] = 1;
        }
        else
        {
            memo[i][3] = 0;
        }
    }
    for (int i = 3; i < n; i++)
    {
        uint64_t tmp[1000] = { 0 };
        for (int j = 0; j < 1000; j++)
        {
            int add = 0;
            for (int k = memo[j][1] + memo[j][2]; k <= 9; k++)
            {
                tmp[memo[j][1] * 100 + memo[j][2] * 10 + add] += memo[j][3];
                add++;
            }
        }
        for (int j = 0; j < 1000; j++)
        {
            memo[j][3] = tmp[j];
        }
    }
    uint64_t sum = 0;
    for (int i = 0; i < 1000; i++)
    {
        sum += memo[i][3];
    }
    return sum;
}
