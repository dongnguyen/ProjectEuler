//Prize Strings
//Problem 191
//A particular school offers cash rewards to children with good attendance and punctuality. If they are absent for three consecutive days or late on more than one occasion then they forfeit their prize.
//
//During an n-day period a trinary string is formed for each child consisting of L's (late), O's (on time), and A's (absent).
//
//Although there are eighty-one trinary strings for a 4-day period that can be formed, exactly forty-three strings would lead to a prize:
//
//OOOO OOOA OOOL OOAO OOAA OOAL OOLO OOLA OAOO OAOA
//OAOL OAAO OAAL OALO OALA OLOO OLOA OLAO OLAA AOOO
//AOOA AOOL AOAO AOAA AOAL AOLO AOLA AAOO AAOA AAOL
//AALO AALA ALOO ALOA ALAO ALAA LOOO LOOA LOAO LOAA
//LAOO LAOA LAAO
//
//How many "prize" strings exist over a 30-day period?

#include "template_class.h"

uint64_t count_prize_no_L(int n)
{
    if(n == 4)
    {
        return 13;
    }
    if(n == 3)
    {
        return 7;
    }
    if(n == 2)
    {
        return 4;
    }
    if(n == 1)
    {
        return 2;
    }
    if(n < -1)
    {
        return 0;
    }
    if(n < 1)
    {
        return 1;
    }
    uint64_t prize = count_prize_no_L(n - 1);
    uint64_t prize_add_A = count_prize_no_L(n - 4);
    return prize + (prize - prize_add_A);
}

uint64_t count_prize_1_L(int n)
{
    uint64_t prize = 0;
    for(int i = 0; i < n; i++)
    {
        prize += count_prize_no_L(i)*count_prize_no_L(n - i - 1);
    }
    //prize += 2*count_prize_no_L(n - 1);
    return prize;
}

uint64_t count_prize(int n)
{
    if(n == 4)
    {
        return 43;
    }
    if(n == 3)
    {
        return 19;
    }
    if(n == 2)
    {
        return 8;
    }
    if(n == 1)
    {
        return 3;
    }
    if(n < 1)
    {
        return 0;
    }
    uint64_t prize = count_prize(n - 1);
    //uint64_t prize_add_A = count_prize(n - 4) + count_prize_no_L(n - 4) + count_prize_1_L(n - 4);
    /*uint64_t prize_add_A = 0;
    if(n - 4 > 0)
    {
        prize_add_A = count_prize(n - 4) + count_prize_no_L(n - 4);
    }
    else
    {
        prize_add_A = count_prize(n - 4);
    }*/
    uint64_t prize_add_A = count_prize(n - 4) + count_prize_no_L(n - 4);
    uint64_t prize_add_L = count_prize_1_L(n - 1);
    /*if(n == 7)
    {
        return prize + (prize - prize_add_A) + (prize - prize_add_L);
    }*/
    return prize + (prize - prize_add_A) + (prize - prize_add_L);
}

uint64_t verify(int n)
{
    vector<int> list;
    list.resize(n);
    uint64_t count = 0;
    uint64_t count_add_A = 0;
    uint64_t count_add_L = 0;
    for(uint64_t i = 0; i < pow(3, n); i++)
    {
        // A
        bool foundA = false;
        int a_idx = 0;
        for(; a_idx < n - 2; a_idx++)
        {
            if(list[a_idx] == 1)
            {
                if(list[a_idx+1] == 1)
                {
                    if(list[a_idx+2] == 1)
                    {
                        foundA = true;
                        //count++;
                        break;
                    }
                    else
                    {
                        a_idx += 2;
                    }
                }
                else
                {
                    a_idx++;
                }
            }
        }

        //if(!foundA)
        //{
        // L
        bool foundL = false;
        int countL = 0;
        int l_idx = 0;
        for(; l_idx < n; l_idx++)
        {
            if(list[l_idx] == 2)
            {
                countL++;
                if(countL == 2)
                {
                    foundL = true;
                    //count++;
                    break;
                }
            }
        }
        //}
        if(foundA || foundL)
        {
            count++;
        }
        if(foundA && !foundL)
        {
            if(a_idx == n - 3)
            {
                count_add_A++;
            }
        }
        if(!foundA && foundL)
        {
            if(l_idx == n - 1)
            {
                count_add_L++;
            }
        }

        int idx = n - 1;
        list[idx]++;
        while(list[idx] == 3)
        {
            list[idx--] = 0;
            if(idx < 0)
            {
                break;
            }
            list[idx]++;
        }

        //if(foundL || foundA
    }
    return pow(3, n) - count;
}

uint64_t problem_191(int n)
{
    //uint64_t d = verify(n);
    uint64_t ret = count_prize(n);
    return ret;
}