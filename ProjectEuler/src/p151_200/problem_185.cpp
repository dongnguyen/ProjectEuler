// Number Mind
//  Problem 185
// The game Number Mind is a variant of the well known game Master Mind.

// Instead of coloured pegs, you have to guess a secret sequence of digits. After each guess you're only told in how many places you've guessed the correct digit. So, if the sequence was 1234 and you guessed 2036, you'd be told that you have one correct digit; however, you would NOT be told that you also have another digit in the wrong place.

// For instance, given the following guesses for a 5-digit secret sequence,

// 90342 ;2 correct
// 70794 ;0 correct
// 39458 ;2 correct
// 34109 ;1 correct
// 51545 ;2 correct
// 12531 ;1 correct

// The correct sequence 39542 is unique.

// Based on the following guesses,

// 5616185650518293 ;2 correct
// 3847439647293047 ;1 correct
// 5855462940810587 ;3 correct
// 9742855507068353 ;3 correct
// 4296849643607543 ;3 correct
// 3174248439465858 ;1 correct
// 4513559094146117 ;2 correct
// 7890971548908067 ;3 correct
// 8157356344118483 ;1 correct
// 2615250744386899 ;2 correct
// 8690095851526254 ;3 correct
// 6375711915077050 ;1 correct
// 6913859173121360 ;1 correct
// 6442889055042768 ;2 correct
// 2321386104303845 ;0 correct
// 2326509471271448 ;2 correct
// 5251583379644322 ;2 correct
// 1748270476758276 ;3 correct
// 4895722652190306 ;1 correct
// 3041631117224635 ;3 correct
// 1841236454324589 ;3 correct
// 2659862637316867 ;2 correct

// Find the unique 16-digit secret sequence.

#include "template_class.h"

#define NUM_DIGITS 16

class guess_memo
{
public:
    int pos;
    int num;
    guess_memo()
    {
        pos = 0;
        num = 0;
    }
    guess_memo(int p, int n)
    {
        pos = p;
        num = n;
    }
};

extern void combination(int offset, int k, vector<int>& set, vector<int>& com, vector < vector < int >> &outset);

string input_185 [] =
{
    "5616185650518290",
    "3847439647293040",
    "5855462940810580",
    "9742855507068350",
    "4296849643607540",
    "3174248439465850",
    "4513559094146110",
    "7890971548908060",
    "8157356344118480",
    "2615250744386890",
    "8690095851526250",
    "6375711915077050",
    "6913859173121360",
    "6442889055042760",
    "2321386104303840",
    "2326509471271440",
    "5251583379644320",
    "1748270476758270",
    "4895722652190300",
    "3041631117224630",
    "1841236454324580",
    "2659862637316860"
};

int num_corr [] =
{
    2,
    1,
    3,
    3,
    3,
    1,
    2,
    3,
    1,
    2,
    3,
    1,
    1,
    2,
    0,
    2,
    2,
    3,
    1,
    3,
    3,
    2
};

int size_185 = 0;

bool problem_185_recur(vector<guess_memo>& memo, vector<int>& valid_pos, int idx)
{
    if (idx >= size_185)
    {
        return false;
    }
    if (idx == size_185 - 1)
    {
        // verify code
    }

    int num_correct = 0;
    for (int i = 0; i < memo.size(); i++)
    {
        if (input_185[idx][memo[i].pos] == memo[i].num)
        {
            num_correct++;
        }
    }
    int num_correct_remain = num_corr[idx] - num_correct;
    if (num_correct_remain < 0)
    {
        return false;
    }
    if (num_correct_remain == 0)
    {
        // verify code
    }
    // check all possible valid
    vector<int> com;
    vector<vector<int>> outset;
    combination(0, num_corr[idx], valid_pos, com, outset);
    int next_idx = idx + 1;
    int next_num_corr = num_corr[next_idx];

    for (int i = 0; i < outset.size(); i++)
    {
    }
    return true;
}

string problem_185()
{
    /*uint64_t five_d = 1;
    for (int i = 2; i <= 100000; i++)
    {
        five_d *= i;
        while (five_d % 10 == 0)
        {
            five_d /= 10;
        }
        if (five_d > 100000)
        {
            five_d %= 100000;
        }
    }
    cout << five_d << endl;
    five_d = 1;
    for (int i = 100001; i <= 200000; i++)
    {
        five_d *= i;
        while (five_d % 10 == 0)
        {
            five_d /= 10;
        }
        if (five_d > 100000)
        {
            five_d %= 100000;
        }
    }
    cout << five_d << endl;*/

    size_185 = sizeof(num_corr) / sizeof(int);

    vector<guess_memo> memo_185;
    vector<int> valid_pos;
    valid_pos.resize(NUM_DIGITS);
    for (int i = 0; i < NUM_DIGITS; i++)
    {
        valid_pos[i] = i;
    }
    problem_185_recur(memo_185, valid_pos, 0);
    string ret;
    return ret;
}
