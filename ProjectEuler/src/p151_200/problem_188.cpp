//he hyperexponentiation of a number
//Problem 188
//The hyperexponentiation or tetration of a number a by a positive integer b, denoted by a??b or ba, is recursively defined by:
//
//a??1 = a,
//a??(k+1) = a(a??k).
//
//Thus we have e.g. 3??2 = 33 = 27, hence 3??3 = 327 = 7625597484987 and 3??4 is roughly 103.6383346400240996*10^12.
//
//Find the last 8 digits of 1777??1855.

#include "template_class.h"
#include "mpirxx.h"

extern vector<int> prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

int totient(int n)
{
    int ret = n;
    int tmp = n;
    for(int i = 0; i < prime_list.size(); i++)
    {
        if(tmp % prime_list[i] == 0)
        {
            do
            {
                tmp /= prime_list[i];
            }
            while(tmp % prime_list[i] == 0);
            ret /= prime_list[i];
            ret *= prime_list[i] - 1;
        }
        if(tmp == 1)
        {
            break;
        }
    }
    if(tmp == n)
    {
        ret = n - 1;
    }
    return ret;
}

// a^b mod m;
int large_mode(int a, int b, int m)
{
    if(b == 0)
    {
        return 1;
    }
    if(b > 10)
    {
        int pow_2 = log(b) / log(2);
        int remain = b - pow(2, pow_2);

        vector<uint64_t> pow2_2;
        pow2_2.resize(pow_2 + 1);
        pow2_2[0] = a % m;
        for(int i = 1; i <= pow_2; i++)
        {
            pow2_2[i] = (pow2_2[i-1]*pow2_2[i-1]) % m;
        }
        uint64_t tmp = 1;
        int mask = 1;
        for(int i = 0; i <= pow_2; i++)
        {
            if(b & mask)
            {
                tmp *= pow2_2[i];
                tmp %= m;
            }
            mask <<= 1;
        }
        return tmp;

        tmp = a % m;
        for(int i = 0; i < pow_2; i++)
        {
            tmp *= tmp;
            tmp %= m;
        }
        for(int i = 0; i < remain; i++)
        {
            tmp *= a;
            tmp %= m;
        }
        return tmp;
    }
    uint64_t tmp = a % m;
    for(int i = 1; i < b; i++)
    {
        tmp *= a;
        tmp %= m;
    }
    return tmp;
}

int problem_188()
{
    create_prime_filter_method(1e2);
    vector<int> mod_list;

    mod_list.push_back(1e8);
    for(int i = 1; i < 1855; i++)
    {
        mod_list.push_back(totient(mod_list.back()));
        if(mod_list.back() == 2)
        {
            break;
        }
    }

    int p = 1;
    for(int i = mod_list.size() - 2; i >= 0; i--)
    {
        p = large_mode(1777, p, mod_list[i]);
    }

    return p;
}