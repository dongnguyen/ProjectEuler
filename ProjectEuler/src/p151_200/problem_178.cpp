//Step Numbers
//Problem 178
//Consider the number 45656.
//It can be seen that each pair of consecutive digits of 45656 has a difference of one.
//A number for which every pair of consecutive digits has a difference of one is called a step number.
//A pandigital number contains every decimal digit from 0 to 9 at least once.
//How many pandigital step numbers less than 1040 are there?

#include "template_class.h"
#include "mpirxx.h"

uint64_t memo_no_lack[10][31];
uint64_t memo_lack_0[10][31];
uint64_t memo_lack_9[10][31];
uint64_t memo_lack_both[10][31];

#define LACK_9  (1 << 0)
#define LACK_0  (1 << 1)
#define LACK_BOTH  (LACK_9 | LACK_0)
#define NO_LACK (0)

uint64_t count_178(int start, int len, int flag)
{
    if(flag == NO_LACK)
    {
        if(memo_no_lack[start][len] > 0)
        {
            return memo_no_lack[start][len];
        }
    }
    if(flag == LACK_9)
    {
        if(memo_lack_9[start][len] > 0)
        {
            return memo_lack_9[start][len];
        }
    }
    if(flag == LACK_0)
    {
        if(memo_lack_0[start][len] > 0)
        {
            return memo_lack_0[start][len];
        }
    }
    if(flag == LACK_BOTH)
    {
        if(memo_lack_both[start][len] > 0)
        {
            return memo_lack_both[start][len];
        }
    }
    if(flag & LACK_9)
    {
        if(9 - start == len)
        {
            return 1;
        }
        else if(9 - start > len)
        {
            return 0;
        }
    }
    if(flag & LACK_0)
    {
        if(start == len)
        {
            return 1;
        }
        else if(start > len)
        {
            return 0;
        }
    }
    if(flag == LACK_BOTH)
    {
        // way 1 up --> down
        int need_way_1 = 9 - start + 9;
        int need_way_2 = start + 9;
        int min_way = min(need_way_1, need_way_2);
        if(min_way > len)
        {
            return 0;
        }
        else if(min_way == len)
        {
            return 1;
        }
    }
    if(len == 0)
    {
        return 0;
    }
    if(len == 1)
    {
        if(start == 9)
        {
            return 1;
        }
        if(start == 0)
        {
            return 1;
        }
        return 2;
    }
    uint64_t sum = 0;
    if(start + 1 < 10)
    {
        if((flag & LACK_9) && (start + 1 == 9))
        {
            sum += count_178(start + 1, len - 1, flag ^ LACK_9);
        }
        else
        {
            sum += count_178(start + 1, len - 1, flag);
        }
    }
    if(start - 1 >= 0)
    {
        if((flag & LACK_0) && (start - 1 == 0))
        {
            sum += count_178(start - 1, len - 1, flag ^ LACK_0);
        }
        else
        {
            sum += count_178(start - 1, len - 1, flag);
        }
    }
    if(flag == NO_LACK)
    {
        memo_no_lack[start][len] = sum;
    }
    else if(flag == LACK_9)
    {
        memo_lack_9[start][len] = sum;
    }
    else if(flag == LACK_0)
    {
        memo_lack_0[start][len] = sum;
    }
    else if(flag == LACK_BOTH)
    {
        memo_lack_both[start][len] = sum;
    }
    return sum;
}

uint64_t count_178(int n)
{
#if 0
    if(n == 1)
    {
        return 10;
    }
    uint64_t sum = 0;
    // increase
    int step_num = n / 2;
    int max = step_num;
    sum += 9 - max;
    // decrease
    int min = 9 - step_num;
    sum += min + 1;
    return sum;
#endif
    uint64_t sum = 0;
    int len = n;
    for(int i = 1; i < 9; i++)
    {
        sum += count_178(i, len - 1, LACK_BOTH);
    }
    sum += count_178(9, len - 1, LACK_0);
    return sum;
}

uint64_t problem_178()
{
    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j <= 30; j++)
        {
            memo_no_lack[i][j] = 0;
            memo_lack_0[i][j] = 0;
            memo_lack_9[i][j] = 0;
            memo_lack_both[i][j] = 0;
        }
    }
    uint64_t sum = 0;
    for(int i = 10; i <= 40; i++)
    {
        uint64_t tmp = count_178(i);
        sum += tmp;
        //cout << i << ": " << tmp << endl;
    }
    return sum;
}
