
#include "template_class.h"

#define STEP 0.0001
#define MUL ((int)(1.0/STEP))

//vector<vector<double>> memo_190;
//vector<double> log_memo;
double* log_memo;
double** memo_190;

double p190_rec(int sum, int num)
{
    if (num == 1)
    {
        //xx[num] = double(sum) * STEP;
        return double(sum) * STEP;
        //return log_memo[sum];
    }
    /*if (sum >= 15 * MUL)
    {
        cout << sum << " - " << num << endl;
        exit(0);
    }*/
    if (memo_190[num][sum] > 0)
    {
        return memo_190[num][sum];
    }
    double max = -99999999;
    int sum_remain = sum - (num - 1);
    int start = (sum_remain > (sum / num)) ? (sum / num) : 1;
    //int start = 1;
    for (int x = start; x <= sum_remain; x++)
    {
        double rx = double(x) * STEP;
        //double rx = log_memo[x];
        rx = pow(rx, num)*p190_rec(sum - x, num - 1);
        //rx = num*rx + p190_rec(sum - x, num - 1);
        if (max < rx)
        {
            max = rx;
            //xx[num] = double(x) * STEP;
        }
    }
    memo_190[num][sum] = max;
    return max;
}

uint64_t problem_190()
{
    //log_memo.resize(15 * MUL + 1);
    /*log_memo = new double[15 * MUL + 1];
    for (int i = 1; i < 15 * MUL + 1; i++)
    {
        log_memo[i] = log(double(i) * STEP);
    }*/
    /*memo_190.resize(15+1);
    for (int i = 1; i <= 15; i++)
    {
        memo_190[i].resize(15 * MUL + 1);
    }*/
    memo_190 = new double*[15 + 1];
    for (int i = 1; i <= 15; i++)
    {
        memo_190[i] = new double[15 * MUL + 1];
    }

    /* p190_rec(5*MUL, 5);
     for (int i = 1; i <= 5; i++)
     {
         cout << "x" << i << " = " << xx[i] << endl;
     }
     return 0;*/
    uint64_t ret = 0;
    for (int i = 2; i <= 15; i++)
    {
        //double rt = p190_rec(i*MUL, i);
        //uint64_t tmp = exp(rt);
        uint64_t tmp = p190_rec(i*MUL, i);
        cout << tmp << endl;
        ret += tmp;
    }
    //delete []log_memo;
    for (int i = 1; i <= 15; i++)
    {
        delete []memo_190[i];
    }
    delete []memo_190;
    return ret;
}