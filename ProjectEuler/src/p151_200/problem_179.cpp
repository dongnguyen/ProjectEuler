//Consecutive positive divisors
//Problem 179
//Find the number of integers 1 < n < 107, for which n and n + 1 have the same number of positive divisors. For example, 14 has the positive divisors 1, 2, 7, 14 while 15 has 1, 3, 5, 15.

#include "template_class.h"

extern int find_num_divisor(int x);
extern void create_prime_list(int x);
extern vector<int> prime_list;
extern set<int> set_prime_list;
extern bool prime_test3(int x);
extern bool prime_test(int x);

struct int_divisor
{
    int val;
    int num_divisors;
    int_divisor()
    {
        val = 0;
        num_divisors = 0;
    };
    int_divisor(int n, int d)
    {
        val = n;
        num_divisors = d;
    };
    bool operator==(const int_divisor& other) const
    {
        return (val == other.val);
    }
};

vector<int> list_num_divisor;
extern int calc_num_divisor(int x, int loop_idx = 0);
int calc_num_divisor(int x, int loop_idx)
{
    if(list_num_divisor[x])
    {
        return list_num_divisor[x];
    }
    if(set_prime_list.find(x) != set_prime_list.end())
    {
        list_num_divisor[x] = 2;
        return 2;
    }
    int tmp = x;
    for(int i = loop_idx; i < prime_list.size(); i++)
    {
        int count = 0;
        while(tmp % prime_list[i] == 0)
        {
            tmp /= prime_list[i];
            count++;
        }
        if(count)
        {
            list_num_divisor[x] = (count + 1)*calc_num_divisor(tmp, i + 1);
            return list_num_divisor[x];
        }
    }
    cout << "error code -1" << endl;
    exit(-1);
    return -1;
}

int problem_179(int n)
{
    create_prime_list(n);
    list_num_divisor.resize(n + 1);
    list_num_divisor[0] = 0;
    list_num_divisor[1] = 1;
    uint64_t count = 0;
    uint64_t pre_num_divisor = 0;
    uint64_t num_divisor = 0;
    for(uint64_t i = 1; i < n; i++)
    {
        num_divisor = calc_num_divisor(i);
        if(pre_num_divisor == num_divisor)
        {
            count++;
        }
        pre_num_divisor = num_divisor;
    }
    return count;

    //uint64_t count = 0;
    ////uint64_t tmp_p = 2*3;
    //uint64_t num_prime = 2;
    //uint64_t upper_bound = n/2;
    //for(uint64_t i = 5; ; i += 2)
    //{
    //    if(prime_test(i))
    //    {
    //        /*tmp_p *= i;
    //        if(tmp_p > n)
    //        {
    //            break;
    //        }*/
    //        if(i > upper_bound)
    //        {
    //            break;
    //        }
    //        num_prime++;
    //    }
    //}
    ////uint64_t max_divisor = pow(2, num_prime);
    //vector<vector<uint64_t>> list_number_acc_divisor;
    //vector<uint64_t> iterator;
    //iterator.resize(num_prime);
    ////list_number_acc_divisor.resize(max_divisor + 1);
    //while(1)
    //{
    //    uint64_t num = 1;
    //    uint64_t count_div = 1;
    //    for(uint64_t i = 0; i < num_prime; i++)
    //    {
    //        if(iterator[i])
    //        {
    //            num *= pow(prime_list[i], iterator[i]);
    //            count_div *= iterator[i] + 1;
    //        }
    //    }
    //    if(num < n)
    //    {
    //        if(count_div >= list_number_acc_divisor.size())
    //        {
    //            list_number_acc_divisor.resize(count_div + 1);
    //        }
    //        list_number_acc_divisor[count_div].push_back(num);
    //        iterator[0]++;
    //    }
    //    else
    //    {
    //        bool all_zero = true;
    //        for(uint64_t i = 0; i < num_prime - 1; i++)
    //        {
    //            if(iterator[i])
    //            {
    //                iterator[i] = 0;
    //                iterator[i+1]++;
    //                all_zero = false;
    //                break;
    //            }
    //        }
    //        if(all_zero)
    //        {
    //            break;
    //        }
    //    }
    //}
    //for(uint64_t i = 0; i < list_number_acc_divisor.size(); i++)
    //{
    //    if(list_number_acc_divisor[i].size())
    //    {
    //        sort(list_number_acc_divisor[i].begin(), list_number_acc_divisor[i].end());
    //        uint64_t limit = list_number_acc_divisor[i].size() - 1;
    //        for(uint64_t j = 0; j < limit; j++)
    //        {
    //            if(list_number_acc_divisor[i][j] + 1 == list_number_acc_divisor[i][j+1])
    //            {
    //                count++;
    //            }
    //        }
    //    }
    //}
    //return count;

    //create_prime_list(n);
    //set<int_divisor> calc_ed_list;
    //set<int_divisor>::iterator it;
    ///*cout << prime_list.size();
    //exit(0);*/
    //uint64_t pre_num_divisor = 0;
    //uint64_t num_divisor = 0;
    //for(uint64_t i = 1; i < n; i++)
    //{
    //    if(i % 10000 == 0)
    //    {
    //        cout << (double)i/n*100 << endl;
    //    }
    //    /*if(prime_test3(i))
    //    {
    //        pre_num_divisor = 0;
    //        continue;
    //    }*/
    //    if(set_prime_list.find(i) != set_prime_list.end())
    //    {
    //        pre_num_divisor = -1;
    //        continue;
    //    }
    //    /*int_divisor tmp(i, 0);
    //    it = calc_ed_list.find(tmp);
    //    if(it != calc_ed_list.end())
    //    {
    //        num_divisor = *it.
    //    }*/
    //    num_divisor = find_num_divisor(i);
    //    if(pre_num_divisor == num_divisor)
    //    {
    //        count++;
    //    }
    //    pre_num_divisor = num_divisor;
    //}
    //return count;
}
