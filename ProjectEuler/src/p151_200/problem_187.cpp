//Semiprimes
//Problem 187
//A composite is a number containing at least two prime factors. For example, 15 = 3 � 5; 9 = 3 � 3; 12 = 2 � 2 � 3.
//
//There are ten composites below thirty containing precisely two, not necessarily distinct, prime factors: 4, 6, 9, 10, 14, 15, 21, 22, 25, 26.
//
//How many composite integers, n < 108, have precisely two, not necessarily distinct, prime factors?

#include "template_class.h"

extern vector<int> prime_list;
extern set<int> set_prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

int problem_187()
{
    int n = 1e8;
    create_prime_filter_method(n, true);
    int sum = 0;
    cout << "start!" << endl;
    int limit = sqrt(n);
    for(int i = 0; i < prime_list.size(); i++)
    {
        if(prime_list[i] > limit)
        {
            break;
        }
        int div = n / prime_list[i];
        if((div & 1) == 0)
        {
            div--;
        }
        int idx = 0;
        for(int j = div;; j -= 2)
        {
            set<int>::iterator ite = set_prime_list.find(j);
            if(ite != set_prime_list.end())
            {
                idx = distance(set_prime_list.begin(), ite);
                break;
            }
        }
        if(idx < i)
        {
            break;
        }
        sum += idx - i + 1;
    }
    return sum;
}
