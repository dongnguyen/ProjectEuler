//Best Approximations
//Problem 192
//Let x be a real number.
//A best approximation to x for the denominator bound d is a rational number r / s in reduced form, with s ? d, such that any rational number which is closer to x than r / s has a denominator larger than d :
//
//| p / q - x | < | r / s - x | ? q > d
//For example, the best approximation to ?13 for the denominator bound 20 is 18 / 5 and the best approximation to ?13 for the denominator bound 30 is 101 / 28.
//
//Find the sum of all denominators of the best approximations to ?n for the denominator bound 1012, where n is not a perfect square and 1 < n ? 100000.


#include "template_class.h"
//#include "mpirxx.h"
#include "int128.h"

extern vector<int> continued_fractions(int n);
extern bool is_square(uint64_t n);
#define BOUND 1000000000000ULL
//#define BOUND 30

//uint64_t BOUND("1000000000000");
//uint64_t BOUND("30");

bool verify_192(uint64_t h1, uint64_t k1, uint64_t h2, uint64_t k2, int n)
{
    //uint64_t diff1 = abs(h1*h1 - k1*k1*n);
    //cout << "diff1: " << diff1*k2*k2 << endl;
    //uint64_t diff2 = abs(h2*h2 - k2*k2*n);
    //cout << "diff2: " << diff2*k1*k1 << endl;
    //return diff1*k2*k2 < diff2*k1*k1;

    int128_t h1_2((unsigned long long int)h1);
    int128_t h2_2((unsigned long long int)h2);
    int128_t k1_2((unsigned long long int)k1);
    int128_t k2_2((unsigned long long int)k2);
    h1_2 *= h1_2;
    h2_2 *= h2_2;
    k1_2 *= k1_2;
    k2_2 *= k2_2;
    int128_t diff1 = h1_2 - k1_2*n;
    int128_t diff2 = h2_2 - k2_2*n;
    if (diff1 < 0)
    {
        diff1 = -diff1;
    }
    if (diff2 < 0)
    {
        diff2 = -diff2;
    }
    diff1 *= k2_2;
    diff2 *= k1_2;
    return diff1 < diff2;
}

uint64_t best_app(int n)
{
    if (is_square(n))
    {
        return 0;
    }
    vector<int> cont_frac = continued_fractions(n);
    int period = cont_frac.size() - 1;
    uint64_t h_2;
    uint64_t h_1;
    uint64_t h;
    uint64_t k_2;
    uint64_t k_1;
    uint64_t k;
    h_2 = 1;
    h_1 = cont_frac[0];
    k_2 = 0;
    k_1 = 1;
    for (int i = 0; ; i++)
    {
        uint64_t a = cont_frac[(i % period) + 1];
        h = a*h_1 + h_2;
        k = a*k_1 + k_2;
        if (k > BOUND)
        {
            uint64_t a1 = (BOUND - k_2) / k_1;
            //double cmp = sqrt((double)n);
            uint64_t k_t = k_2 + a1 * k_1;
            uint64_t h_t = h_2 + a1 * h_1;
            //if (abs(cmp - double(h_1) / k_1) < abs(cmp - double(h_t) / k_t))
            if (verify_192(h_1, k_1, h_t, k_t, n))
            {
                return k_1;
            }
            else
            {
                return k_t;
            }

            //return k_1;
            //return BOUND - k_1;
            //return k_2 + ((BOUND - k_2) / k_1) * k_1;
            /*uint64_t a = (BOUND - k_2) / k_1;
            double cmp = sqrt(n);
            cout << h_1 << " / " << k_1 << ": " << abs(cmp - double(h_1) / k_1) << endl;
            for (int a_ = 1; a_ <= a; a_++)
            {
                int k_t = k_2 + a_ * k_1;
                int h_t = h_2 + a_ * h_1;
                cout << h_t << " / " << k_t << ": " << abs(cmp - double(h_t) / k_t) << endl;
            }
            return 0;*/
        }
        h_2 = h_1;
        h_1 = h;
        k_2 = k_1;
        k_1 = k;
    }
    return 0;
}

uint64_t problem_192()
{
    uint64_t sum = 0;
    /*uint64_t tmp = best_app(13);
    cout << 13 << ": " << tmp << endl;
    return sum;*/
    for (int i = 1; i <= 100000; i++)
        //for (int i = 1; i <= 100; i++)
    {
        uint64_t tmp = best_app(i);
        sum += tmp;
        //cout << i << ": " << tmp << endl;
    }
    return sum;
}
