// Counting the number of "hollow" square laminae that can form one, two, three, ... distinct arrangements.
// Problem 174
// We shall define a square lamina to be a square outline with a square "hole" so that the shape possesses vertical and horizontal symmetry.

// Given eight tiles it is possible to form a lamina in only one way: 3x3 square with a 1x1 hole in the middle. However, using thirty-two tiles it is possible to form two distinct laminae.


// If t represents the number of tiles used, we shall say that t = 8 is type L(1) and t = 32 is type L(2).

// Let N(n) be the number of t = 1000000 such that t is type L(n); for example, N(15) = 832.

// What is ? N(n) for 1 = n = 10?
#include "template_class.h"

int problem_174(int limit)
{
    vector<int> memo;
    memo.resize(limit + 1);
    int max_n = (limit + 1) / 2;
    int center_n = sqrt(limit);
    for(int64_t n = max_n; n > center_n; n--)
    {
        int k = 0;
        double sqrt_delta = sqrt(n*n - limit);
        k = n - sqrt_delta;
        for(int i = 2; i <= k; i += 2)
        {
            memo[n*n - (n-i)*(n-i)]++;
        }
    }
    for(int64_t n = center_n; n > 2; n--)
    {
        for(int i = 2; (n - i) >= 1; i += 2)
        {
            memo[n*n - (n-i)*(n-i)]++;
        }
    }
    int sum = 0;
    for(int i = 0; i <= limit; i++)
    {
        if(memo[i] >= 1 && memo[i] <= 10)
        {
            sum++;
        }
    }
    return sum;
}