//Investigating the behaviour of a recursively defined sequence
//Problem 197
//Given is the function f(x) = ?230.403243784 - x2? � 10 - 9 (? ? is the floor - function),
//the sequence un is defined by u0 = -1 and un + 1 = f(un).
//
//Find un + un + 1 for n = 1012.
//Give your answer with 9 digits after the decimal point.

#include "template_class.h"

double problem_197()
{
    double x = -1;
    int u1 = 0;
    int u2 = 0;
    for (int i = 0; ; i++)
    {
        x = floor(pow(2, 30.403243784 - x*x));
        int t1 = x;
        x /= 1e9;
        x = floor(pow(2, 30.403243784 - x*x));
        int t2 = x;
        x /= 1e9;
        if (t1 == u1 && t2 == u2)
        {
            break;
        }
        else
        {
            u1 = t1;
            u2 = t2;
        }
    }
    double ret = double(u1 + u2) / 1e9;
    return ret;
}