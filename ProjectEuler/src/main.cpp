#include "template_class.h"
#include "problem_header.h"

#ifndef WIN32
#include <ctime>
#endif

#include <cstdio>

using namespace std;

int main()
{
#ifdef WIN32
	CProfiler profiler;
	profiler.ProfileStart(LOGMILISECS);
#else
	time_t s = time(0);
#endif

	//int p1 = problem_1(1000);
	//int64_t p2 = problem_2(4e6);
	//int64_t p3 = problem_3(600851475143);
	//int p4 = problem_4();
	//int p5 = problem_5(20);
	//int p6 = problem_6(100);
	//int p7 = problem_7(10001);
	//int p8 = problem_8();
	//int p9 = problem_9(1000);
	//int64_t p10 = problem_10(2e6);
	//int p11 = problem_11();

	/*int p12 = problem_12(500);
	cout << p12 << endl;*/

	/*char out[10];
	int p13 = problem_13(out);*/
	//uint64_t p14 = problem_14((uint32_t)1e6);
	//cout << p14 << endl;
	//int p15 = 40!/20!/20! = 137846528820;
	//int p16 = problem_16(1000);
	//int p17 = problem_17(1000);
	//int p18 = problem_18("problem_18.txt");
	//int p19 = problem_19();
	//int p20 = problem_20(100
	//int p21 = problem_21(10000);
	//int p22 = problem_22("problem_22_names.txt");
	//int p23 = problem_23();
	//int p24 = problem_24();
	//int p25 = problem_25(1000);
	//int p26 = problem_26();
	//couple_int p27 = problem_27();
	//int64_t p28 = problem_28(1001);
	//int p29 = problem_29(100);
	//int p30 = problem_30();
	//int p31 = problem_31(500);
	//int p32 = problem_32();
	//int p33 = problem_33();
	//int p34 = problem_34();
	//int p35 = problem_35();
	//int p36 = problem_36(1e6);
	//int p37 = problem_37();
	//int p38 = problem_38();
	//int p39 = problem_39(1000);
	//int p40 = problem_40();
	//int p41 = problem_41();
	//int p42 = problem_42("problem_42_words.txt");
	/*uint64_t p43 = problem_43();
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%x\n", (uint32_t)(p43 & uint64_t(0x00000000ffffffff)));
	fprintf(fp, "%x\n", (uint32_t)((p43 >> 32) & uint64_t(0x00000000ffffffff)));
	fclose(fp);*/
	//uint64_t p44 = problem_44();
	/*FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%x\n", (uint32_t)(p44 & uint64_t(0x00000000ffffffff)));
	fprintf(fp, "%x\n", (uint32_t)((p44 >> 32) & uint64_t(0x00000000ffffffff)));
	fclose(fp);*/
	//uint64_t p45 = problem_45();
	//int p46 = problem_46();
	//int p47 = problem_47();
	//uint64_t p48 = problem_48();
	//uint64_t p49 = problem_49();
	/*int p50 = problem_50(1e6);
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%d", p50);
	fclose(fp);*/
	/*int p51 = problem_51();
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%d", p51);
	fclose(fp);*/
	/*int p52 = problem_52();
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%d", p52);
	fclose(fp);*/
	//int p53 = problem_53();
	//int p54 = problem_54("problem_54_poker_example.txt");
	/*int p54 = problem_54("problem_54_poker.txt");
	cout << p54 << endl;*/
	//int p55 = problem_55();
	//int p56 = problem_56();
	/*int p57 = problem_57();
	cout << p57 << endl;*/
	/*int p58 = problem_58();
	cout << p58 << endl;*/

	//int p59 = problem_59("problem_59_cipher1.txt");

	/*vector<int> p62 = problem_62();
	for(int i = p62.size() - 1; i >= 0; i--)
	{
		cout << p62[i];
	}
	cout << endl;*/
	//cout << p62 << endl;

	/*int p60 = problem_60();
	cout << p60 << endl;*/

	/*int p61 = problem_61();
	cout << p61 << endl;*/

	//int p63 = problem_63();

	/*int p64 = problem_64();
	cout << p64 << endl;*/

	//int p65 = problem_65(100);
	//cout << p65 << endl;

	/*int p66 = problem_66();
	cout << p66 << endl;*/

	//int p67 = problem_18("problem_67.txt");

	/*vector<int> p68 = problem_68();
	for(int i = 0; i < p68.size(); i++)
	{
		cout << p68[i];
	}
	cout << endl;*/

	/*int p69 = problem_69(1e6);
	cout << p69 << endl;*/

	/*int p70 = problem_70(1e7);
	cout << p70 << endl;*/

	//int p71 = problem_71();
	//cout << p71 << endl;

	/*uint64_t p72 = problem_72(1e6);
	cout << p72 << endl;*/

	//int p73 = problem_73();
	//cout << p73 << endl;

	/*int p74 = problem_74();
	cout << p74 << endl;*/

	/*int p75 = problem_75();
	cout << p75 << endl;*/

	/*int p76 = problem_76(100);
	cout << p76 << endl;*/

	/*int p77 = problem_77();
	cout << p77 << endl;*/

	/*uint64_t p78 = problem_78();
	cout << p78 << endl;*/

	//int p79 = problem_79("problem_79_keylog.txt");

	/*int p80 = problem_80();
	cout << p80 << endl;*/

	//int p81 = problem_81("problem_81_matrix.txt");

	/*uint32_t p82 = problem_82("problem_82_matrix.txt");
	cout << p82 << endl;*/

	/*int p83 = problem_83("problem_83_matrix.txt");
	cout << p83 << endl;*/

	/*int p84 = problem_84();
	cout << p84 << endl;*/

	/*int p85 = problem_85();
	cout << p85 << endl;*/

	/*int p86 = problem_86();
	cout << p86 << endl;*/

	/*int p87 = problem_87();
	cout << p87 << endl;*/

	/*uint64_t p88 = problem_88(12e3);
	cout << p88 << endl;*/

	/*int p89 = problem_89("problem_89_roman.txt");
	cout << p89 << endl;*/

	/*int p90 = problem_90();
	cout << p90 << endl;*/

	/*int p91 = problem_91(50);
	cout << p91 << endl;*/

	/*int p92 = problem_92();
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%d", p92);
	fclose(fp);*/

	/*int p93 = problem_93();
	cout << p93 << endl;*/

	//BigInteger p94 = problem_94(1e9);
	//uint64_t p94 = problem_94(1e9);
	//cout << p94 << endl;

	/*int p95 = problem_95();
	cout << p95 << endl;*/

	/*int p96 = problem_96("problem_96_sudoku.txt");
	cout << p96 << endl;*/

	//uint64_t p97 = problem_97();

	/*int p98 = problem_98();
	cout << p98 << endl;*/

	/*int p99 = problem_99("problem_99_base_exp.txt");
	cout << p99 << endl;*/

	/*BigInteger p100 = problem_100();
	cout << p100 << endl;*/

	//BigInteger p101 = problem_101();
	//InfInt p101 = problem_101();
	/*mpz_class p101 = problem_101();
	cout << p101 << endl;*/

	/*int p102 = problem_102("problem_102_triangles.txt");
	cout << p102 << endl;*/

	/*std::string p103 = problem_103();
	cout << p103 << endl;*/

	/*int p104 = problem_104();
	cout << p104 << endl;*/

	/*std::string p105 = problem_105();
	cout << p105 << endl;*/

	/*std::string p106 = problem_106();
	cout << p106 << endl;*/

	/*int p107 = problem_107("problem_107_network.txt");
	cout << p107 << endl;*/

	/*uint64_t p108 = problem_108(1000);
	cout << p108 << endl;*/

	/*uint64_t p110 = problem_108(4000000);
	cout << p110 << endl;*/

	/*int p112 = problem_112();
	cout << p112 << endl;*/

	/*uint64_t p113 = problem_113();
	cout << p113 << endl;*/

	/*uint64_t p114 = problem_114(50);
	cout << p114 << endl;*/

	/*int p115 = problem_115(50, 1e6);
	cout << p115 << endl;*/

	/*uint64_t p116 = problem_116(50);
	cout << p116 << endl;*/

	/*uint64_t p117 = problem_117(50);
	cout << p117 << endl;*/

	/*uint64_t p119 = problem_119();
	cout << p119 << endl;*/

	/*int p120 = problem_120();
	cout << p120 << endl;*/

	/*int p121 = problem_121(15);
	cout << p121 << endl;*/

	/*int p122 = problem_122(200);
	cout << p122 << endl;*/

	/*int p123 = problem_123();
	cout << p123 << endl;*/

	/*int p124 = problem_124();
	cout << p124 << endl;*/

	/*uint64_t p125 = problem_125();
	cout << p125 << endl;*/

	/*uint64_t p127 = problem_127(120000);
	cout << p127 << endl;*/

	/*int p129 = problem_129(1e6);
	cout << p129 << endl;*/

	/*uint64_t p130 = problem_130();
	cout << p130 << endl;*/

	/*int p132 = problem_132();
	cout << p132 << endl;*/

	/*mpz_class p133 = problem_133();
	cout << p133 << endl;*/

	/*int p135 = problem_135(10);
	cout << p135 << endl;*/

	/*int p136 = problem_136();
	cout << p136 << endl;*/

	/*uint64_t p138 = problem_138();
	cout << p138 << endl;*/

	/*int p139 = problem_139(100e6);
	cout << p139 << endl;*/

	/*uint64_t p142 = problem_142();
	cout << p142 << endl;*/

	/*int p145 = problem_145(9);
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%d", p145);
	fclose(fp);*/

	/*uint64_t p164 = problem_164(20);
	cout << p164 << endl;*/

	/*uint64_t p172 = problem_172();
	cout << p172 << endl;*/

	/*uint64_t p173 = problem_173(1e6);
	cout << p173 << endl;*/

	/*int p174 = problem_174(1e6);
	cout << p174 << endl;*/

	/*uint64_t p178 = problem_178();
	cout << p178 << endl;*/

	/*int p179 = problem_179(1e7);
	FILE* fp = fopen("result.txt", "w");
	fprintf(fp, "%d", p179);
	fclose(fp);*/

	/*string p185 = problem_185();
	cout << p185 << endl;*/

	/*int p187 = problem_187();
	cout << p187 << endl;*/

	/*int p188 = problem_188();
	cout << p188 << endl;*/

	//uint64_t p191 = problem_191(30);

	/*uint64_t p190 = problem_190();
	cout << p190 << endl;*/

	/*uint64_t p192 = problem_192();
	cout << p192 << endl;*/

	/*double p197 = problem_197();
	printf("%1.9lf\n", p197);*/

	//uint64_t p206 = problem_206();

	/*int p204 = problem_204(100, 1e9);
	cout << p204 << endl;*/

	/*double p205 = problem_205();
	printf("%0.7lf\n", p205);*/

	//mpz_class p211 = problem_211(64e6);
	/*mpz_class p211 = problem_211(1e6);
	cout << p211 << endl;*/

	/*uint64_t p214 = problem_214(40000000, 25);
	cout << p214 << endl;*/

	/*int p216 = problem_216();
	cout << p216 << endl;*/

	/*uint64_t p231 = problem_231(15e6, 20e6);
	cout << p231 << endl;*/

	/*BigInteger p243 = problem_243(factor<BigInteger>(15499, 94744));
	cout << p243 << endl;*/

	/*uint64_t p258 = problem_258();
	cout << p258 << endl;*/

	/*int p301 = problem_301();
	cout << p301 << endl;*/

	/*mpz_class p303 = problem_303();
	cout << p303 << endl;*/

	/*uint64_t p304 = problem_304();
	cout << p304 << endl;*/

	/*uint64_t p315 = problem_315();
	cout << p315 << endl;*/

	/*double p323 = problem_323();
	cout << p323 << endl;*/

	/*std::string p336 = problem_336();
	cout << p336 << endl;*/

	/*int p345 = problem_345();
	cout << p345 << endl;*/

	/*uint64_t p346 = problem_346();
	cout << p346 << endl;*/

	/*uint64_t p347 = problem_347();
	cout << p347 << endl;*/

	/*BigInteger p357 = problem_357(1e8);
	cout << p357 << endl;*/

	/*uint64_t p381 = problem_381();
	cout << p381 << endl;*/

	/*uint64_t p387 = problem_387();
	cout << p387 << endl;*/

	/*uint64_t p407 = problem_407();
	cout << p407 << endl;*/

	/*uint64_t p429 = problem_429();
	cout << p429 << endl;*/

	/*double p441 = problem_441();
	cout << p441 << endl;*/

	/*std::string p483 = problem_483();
	cout << p483 << endl;*/

	uint64_t p491 = problem_491();
	cout << p491 << endl;

	/*std::string p493 = problem_493();
	cout << p493 << endl;*/

	/*uint64_t p500 = problem_500();
	cout << p500 << endl;*/

	/*uint64_t p504 = problem_504();
	cout << p504 << endl;*/

	/*uint64_t p516 = problem_516();
	cout << p516 << endl;*/

	/*uint64_t p518 = problem_518();
	cout << p518 << endl;*/

	/*uint64_t p549 = problem_549();
	cout << p549 << endl;*/

#ifdef WIN32
	profiler.ProfileEnd();
#else
	time_t e = time(0);
	cout << "ProfileEnd. " << e - s << " seconds." << endl;
#endif
	std::getchar();
	return 0;
}
