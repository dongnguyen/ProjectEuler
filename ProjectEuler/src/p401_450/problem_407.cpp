// Idempotents
// Problem 407
// If we calculate a2 mod 6 for 0 ≤ a ≤ 5 we get: 0,1,4,3,4,1.

// The largest value of a such that a2 ≡ a mod 6 is 4.
// Let's call M(n) the largest value of a < n such that a2 ≡ a (mod n).
// So M(6) = 4.

// Find ∑M(n) for 1 ≤ n ≤ 107.

#include "template_class.h"
#include <thread>
#include <mutex>

//~24 min, TODO: improve performance
#define N ((uint64_t)1e7)
#define THREAD_NUM 12

extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);
static vector<uint64_t> prime_list;
static set<uint64_t> prime_set;
static vector<int> exp_list;

// TODO: improve performance
template <typename T>
set<T> divisors_of_n_local(T n, vector<T>& prime_list, set<T>& prime_set)
{
	vector<T> num_each_prime;
	vector<T> divisor_prime;
	set<T> ret;
	if (n <= (T)1)
	{
		ret.insert((T)1);
		return ret;
	}
	T tmp_n = n;
	int pSize = prime_list.size();
	if (prime_set.size())
	{
		if (prime_set.find(n) != prime_set.end())
		{
			ret.insert((T)1);
			ret.insert(n);
			return ret;
		}
	}
	ret.insert((T)1);
	ret.insert(n);
	for (int i = 0; i < pSize; i++)
	{
		if (tmp_n == 1)
		{
			break;
		}
		T tmp = 0;
		while (tmp_n % prime_list[i] == 0)
		{
			tmp_n /= prime_list[i];
			tmp++;
		}
		if (tmp)
		{
			//count_type++;
			num_each_prime.push_back(tmp);
			divisor_prime.push_back(prime_list[i]);
		}
	}
	vector<T> iterator;
	iterator.resize(num_each_prime.size());
	iterator.back() = 1;
	T limit = (T)(sqrt(n) + 0.5);
	while (1)
	{
		T p = 1;
		for (T i = 0; i < num_each_prime.size(); i++)
		{
			for (T j = 0; j < iterator[i]; j++)
			{
				p *= divisor_prime[i];
			}
		}
		if (p < n)
		{
			ret.insert(p);
			//ret.insert(n/p);
		}

		iterator.back()++;

		for (T i = iterator.size() - 1; i > 0; i--)
		{
			T once = false;
			if ((iterator[i] > num_each_prime[i]))
			{
				iterator[i] = 0;
				iterator[i - 1]++;
			}
		}
		// break condition
		if (iterator[0] == num_each_prime[0] + 1)
		{
			break;
		}
	}
	//sort(ret);
	return ret;
}

static vector<uint64_t> maxM;
static std::mutex mylock;

void thread_407(int id)
{
	uint64_t segment = N / THREAD_NUM;
	uint64_t start = segment * id;
	uint64_t end = start + segment;
	if (id == 0)
	{
		start = 2;
	}
	if (id == (THREAD_NUM - 1))
	{
		end = N + 1;
	}

	set<uint64_t> divisors_1;
	set<uint64_t> divisors;
	if (id == 0)
	{
		divisors_1.insert(1);
	}
	else
	{
		divisors_1 = divisors_of_n_local(start - 1, prime_list, prime_set);
	}
	for (uint64_t i = start; i < end; i++)
	{
		divisors = divisors_of_n_local(i, prime_list, prime_set);
		// a(a-1) = c * m
		// a == i
		for (auto d1 : divisors)
		{
			for (auto d2 : divisors_1)
			{
				// new m
				uint64_t newdivisor = d1 * d2;
				if (newdivisor > N)
				{
					break;
				}
				// m > a
				if (newdivisor > i)
				{
					mylock.lock();
					// if a > old max a for new m
					if (i > maxM[newdivisor])
					{
						maxM[newdivisor] = i;
					}
					mylock.unlock();
				}
			}
		}
		divisors_1 = divisors;
	}
}

uint64_t problem_407()
{
	uint64_t ret = 0;

	maxM.resize(N + 1);
	for (int i = 2; i <= N; i++)
	{
		maxM[i] = 1;
	}

	create_prime_filter_method(&prime_list, 0, N);
	prime_set.insert(prime_list.begin(), prime_list.end());

	std::thread threads[THREAD_NUM];
	for (int i = 0; i < THREAD_NUM; i++)
	{
		threads[i] = std::thread(thread_407, i);
	}
	for (int i = 0; i < THREAD_NUM; i++)
	{
		threads[i].join();
	}

	for (int i = 1; i <= N; i++)
	{
		ret += maxM[i];
	}

	return ret;
}
