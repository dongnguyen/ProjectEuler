// Sum of squares of unitary divisors
// Problem 429
// A unitary divisor d of a number n is a divisor of n that has the property gcd(d, n/d) = 1.
// The unitary divisors of 4! = 24 are 1, 3, 8 and 24.
// The sum of their squares is 12 + 32 + 82 + 242 = 650.

// Let S(n) represent the sum of the squares of the unitary divisors of n. Thus S(4!)=650.

// Find S(100 000 000!) modulo 1 000 000 009.

#include "template_class.h"

#define N ((uint64_t)100000000)
#define MOD ((uint64_t)1000000009)

extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);
static vector<uint64_t> prime_list;
static vector<int> exp_list;

uint64_t problem_429()
{
	create_prime_filter_method(&prime_list, 0, N);
	int size = prime_list.size();
	exp_list.resize(size);
	for (int i = 0; i < size; i++)
	{
		uint64_t p = prime_list[i];
		exp_list[i] = 0;
		while (1)
		{
			int num = N / p;
			if (num == 0)
			{
				break;
			}
			exp_list[i] += num;
			p *= prime_list[i];
		}
	}

	uint64_t ret = 1;

	for (int i = 0; i < size; i++)
	{
		uint64_t p = prime_list[i];
		uint64_t e = exp_list[i] * 2;
		uint64_t sigma = 1;
		for (int j = 0; j < e; j++)
		{
			sigma *= p;
			if (sigma > MOD)
			{
				sigma = sigma % MOD;
			}
		}
		sigma++;
		ret *= sigma;
		if (ret > MOD)
		{
			ret = ret % MOD;
		}
	}

	return ret;
}
