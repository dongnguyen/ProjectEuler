
#include "template_class.h"
#include <thread>         // std::thread

extern vector<int> prime_list;
extern set<int> set_prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

void thread1(double* sum)
{
    int n = 10000000;
    //int n = 10000;
    int limit = sqrt(n);
    queue<couple_int> memo;
    vector<couple_int> store_seeds;
    couple_int seed(2, 1);
    int min_M = max(seed.m_a, seed.m_b);
    int max_M = min(seed.m_a + seed.m_b, n);
    double pq = double(seed.m_a) * seed.m_b;
    *sum += (max_M - min_M + 1) / pq;
    memo.push(seed);
    while(!memo.empty())
    {
        couple_int s = memo.front();
        memo.pop();
        couple_int p1(2*s.m_a-s.m_b, s.m_a);
        couple_int p2(2*s.m_a+s.m_b, s.m_a);
        couple_int p3(s.m_a+2*s.m_b, s.m_b);
        if(p1.m_a <= limit && p1.m_b <= limit)
        {
            memo.push(p1);
            int min_M = max(p1.m_a, p1.m_b);
            int max_M = min(p1.m_a + p1.m_b, n);
            double pq = double(p1.m_a) * p1.m_b;
            *sum += (max_M - min_M + 1) / pq;
        }
        else
        {
            store_seeds.push_back(p1);
        }
        if(p2.m_a <= limit && p2.m_b <= limit)
        {
            memo.push(p2);
            int min_M = max(p2.m_a, p2.m_b);
            int max_M = min(p2.m_a + p2.m_b, n);
            double pq = double(p2.m_a) * p2.m_b;
            *sum += (max_M - min_M + 1) / pq;
        }
        else
        {
            store_seeds.push_back(p2);
        }
        if(p3.m_a <= limit && p3.m_b <= limit)
        {
            memo.push(p3);
            int min_M = max(p3.m_a, p3.m_b);
            int max_M = min(p3.m_a + p3.m_b, n);
            double pq = double(p3.m_a) * p3.m_b;
            *sum += (max_M - min_M + 1) / pq;
        }
        else
        {
            store_seeds.push_back(p3);
        }
    }

    cout << "(2, 1): " << store_seeds.size() << endl;
    for(int i = 0; i < store_seeds.size(); i++)
    {
        //if(i % 100 == 0)
        {
            // cout << i << endl;
        }
        seed = store_seeds[i];
        memo.push(seed);
        int min_M = max(seed.m_a, seed.m_b);
        int max_M = min(seed.m_a + seed.m_b, n);
        double pq = double(seed.m_a) * seed.m_b;
        *sum += (max_M - min_M + 1) / pq;

        while(!memo.empty())
        {
            couple_int s = memo.front();
            memo.pop();
            couple_int p1(2*s.m_a-s.m_b, s.m_a);
            couple_int p2(2*s.m_a+s.m_b, s.m_a);
            couple_int p3(s.m_a+2*s.m_b, s.m_b);
            if(p1.m_a <= n && p1.m_b <= n)
            {
                memo.push(p1);
                int min_M = max(p1.m_a, p1.m_b);
                int max_M = min(p1.m_a + p1.m_b, n);
                double pq = double(p1.m_a) * p1.m_b;
                *sum += (max_M - min_M + 1) / pq;
            }
            if(p2.m_a <= n && p2.m_b <= n)
            {
                memo.push(p2);
                int min_M = max(p2.m_a, p2.m_b);
                int max_M = min(p2.m_a + p2.m_b, n);
                double pq = double(p2.m_a) * p2.m_b;
                *sum += (max_M - min_M + 1) / pq;
            }
            if(p3.m_a <= n && p3.m_b <= n)
            {
                memo.push(p3);
                int min_M = max(p3.m_a, p3.m_b);
                int max_M = min(p3.m_a + p3.m_b, n);
                double pq = double(p3.m_a) * p3.m_b;
                *sum += (max_M - min_M + 1) / pq;
            }
        }
    }
}

void thread2(double* sum)
{
    int n = 10000000;
    //int n = 10000;
    int limit = sqrt(n);
    queue<couple_int> memo;
    vector<couple_int> store_seeds;
    couple_int seed(3, 1);
    int min_M = max(seed.m_a, seed.m_b);
    int max_M = min(seed.m_a + seed.m_b, n);
    double pq = double(seed.m_a) * seed.m_b;
    *sum += (max_M - min_M + 1) / pq;
    memo.push(seed);
    while(!memo.empty())
    {
        couple_int s = memo.front();
        memo.pop();
        couple_int p1(2*s.m_a-s.m_b, s.m_a);
        couple_int p2(2*s.m_a+s.m_b, s.m_a);
        couple_int p3(s.m_a+2*s.m_b, s.m_b);
        if(p1.m_a <= limit && p1.m_b <= limit)
        {
            memo.push(p1);
            int min_M = max(p1.m_a, p1.m_b);
            int max_M = min(p1.m_a + p1.m_b, n);
            double pq = double(p1.m_a) * p1.m_b;
            *sum += (max_M - min_M + 1) / pq;
        }
        else
        {
            store_seeds.push_back(p1);
        }
        if(p2.m_a <= limit && p2.m_b <= limit)
        {
            memo.push(p2);
            int min_M = max(p2.m_a, p2.m_b);
            int max_M = min(p2.m_a + p2.m_b, n);
            double pq = double(p2.m_a) * p2.m_b;
            *sum += (max_M - min_M + 1) / pq;
        }
        else
        {
            store_seeds.push_back(p2);
        }
        if(p3.m_a <= limit && p3.m_b <= limit)
        {
            memo.push(p3);
            int min_M = max(p3.m_a, p3.m_b);
            int max_M = min(p3.m_a + p3.m_b, n);
            double pq = double(p3.m_a) * p3.m_b;
            *sum += (max_M - min_M + 1) / pq;
        }
        else
        {
            store_seeds.push_back(p3);
        }
    }
    cout << "(3, 1): " << store_seeds.size() << endl;
    for(int i = 0; i < store_seeds.size(); i++)
    {
        //if(i % 100 == 0)
        {
            //cout << double(i)/store_seeds.size()*100 << endl;
        }
        seed = store_seeds[i];
        memo.push(seed);
        int min_M = max(seed.m_a, seed.m_b);
        int max_M = min(seed.m_a + seed.m_b, n);
        double pq = double(seed.m_a) * seed.m_b;
        *sum += (max_M - min_M + 1) / pq;

        while(!memo.empty())
        {
            couple_int s = memo.front();
            memo.pop();
            couple_int p1(2*s.m_a-s.m_b, s.m_a);
            couple_int p2(2*s.m_a+s.m_b, s.m_a);
            couple_int p3(s.m_a+2*s.m_b, s.m_b);
            if(p1.m_a <= n && p1.m_b <= n)
            {
                memo.push(p1);
                int min_M = max(p1.m_a, p1.m_b);
                int max_M = min(p1.m_a + p1.m_b, n);
                double pq = double(p1.m_a) * p1.m_b;
                *sum += (max_M - min_M + 1) / pq;
            }
            if(p2.m_a <= n && p2.m_b <= n)
            {
                memo.push(p2);
                int min_M = max(p2.m_a, p2.m_b);
                int max_M = min(p2.m_a + p2.m_b, n);
                double pq = double(p2.m_a) * p2.m_b;
                *sum += (max_M - min_M + 1) / pq;
            }
            if(p3.m_a <= n && p3.m_b <= n)
            {
                memo.push(p3);
                int min_M = max(p3.m_a, p3.m_b);
                int max_M = min(p3.m_a + p3.m_b, n);
                double pq = double(p3.m_a) * p3.m_b;
                *sum += (max_M - min_M + 1) / pq;
            }
        }
    }
}

double g_sum = 0;
double g_sum1 = 0;
double g_sum1_1 = 0;
double g_sum1_2 = 0;
double g_sum2 = 0;
#define LIMIT_441 10000000
void p441_re_1(int a, int b)
{
    if (a > LIMIT_441 || b > LIMIT_441)
    {
        return;
    }
    int min_M = max(a, b);
    int max_M = min(a + b, LIMIT_441);
    double pq = double(a) * b;
    g_sum1_1 += (max_M - min_M + 1) / pq;

    p441_re_1(2 * a - b, a);
    p441_re_1(2 * a + b, a);
    p441_re_1(a + 2 * b, b);
}
void p441_re_2(int a, int b)
{
    if (a > LIMIT_441 || b > LIMIT_441)
    {
        return;
    }
    int min_M = a;
    int max_M = min(a + b, LIMIT_441);
    double pq = double(a) * b;
    g_sum2 += (max_M - min_M + 1) / pq;

    p441_re_2(2 * a - b, a);
    p441_re_2(2 * a + b, a);
    p441_re_2(a + 2 * b, b);
}

//int memo_2_1[4052801][2];
//int memo_3_1[2025651][2];
int *memo_2_1_a;
int *memo_2_1_b;
int *memo_3_1_a;
int *memo_3_1_b;

int size_1_1 = 0; // 2026400
int size_1_2 = 0; // 4052801
int size_2 = 0; // 2025651

bool is_prime_441(int a, int b)
{
    if (b == 1)
    {
        return true;
    }
    if (set_prime_list.find(a) != set_prime_list.end())
    {
        return true;
    }
    if (set_prime_list.find(b) != set_prime_list.end())
    {
        return true;
    }
    return false;
}

void new_th_1_1()
{
    //int memo[20000000][2];
    int *memo_a = new int[20000000];
    int *memo_b = new int[20000000];
    for (int i = 0; i < size_1_1; i++)
    {
        memo_a[0] = memo_2_1_a[i];
        memo_b[0] = memo_2_1_b[i];
        int front = 0;
        int back = 1;
        do
        {
            int min_M = memo_a[front];
            int max_M = min(memo_a[front] + memo_b[front], LIMIT_441);
            double pq = double(memo_a[front]) * memo_b[front];
            g_sum1_1 += (max_M - min_M + 1) / pq;

            int p1[2];
            p1[0] = 2 * memo_a[front] - memo_b[front];
            p1[1] = memo_a[front];

            int p2[2];
            p2[0] = 2 * memo_a[front] + memo_b[front];
            p2[1] = memo_a[front];

            int p3[2];
            p3[0] = memo_a[front] + 2 * memo_b[front];
            p3[1] = memo_b[front];

            if (p1[0] <= LIMIT_441 && !is_prime_441(p1[0], p1[1]))
            {
                memo_a[back] = p1[0];
                memo_b[back] = p1[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(0)" << endl;
                    exit(-1);
                };
            }
            if (p2[0] <= LIMIT_441 && !is_prime_441(p2[0], p2[1]))
            {
                memo_a[back] = p2[0];
                memo_b[back] = p2[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(1)" << endl;
                    exit(-1);
                };
            }
            if (p3[0] <= LIMIT_441 && !is_prime_441(p3[0], p3[1]))
            {
                memo_a[back] = p3[0];
                memo_b[back] = p3[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(2)" << endl;
                    exit(-1);
                };
            }
            front++;
        }
        while (front != back);
    }
    delete []memo_a;
    delete []memo_b;
}

void new_th_1_2()
{
    //int memo[20000000][2];
    int *memo_a = new int[20000000];
    int *memo_b = new int[20000000];
    for (int i = size_1_1; i < size_1_2; i++)
    {
        memo_a[0] = memo_2_1_a[i];
        memo_b[0] = memo_2_1_b[i];
        int front = 0;
        int back = 1;
        do
        {
            int min_M = memo_a[front];
            int max_M = min(memo_a[front] + memo_b[front], LIMIT_441);
            double pq = double(memo_a[front]) * memo_b[front];
            g_sum1_2 += (max_M - min_M + 1) / pq;

            int p1[2];
            p1[0] = 2 * memo_a[front] - memo_b[front];
            p1[1] = memo_a[front];

            int p2[2];
            p2[0] = 2 * memo_a[front] + memo_b[front];
            p2[1] = memo_a[front];

            int p3[2];
            p3[0] = memo_a[front] + 2 * memo_b[front];
            p3[1] = memo_b[front];

            if (p1[0] <= LIMIT_441 && !is_prime_441(p1[0], p1[1]))
            {
                memo_a[back] = p1[0];
                memo_b[back] = p1[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(3)" << endl;
                    exit(-1);
                };
            }
            if (p2[0] <= LIMIT_441 && !is_prime_441(p2[0], p2[1]))
            {
                memo_a[back] = p2[0];
                memo_b[back] = p2[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(4)" << endl;
                    exit(-1);
                };
            }
            if (p3[0] <= LIMIT_441 && !is_prime_441(p3[0], p3[1]))
            {
                memo_a[back] = p3[0];
                memo_b[back] = p3[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(5)" << endl;
                    exit(-1);
                };
            }
            front++;
        }
        while (front != back);
    }
    delete []memo_a;
    delete []memo_b;
}

void new_th_2()
{
    //int memo[20000000][2];
    int *memo_a = new int[20000000];
    int *memo_b = new int[20000000];

    memo_a[0] = 3;
    memo_b[0] = 1;

    int n = sqrt(LIMIT_441);
    int front = 0;
    int back = 1;
    int idx = 0;
    do
    {
        int min_M = memo_a[front];
        int max_M = min(memo_a[front] + memo_b[front], LIMIT_441);
        double pq = double(memo_a[front]) * memo_b[front];
        g_sum2 += (max_M - min_M + 1) / pq;

        int p1[2];
        p1[0] = 2 * memo_a[front] - memo_b[front];
        p1[1] = memo_a[front];

        int p2[2];
        p2[0] = 2 * memo_a[front] + memo_b[front];
        p2[1] = memo_a[front];

        int p3[2];
        p3[0] = memo_a[front] + 2 * memo_b[front];
        p3[1] = memo_b[front];

        if (p1[0] <= n && !is_prime_441(p1[0], p1[1]))
        {
            memo_a[back] = p1[0];
            memo_b[back] = p1[1];
            back++;
            if(back >= 10000000)
            {
                cout << "exit(6)" << endl;
                exit(-1);
            };
        }
        else
        {
            memo_3_1_a[idx] = p1[0];
            memo_3_1_b[idx] = p1[1];
            idx++;
        }
        if (p2[0] <= n && !is_prime_441(p2[0], p2[1]))
        {
            memo_a[back] = p2[0];
            memo_b[back] = p2[1];
            back++;
            if(back >= 10000000)
            {
                cout << "exit(7)" << endl;
                exit(-1);
            };
        }
        else
        {
            memo_3_1_a[idx] = p2[0];
            memo_3_1_b[idx] = p2[1];
            idx++;
        }
        if (p3[0] <= n && !is_prime_441(p3[0], p3[1]))
        {
            memo_a[back] = p3[0];
            memo_b[back] = p3[1];
            back++;
            if(back >= 10000000)
            {
                cout << "exit(8)" << endl;
                exit(-1);
            };
        }
        else
        {
            memo_3_1_a[idx] = p3[0];
            memo_3_1_b[idx] = p3[1];
            idx++;
        }
        front++;
    }
    while (front != back);

    size_2 = idx;
    cout << size_2 << endl;
    for (int i = 0; i < size_2; i++)
    {
        memo_a[0] = memo_3_1_a[i];
        memo_b[0] = memo_3_1_b[i];
        int front = 0;
        int back = 1;
        do
        {
            int min_M = memo_a[front];
            int max_M = min(memo_a[front] + memo_b[front], LIMIT_441);
            double pq = double(memo_a[front]) * memo_b[front];
            g_sum2 += (max_M - min_M + 1) / pq;

            int p1[2];
            p1[0] = 2 * memo_a[front] - memo_b[front];
            p1[1] = memo_a[front];

            int p2[2];
            p2[0] = 2 * memo_a[front] + memo_b[front];
            p2[1] = memo_a[front];

            int p3[2];
            p3[0] = memo_a[front] + 2 * memo_b[front];
            p3[1] = memo_b[front];

            if (p1[0] <= LIMIT_441 && !is_prime_441(p1[0], p1[1]))
            {
                memo_a[back] = p1[0];
                memo_b[back] = p1[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(9)" << endl;
                    exit(-1);
                };
            }
            if (p2[0] <= LIMIT_441 && !is_prime_441(p2[0], p2[1]))
            {
                memo_a[back] = p2[0];
                memo_b[back] = p2[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(10)" << endl;
                    exit(-1);
                };
            }
            if (p3[0] <= LIMIT_441 && !is_prime_441(p3[0], p3[1]))
            {
                memo_a[back] = p3[0];
                memo_b[back] = p3[1];
                back++;
                if(back >= 10000000)
                {
                    cout << "exit(11)" << endl;
                    exit(-1);
                };
            }
            front++;
        }
        while (front != back);
    }
    delete []memo_a;
    delete []memo_b;
}

double problem_441()
{
    create_prime_filter_method(LIMIT_441, true);
    memo_2_1_a = new int[4052801];
    memo_2_1_b = new int[4052801];
    memo_3_1_a = new int[2025651];
    memo_3_1_b = new int[2025651];

    double sum = 0;
    //int memo[20000000][2];
    int *memo_a = new int[20000000];
    int *memo_b = new int[20000000];

    memo_a[0] = 2;
    memo_b[0] = 1;

    int n = sqrt(LIMIT_441);
    int front = 0;
    int back = 1;
    int idx = 0;
    do
    {
        int min_M = memo_a[front];
        int max_M = min(memo_a[front] + memo_b[front], LIMIT_441);
        double pq = double(memo_a[front]) * memo_b[front];
        g_sum1 += (max_M - min_M + 1) / pq;

        int p1[2];
        p1[0] = 2 * memo_a[front] - memo_b[front];
        p1[1] = memo_a[front];

        int p2[2];
        p2[0] = 2 * memo_a[front] + memo_b[front];
        p2[1] = memo_a[front];

        int p3[2];
        p3[0] = memo_a[front] + 2 * memo_b[front];
        p3[1] = memo_b[front];

        if (p1[0] <= n && !is_prime_441(p1[0], p1[1]))
        {
            memo_a[back] = p1[0];
            memo_b[back] = p1[1];
            back++;
            if(back >= 10000000)
            {
                cout << "exit(12)" << endl;
                exit(-1);
            };
        }
        else
        {
            memo_2_1_a[idx] = p1[0];
            memo_2_1_b[idx] = p1[1];
            idx++;
        }
        if (p2[0] <= n && !is_prime_441(p2[0], p2[1]))
        {
            memo_a[back] = p2[0];
            memo_b[back] = p2[1];
            back++;
            if(back >= 10000000)
            {
                cout << "exit(13)" << endl;
                exit(-1);
            };
        }
        else
        {
            memo_2_1_a[idx] = p2[0];
            memo_2_1_b[idx] = p2[1];
            idx++;
        }
        if (p3[0] <= n && !is_prime_441(p3[0], p3[1]))
        {
            memo_a[back] = p3[0];
            memo_b[back] = p3[1];
            back++;
            if(back >= 10000000)
            {
                cout << "exit(14)" << endl;
                exit(-1);
            };
        }
        else
        {
            memo_2_1_a[idx] = p3[0];
            memo_2_1_b[idx] = p3[1];
            idx++;
        }
        front++;
    }
    while (front != back);

    size_1_2 = idx;
    size_1_1 = size_1_2 / 2;
    //cout << size_1_2 << endl;


    std::thread thr3(new_th_2);     // spawn new thread that calls foo()
    std::thread thr1(new_th_1_1);     // spawn new thread that calls foo()
    std::thread thr2(new_th_1_2);  // spawn new thread that calls bar(0)

    thr3.join();
    thr1.join();
    thr2.join();

    sum = g_sum1 + g_sum2 + g_sum1_1 + g_sum1_2;

    delete []memo_2_1_a;
    delete []memo_2_1_b;
    delete []memo_3_1_a;
    delete []memo_3_1_b;
    delete []memo_a;
    delete []memo_b;
#if 0
    /*p441_re_1(2, 1);
    p441_re_2(3, 1);
    return g_sum1 + g_sum2;*/
    double sum1 = 0;
    double sum2 = 0;


    std::thread first (thread1, &sum1);     // spawn new thread that calls foo()
    std::thread second (thread2, &sum2);  // spawn new thread that calls bar(0)

    // synchronize threads:
    first.join();                // pauses until first finishes
    second.join();               // pauses until second finishes

    sum = sum1 + sum2;

    /*FILE *fp = fopen("c:\\result.txt", "wb");
    fwrite(&sum, 1, sizeof(double), fp);
    fclose(fp);*/
#endif
    return sum;
#if 0
    int n = 10000000;
    //int n = 10000;
    int limit = sqrt(n);
    queue<couple_int> memo;
    vector<couple_int> store_seeds;
    couple_int seed(2, 1);
    memo.push(seed);
    double sum = 0;
    while(!memo.empty())
    {
        couple_int s = memo.front();
        int min_M = max(s.m_a, s.m_b);
        int max_M = min(s.m_a + s.m_b, n);
        double pq = double(s.m_a) * s.m_b;
        sum += (max_M - min_M + 1) / pq;
        memo.pop();
        couple_int p1(2*s.m_a-s.m_b, s.m_a);
        couple_int p2(2*s.m_a+s.m_b, s.m_a);
        couple_int p3(s.m_a+2*s.m_b, s.m_b);
        if(p1.m_a <= limit && p1.m_b <= limit)
        {
            memo.push(p1);
        }
        else
        {
            store_seeds.push_back(p1);
        }
        if(p2.m_a <= limit && p2.m_b <= limit)
        {
            memo.push(p2);
        }
        else
        {
            store_seeds.push_back(p2);
        }
        if(p3.m_a <= limit && p3.m_b <= limit)
        {
            memo.push(p3);
        }
        else
        {
            store_seeds.push_back(p3);
        }
    }

    for(int i = 0; i < store_seeds.size(); i++)
    {
        seed = store_seeds[i];
        memo.push(seed);
        while(!memo.empty())
        {
            couple_int s = memo.front();
            int min_M = max(s.m_a, s.m_b);
            int max_M = min(s.m_a + s.m_b, n);
            double pq = double(s.m_a) * s.m_b;
            sum += (max_M - min_M + 1) / pq;
            memo.pop();
            couple_int p1(2*s.m_a-s.m_b, s.m_a);
            couple_int p2(2*s.m_a+s.m_b, s.m_a);
            couple_int p3(s.m_a+2*s.m_b, s.m_b);
            if(p1.m_a <= n && p1.m_b <= n)
            {
                memo.push(p1);
            }
            if(p2.m_a <= n && p2.m_b <= n)
            {
                memo.push(p2);
            }
            if(p3.m_a <= n && p3.m_b <= n)
            {
                memo.push(p3);
            }
        }
    }
    store_seeds.clear();


    seed = couple_int(3, 1);
    memo.push(seed);
    while(!memo.empty())
    {
        couple_int s = memo.front();
        int min_M = max(s.m_a, s.m_b);
        int max_M = min(s.m_a + s.m_b, n);
        double pq = double(s.m_a) * s.m_b;
        sum += (max_M - min_M + 1) / pq;
        memo.pop();
        couple_int p1(2*s.m_a-s.m_b, s.m_a);
        couple_int p2(2*s.m_a+s.m_b, s.m_a);
        couple_int p3(s.m_a+2*s.m_b, s.m_b);
        if(p1.m_a <= limit && p1.m_b <= limit)
        {
            memo.push(p1);
        }
        else
        {
            store_seeds.push_back(p1);
        }
        if(p2.m_a <= limit && p2.m_b <= limit)
        {
            memo.push(p2);
        }
        else
        {
            store_seeds.push_back(p2);
        }
        if(p3.m_a <= limit && p3.m_b <= limit)
        {
            memo.push(p3);
        }
        else
        {
            store_seeds.push_back(p3);
        }
    }

    for(int i = 0; i < store_seeds.size(); i++)
    {
        seed = store_seeds[i];
        memo.push(seed);
        while(!memo.empty())
        {
            couple_int s = memo.front();
            int min_M = max(s.m_a, s.m_b);
            int max_M = min(s.m_a + s.m_b, n);
            double pq = double(s.m_a) * s.m_b;
            sum += (max_M - min_M + 1) / pq;
            memo.pop();
            couple_int p1(2*s.m_a-s.m_b, s.m_a);
            couple_int p2(2*s.m_a+s.m_b, s.m_a);
            couple_int p3(s.m_a+2*s.m_b, s.m_b);
            if(p1.m_a <= n && p1.m_b <= n)
            {
                memo.push(p1);
            }
            if(p2.m_a <= n && p2.m_b <= n)
            {
                memo.push(p2);
            }
            if(p3.m_a <= n && p3.m_b <= n)
            {
                memo.push(p3);
            }
        }
    }

    return sum;
#endif
}