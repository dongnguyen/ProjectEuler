//Prime square remainders
//Problem 123
//Let pn be the nth prime: 2, 3, 5, 7, 11, ..., and let r be the remainder when (pn?1)n + (pn+1)n is divided by pn2.
//
//For example, when n = 3, p3 = 5, and 43 + 63 = 280 ? 5 mod 25.
//
//The least value of n for which the remainder first exceeds 109 is 7037.
//
//Find the least value of n for which the remainder first exceeds 1010.

#include "template_class.h"
#include "BigIntegerLibrary.hh"

extern void create_prime_filter_method(int n, bool need_set = false);
extern vector<int> prime_list;

int problem_123()
{
    create_prime_filter_method(1e6);
    BigInteger limit(100000);
    limit *= 100000;
    int idx = 0;
    for(int i = 0;; i++)
    {
        BigInteger tmp = prime_list[i];
        tmp *= tmp;
        if(tmp > limit)
        {
            idx = i;
            break;
        }
    }
    if((idx & 1))
    {
        idx++;
    }
    for(int i = idx;; i += 2)
    {
        BigInteger n = 2*(i + 1);
        BigInteger p = prime_list[i];
        BigInteger p2 = p*p;
        BigInteger r = n*p % p2;
        if(r > limit)
        {
            return i + 1;
        }
    }
    return 0;
}