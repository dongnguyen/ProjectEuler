//How many reversible numbers are there below one - billion ?
//Problem 145
//Some positive integers n have the property that the sum[n + reverse(n)] consists entirely of odd(decimal) digits.For instance, 36 + 63 = 99 and 409 + 904 = 1313. We will call such numbers reversible; so 36, 63, 409, and 904 are reversible.Leading zeroes are not allowed in either n or reverse(n).
//
//There are 120 reversible numbers below one - thousand.
//
//How many reversible numbers are there below one - billion(109) ?

#include "template_class.h"

int problem_145(int ten_exp)
{
    // n8 n7 n6 n5 n4 n3 n2 n1 n0
    // n0 n1 n2 n3 n4 n5 n6 n7 n8
    int limit = ten_exp + 1;
    int count = 0;
    for(int num_digit = 1; num_digit < limit; num_digit++)
    {
        vector<int> digits;
        digits.resize(num_digit);
        do
        {
            /*if(num_digit == 2 && digits[0] == 6 && digits[1] == 3)
            {
                int d = 0;
                d++;
            }*/
            if(digits[0] == 0)
            {
                digits[0]++;
                /*for(int i = 1; i < num_digit; i++)
                {
                    digits[i] = 0;
                }*/
            }
            if(digits[num_digit - 1] == 0)
            {
                digits[num_digit - 1]++;
            }
            int remain = 0;
            bool satisfy = true;
            for(int i = 0; i < num_digit; i++)
            {
                int tmp = digits[i] + digits[num_digit - i - 1] + remain;
                if(tmp & 1)
                {
                    remain = tmp / 10;
                }
                else
                {
                    satisfy = false;
                    /*for(int idx = i + 1; idx < num_digit; idx++)
                    {
                        digits[idx] = 0;
                    }
                    int idx = i;
                    digits[idx]++;
                    while(1)
                    {
                        if(digits[idx] == 10 && idx > 0)
                        {
                            digits[idx] = 0;
                            idx--;
                            digits[idx]++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if(digits[0] == 10)
                    {
                        goto DONE;
                    }*/
                    break;
                }
            }
            if(satisfy /*&& (remain & 1)*/)
            {
                count++;
            }
            int idx = num_digit - 1;
            digits[idx]++;
            while(1)
            {
                if(digits[idx] == 10 && idx > 0)
                {
                    digits[idx] = 0;
                    idx--;
                    digits[idx]++;
                }
                else
                {
                    break;
                }
            }
            if(digits[0] == 10)
            {
                goto DONE;
            }
        }
        while(1);
DONE:
        ;
    }
    return count;
}
