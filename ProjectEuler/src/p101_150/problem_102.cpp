//Triangle containment
//Problem 102
//Three distinct points are plotted at random on a Cartesian plane, for which -1000 ? x, y ? 1000, such that a triangle is formed.
//
//Consider the following two triangles:
//
//A(-340,495), B(-153,-910), C(835,-947)
//
//X(-175,41), Y(-421,-714), Z(574,-645)
//
//It can be verified that triangle ABC contains the origin, whereas triangle XYZ does not.
//
//Using triangles.txt (right click and 'Save Link/Target As...'), a 27K text file containing the co-ordinates of one thousand "random" triangles, find the number of triangles for which the interior contains the origin.
//
//NOTE: The first two examples in the file represent the triangles in the example given above.

#include "template_class.h"

struct point
{
    int x;
    int y;
    point(int a, int b)
    {
        x = a;
        y = b;
    }
    point()
    {
        x = 0;
        y = 0;
    }
};

int triangle_area_x2(point& A, point& B, point& C)
{
    return abs((A.x - C.x)*(B.y - A.y) - (A.x - B.x)*(C.y - A.y));
}

bool verify_origin(point& A, point& B, point& C)
{
    int s = triangle_area_x2(A, B, C);
    point zero(0, 0);
    int s1 = triangle_area_x2(zero, B, C);
    int s2 = triangle_area_x2(A, zero, C);
    int s3 = triangle_area_x2(A, B, zero);
    if(s == s1 + s2 + s3)
    {
        return true;
    }
    return false;
}

#define LIMIT_102 1000

vector<vector<point>> input_102;

void read_triangles_input(char* path)
{
    FILE* fp = fopen(path, "r");
    for(int i = 0; i < LIMIT_102; i++)
    {
        fscanf(fp, "%d,%d,%d,%d,%d,%d\n", &input_102[i][0].x,
               &input_102[i][0].y,
               &input_102[i][1].x,
               &input_102[i][1].y,
               &input_102[i][2].x,
               &input_102[i][2].y);
    }
    fclose(fp);
}

int problem_102(char* path)
{
    /*point A(-340,495);
    point B(-153,-910);
    point C(835,-947);
    bool s = verify_origin(A, B, C);
    point X(-175,41);
    point Y(-421,-714);
    point Z(574,-645);
    s = verify_origin(X, Y, Z);*/
    input_102.resize(LIMIT_102);
    for(int i = 0; i < LIMIT_102; i++)
    {
        input_102[i].resize(3);
    }
    read_triangles_input(path);
    int count = 0;
    for(int i = 0; i < LIMIT_102; i++)
    {
        if(verify_origin(input_102[i][0], input_102[i][1], input_102[i][2]))
        {
            count++;
        }
    }
    return count;
}
