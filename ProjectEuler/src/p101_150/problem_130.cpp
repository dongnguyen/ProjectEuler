//Composites with prime repunit property
//Problem 130
//A number consisting entirely of ones is called a repunit.We shall define R(k) to be a repunit of length k; for example, R(6) = 111111.
//
//Given that n is a positive integer and GCD(n, 10) = 1, it can be shown that there always exists a value, k, for which R(k) is divisible by n, and let A(n) be the least such value of k; for example, A(7) = 6 and A(41) = 5.
//
//You are given that for all primes, p > 5, that p ? 1 is divisible by A(p).For example, when p = 41, A(41) = 5, and 40 is divisible by 5.
//
//However, there are rare composite values for which this is also true; the first five examples being 91, 259, 451, 481, and 703.
//
//Find the sum of the first twenty - five composite values of n for which
//GCD(n, 10) = 1 and n ? 1 is divisible by A(n).

#include "template_class.h"

extern vector<int> prime_list;
extern set<int> set_prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

extern int totient(int n);
extern int large_mode(int a, int b, int m);
extern int count_num_digit(int n);

#define LIMIT_130 (2e4)
vector<int> totient_130;

int least_k_130(int p)
{
    int to_p = totient_130[p] * 6;
    uint64_t m = 9 * p;
    uint64_t tmp = 10;
    for (int k = 1; k < to_p; k++)
    {
        if (tmp > m)
            tmp %= m;
        if (tmp == 1)
        {
            return k;
        }
        tmp *= 10;
    }
    return 0;
}

uint64_t problem_130()
{
    create_prime_filter_method(LIMIT_130, true);
    totient_130.resize(LIMIT_130);
    for (int i = 1; i < LIMIT_130; i++)
    {
        totient_130[i] = i;
    }
    for (int i = 0; prime_list[i] < sqrt(LIMIT_130); i++)
    {
        int p = prime_list[i];
        for (int j = p; j < LIMIT_130; j += p)
        {
            totient_130[j] /= p;
            totient_130[j] *= p - 1;
        }
    }
    int count = 0;
    uint64_t sum = 0;
    for (int i = 3; i < LIMIT_130;)
    {
        int k = 0;
        if (set_prime_list.find(i) == set_prime_list.end())
        {
            k = least_k_130(i);
            if ((i - 1) % k == 0)
            {
                count++;
                sum += i;
                if (count == 25)
                {
                    break;
                }
                //cout << i << endl;
            }
        }
        /*================================================*/
        i += 4;
        if (set_prime_list.find(i) == set_prime_list.end())
        {
            k = least_k_130(i);
            if ((i - 1) % k == 0)
            {
                count++;
                sum += i;
                if (count == 25)
                {
                    break;
                }
                //cout << i << endl;
            }
        }
        /*================================================*/
        i += 2;
        if (set_prime_list.find(i) == set_prime_list.end())
        {
            k = least_k_130(i);
            if ((i - 1) % k == 0)
            {
                count++;
                sum += i;
                if (count == 25)
                {
                    break;
                }
                //cout << i << endl;
            }
        }
        /*================================================*/
        i += 2;
        if (set_prime_list.find(i) == set_prime_list.end())
        {
            k = least_k_130(i);
            if ((i - 1) % k == 0)
            {
                count++;
                sum += i;
                if (count == 25)
                {
                    break;
                }
                //cout << i << endl;
            }
        }
        /*================================================*/
        i += 2;
    }
    return sum;
}
