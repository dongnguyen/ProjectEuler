// Special subset sums: optimum
// Problem 103
// Let S(A) represent the sum of elements in set A of size n. We shall call it a special sum set if for any two non-empty disjoint subsets, B and C, the following properties are true:

// S(B) ≠ S(C); that is, sums of subsets cannot be equal.
// If B contains more elements than C then S(B) > S(C).
// If S(A) is minimised for a given n, we shall call it an optimum special sum set. The first five optimum special sum sets are given below.

// n = 1: {1}
// n = 2: {1, 2}
// n = 3: {2, 3, 4}
// n = 4: {3, 5, 6, 7}
// n = 5: {6, 9, 11, 12, 13}

// It seems that for a given optimum set, A = {a1, a2, ... , an}, the next optimum set is of the form B = {b, a1+b, a2+b, ... ,an+b}, where b is the "middle" element on the previous row.

// By applying this "rule" we would expect the optimum set for n = 6 to be A = {11, 17, 20, 22, 23, 24}, with S(A) = 117. However, this is not the optimum set, as we have merely applied an algorithm to provide a near optimum set. The optimum set for n = 6 is A = {11, 18, 19, 20, 22, 25}, with S(A) = 115 and corresponding set string: 111819202225.

// Given that A is an optimum special sum set for n = 7, find its set string.

// NOTE: This problem is related to Problem 105 and Problem 106.

// I don't believe it https://oeis.org/A037254
//T(1,1)=1; T(n,1)=T(n-1,[(n+1)/2]); T(n,k)=T(n,1)+T(n-1,k-1) for k>1.

#include "template_class.h"
#include <string>
#include <map>

using std::map;

extern void combination(int offset, int k, vector<int>& set, vector<int>& com, vector < vector < int >> &outset);
extern bool isNumInSet90(int n, vector<int>& set);

bool isDisJoin(vector<int>& setA, vector<int>& setB)
{
	for (int i = 0; i < setA.size(); i++)
	{
		for (int j = 0; j < setB.size(); j++)
		{
			if (setA[i] == setB[j])
			{
				return false;
			}
		}
	}
	return true;
}

void generateAllSubSet(vector<int>& set, vector < vector < int >>& subsets)
{
	vector < vector < int >> outsets;
	for (int i = 1; i <= set.size(); i++)
	{
		vector<int> com;
		vector < vector < int >> outsetstmp;
		combination(0, i, set, com, outsetstmp);
		for (int j = 0; j < outsetstmp.size(); j++)
		{
			outsets.push_back(outsetstmp[j]);
		}
	}
	subsets = outsets;
}

int sumSet(vector<int>& set)
{
	int ret = 0;
	for (int i = 0; i < set.size(); i++)
	{
		ret += set[i];
	}
	return ret;
}

void debugSet(vector<int> set)
{
	cout << "print set: {";
	for (int i = 0; i < set.size() - 1; i++)
	{
		cout << set[i] << ", ";
	}
	cout << set[set.size() - 1] << "}" << endl;
}

void printSetString(vector<int> set)
{
	cout << "print set: ";
	for (int i = 0; i < set.size(); i++)
	{
		cout << set[i];
	}
	cout << endl;
}

extern bool test105rule1(vector<int> optimum_set);
extern bool test105rule2(vector<int> optimum_set);

bool test103rule2(vector<int> optimum_set)
{
	if (test105rule2(optimum_set))
	{
		if (test105rule1(optimum_set))
		{
			return true;
		}
	}
	return false;
#if 0
	int size = subsets.size();
	if (size < 2)
		return true;
	if (size == 3)
		if (optimum_set[0] + optimum_set[1] < optimum_set[2])
			return false;
	if (size == 4)
		return true;
#endif
#if 0
	vector < vector < int >> subsets;
	generateAllSubSet(optimum_set, subsets);
	int size = subsets.size();
	//cout << "size: " << size << endl;
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (isDisJoin(subsets[i], subsets[j]))
			{
				// rule 1
				if (sumSet(subsets[i]) == sumSet(subsets[j]))
				{
					/*debugSet(subsets[i]);
					cout << "disjoint with" << endl;
					debugSet(subsets[j]);
					cout << "fail rule 1" << endl;*/
					return false;
				}
				// rule 2
				if (subsets[i].size() > subsets[j].size())
				{
					if (sumSet(subsets[i]) < sumSet(subsets[j]))
					{
						//cout << "fail rule 2" << endl;
						return false;
					}
				}
				else if (subsets[i].size() < subsets[j].size())
				{
					if (sumSet(subsets[i]) > sumSet(subsets[j]))
					{
						//cout << "fail rule 2" << endl;
						return false;
}
				}
			}
		}
	}
	//debugSet(subsets[size - 1]);
	return true;
#endif
}
const int max103 = 50;

vector<set<int>> list_num_never_pair[max103 + 1];

typedef enum _set_type
{
	UNDEFINED,
	NORMAL,
	SPECIAL
} set_type;

struct special_set
{
	int sum;
	int size;
	vector<int> set;
	special_set(vector<int> in_set)
	{
		sum = 0;
		size = in_set.size();
		for (int i = 0; i < size; i++)
		{
			sum += in_set[i];
			set[i] = in_set[i];
		}
	}
	special_set()
	{
		sum = 0;
		size = 0;
	}
	bool checkEqual(const special_set& ref) const
	{
		for (int i = 0; i < size; i++)
		{
			if (set[i] != ref.set[i])
			{
				return false;
			}
		}
		return true;
	}
	bool operator< (const special_set& ref) const
	{
		if (size < ref.size)
		{
			return true;
		}
		if (size > ref.size)
		{
			return false;
		}
		if (checkEqual(ref))
		{
			return false;
		}
		for (int i = 0; i < size; i++)
		{
			if (set[i] < ref.set[i])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	bool operator> (const special_set& ref) const
	{
		if (size > ref.size)
		{
			return true;
		}
		if (size < ref.size)
		{
			return false;
		}
		if (checkEqual(ref))
		{
			return false;
		}
		for (int i = 0; i < size; i++)
		{
			if (set[i] > ref.set[i])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	bool operator== (const special_set& ref) const
	{
		if (size != ref.size)
		{
			return false;
		}
		return checkEqual(ref);
	}
};

map<special_set, set_type> resultBuffers;

std::string problem_103()
{
	std::string ret;

	vector < vector < int >> subsets2;
	vector < vector < int >> subsets3;
	vector < vector < int >> subsets4;
	vector < vector < int >> subsets5;
	vector < vector < int >> subsets6;
	vector < vector < int >> subsets7;

	for (int i = 1; i < max103 - 1; i++)
	{
		for (int j = i + 1; j < max103; j++)
		{
			vector<int> optimum_set;
			optimum_set.resize(2);
			optimum_set[0] = i;
			optimum_set[1] = j;
			if (i + j <= 255 && i + j >= 3)
				if (test103rule2(optimum_set))
				{
					//cout << sumSet(optimum_set) << ": ";
					//printSetString(optimum_set);
					subsets2.push_back(optimum_set);
				}
		}
	}
	cout << "subsets2 size: " << subsets2.size() << endl;
	for (int idx = 0; idx < subsets2.size(); idx++)
	{
		for (int i = 1; i < subsets2[idx][0]; i++)
		{
			if (i + subsets2[idx][0] + subsets2[idx][1] <= 255 &&
				i + subsets2[idx][0] + subsets2[idx][1] >= 9)
			{
				vector<int> optimum_set;
				optimum_set.resize(3);
				optimum_set[0] = i;
				optimum_set[1] = subsets2[idx][0];
				optimum_set[2] = subsets2[idx][1];
				if (test103rule2(optimum_set))
				{
					subsets3.push_back(optimum_set);
				}
			}
		}
	}
	cout << "subsets3 size: " << subsets3.size() << endl;
	for (int idx = 0; idx < subsets3.size(); idx++)
	{
		for (int i = 1; i < subsets3[idx][0]; i++)
		{
			if (i + subsets3[idx][0] + subsets3[idx][1] + subsets3[idx][2] <= 255 &&
				i + subsets3[idx][0] + subsets3[idx][1] + subsets3[idx][2] >= 21)
			{
				vector<int> optimum_set;
				optimum_set.resize(4);
				optimum_set[0] = i;
				optimum_set[1] = subsets3[idx][0];
				optimum_set[2] = subsets3[idx][1];
				optimum_set[3] = subsets3[idx][2];
				if (test103rule2(optimum_set))
				{
					subsets4.push_back(optimum_set);
				}
			}
		}
	}
	cout << "subsets4 size: " << subsets4.size() << endl;
	for (int idx = 0; idx < subsets4.size(); idx++)
	{
		for (int i = 1; i < subsets4[idx][0]; i++)
		{
			if (i + subsets4[idx][0] + subsets4[idx][1] + subsets4[idx][2] + subsets4[idx][3] <= 255 &&
				i + subsets4[idx][0] + subsets4[idx][1] + subsets4[idx][2] + subsets4[idx][3] >= 51)
			{
				vector<int> optimum_set;
				optimum_set.resize(5);
				optimum_set[0] = i;
				optimum_set[1] = subsets4[idx][0];
				optimum_set[2] = subsets4[idx][1];
				optimum_set[3] = subsets4[idx][2];
				optimum_set[4] = subsets4[idx][3];
				if (test103rule2(optimum_set))
				{
					subsets5.push_back(optimum_set);
				}
			}
		}
	}
	cout << "subsets5 size: " << subsets5.size() << endl;
	for (int idx = 0; idx < subsets5.size(); idx++)
	{
		for (int i = 1; i < subsets5[idx][0]; i++)
		{
			if (i + subsets5[idx][0] + subsets5[idx][1] + subsets5[idx][2] + subsets5[idx][3] + subsets5[idx][4] <= 255 &&
				i + subsets5[idx][0] + subsets5[idx][1] + subsets5[idx][2] + subsets5[idx][3] + subsets5[idx][4] >= 115)
			{
				vector<int> optimum_set;
				optimum_set.resize(6);
				optimum_set[0] = i;
				optimum_set[1] = subsets5[idx][0];
				optimum_set[2] = subsets5[idx][1];
				optimum_set[3] = subsets5[idx][2];
				optimum_set[4] = subsets5[idx][3];
				optimum_set[5] = subsets5[idx][4];
				if (test103rule2(optimum_set))
				{
					subsets6.push_back(optimum_set);
				}
			}
		}
	}
	cout << "subsets6 size: " << subsets6.size() << endl;
	for (int idx = 0; idx < subsets6.size(); idx++)
	{
		for (int i = 1; i < subsets6[idx][0]; i++)
		{
			if (i + subsets6[idx][0] + subsets6[idx][1] + subsets6[idx][2] + subsets6[idx][3] + subsets6[idx][4] + subsets6[idx][5] <= 255 &&
				i + subsets6[idx][0] + subsets6[idx][1] + subsets6[idx][2] + subsets6[idx][3] + subsets6[idx][4] + subsets6[idx][5] > 125)
			{
				vector<int> optimum_set;
				optimum_set.resize(7);
				optimum_set[0] = i;
				optimum_set[1] = subsets6[idx][0];
				optimum_set[2] = subsets6[idx][1];
				optimum_set[3] = subsets6[idx][2];
				optimum_set[4] = subsets6[idx][3];
				optimum_set[5] = subsets6[idx][4];
				optimum_set[6] = subsets6[idx][5];
				if (test103rule2(optimum_set))
				{
					subsets7.push_back(optimum_set);
				}
			}
		}
	}
	cout << "subsets7 size: " << subsets7.size() << endl;
	for (int i = 0; i < subsets7.size(); i++)
	{
		cout << sumSet(subsets7[i]) << ": ";
		printSetString(subsets7[i]);
	}
#if 0
	int A[] = { 11, 18, 19, 20, 22, 25 };
	int b = 0;
	vector<int> optimum_set;
	optimum_set.resize(5);
	/*optimum_set[0] = 20;
	for (int i = 1; i < 7; i++)
	{
		optimum_set[i] = A[i - 1] + optimum_set[0];
	}*/

	//{20, 31, 38, 39, 40, 42, 45}
	uint64_t cnt = 0;
	__pragma(loop(hint_parallel(8)))
		for (int i = 1; i <= 51; i++)
		{
			cout << "i: " << i << endl;
			for (int j = i + 1; j <= 51; j++)
			{
				for (int k = j + 1; k <= 255; k++)
				{
					for (int l = k + 1; l <= 255; l++)
					{
						for (int m = l + 1; m <= 255; m++)
						{
							if (i + j + k + l + m <= 255)
							{
								optimum_set[0] = i;
								optimum_set[1] = j;
								optimum_set[2] = k;
								optimum_set[3] = l;
								optimum_set[4] = m;
								if (test103rule2(optimum_set))
								{
									cout << sumSet(optimum_set) << ": ";
									printSetString(optimum_set);
								}
							}
							continue;
							if (i + j + k + l + m + m + 1 <= 255)
								for (int n = m + 1; n <= 255; n++)
								{
									//if (i + j + k + l + m + n + n + 1 <= 255)
										//if (i + j + k + l + m + n + n + 1 > 200)
									//for (int p = n + 1; p <= 65; p++)
									//{
									//	if (i + j + k + l + m + n + p <= 255)
									//		if (i + j + k + l + m + n + p >= 245)
									//		{
									//			cnt++;
									//			optimum_set[0] = i;
									//			optimum_set[1] = j;
									//			optimum_set[2] = k;
									//			optimum_set[3] = l;
									//			optimum_set[4] = m;
									//			optimum_set[5] = n;
									//			optimum_set[6] = p;
									//			if (test103rule2(optimum_set))
									//			{
									//				cout << sumSet(optimum_set) << ": ";
									//				printSetString(optimum_set);
									//			}
									//			//goto fn_exit;
									//		}
									//}
								}
						}
					}
				}
			}
		}
	//fn_exit:
	cout << cnt << endl;

	//{20, 31, 38, 39, 40, 42, 45}
	/*optimum_set[0] = 20;
	optimum_set[1] = 31;
	optimum_set[2] = 38;
	optimum_set[3] = 39;
	optimum_set[4] = 40;
	optimum_set[5] = 42;
	optimum_set[6] = 45;
	if (test103rule2(optimum_set))
	{
		cout << sumSet(optimum_set) << ": ";
		printSetString(optimum_set);
	}*/
#endif
	return ret;
		}
