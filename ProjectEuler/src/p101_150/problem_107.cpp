//Minimal network
//Problem 107
//The following undirected network consists of seven vertices and twelve edges with a total weight of 243.
//
//
//The same network can be represented by the matrix below.
//
//    	A	B	C	D	E	F	G
//A	-	16	12	21	-	-	-
//B	16	-	-	17	20	-	-
//C	12	-	-	28	-	31	-
//D	21	17	28	-	18	19	23
//E	-	20	-	18	-	-	11
//F	-	-	31	19	-	-	27
//G	-	-	-	23	11	27	-
//However, it is possible to optimise the network by removing some edges and still ensure that all points on the network remain connected. The network which achieves the maximum saving is shown below. It has a weight of 93, representing a saving of 243 ? 93 = 150 from the original network.
//
//
//Using network.txt (right click and 'Save Link/Target As...'), a 6K text file containing a network with forty vertices, and given in matrix form, find the maximum saving which can be achieved by removing redundant edges whilst ensuring that the network remains connected.

#include "template_class.h"

struct edge
{
    int u;
    int v;
    int weight;
    edge()
    {
        u = 0;
        v = 0;
        weight = 0;
    }
    edge(int m, int n, int w)
    {
        u = m;
        v = n;
        weight = w;
    }
    bool operator< (const edge& ref) const
    {
        return weight < ref.weight;
    }
};

int read_input_107(char* path, vector<edge>& edges)
{
    FILE* fp = fopen(path, "r");
    //vector<vector<int>> test;
    //test.resize(40);
    int sum = 0;
    for(int i = 0; i < 40; i++)
    {
        //test[i].resize(40);
        for(int j = 0; j < 40; j++)
        {
            int tmp = 0;
            fscanf(fp, "%d,", &tmp);
            //test[i][j] = tmp;
            if(tmp != -1 && j > i)
            {
                edge e(i, j, tmp);
                edges.push_back(e);
                sum += tmp;
            }
        }
    }
    fclose(fp);
    return sum;
}

vector<int> mParent;

int findSet(int x)
{
    int parent=mParent[x];
    if(x!=parent)
    {
        mParent[x]=findSet(parent);
    }
    return mParent[x];
}

int problem_107(char* path)
{
    vector<edge> edges;
    int org_weight = read_input_107(path, edges);
    sort(edges.begin(), edges.end());
    mParent.resize(40);
    for(int i = 0; i < 40; i++)
    {
        mParent[i] = i;
    }
    int mNumEdge = edges.size();
    int sum_weight = 0;
    for(int i = 0; i < mNumEdge; i++)
    {
        int u=edges[i].u;
        int v=edges[i].v;
        int pu=findSet(u);
        int pv=findSet(v);
        if(pu!=pv)
        {
            sum_weight += edges[i].weight;
            mParent[pu]=mParent[pv]; // link
        }
    }
    return org_weight - sum_weight;
}
