//Special isosceles triangles
//Problem 138
//Consider the isosceles triangle with base length, b = 16, and legs, L = 17.
//
//
//By using the Pythagorean theorem it can be seen that the height of the triangle, h = ?(172 ? 82) = 15, which is one less than the base length.
//
//With b = 272 and L = 305, we get h = 273, which is one more than the base length, and this is the second smallest isosceles triangle with the property that h = b � 1.
//
//Find ? L for the twelve smallest isosceles triangles for which h = b � 1 and b, L are positive integers.

#include "template_class.h"
#include <thread>         // std::thread
#include "mpirxx.h"
#include "int128.h"


bool is_square(int64_t n)
{
    int64_t tmp = sqrt(n);
    if (n == tmp * tmp)
    {
        return true;
    }
    return false;
}

bool is_square(mpz_class n)
{
    mpz_class tmp = sqrt(n);
    if (n == tmp * tmp)
    {
        return true;
    }
    return false;
}

extern vector<int> continued_fractions(int n);

bool verify_138(uint64_t h, uint64_t k, int n)
{
    int128_t h_2((u64)h);
    int128_t k_2((u64)k);
    h_2 *= h_2;
    k_2 *= k_2;
    int128_t diff = h_2 - k_2*n;
    if (diff == 1 || diff == -1)
    {
        return true;
    }
    return false;
}

uint64_t problem_138()
{
    uint64_t sum = 0;
    vector<int> cont_frac = continued_fractions(5);
    int period = cont_frac.size() - 1;
    uint64_t h_2;
    uint64_t h_1;
    uint64_t h;
    uint64_t k_2;
    uint64_t k_1;
    uint64_t k;
    int count = 0;
    h_2 = 1;
    h_1 = cont_frac[0];
    k_2 = 0;
    k_1 = 1;
    if (verify_138(h_1, k_1, 5))
    {
        uint64_t n = k_1;
        uint64_t m = 2 * k_1 + h_1;
        uint64_t L = m*m + n*n;
        cout << L << endl;
        sum += L;
        count++;
    }
    for (int i = 0; ; i++)
    {
        uint64_t a = cont_frac[(i % period) + 1];
        h = a*h_1 + h_2;
        k = a*k_1 + k_2;
        if (verify_138(h, k, 5))
        {
            uint64_t n = k;
            uint64_t m = 2 * k + h;
            uint64_t L = m*m + n*n;
            cout << L << endl;
            sum += L;
            count++;
            if (count == 12)
            {
                break;
            }
        }
        h_2 = h_1;
        h_1 = h;
        k_2 = k_1;
        k_1 = k;
    }
    cout << endl;
    return sum;
#if 0
    mpz_class sum = 0;
    mpz_class b = 2;
    mpz_class h1 = 1;
    mpz_class h2 = 3;
    int count = 0;
    for (;;)
    {
        mpz_class b2 = b >> 1;
        b2 *= b2;
        mpz_class L = b2 + h1*h1;
        if (is_square(L))
        {
            sum += sqrt(L);
            cout << sqrt(L) << endl;
            count++;
            if (count == 12)
            {
                break;
            }
        }
        L = b2 + h2*h2;
        if (is_square(L))
        {
            sum += sqrt(L);
            cout << sqrt(L) << endl;
            count++;
            if (count == 12)
            {
                break;
            }
        }
        b += 2;
        h1 += 2;
        h2 += 2;
    }
    cout << sum << endl;
    return 0;
#endif

    vector<int64_t> ret;
    int64_t n_138 = 1e15;
    int64_t limit = sqrt(n_138);
    queue<triple<int64_t>> memo;
    vector<triple<int64_t>> store_seeds;
    triple<int64_t> seed(3, 4, 5);
    memo.push(seed);
    while (!memo.empty())
        //while (1)
    {
        triple<int64_t> s = memo.front();
        memo.pop();
        int64_t a = s.a;
        int64_t b = s.b;
        int64_t c = s.c;
        if (a < b)
        {
            if (abs((a << 1) - b) == 1)
            {
                cout << c << endl;
                //sum += c;
                ret.push_back(c);
            }
        }
        else
        {
            if (abs((b << 1) - a) == 1)
            {
                cout << c << endl;
                //sum += c;
                ret.push_back(c);
            }
        }

        if (s.a * 2 > s.b)
        {
            triple<int64_t> p1(a - 2 * b + 2 * c, 2 * a - b + 2 * c, 2 * a - 2 * b + 3 * c);
            if (p1.c <= limit)
            {
                memo.push(p1);
            }
            else
            {
                store_seeds.push_back(p1);
            }
        }
        //if (abs(s.a - s.b) > min(s.a, s.b))
        {
            triple<int64_t> p2(a + 2 * b + 2 * c, 2 * a + b + 2 * c, 2 * a + 2 * b + 3 * c);
            if (p2.c <= limit)
            {
                memo.push(p2);
            }
            else
            {
                store_seeds.push_back(p2);
            }
        }
        if (s.a < s.b * 2)
        {
            triple<int64_t> p3(-a + 2 * b + 2 * c, -2 * a + b + 2 * c, -2 * a + 2 * b + 3 * c);
            if (p3.c <= limit)
            {
                memo.push(p3);
            }
            else
            {
                store_seeds.push_back(p3);
            }
        }
    }
    for(int i = 0; i < store_seeds.size(); i++)
    {
        seed = store_seeds[i];
        memo.push(seed);
        while (!memo.empty())
        {
            triple<int64_t> s = memo.front();
            memo.pop();
            int64_t a = s.a;
            int64_t b = s.b;
            int64_t c = s.c;
            if (a < b)
            {
                if (abs((a << 1) - b) == 1)
                {
                    cout << c << endl;
                    //sum += c;
                    ret.push_back(c);
                }
            }
            else
            {
                if (abs((b << 1) - a) == 1)
                {
                    cout << c << endl;
                    //sum += c;
                    ret.push_back(c);
                }
            }

            if (s.a * 2 > s.b)
            {
                triple<int64_t> p1(a - 2 * b + 2 * c, 2 * a - b + 2 * c, 2 * a - 2 * b + 3 * c);
                if (p1.c <= n_138)
                {
                    memo.push(p1);
                }
            }
            //if (abs(s.a - s.b) > min(s.a, s.b))
            {
                triple<int64_t> p2(a + 2 * b + 2 * c, 2 * a + b + 2 * c, 2 * a + 2 * b + 3 * c);
                if (p2.c <= n_138)
                {
                    memo.push(p2);
                }
            }
            if (s.a < s.b * 2)
            {
                triple<int64_t> p3(-a + 2 * b + 2 * c, -2 * a + b + 2 * c, -2 * a + 2 * b + 3 * c);
                if (p3.c <= n_138)
                {
                    memo.push(p3);
                }
            }
        }
    }
#if 0
    for (int i = 0; i < store_seeds.size(); i++)
    {
        seed = store_seeds[i];
        memo.push(seed);

        while (!memo.empty())
        {
            triple<int64_t> s = memo.front();
            memo.pop();

            int64_t a = s.a;
            int64_t b = s.b;
            int64_t c = s.c;
            if (a < b)
            {
                if (abs((a << 1) - b) == 1)
                {
                    cout << c << endl;
                    //sum += c;
                    ret.push_back(c);
                }
            }
            else
            {
                if (abs((b << 1) - a) == 1)
                {
                    cout << c << endl;
                    //sum += c;
                    ret.push_back(c);
                }
            }
            //triple<int64_t> p1(a - 2 * b + 2 * c, 2 * a - b + 2 * c, 2 * a - 2 * b + 3 * c);
            triple<int64_t> p2(a + 2 * b + 2 * c, 2 * a + b + 2 * c, 2 * a + 2 * b + 3 * c);
            //triple<int64_t> p3(-a + 2 * b + 2 * c, -2 * a + b + 2 * c, -2 * a + 2 * b + 3 * c);
            /*if (p1.c <= n_138)
            {
                memo.push(p1);
            }*/
            if (p2.c <= n_138)
            {
                memo.push(p2);
            }
            /*if (p3.c <= n_138)
            {
                memo.push(p3);
            }*/
        }
    }
#endif
    return ret.size();
#if 0
    int limit = 1e5;
    int count = 0;
    for (int n_138 = 1; n_138 < limit - 1; n_138++)
    {
        for (int m = n_138 + 1; m < limit; m += 2)
        {
            if (gcd(n_138, m) == 1)
            {
                int64_t a = m*m - n*n;
                int64_t b = 2 * m*n;
                int64_t c = m*m + n*n;
                if (a < b)
                {
                    if (abs((a << 1) - b) == 1)
                    {
                        cout << c << endl;
                        sum += c;
                        count++;
                        if (count == 12)
                        {
                            goto exit;
                        }
                    }
                }
                else
                {
                    if (abs((b << 1) - a) == 1)
                    {
                        cout << c << endl;
                        sum += c;
                        count++;
                        if (count == 12)
                        {
                            goto exit;
                        }
                    }
                }
            }
        }
    }
exit:
#endif
    return sum;
}
