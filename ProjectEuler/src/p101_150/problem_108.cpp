//Diophantine reciprocals I
//Problem 108
//In the following equation x, y, and n are positive integers.
//
//1
//
//x
//+
//1
//
//y
//=
//1
//
//n
//For n = 4 there are exactly three distinct solutions:
//
//1
//
//5
//+
//1
//
//20
//=
//1
//
//4
//1
//
//6
//+
//1
//
//12
//=
//1
//
//4
//1
//
//8
//+
//1
//
//8
//=
//1
//
//4
//What is the least value of n for which the number of distinct solutions exceeds one-thousand?
//
//NOTE: This problem is an easier version of problem 110; it is strongly advised that you solve this one first.

#include "template_class.h"
//#include "lp_lib.h"
//#include "BigIntegerLibrary.hh"

extern vector<int> prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

// solved by hand and some luck
// do some manipulations on the original equation
// to form the new one (x - n)*(y - n) = n^2
// therefor total solution will be number of divisor of n^2
// and it will be (2a1 + 1)...(2an + 1)
// but since x, y can exchange so the distint solutions is (S + 1)/2

int calc_num_solution(vector<int>& solution)
{
	int p = 1;
	for (int i = 0; i < solution.size(); i++)
	{
		p *= 2 * solution[i] + 1;
	}
	return p;
}

uint64_t problem_108(int n)
{
	create_prime_filter_method(100);
	int num_solutions = n * 2 - 1;
	double log_3 = log(3);
	double log_num_sol = log(num_solutions);
	int num_var = (int)ceil(log_num_sol / log_3);
	vector<int> solution;
	solution.resize(num_var);
	for (int i = 0; i < num_var; i++)
	{
		solution[i] = 1;
	}

	vector<vector<int>> p_idx;
	vector<vector<int>> p_count;
	int size = prime_list[num_var - 1];
	p_idx.resize(size);
	p_count.resize(size);
	for (int i = 2; i < size; i++)
	{
		int idx = 0;
		int tmp = i;
		while (tmp != 1)
		{
			int count = 0;
			while (tmp % prime_list[idx] == 0)
			{
				count++;
				tmp /= prime_list[idx];
			}
			if (count)
			{
				p_idx[i].push_back(idx);
				p_count[i].push_back(count);
			}
			idx++;
		}
	}

	int remove_idx = num_var - 1;
	for (;;)
	{
		int need_remove = prime_list[remove_idx];
		bool can_remove = false;
		for (int i = 2; i < need_remove; i++)
		{
			vector<int> new_solution;
			new_solution = solution;
			new_solution[remove_idx] = 0;
			for (int j = 0; j < p_idx[i].size(); j++)
			{
				new_solution[p_idx[i][j]] += p_count[i][j];
			}
			if (calc_num_solution(new_solution) > num_solutions)
			{
				can_remove = true;
				solution = new_solution;
				remove_idx--;
				break;
			}
		}
		if (can_remove == false)
		{
			break;
		}
	}

	uint64_t ret = 1;
	for (int i = 0; i <= remove_idx; i++)
	{
		ret *= pow(prime_list[i], solution[i]);
	}
	return ret;

#if 0
	create_prime_filter_method(1e3);
	vector<int> list;
	vector<int> count;
	list.reserve(10);
	list.resize(1);
	list[0] = 2;
	count.reserve(10);
	count.resize(1);
	count[0] = 1;

	uint64_t num_solutions = 3;
	uint64_t ret = 2;
	uint64_t new_num_solutions = 0;
	uint64_t new_ret = 0;
	int next_candidate_idx = 1;
	int next_candidate = prime_list[next_candidate_idx];
	while ((num_solutions + 1) / 2 <= n)
	{
		//double ratio_add_old = 0;
		uint64_t min = INT_MAX;
		int found_idx = 0;
		for (int i = 0; i < list.size(); i++)
		{
			uint64_t tmp = pow(list[i], count[i] + 1);
			if (min > tmp)
			{
				min = tmp;
				found_idx = i;
			}
			////double tmp = double(2*count[i] + 3)/(2*count[i] + 1)/list[i];
			//new_num_solutions = num_solutions / (2*count[i] + 1) * (2*count[i] + 3);
			//new_ret = ret * list[i];
			//double tmp = double(new_num_solutions) / new_ret;
			////if(tmp > ratio_add_old)
			//if(max < new_num_solutions)
			//{
			//    //ratio_add_old = tmp;
			//    max = new_num_solutions;
			//    found_idx = i;
			//}
		}
		//double ratio_add_new = 3.0/next_candidate;
		/*new_num_solutions = num_solutions * 3;
		new_ret = ret * next_candidate;
		double ratio_add_new = double(new_num_solutions) / new_ret;*/
		//if(ratio_add_old > ratio_add_new)
		//if(max > new_num_solutions)
		if (min < next_candidate)
		{
			num_solutions = num_solutions / (2 * count[found_idx] + 1);
			count[found_idx]++;
			num_solutions = num_solutions * (2 * count[found_idx] + 1);
			ret *= list[found_idx];
		}
		else
		{
			list.push_back(next_candidate);
			ret *= next_candidate;
			count.push_back(1);
			next_candidate_idx++;
			next_candidate = prime_list[next_candidate_idx];
			num_solutions *= 3;
		}
	}
	return ret;
#endif
}
