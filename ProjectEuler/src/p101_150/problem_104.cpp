//Pandigital Fibonacci ends
//Problem 104
//The Fibonacci sequence is defined by the recurrence relation:
//
//Fn = Fn?1 + Fn?2, where F1 = 1 and F2 = 1.
//It turns out that F541, which contains 113 digits, is the first Fibonacci number for which the last nine digits are 1-9 pandigital (contain all the digits 1 to 9, but not necessarily in order). And F2749, which contains 575 digits, is the first Fibonacci number for which the first nine digits are 1-9 pandigital.
//
//Given that Fk is the first Fibonacci number for which the first nine digits AND the last nine digits are 1-9 pandigital, find k.

#include "template_class.h"

bool check_104(vector<int>& F)
{
    if(F.size() < 18)
    {
        return false;
    }
    vector<int> first_9;
    vector<int> last_9;
    first_9.assign(F.begin(), F.begin() + 9);
    last_9.assign(F.end() - 9, F.end());
    sort(first_9.begin(), first_9.end());
    sort(last_9.begin(), last_9.end());
    for(int i = 0; i < 9; i++)
    {
        if(first_9[i] != i + 1)
        {
            return false;
        }
        if(last_9[i] != i + 1)
        {
            return false;
        }
    }
    return true;
}
double golden_ratio = (1 + sqrt(5))/2;
int problem_104()
{
    vector<int> Fn_2;
    vector<int> Fn_1;
    vector<int> Fn;
    Fn_2.push_back(1);
    Fn_1.push_back(1);
    int i = 3;
    for(;; i++)
    {
        add_two_vector(Fn_2, Fn_1, Fn);
        if(check_104(Fn))
        {
            return i;
        }
        swap(Fn_2, Fn_1);
        swap(Fn_1, Fn);
    }
    return -1;
}
