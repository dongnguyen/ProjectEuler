//Non-bouncy numbers
//Problem 113
//Working from left-to-right if no digit is exceeded by the digit to its left it is called an increasing number; for example, 134468.
//
//Similarly if no digit is exceeded by the digit to its right it is called a decreasing number; for example, 66420.
//
//We shall call a positive integer that is neither increasing nor decreasing a "bouncy" number; for example, 155349.
//
//As n increases, the proportion of bouncy numbers below n increases such that there are only 12951 numbers below one-million that are not bouncy and only 277032 non-bouncy numbers below 1010.
//
//How many numbers below a googol (10100) are not bouncy?

#include "template_class.h"

#define INCREASING  0
#define DECREASING  1
#define BOTH        2

struct int2
{
    int m;
    int n;
    int2(int a, int b)
    {
        m = a;
        n = b;
    }
    int2()
    {
        m = 0;
        n = 0;
    }
};

uint64_t problem_113()
{
    /*vector<vector<int2>> memo;
    memo.resize(100);
    for(int i = 1; i < 10; i++)
    {
        memo[0].push_back(int2(i, BOTH));
    }
    for(int i = 1; i < 100; i++)
    {
        for(int j = 0; j < memo[i-1].size(); j++)
        {
            for(int add = 0; add < 10; add++)
            {
                switch(memo[i-1][j].n)
                {
                    case INCREASING:
                        if(add >= memo[i-1][j].m)
                        {
                            memo[i].push_back(int2(add, INCREASING));
                        }
                        break;
                    case DECREASING:
                        if(add <= memo[i-1][j].m)
                        {
                            memo[i].push_back(int2(add, DECREASING));
                        }
                        break;
                    case BOTH      :
                        if(add > memo[i-1][j].m)
                        {
                            memo[i].push_back(int2(add, INCREASING));
                        }
                        else if(add < memo[i-1][j].m)
                        {
                            memo[i].push_back(int2(add, DECREASING));
                        }
                        else
                        {
                            memo[i].push_back(int2(add, BOTH));
                        }
                        break;
                    default:
                        exit(-1);
                }
            }
        }
    }*/
    uint64_t num_increase[10];
    uint64_t num_decrease[10];
    uint64_t num_both    [10];
    uint64_t tmp_num_increase[10];
    uint64_t tmp_num_decrease[10];

    memset(num_increase, 0, 10*sizeof(uint64_t));
    memset(num_decrease, 0, 10*sizeof(uint64_t));
    memset(num_both    , 0, 10*sizeof(uint64_t));

    memset(tmp_num_increase, 0, 10*sizeof(uint64_t));
    memset(tmp_num_decrease, 0, 10*sizeof(uint64_t));

    for(int i = 1; i < 10; i++)
    {
        num_both[i] = 1;
    }
    uint64_t sum = 0;
    for(int i = 1; i < 100; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            sum += num_increase[j];
            sum += num_decrease[j];
            sum += num_both[j];
        }
        memset(tmp_num_increase, 0, 10*sizeof(uint64_t));
        memset(tmp_num_decrease, 0, 10*sizeof(uint64_t));

        for(int add = 1; add < 10; add++)
        {
            for(int j = 1; j <= add; j++)
            {
                tmp_num_increase[add] += num_increase[j];
            }
        }
        for(int add = 0; add < 10; add++)
        {
            for(int j = 0; j < add; j++)
            {
                tmp_num_increase[add] += num_both[j];
            }
            for(int j = add + 1; j < 10; j++)
            {
                tmp_num_decrease[add] += num_both[j];
            }
        }
        for(int add = 0; add < 10; add++)
        {
            for(int j = add; j < 10; j++)
            {
                tmp_num_decrease[add] += num_decrease[j];
            }
        }
        memcpy(num_increase, tmp_num_increase, 10*sizeof(uint64_t));
        memcpy(num_decrease, tmp_num_decrease, 10*sizeof(uint64_t));
    }
    for(int j = 0; j < 10; j++)
    {
        sum += num_increase[j];
        sum += num_decrease[j];
        sum += num_both[j];
    }
    return sum;
}
