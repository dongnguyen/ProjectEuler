// Same differences
// Problem 135
// Given the positive integers, x, y, and z, are consecutive terms of an arithmetic progression, the least value of the positive integer, n, for which the equation, x2 - y2 - z2 = n, has exactly two solutions is n = 27:

// 342 - 272 - 202 = 122 - 92 - 62 = 27

// It turns out that n = 1155 is the least value which has exactly ten solutions.

// How many values of n less than one million have exactly ten distinct solutions?

#include "template_class.h"

class x_d_135
{
public:
    int x;
    int d;
    x_d_135()
    {
        x = 0;
        d = 0;
    }
    x_d_135(int _x, int _d)
    {
        x = _x;
        d = _d;
    }
    bool operator==(const x_d_135& ref) const
    {
        return (x == ref.x) && (d == ref.d);
    }
    bool operator<(const x_d_135& ref) const
    {
        if (x == ref.x)
        {
            return d < ref.d;
        }
        return x < ref.x;
    }
};

int problem_135(int n)
{
    vector<vector<int>> memo;
    memo.resize(1e6);
    for(int i = 2; i < 1e3; i++)
    {
        int start = i*i;
        for (int j = start; j < 1e6; j += i)
        {
            memo[j].push_back(i);
        }
    }
    int count = 0;
    for(int i = 2; i < 1e6; i++)
    {
        //int c = 0;
        set<x_d_135> tmp;
        if ((1 + i) % 4 == 0)
        {
            int d = (1 + i) / 4;
            if (d < i)
            {
                //c++;
                tmp.insert(x_d_135(i, d));
            }
        }
        for (int j = 0; j < memo[i].size(); j++)
        {
            int a = memo[i][j];
            int b = i / a;
            if (a == b)
            {
                if ((b + a) % 4 == 0)
                {
                    int d = (a + b) / 4;
                    if (d < a)
                    {
                        //c++;
                        tmp.insert(x_d_135(a, d));
                    }
                }
            }
            else
            {
                if ((b + a) % 4 == 0)
                {
                    int d = (a + b) / 4;
                    if (d < a)
                    {
                        //c++;
                        tmp.insert(x_d_135(a, d));
                    }
                    if (d < b)
                    {
                        //c++;
                        tmp.insert(x_d_135(b, d));
                    }
                }
            }
        }
        //if (c == n)
        if (tmp.size() == n)
        {
            count++;
        }
    }
    return count;
#if 0
    vector<int> memo;
    memo.resize(1e6);
    int c = 0;
    for (int64_t x = 3;; x++)
    {
        c++;
        if (c == 10000)
        {
            cout << x << endl;
            c = 0;
        }
        bool found = false;
        int64_t x2 = x*x;
        for (int64_t d = 1; d < ((x+1)/2); d++)
        {
            /*int64_t y = x - d;
            int64_t y2 = y*y;

            int64_t z = y - d;
            int64_t z2 = z*z;
            int64_t tmp = x2 - y2 - z2;*/
            int64_t tmp = 4*x*d - x2;
            if (tmp >= 0 && tmp < 1e6)
            {
                //if (tmp == 27)
                {
                    found = true;
                    memo[tmp]++;
                }
            }
        }
        if (found == false)
        {
            break;
        }
    }
    int count = 0;
    for (int i = 0; i < 1e6; i++)
    {
        if (memo[i] == n)
        {
            count++;
        }
    }
    return count;
#endif
}