#include "template_class.h"
#include <string>

extern int sumSet(vector<int>& set);
extern void debugSet(vector<int> set);
extern void printSetString(vector<int> set);

extern uint64_t factorial(int n);

uint64_t numCombination(int k, int n)
{
	if (k == 0)
	{
		return 1;
	}
	return(factorial(n) / factorial(k) / factorial(n - k));
	return 0;
}

extern bool isDisJoin(vector<int>& setA, vector<int>& setB);
extern void generateAllSubSet(vector<int>& set, vector < vector < int >>& subsets);
extern int sumSet(vector<int>& set);

bool allIndexLower(vector<int>& setA, vector<int>& setB)
{
	for (int i = 0; i < setA.size(); i++)
	{
		if (setA[i] > setB[i])
		{
			return false;
		}
	}
	return true;
}

int numNeedTest(int n)
{
	if (n < 3)
	{
		return 0;
	}
	vector < int > optimum_set;
	optimum_set.resize(n);
	for (int i = 0; i < n; i++)
	{
		optimum_set[i] = i;
	}
	vector < vector < int >> subsets;
	generateAllSubSet(optimum_set, subsets);
	int size = subsets.size();
	int sum = 0;
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (subsets[i].size() == subsets[j].size())
			{
				if (isDisJoin(subsets[i], subsets[j]))
				{
					if (!allIndexLower(subsets[i], subsets[j]))
					{
						sum++;
					}
				}
			}
		}
	}
	return sum;
}

std::string problem_106()
{
	std::string ret;
	for (int n = 1; n <= 12; n++)
	{
		cout << "n = " << n << ": " << numNeedTest(n) << endl;
	}
	return ret;
}
