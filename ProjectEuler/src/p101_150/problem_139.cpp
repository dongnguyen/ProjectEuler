//Pythagorean tiles
//Problem 139
//Let (a, b, c) represent the three sides of a right angle triangle with integral length sides. It is possible to place four such triangles together to form a square with length c.
//
//For example, (3, 4, 5) triangles can be placed together to form a 5 by 5 square with a 1 by 1 hole in the middle and it can be seen that the 5 by 5 square can be tiled with twenty-five 1 by 1 squares.
//
//
//However, if (5, 12, 13) triangles were used then the hole would measure 7 by 7 and these could not be used to tile the 13 by 13 square.
//
//Given that the perimeter of the right triangle is less than one-hundred million, how many Pythagorean triangles would allow such a tiling to take place?

#include "template_class.h"

extern int gcd(int a, int b);

int problem_139(int p)
{
    int limit = sqrt(p/2);
    int p_2 = p / 2;
    int count = 0;
    for(int n = 1; n < limit - 1; n++)
    {
        for(int m = n + 1;; m += 2)
        {
            if(gcd(n, m) == 1)
            {
                if(m*(m+n) > p_2)
                {
                    break;
                }
                int a = m*m - n*n;
                int b = 2*m*n;
                int c = m*m + n*n;
                int s = abs(a - b);
                int l = a + b + c;
                if((c % s) == 0)
                {
                    count += p / l;
                }
                //cout << "[" << a << ", " << b << ", " << c << "]" << endl;
            }
        }
    }
    return count;
}
