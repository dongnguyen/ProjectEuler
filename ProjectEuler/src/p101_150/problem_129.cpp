// Repunit divisibility
// Problem 129
// A number consisting entirely of ones is called a repunit. We shall define R(k) to be a repunit of length k; for example, R(6) = 111111.

// Given that n is a positive integer and GCD(n, 10) = 1, it can be shown that there always exists a value, k, for which R(k) is divisible by n, and let A(n) be the least such value of k; for example, A(7) = 6 and A(41) = 5.

// The least value of n for which A(n) first exceeds ten is 17.

// Find the least value of n for which A(n) first exceeds one-million.

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

extern int totient(int n);
extern int large_mode(int a, int b, int m);
extern int count_num_digit(int n);

#define LIMIT_129 (1000023 + 100)
vector<int> totient_129;

int least_k_129(int p)
{
    //p = p*9;
    //int to_p = totient(p)*6;
    int to_p = totient_129[p] * 6;
    //mpz_class tmp = pow_a_b_mpz(10, int(1e9) % to_p);
    //mpz_class mod = tmp % p;
    //for(int k = count_num_digit(p) + 1; ; k++)
    uint64_t m = 9 * p;
    uint64_t tmp = 10;
    for (int k = 1; k < to_p; k++)
    {
        if (tmp > m)
            tmp %= m;
        if (tmp == 1)
        {
            return k;
        }
        tmp *= 10;
    }
    return 0;
}

int problem_129(int x)
{
    create_prime_filter_method(LIMIT_129);
    totient_129.resize(LIMIT_129);
    for (int i = 1; i < LIMIT_129; i++)
    {
        totient_129[i] = i;
    }
    for (int i = 0; prime_list[i] < sqrt(LIMIT_129); i++)
    {
        int p = prime_list[i];
        for (int j = p; j < LIMIT_129; j+=p)
        {
            totient_129[j] /= p;
            totient_129[j] *= p-1;
        }
    }
    //int d1 = least_k_129(15);
    //int d2 = least_k_129(27);
    vector<int> memo;
    memo.resize(LIMIT_129);
    memo[1] = 1;
    //int count = 0;
    for (int i = 3; i < LIMIT_129;)
    {
        /*count++;
        if(count == 1000)
        {
            cout << i << endl;
            count = 0;
        }*/
        int t = 0;

        /*==================================*/
        if (memo[i])
        {
            if (memo[i] > x)
            {
                return i;
            }
        }
        else
        {
            t = least_k_129(i);
            memo[i] = t;
            if (t > x)
            {
                return i;
            }
            int mul = 1;
            int p = i;
            for (; p < LIMIT_129;)
            {
                if (memo[mul])
                {
                    int xx = memo[mul];
                    memo[p] = t / gcd(t, xx) * xx;
                }
                else
                {
                    int xx = least_k_129(mul);
                    memo[mul] = xx;
                    memo[p] = t / gcd(t, xx) * xx;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 4;
                p += 4 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
            }
        }
        /*==================================*/
        i += 4;
        /*==================================*/
        if (memo[i])
        {
            if (memo[i] > x)
            {
                return i;
            }
        }
        else
        {
            t = least_k_129(i);
            memo[i] = t;
            if (t > x)
            {
                return i;
            }
            int mul = 1;
            int p = i;
            for (; p < LIMIT_129;)
            {
                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 4;
                p += 4 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
            }
        }
        /*==================================*/
        i += 2;
        /*==================================*/
        if (memo[i])
        {
            if (memo[i] > x)
            {
                return i;
            }
        }
        else
        {
            t = least_k_129(i);
            memo[i] = t;
            if (t > x)
            {
                return i;
            }
            int mul = 1;
            int p = i;
            for (; p < LIMIT_129;)
            {
                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 4;
                p += 4 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
            }
        }
        /*==================================*/
        i += 2;
        /*==================================*/
        if (memo[i])
        {
            if (memo[i] > x)
            {
                return i;
            }
        }
        else
        {
            t = least_k_129(i);
            memo[i] = t;
            if (t > x)
            {
                return i;
            }
            int mul = 1;
            int p = i;
            for (; p < LIMIT_129;)
            {
                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 4;
                p += 4 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
                if (p >= LIMIT_129)
                {
                    break;
                }

                if (memo[mul])
                {
                    int x = memo[mul];
                    memo[p] = t / gcd(t, x) * x;
                }
                else
                {
                    int x = least_k_129(mul);
                    memo[mul] = x;
                    memo[p] = t / gcd(t, x) * x;
                }
                mul += 2;
                p += 2 * i;
            }
        }
        /*==================================*/
        i += 2;
    }
    //int a = least_k_129(7);
    //int b = least_k_129(41);
    return 0;
}