//Palindromic sums
//Problem 125
//The palindromic number 595 is interesting because it can be written as the sum of consecutive squares: 62 + 72 + 82 + 92 + 102 + 112 + 122.
//
//There are exactly eleven palindromes below one-thousand that can be written as consecutive square sums, and the sum of these palindromes is 4164. Note that 1 = 02 + 12 has not been included as this problem is concerned with the squares of positive integers.
//
//Find the sum of all the numbers less than 108 that are both palindromic and can be written as the sum of consecutive squares.

#include "template_class.h"

extern bool check_palindromic(int x);

uint64_t problem_125()
{
    int n = 1e8;
    int sqrt_n = sqrt(n);
    int limit2 = sqrt(n/2);
    vector<int> squares;
    set<int> results;
    squares.resize(sqrt_n);
    for(int i = 0; i < sqrt_n; i++)
    {
        squares[i] = i*i;
    }
    for(int i = 1; i < limit2; i++)
    {
        int idx = i + 1;
        int sum = squares[i] + squares[idx];
        while(sum < n)
        {
            results.insert(sum);
            idx++;
            sum += squares[idx];
        }
    }
    uint64_t sum = 0;
    for(set<int>::iterator i = results.begin(); i != results.end(); ++i)
    {
        if(check_palindromic(*i))
        {
            sum += *i;
        }
    }
    return sum;
}
