
#include "template_class.h"

#define LIMIT_136 50e6
#define LIMIT2_136 1

class x_d_136
{
public:
    int x;
    int d;
    x_d_136()
    {
        x = 0;
        d = 0;
    }
    x_d_136(int _x, int _d)
    {
        x = _x;
        d = _d;
    }
    bool operator==(const x_d_136& ref) const
    {
        return (x == ref.x) && (d == ref.d);
    }
    bool operator<(const x_d_136& ref) const
    {
        if (x == ref.x)
        {
            return d < ref.d;
        }
        return x < ref.x;
    }
};

#define INVALID 0xCDCDCDCD

int problem_136()
{
    //vector<vector<int>> memo;
    vector<uint64_t> memo;
    memo.resize(LIMIT_136);
    for (int i = 2; i < sqrt(LIMIT_136); i++)
    {
        int start = i*i;
        int x = i;
        for (int j = start; j < LIMIT_136; j += i)
        {
            if ((INVALID != memo[j]))
            {
                //j = i * (j/i);
                if ((x + i) % 4 == 0)
                {
                    int d = (x + i) / 4;
                    if (d < x)
                    {
                        if (memo[j] == 0)
                        {
                            memo[j] = uint64_t(x) << 32 | uint64_t(d);
                        }
                        else
                        {
                            if ((memo[j] >> 32 == x) && ((memo[j] & 0xffffffff) == d))
                            {

                            }
                            else
                            {
                                memo[j] = INVALID;
                            }
                        }
                    }
                    if (d < i)
                    {
                        if (memo[j] == 0)
                        {
                            memo[j] = uint64_t(i) << 32 | uint64_t(d);
                        }
                        else
                        {
                            if ((memo[j] >> 32 == i) && ((memo[j] & 0xffffffff) == d))
                            {

                            }
                            else
                            {
                                memo[j] = INVALID;
                            }
                        }
                    }
                }
            }
            x++;
        }
    }
    //cout << "here" << endl;
    int count = 0;
    for (int i = 2; i < LIMIT_136; i++)
    {
        if ((INVALID != memo[i]))
        {
            if ((1 + i) % 4 == 0)
            {
                int d = (1 + i) / 4;
                if (d < i)
                {
                    if (memo[i] == 0)
                    {
                        memo[i] = uint64_t(i) << 32 | uint64_t(d);
                    }
                    else
                    {
                        if ((memo[i] >> 32 == i) && ((memo[i] & 0xffffffff) == d))
                        {

                        }
                        else
                        {
                            memo[i] = INVALID;
                        }
                    }
                }
            }
        }
        if ((INVALID != memo[i]) && (memo[i]))
        {
            count++;
        }
    }
    /*int count = 0;
    for (int i = 2; i < LIMIT_136; i++)
    {
        int c = 0;
        if ((1 + i) % 4 == 0)
        {
            int d = (1 + i) / 4;
            if (d < i)
            {
                c++;
            }
        }
        for (int j = 0; j < memo[i].size(); j++)
        {
            int a = memo[i][j];
            int b = i / a;
            if (a == b)
            {
                if ((b + a) % 4 == 0)
                {
                    int d = (a + b) / 4;
                    if (d < a)
                    {
                        c++;
                        if (c > 1)
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                if ((b + a) % 4 == 0)
                {
                    int d = (a + b) / 4;
                    if (d < a)
                    {
                        c++;
                        if (c > 1)
                        {
                            break;
                        }
                    }
                    if (d < b)
                    {
                        c++;
                        if (c > 1)
                        {
                            break;
                        }
                    }
                }
            }
        }
        if (c == 1)
        {
            count++;
        }
    }*/
    return count;
}