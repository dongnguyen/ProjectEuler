//Large repunit factors
//Problem 132
//A number consisting entirely of ones is called a repunit. We shall define R(k) to be a repunit of length k.
//
//For example, R(10) = 1111111111 = 11�41�271�9091, and the sum of these prime factors is 9414.
//
//Find the sum of the first forty prime factors of R(109).

#include "template_class.h"
#include "mpirxx.h"

extern void create_prime_filter_method(int n, bool need_set = false);
extern vector<int> prime_list;
vector<vector<mpz_class>> memo_132;

#define LIMIT_132 2e6

vector<int> factory_132(int n)
{
    char *s = new char[n+1];
    memset(s, '1', n);
    s[n] = '\0';
    mpz_class t(s);
    delete []s;

    vector<int> ret;
    for(int i = 0; i < prime_list.size(); i++)
    {
        if(t % prime_list[i] == 0)
        {
            ret.push_back(prime_list[i]);
            do
            {
                t /= prime_list[i];
            }
            while(t % prime_list[i] == 0);
            if(t == 1)
            {
                break;
            }
        }
    }
    if(t != 1)
    {
        if(sqrt(t) > LIMIT_132)
        {
            cout << "not enough" << endl;
            exit(-1);
        }
        ret.push_back(t.get_ui());
    }
    return ret;
}

vector<mpz_class> factory_132(vector<int>& all, int idx)
{
    vector<mpz_class> ret;
    int i = idx - 1;
    for(; i >= 0; i--)
    {
        if(all[idx] % all[i] == 0)
        {
            break;
        }
    }
    if(i >= 0)
    {
        ret = memo_132[i];

        char *s = new char[all[i]+1];
        memset(s, '1', all[i]);
        s[all[i]] = '\0';
        mpz_class old(s);
        delete []s;

        s = new char[all[idx]+1];
        memset(s, '1', all[idx]);
        s[all[idx]] = '\0';
        mpz_class t(s);
        delete []s;

        t /= old;

        for(int i = 0; i < prime_list.size(); i++)
        {
            if(t % prime_list[i] == 0)
            {
                ret.push_back(prime_list[i]);
                do
                {
                    t /= prime_list[i];
                }
                while(t % prime_list[i] == 0);
                if(t == 1)
                {
                    break;
                }
            }
        }
        if(t != 1)
        {
            if(sqrt(t) > LIMIT_132)
            {
                cout << t << endl;
                cout << "not enough" << endl;
                //exit(-1);
                return ret;
            }
            ret.push_back(t);
        }

        sort(ret.begin(), ret.end());
        memo_132[idx] = ret;
        return ret;
    }

    int n = all[idx];

    char *s = new char[n+1];
    memset(s, '1', n);
    s[n] = '\0';
    mpz_class t(s);
    delete []s;

    for(int i = 0; i < prime_list.size(); i++)
    {
        if(t % prime_list[i] == 0)
        {
            ret.push_back(prime_list[i]);
            do
            {
                t /= prime_list[i];
            }
            while(t % prime_list[i] == 0);
            if(t == 1)
            {
                break;
            }
        }
    }
    if(t != 1)
    {
        if(sqrt(t) > LIMIT_132)
        {
            cout << t << endl;
            cout << "not enough" << endl;
            //exit(-1);
            return ret;
        }
        ret.push_back(t);
    }
    return ret;
}

extern int count_num_digit(int n);

bool verify_132(vector<int>& all, int p)
{
    int num_d_p = count_num_digit(p);
    int idx = 0;
    for(; idx < all.size(); idx++)
    {
        if(all[idx] > num_d_p)
        {
            break;
        }
    }
    for(; idx < all.size(); idx++)
    {
        if(all[idx] > 1e5)
        {
            break;
        }
        char *s = new char[all[idx] + 1];
        memset(s, '1', all[idx]);
        s[all[idx]] = '\0';
        mpz_class t(s);
        delete []s;
        if(t % p ==0)
        {
            return true;
        }
    }
    return false;
}

vector<int> all_divisor_of_1e9()
{
    vector<int> ret;
#if 1
    for(int i = 0; i <= 9; i++)
    {
        int a = pow(2, i);
        for(int j = 0; j <= 9; j++)
        {
            int tmp = a * pow(5, j);
            ret.push_back(tmp);
        }
    }
#endif
    /*for(int i = 0; i <= 9; i++)
    {
        int tmp = pow(2, i);
        ret.push_back(tmp);
    }
    for(int i = 0; i <= 9; i++)
    {
        int tmp = pow(5, i);
        ret.push_back(tmp);
    }*/
    sort(ret.begin(), ret.end());
    return ret;
}

extern int totient(int n);
extern mpz_class pow_a_b_mpz(int a, int b);
extern int large_mode(int a, int b, int m);

bool verify_132(int p)
{
    int to_p = totient(p);
    //mpz_class tmp = pow_a_b_mpz(10, int(1e9) % to_p);
    //mpz_class mod = tmp % p;
    int mod = large_mode(10, int(1e9) % to_p, p);
    if(mod == 1)
    {
        return true;
    }
    return false;
}

int problem_132()
{
    create_prime_filter_method(LIMIT_132);
    int sum = 0;
    int count = 0;
    cout << "==========================" << endl;
    cout << "All primes:" << endl;
    for(int i = 4; i < prime_list.size(); i++)
    {
        if(verify_132(prime_list[i]))
        {
            sum += prime_list[i];
            cout << prime_list[i] << endl;
            count++;
            if(count == 40)
            {
                break;
            }
        }
    }
    cout << "==========================" << endl;
    return sum;

    //vector<int> ret;
    //ret.resize(40);
    set<mpz_class> ret;

    vector<int> all = all_divisor_of_1e9();
    memo_132.resize(all.size());
    for(int i = 0; i < all.size(); i++)
    {
        memo_132[i] = factory_132(all, i);
        for(int j = 0; j < memo_132[i].size(); j++)
        {
            ret.insert(memo_132[i][j]);
            /*if(ret.size() == 40)
            {
                cout << "R(" << all[i] << ")" << endl;
                mpz_class tmp = 0;
                set<mpz_class>::iterator ite = ret.begin();
                cout << "==========================" << endl;
                cout << "All primes:" << endl;
                for(; ite != ret.end(); ite++)
                {
                    cout << *ite << endl;
                    tmp += *ite;
                }
                cout << tmp << endl;
                return -1;
            }*/
        }
#if 1
        if(all[i] == 4000)
        {
            cout << "R(" << all[i] << ")" << endl;
            mpz_class tmp = 0;
            set<mpz_class>::iterator ite = ret.begin();
            cout << "==========================" << endl;
            cout << "All primes:" << endl;
            for(; ite != ret.end(); ite++)
            {
                cout << *ite << endl;
                tmp += *ite;
            }
            cout << tmp << endl;
            return -1;
        }
#endif
    }
    /*mpz_class tmp = 0;
    set<mpz_class>::iterator ite = ret.begin();
    cout << "==========================" << endl;
    cout << "All primes:" << endl;
    for(; ite != ret.end(); ite++)
    {
        cout << *ite << endl;
        tmp += *ite;
    }
    cout << tmp << endl;
    return -1;*/
    /*vector<int> num_1;
    num_1.push_back(16);
    num_1.push_back(20);
    num_1.push_back(25);

    vector<int> t1 = factory_132(32);
    vector<int> t2 = factory_132(20);
    vector<int> t3 = factory_132(25);*/
#if 0
    int idx = 0;
    vector<int> all = all_divisor_of_1e9();
    bool found = false;
    for(int i = 0; i < prime_list.size(); i++)
    {
        int p = prime_list[i];
        /*if(verify_132(all, p))
        {
            ret[idx] = p;
            cout << idx << ": " << p << endl;
            idx++;
            if(idx == 40)
            {
                found = true;
                goto END;
            }
        }*/
        switch(p % 10)
        {
        case 1:
        case 3:
        case 7:
        case 9:
            if(verify_132(all, p))
            {
                ret[idx] = p;
                cout << idx << ": " << p << endl;
                idx++;
                if(idx == 40)
                {
                    found = true;
                    goto END;
                }
            }
            break;
        default:
            break;
        }
    }
END:
    uint64_t sum = 0;
    for(int i = 0; i < 40; i++)
    {
        sum += ret[i];
    }
    cout << (found ? "found!" : "false result ") << sum << endl;
#endif
    return 0;
}