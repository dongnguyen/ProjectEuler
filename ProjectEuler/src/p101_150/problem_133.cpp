//Repunit nonfactors
//Problem 133
//A number consisting entirely of ones is called a repunit.We shall define R(k) to be a repunit of length k; for example, R(6) = 111111.
//
//Let us consider repunits of the form R(10n).
//
//Although R(10), R(100), or R(1000) are not divisible by 17, R(10000) is divisible by 17. Yet there is no value of n for which R(10n) will divide by 19. In fact, it is remarkable that 11, 17, 41, and 73 are the only four primes below one - hundred that can be a factor of R(10n).
//
//Find the sum of all the primes below one - hundred thousand that will never be a factor of R(10n).

#include "template_class.h"
#include "mpirxx.h"

extern vector<int> prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

#define LIMIT_133 100000

int least_k_133(int p)
{
    int to_p = (p - 1)*6;
    uint64_t m = p*9;
    uint64_t tmp = 10;
    for (int k = 1; k < to_p; k++)
    {
        if (tmp > m)
            tmp %= m;
        if (tmp == 1)
        {
            return k;
        }
        tmp *= 10;
    }
    return 0;
}

bool check_133(int n)
{
    if (n == 0)
    {
        return false;
    }
    if (n == 1)
    {
        return true;
    }
#if 0
    int sqrt_n = sqrt(n);
    if (n % 3 == 0)
    {
        return false;
    }
    for (int i = 3; prime_list[i] <= sqrt_n; i++)
    {
        if (n % prime_list[i] == 0)
        {
            return false;
        }
    }
    return true;
#endif
    while (n % 2 == 0)
    {
        n >>= 1;
    }
    while (n % 5 == 0)
    {
        n /= 5;
    }
    if (n == 1)
    {
        return true;
    }
    return false;
}

mpz_class problem_133()
{
    create_prime_filter_method(LIMIT_133);
    int size = prime_list.size();
    mpz_class sum = 2;
    //int count = 0;
    for (int i = 1; i < size; i++)
    {
        if (!check_133(least_k_133(prime_list[i])))
        {
            sum += prime_list[i];
            //cout << prime_list[i] << endl;
            /*count++;
            if (count == 10)
            {
                exit(0);
            }*/
        }
    }
    return sum;
}