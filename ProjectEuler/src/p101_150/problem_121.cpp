//Square remainders
//Problem 120
//Let r be the remainder when (a?1)n + (a+1)n is divided by a2.
//
//For example, if a = 7 and n = 3, then r = 42: 63 + 83 = 728 ? 42 mod 49. And as n varies, so too will r, but for a = 7 it turns out that rmax = 42.
//
//For 3 ? a ? 1000, find ? rmax.

#include "template_class.h"

int problem_121()
{
    uint64_t sum = 0;
    for(int a = 3; a <= 1000; a++)
    {
        //int a2 = a*a;
        int max = 2;
        //for(int i = 1; i < 2*a; i += 2)
        {
            //int k = (a / 2 - 1) / 2;
            //int tmp = 2*i*a % a2;
            //int tmp = 2*(2*k + 1)*a;
            int tmp = (a & 1) ? (a - 1)*a : (a - 2)*a;
            if(max < tmp)
            {
                max = tmp;
            }
        }
        sum += max;
    }
    return sum;
}
