//Counting block combinations I
//Problem 114
//A row measuring seven units in length has red blocks with a minimum length of three units placed on it, such that any two red blocks (which are allowed to be different lengths) are separated by at least one black square. There are exactly seventeen ways of doing this.
//
//How many ways can a row measuring fifty units in length be filled?
//
//NOTE: Although the example above does not lend itself to the possibility, in general it is permitted to mix block sizes. For example, on a row measuring eight units in length you could use red (3), black (1), and red (4).

#include "template_class.h"

vector<uint64_t> memo_115;

uint64_t count_115(int len)
{
    if(3 > len)
    {
        return 1;
    }
    if(memo_115[len])
    {
        return memo_115[len];
    }
    uint64_t sum = 1;
    for(int b = 3; b <= len; b++)
    {
        for(int i = 0; i <= (len - b); i++)
        {
            sum += count_115(len - b - i - 1);
        }
    }
    memo_115[len] = sum;
    return sum;
}

uint64_t problem_115(int n)
{
    //n = 7;
    memo_115.resize(n+1);
    uint64_t d = 0;
    d = count_115(n);
    return d;
}
