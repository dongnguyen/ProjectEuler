
#include "template_class.h"

__inline int gcd(int m, int n, int p, int q)
{
    return gcd(gcd(gcd(m, n), p), q);
}

bool is_square(int n)
{
    int tmp = sqrt(n);
    if (n == tmp * tmp)
    {
        return true;
    }
    return false;
}

bool verify(int x, int y, int z)
{
    /*if (!is_square(x + y))
    {
        return false;
    }
    if (!is_square(x - y))
    {
        return false;
    }*/
    if (!is_square(x + z))
    {
        return false;
    }
    if (!is_square(x - z))
    {
        return false;
    }
    /*if (!is_square(y + z))
    {
        return false;
    }
    if (!is_square(y - z))
    {
        return false;
    }*/
    return true;
}

uint64_t problem_142()
{
#if 0
    vector<uint64_t> memo;
    set<uint64_t> memos;
    int limit_x = 1e7;
    //memo.resize(limit_x*2);
    //memo[1] = 1;
    for(uint64_t i = 2; i < 2*limit_x; i++)
    {
        //memo[i] = i * i;
        memos.insert(i * i);
    }
    for(int x = 3; x <= limit_x; x++)
    {
        for(int y = x - 1; y > 1; y--)
        {
            for(int z = y - 1; z > 0; z--)
            {
                uint64_t s1 = x + y;
                uint64_t s2 = z + y;
                uint64_t s3 = x + z;
                uint64_t d1 = x - y;
                uint64_t d2 = x - z;
                uint64_t d3 = y - z;
                if(memos.find(s1) != memos.end())
                {
                    if (memos.find(s2) != memos.end())
                    {
                        if (memos.find(s3) != memos.end())
                        {
                            if (memos.find(d1) != memos.end())
                            {
                                if (memos.find(d2) != memos.end())
                                {
                                    if (memos.find(d3) != memos.end())
                                    {
                                        return x + y + z;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
#endif
#if 1
    int limit_x = 1e7;
    vector<int> m2_n2;
    m2_n2.resize(limit_x);
    vector<uint64_t> memo;
    memo.resize(limit_x * 2);
    memo[1] = 1;
    for (uint64_t i = 2; i < 2 * limit_x; i++)
    {
        memo[i] = i * i;
    }
    for (int m = 1; m <= sqrt(limit_x); m++)
    {
        for (int n = m; n <= sqrt(limit_x); n++)
        {
            if (memo[m] + memo[n] < limit_x)
            {
                m2_n2[memo[m] + memo[n]]++;
            }
        }
    }
    int limit_quad = 300;
    int count = 0;
    for (int m = 1; m <= limit_quad; m++)
    {
        for (int n = 1; n <= limit_quad; n++)
        {
            for (int p = 1; p <= limit_quad; p++)
            {
                for (int q = 1; q <= limit_quad; q++)
                {
                    if ((n*q > m*p) && (memo[m] + memo[n] > memo[p] + memo[q]) && gcd(m, n, p, q) == 1 && ((m + n + p + q) & 1))
                    {
                        //cout << "[" << m << ", " << n << ", " << p << ", " << q << "]" << endl;
                        //count++;
                        int mul = 1;
                        while (1)
                        {
                            uint64_t k = (memo[m] + memo[n] + memo[p] + memo[q])*memo[mul];
                            uint64_t a = (memo[m] + memo[n] - memo[p] - memo[q])*memo[mul];
                            uint64_t b = (2 * (m*q + n*p))*memo[mul];
                            uint64_t c = (2 * (n*q - m*p))*memo[mul];
                            if (a < b)
                            {
                                swap(a, b);
                            }
                            if (a < c)
                            {
                                swap(a, c);
                            }
                            if (b < c)
                            {
                                swap(b, c);
                            }
                            uint64_t sum_k_l = memo[k] + memo[a];
                            int fail = 0;
                            if (sum_k_l < limit_x)
                            {
                                if (m2_n2[sum_k_l] > 1)
                                {
                                    // l = a
                                    if ((((k + a) & 1) == 0) && (((b + c) & 1) == 0))
                                    {
                                        uint64_t x = (memo[k] + memo[a]) / 2;
                                        uint64_t y = (memo[b] + memo[c]) / 2;
                                        uint64_t z = (memo[b] - memo[c]) / 2;
                                        if (verify(x, y, z))
                                        {
                                            cout << "[" << x << ", " << y << ", " << z << "] = " << x + y + z << endl;
                                        }
                                        else
                                        {
                                            //cout << "failed" << endl;
                                        }
                                        /*cout << "[" << k << ", " << a << ", " << b << ", " << c << "]" << endl;
                                        cout << "[" << x << ", " << y << ", " << z << "] = " << x + y + z << endl;
                                        exit(0);*/
                                    }
                                }
                            }
                            else
                            {
                                fail++;
                            }
                            sum_k_l = memo[k] + memo[b];
                            if (sum_k_l < limit_x)
                            {
                                if (m2_n2[sum_k_l] > 1)
                                {
                                    // l = b
                                    if ((((k + b) & 1) == 0) && (((a + c) & 1) == 0))
                                    {
                                        uint64_t x = (memo[k] + memo[b]) / 2;
                                        uint64_t y = (memo[a] + memo[c]) / 2;
                                        uint64_t z = (memo[a] - memo[c]) / 2;
                                        if (verify(x, y, z))
                                        {
                                            cout << "[" << x << ", " << y << ", " << z << "] = " << x + y + z << endl;
                                        }
                                        else
                                        {
                                            //cout << "failed" << endl;
                                        }
                                        /*cout << "[" << k << ", " << b << ", " << a << ", " << c << "]" << endl;
                                        cout << "[" << x << ", " << y << ", " << z << "] = " << x + y + z << endl;
                                        exit(0);*/
                                    }
                                }
                            }
                            else
                            {
                                fail++;
                            }
                            if (fail == 2)
                            {
                                break;
                            }
                            mul++;
                        }
                    }
                }
            }
        }
    }
    return count;
#endif
}