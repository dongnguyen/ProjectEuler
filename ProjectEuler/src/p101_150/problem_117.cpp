//Red, green or blue tiles
//Problem 116
//A row of five black square tiles is to have a number of its tiles replaced with coloured oblong tiles chosen from red (length two), green (length three), or blue (length four).
//
//If red tiles are chosen there are exactly seven ways this can be done.
//
//If green tiles are chosen there are three ways.
//
//And if blue tiles are chosen there are two ways.
//
//
//
//Assuming that colours cannot be mixed there are 7 + 3 + 2 = 12 ways of replacing the black tiles in a row measuring five units in length.
//
//How many different ways can the black tiles in a row measuring fifty units in length be replaced if colours cannot be mixed and at least one coloured tile must be used?
//
//NOTE: This is related to problem 117.

#include "template_class.h"

#define OFFSET 2

vector<vector<uint64_t>> memo_117;
//int speed_up = 0;
//int non_speed_up = 0;

uint64_t count_117(int len, int block)
{
    if(memo_117[block - OFFSET][len])
    {
        //speed_up++;
        return memo_117[block - OFFSET][len];
    }
    //non_speed_up++;
    if(block > len)
    {
        return 1;
    }
    uint64_t sum = 1;
    for(int i = 0; i <= (len - block); i++)
    {
        sum += count_117(len - block - i, block);
    }
    memo_117[block - OFFSET][len] = sum;
    return sum;
}

uint64_t problem_117(int n)
{
    memo_117.resize(3);
    memo_117[0].resize(n+1);
    memo_117[1].resize(n+1);
    memo_117[2].resize(n+1);
    uint64_t d = 0;
    d += count_117(n, 2) - 1;
    d += count_117(n, 3) - 1;
    d += count_117(n, 4) - 1;
    //cout << speed_up << " - " << non_speed_up << endl;
    return d;
}
