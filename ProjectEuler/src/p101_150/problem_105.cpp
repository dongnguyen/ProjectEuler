//Special subset sums : testing
//Problem 105
//Published on Saturday, 24th September 2005, 12 : 00 am; Solved by 5322; Difficulty rating : 45 %
//Let S(A) represent the sum of elements in set A of size n.We shall call it a special sum set if for any two non - empty disjoint subsets, B and C, the following properties are true :
//
//	S(B) ≠ S(C); that is, sums of subsets cannot be equal.
//	If B contains more elements than C then S(B) > S(C).
//	For example, { 81, 88, 75, 42, 87, 84, 86, 65 } is not a special sum set because 65 + 87 + 88 = 75 + 81 + 84, whereas{ 157, 150, 164, 119, 79, 159, 161, 139, 158 } satisfies both rules for all possible subset pair combinations and S(A) = 1286.
//
//	Using sets.txt(right click and "Save Link/Target As..."), a 4K text file with one - hundred sets containing seven to twelve elements(the two examples given above are the first two sets in the file), identify all the special sum sets, A1, A2, ..., Ak, and find the value of S(A1) + S(A2) + ... + S(Ak).
//
//	NOTE: This problem is related to Problem 103 and Problem 106.
//T(1,1)=1; T(n,1)=T(n-1,[(n+1)/2]); T(n,k)=T(n,1)+T(n-1,k-1) for k>1.

#include "template_class.h"
#include <string>

int A1[] = { 81,88,75,42,87,84,86,65 };
int A2[] = { 157,150,164,119,79,159,161,139,158 };
int A3[] = { 673,465,569,603,629,592,584,300,601,599,600 };
int A4[] = { 90,85,83,84,65,87,76,46 };
int A5[] = { 165,168,169,190,162,85,176,167,127 };
int A6[] = { 224,275,278,249,277,279,289,295,139 };
int A7[] = { 354,370,362,384,359,324,360,180,350,270 };
int A8[] = { 599,595,557,298,448,596,577,667,597,588,602 };
int A9[] = { 175,199,137,88,187,173,168,171,174 };
int A10[] = { 93,187,196,144,185,178,186,202,182 };
int A11[] = { 157,155,81,158,119,176,152,167,159 };
int A12[] = { 184,165,159,166,163,167,174,124,83 };
int A13[] = { 1211,1212,1287,605,1208,1189,1060,1216,1243,1200,908,1210 };
int A14[] = { 339,299,153,305,282,304,313,306,302,228 };
int A15[] = { 94,104,63,112,80,84,93,96 };
int A16[] = { 41,88,82,85,61,74,83,81 };
int A17[] = { 90,67,84,83,82,97,86,41 };
int A18[] = { 299,303,151,301,291,302,307,377,333,280 };
int A19[] = { 55,40,48,44,25,42,41 };
int A20[] = { 1038,1188,1255,1184,594,890,1173,1151,1186,1203,1187,1195 };
int A21[] = { 76,132,133,144,135,99,128,154 };
int A22[] = { 77,46,108,81,85,84,93,83 };
int A23[] = { 624,596,391,605,529,610,607,568,604,603,453 };
int A24[] = { 83,167,166,189,163,174,160,165,133 };
int A25[] = { 308,281,389,292,346,303,302,304,300,173 };
int A26[] = { 593,1151,1187,1184,890,1040,1173,1186,1195,1255,1188,1203 };
int A27[] = { 68,46,64,33,60,58,65 };
int A28[] = { 65,43,88,87,86,99,93,90 };
int A29[] = { 83,78,107,48,84,87,96,85 };
int A30[] = { 1188,1173,1256,1038,1187,1151,890,1186,1184,1203,594,1195 };
int A31[] = { 302,324,280,296,294,160,367,298,264,299 };
int A32[] = { 521,760,682,687,646,664,342,698,692,686,672 };
int A33[] = { 56,95,86,97,96,89,108,120 };
int A34[] = { 344,356,262,343,340,382,337,175,361,330 };
int A35[] = { 47,44,42,27,41,40,37 };
int A36[] = { 139,155,161,158,118,166,154,156,78 };
int A37[] = { 118,157,164,158,161,79,139,150,159 };
int A38[] = { 299,292,371,150,300,301,281,303,306,262 };
int A39[] = { 85,77,86,84,44,88,91,67 };
int A40[] = { 88,85,84,44,65,91,76,86 };
int A41[] = { 138,141,127,96,136,154,135,76 };
int A42[] = { 292,308,302,346,300,324,304,305,238,166 };
int A43[] = { 354,342,341,257,348,343,345,321,170,301 };
int A44[] = { 84,178,168,167,131,170,193,166,162 };
int A45[] = { 686,701,706,673,694,687,652,343,683,606,518 };
int A46[] = { 295,293,301,367,296,279,297,263,323,159 };
int A47[] = { 1038,1184,593,890,1188,1173,1187,1186,1195,1150,1203,1255 };
int A48[] = { 343,364,388,402,191,383,382,385,288,374 };
int A49[] = { 1187,1036,1183,591,1184,1175,888,1197,1182,1219,1115,1167 };
int A50[] = { 151,291,307,303,345,238,299,323,301,302 };
int A51[] = { 140,151,143,138,99,69,131,137 };
int A52[] = { 29,44,42,59,41,36,40 };
int A53[] = { 348,329,343,344,338,315,169,359,375,271 };
int A54[] = { 48,39,34,37,50,40,41 };
int A55[] = { 593,445,595,558,662,602,591,297,610,580,594 };
int A56[] = { 686,651,681,342,541,687,691,707,604,675,699 };
int A57[] = { 180,99,189,166,194,188,144,187,199 };
int A58[] = { 321,349,335,343,377,176,265,356,344,332 };
int A59[] = { 1151,1255,1195,1173,1184,1186,1188,1187,1203,593,1038,891 };
int A60[] = { 90,88,100,83,62,113,80,89 };
int A61[] = { 308,303,238,300,151,304,324,293,346,302 };
int A62[] = { 59,38,50,41,42,35,40 };
int A63[] = { 352,366,174,355,344,265,343,310,338,331 };
int A64[] = { 91,89,93,90,117,85,60,106 };
int A65[] = { 146,186,166,175,202,92,184,183,189 };
int A66[] = { 82,67,96,44,80,79,88,76 };
int A67[] = { 54,50,58,66,31,61,64 };
int A68[] = { 343,266,344,172,308,336,364,350,359,333 };
int A69[] = { 88,49,87,82,90,98,86,115 };
int A70[] = { 20,47,49,51,54,48,40 };
int A71[] = { 159,79,177,158,157,152,155,167,118 };
int A72[] = { 1219,1183,1182,1115,1035,1186,591,1197,1167,887,1184,1175 };
int A73[] = { 611,518,693,343,704,667,686,682,677,687,725 };
int A74[] = { 607,599,634,305,677,604,603,580,452,605,591 };
int A75[] = { 682,686,635,675,692,730,687,342,517,658,695 };
int A76[] = { 662,296,573,598,592,584,553,593,595,443,591 };
int A77[] = { 180,185,186,199,187,210,93,177,149 };
int A78[] = { 197,136,179,185,156,182,180,178,99 };
int A79[] = { 271,298,218,279,285,282,280,238,140 };
int A80[] = { 1187,1151,890,593,1194,1188,1184,1173,1038,1186,1255,1203 };
int A81[] = { 169,161,177,192,130,165,84,167,168 };
int A82[] = { 50,42,43,41,66,39,36 };
int A83[] = { 590,669,604,579,448,599,560,299,601,597,598 };
int A84[] = { 174,191,206,179,184,142,177,180,90 };
int A85[] = { 298,299,297,306,164,285,374,269,329,295 };
int A86[] = { 181,172,162,138,170,195,86,169,168 };
int A87[] = { 1184,1197,591,1182,1186,889,1167,1219,1183,1033,1115,1175 };
int A88[] = { 644,695,691,679,667,687,340,681,770,686,517 };
int A89[] = { 606,524,592,576,628,593,591,584,296,444,595 };
int A90[] = { 94,127,154,138,135,74,136,141 };
int A91[] = { 179,168,172,178,177,89,198,186,137 };
int A92[] = { 302,299,291,300,298,149,260,305,280,370 };
int A93[] = { 678,517,670,686,682,768,687,648,342,692,702 };
int A94[] = { 302,290,304,376,333,303,306,298,279,153 };
int A95[] = { 95,102,109,54,96,75,85,97 };
int A96[] = { 150,154,146,78,152,151,162,173,119 };
int A97[] = { 150,143,157,152,184,112,154,151,132 };
int A98[] = { 36,41,54,40,25,44,42 };
int A99[] = { 37,48,34,59,39,41,40 };
int A100[] = { 681,603,638,611,584,303,454,607,606,605,596 };

extern int sumSet(vector<int>& set);
extern void debugSet(vector<int> set);
extern void printSetString(vector<int> set);
extern void generateAllSubSet(vector<int>& set, vector < vector < int >>& subsets);

static vector<vector<int>> special_sets;

vector<int> assignAndSort(int A[], int n)
{
	vector<int> ret;
	ret.resize(n);
	for (int i = 0; i < n; i++)
	{
		ret[i] = A[i];
	}
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = i + 1; j < n; j++)
		{
			if (ret[i] > ret[j])
			{
				int tmp = ret[i];
				ret[i] = ret[j];
				ret[j] = tmp;
			}
		}
	}
	return ret;
}

void readInput105()
{
	special_sets.resize(100);
	special_sets[0] = assignAndSort(A1, sizeof(A1) / sizeof(int));
	special_sets[1] = assignAndSort(A2, sizeof(A2) / sizeof(int));
	special_sets[2] = assignAndSort(A3, sizeof(A3) / sizeof(int));
	special_sets[3] = assignAndSort(A4, sizeof(A4) / sizeof(int));
	special_sets[4] = assignAndSort(A5, sizeof(A5) / sizeof(int));
	special_sets[5] = assignAndSort(A6, sizeof(A6) / sizeof(int));
	special_sets[6] = assignAndSort(A7, sizeof(A7) / sizeof(int));
	special_sets[7] = assignAndSort(A8, sizeof(A8) / sizeof(int));
	special_sets[8] = assignAndSort(A9, sizeof(A9) / sizeof(int));
	special_sets[9] = assignAndSort(A10, sizeof(A10) / sizeof(int));
	special_sets[10] = assignAndSort(A11, sizeof(A11) / sizeof(int));
	special_sets[11] = assignAndSort(A12, sizeof(A12) / sizeof(int));
	special_sets[12] = assignAndSort(A13, sizeof(A13) / sizeof(int));
	special_sets[13] = assignAndSort(A14, sizeof(A14) / sizeof(int));
	special_sets[14] = assignAndSort(A15, sizeof(A15) / sizeof(int));
	special_sets[15] = assignAndSort(A16, sizeof(A16) / sizeof(int));
	special_sets[16] = assignAndSort(A17, sizeof(A17) / sizeof(int));
	special_sets[17] = assignAndSort(A18, sizeof(A18) / sizeof(int));
	special_sets[18] = assignAndSort(A19, sizeof(A19) / sizeof(int));
	special_sets[19] = assignAndSort(A20, sizeof(A20) / sizeof(int));
	special_sets[20] = assignAndSort(A21, sizeof(A21) / sizeof(int));
	special_sets[21] = assignAndSort(A22, sizeof(A22) / sizeof(int));
	special_sets[22] = assignAndSort(A23, sizeof(A23) / sizeof(int));
	special_sets[23] = assignAndSort(A24, sizeof(A24) / sizeof(int));
	special_sets[24] = assignAndSort(A25, sizeof(A25) / sizeof(int));
	special_sets[25] = assignAndSort(A26, sizeof(A26) / sizeof(int));
	special_sets[26] = assignAndSort(A27, sizeof(A27) / sizeof(int));
	special_sets[27] = assignAndSort(A28, sizeof(A28) / sizeof(int));
	special_sets[28] = assignAndSort(A29, sizeof(A29) / sizeof(int));
	special_sets[29] = assignAndSort(A30, sizeof(A30) / sizeof(int));
	special_sets[30] = assignAndSort(A31, sizeof(A31) / sizeof(int));
	special_sets[31] = assignAndSort(A32, sizeof(A32) / sizeof(int));
	special_sets[32] = assignAndSort(A33, sizeof(A33) / sizeof(int));
	special_sets[33] = assignAndSort(A34, sizeof(A34) / sizeof(int));
	special_sets[34] = assignAndSort(A35, sizeof(A35) / sizeof(int));
	special_sets[35] = assignAndSort(A36, sizeof(A36) / sizeof(int));
	special_sets[36] = assignAndSort(A37, sizeof(A37) / sizeof(int));
	special_sets[37] = assignAndSort(A38, sizeof(A38) / sizeof(int));
	special_sets[38] = assignAndSort(A39, sizeof(A39) / sizeof(int));
	special_sets[39] = assignAndSort(A40, sizeof(A40) / sizeof(int));
	special_sets[40] = assignAndSort(A41, sizeof(A41) / sizeof(int));
	special_sets[41] = assignAndSort(A42, sizeof(A42) / sizeof(int));
	special_sets[42] = assignAndSort(A43, sizeof(A43) / sizeof(int));
	special_sets[43] = assignAndSort(A44, sizeof(A44) / sizeof(int));
	special_sets[44] = assignAndSort(A45, sizeof(A45) / sizeof(int));
	special_sets[45] = assignAndSort(A46, sizeof(A46) / sizeof(int));
	special_sets[46] = assignAndSort(A47, sizeof(A47) / sizeof(int));
	special_sets[47] = assignAndSort(A48, sizeof(A48) / sizeof(int));
	special_sets[48] = assignAndSort(A49, sizeof(A49) / sizeof(int));
	special_sets[49] = assignAndSort(A50, sizeof(A50) / sizeof(int));
	special_sets[50] = assignAndSort(A51, sizeof(A51) / sizeof(int));
	special_sets[51] = assignAndSort(A52, sizeof(A52) / sizeof(int));
	special_sets[52] = assignAndSort(A53, sizeof(A53) / sizeof(int));
	special_sets[53] = assignAndSort(A54, sizeof(A54) / sizeof(int));
	special_sets[54] = assignAndSort(A55, sizeof(A55) / sizeof(int));
	special_sets[55] = assignAndSort(A56, sizeof(A56) / sizeof(int));
	special_sets[56] = assignAndSort(A57, sizeof(A57) / sizeof(int));
	special_sets[57] = assignAndSort(A58, sizeof(A58) / sizeof(int));
	special_sets[58] = assignAndSort(A59, sizeof(A59) / sizeof(int));
	special_sets[59] = assignAndSort(A60, sizeof(A60) / sizeof(int));
	special_sets[60] = assignAndSort(A61, sizeof(A61) / sizeof(int));
	special_sets[61] = assignAndSort(A62, sizeof(A62) / sizeof(int));
	special_sets[62] = assignAndSort(A63, sizeof(A63) / sizeof(int));
	special_sets[63] = assignAndSort(A64, sizeof(A64) / sizeof(int));
	special_sets[64] = assignAndSort(A65, sizeof(A65) / sizeof(int));
	special_sets[65] = assignAndSort(A66, sizeof(A66) / sizeof(int));
	special_sets[66] = assignAndSort(A67, sizeof(A67) / sizeof(int));
	special_sets[67] = assignAndSort(A68, sizeof(A68) / sizeof(int));
	special_sets[68] = assignAndSort(A69, sizeof(A69) / sizeof(int));
	special_sets[69] = assignAndSort(A70, sizeof(A70) / sizeof(int));
	special_sets[70] = assignAndSort(A71, sizeof(A71) / sizeof(int));
	special_sets[71] = assignAndSort(A72, sizeof(A72) / sizeof(int));
	special_sets[72] = assignAndSort(A73, sizeof(A73) / sizeof(int));
	special_sets[73] = assignAndSort(A74, sizeof(A74) / sizeof(int));
	special_sets[74] = assignAndSort(A75, sizeof(A75) / sizeof(int));
	special_sets[75] = assignAndSort(A76, sizeof(A76) / sizeof(int));
	special_sets[76] = assignAndSort(A77, sizeof(A77) / sizeof(int));
	special_sets[77] = assignAndSort(A78, sizeof(A78) / sizeof(int));
	special_sets[78] = assignAndSort(A79, sizeof(A79) / sizeof(int));
	special_sets[79] = assignAndSort(A80, sizeof(A80) / sizeof(int));
	special_sets[80] = assignAndSort(A81, sizeof(A81) / sizeof(int));
	special_sets[81] = assignAndSort(A82, sizeof(A82) / sizeof(int));
	special_sets[82] = assignAndSort(A83, sizeof(A83) / sizeof(int));
	special_sets[83] = assignAndSort(A84, sizeof(A84) / sizeof(int));
	special_sets[84] = assignAndSort(A85, sizeof(A85) / sizeof(int));
	special_sets[85] = assignAndSort(A86, sizeof(A86) / sizeof(int));
	special_sets[86] = assignAndSort(A87, sizeof(A87) / sizeof(int));
	special_sets[87] = assignAndSort(A88, sizeof(A88) / sizeof(int));
	special_sets[88] = assignAndSort(A89, sizeof(A89) / sizeof(int));
	special_sets[89] = assignAndSort(A90, sizeof(A90) / sizeof(int));
	special_sets[90] = assignAndSort(A91, sizeof(A91) / sizeof(int));
	special_sets[91] = assignAndSort(A92, sizeof(A92) / sizeof(int));
	special_sets[92] = assignAndSort(A93, sizeof(A93) / sizeof(int));
	special_sets[93] = assignAndSort(A94, sizeof(A94) / sizeof(int));
	special_sets[94] = assignAndSort(A95, sizeof(A95) / sizeof(int));
	special_sets[95] = assignAndSort(A96, sizeof(A96) / sizeof(int));
	special_sets[96] = assignAndSort(A97, sizeof(A97) / sizeof(int));
	special_sets[97] = assignAndSort(A98, sizeof(A98) / sizeof(int));
	special_sets[98] = assignAndSort(A99, sizeof(A99) / sizeof(int));
	special_sets[99] = assignAndSort(A100, sizeof(A100) / sizeof(int));
}

int elementSpecial(int n, int k)
{
	if (k == 1 && n == 1)
	{
		return 1;
	}
	if (k == 1)
	{
		return elementSpecial(n - 1, (n + 1) / 2);
	}
	return elementSpecial(n, 1) + elementSpecial(n - 1, k - 1);
}

void generateAllSpecialSet()
{
	special_sets.resize(13);
	for (int n = 1; n < 13; n++)
	{
		vector<int> set;
		set.resize(n);
		for (int k = 1; k <= n; k++)
		{
			set[k - 1] = elementSpecial(n, k);
		}
		special_sets[n] = set;
		debugSet(set);
	}
}

bool test105rule2(vector<int> optimum_set)
{
	int size = optimum_set.size();
	if (size < 2)
		return true;
	if (size == 3)
		if (optimum_set[0] + optimum_set[1] < optimum_set[2])
			return false;
	for (int num1 = 2, num2 = 1; (num1 + num2) <= size; num1++, num2++)
	{
		int sum1 = 0;
		int sum2 = 0;
		for (int i = 0; i < num1; i++)
		{
			sum1 += optimum_set[i];
		}
		for (int i = size - 1; i >= (size - num2); i--)
		{
			sum2 += optimum_set[i];
		}
		if (sum1 <= sum2)
		{
			return false;
		}
	}
	return true;
}

extern int sumSet(vector<int>& set);

bool test105rule1(vector<int> optimum_set)
{
	vector < vector < int >> subsets;
	generateAllSubSet(optimum_set, subsets);
	int size = subsets.size();
	//cout << "size: " << size << endl;
	set<int> sumAlls;
	for (int i = 0; i < size; i++)
	{
		sumAlls.insert(sumSet(subsets[i]));
	}
	//cout << "size 2: " << sumAlls.size() << endl;
	if (subsets.size() != sumAlls.size())
	{
		return false;
	}
	return true;
}

std::string problem_105()
{
	std::string ret;
	readInput105();
	int cnt = 0;
	int sum = 0;
	for (int i = 0; i < 100; i++)
	{
		if (test105rule2(special_sets[i]))
		{
			if (test105rule1(special_sets[i]))
			{
				cout << cnt++ << ": ";
				debugSet(special_sets[i]);
				sum += sumSet(special_sets[i]);
			}
		}
	}
	cout << "sum : " << sum << endl;
	return ret;
}
