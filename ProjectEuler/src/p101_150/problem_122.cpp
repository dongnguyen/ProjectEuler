//Efficient exponentiation
//Problem 122
//The most naive way of computing n15 requires fourteen multiplications:
//
//n � n � ... � n = n15
//
//But using a "binary" method you can compute it in six multiplications:
//
//n � n = n2
//n2 � n2 = n4
//n4 � n4 = n8
//n8 � n4 = n12
//n12 � n2 = n14
//n14 � n = n15
//
//However it is yet possible to compute it in only five multiplications:
//
//n � n = n2
//n2 � n = n3
//n3 � n3 = n6
//n6 � n6 = n12
//n12 � n3 = n15
//
//We shall define m(k) to be the minimum number of multiplications to compute nk; for example m(15) = 5.
//
//For 1 ? k ? 200, find ? m(k).
//
// 1582 cheat http://oeis.org/A003313/b003313.txt

#include "template_class.h"

void init_count(int d, int k, int& init, int &remain)
{
    if(d == 1)
    {
        int num = log(k) / log(2);
        init = num;
        remain = k - pow(2, num);
    }
    else if(d == 3)
    {
        int num = log(k/3) / log(2);
        init = 2 + num;
        remain = k - 3*pow(2, num);
    }
}

extern int count_bit_1(int n);

int pow_3[] =
{
    1,
    3,
    9,
    27,
    81,
    243,
    729,
    2187
};

int count_remain_for_3(int remain)
{
    int count = 0;
    for(int i = sizeof(pow_3)/sizeof(int)-1; i > 0; i--)
    {
        if(pow_3[i] <= remain)
        {
            count++;
            remain -= pow_3[i];
        }
    }
    if(count == 0)
    {
        count = 1;
    }
    return count;
}

bool found_in_vec(vector<int>& vec, int n)
{
    for(int i = 0; i < vec.size(); i++)
    {
        if(n == vec[i])
        {
            return true;
        }
    }
    return false;
}

bool found_in_vec2(vector<vector<int>>& vec, vector<int> n)
{
    for(int i = 0; i < vec.size(); i++)
    {
        if(n == vec[i])
        {
            return true;
        }
    }
    return false;
}

int count_multiplications(int k)
{
#if 0
    if(k == 1)
    {
        return 0;
    }
    if(k == 2)
    {
        return 1;
    }
    int remain_1 = 0;
    int remain_2 = 0;
    int c_1 = 0;
    init_count(1, k, c_1, remain_1);
    c_1 += count_bit_1(remain_1);
    int c_2 = 0;
    init_count(3, k, c_2, remain_2);
    c_2 += count_remain_for_3(remain_2);
    return min(c_1, c_2);
#endif
#if 0
    int count = 1;
    while(k)
    {
        k >>= 1;
        count++;
    }
    return count;
#endif
    if(k == 1)
    {
        return 0;
    }

#if 1
    int remain_1 = 0;
    int c_1 = 0;
    init_count(1, k, c_1, remain_1);
    c_1 += count_bit_1(remain_1);

    vector<vector<int>> memo;
    vector<int> first;
    first.push_back(1);
    memo.push_back(first);
    while(1)
    {
        // stop condition
        /*bool stop = true;
        for(int i = 0; i < memo.size(); i++)
        {
            if(memo[i].back() != k)
            {
                stop = false;
                break;
            }
        }
        if(stop)
        {
            break;
        }*/
        if(memo.size() == 0)
        {
            return c_1;
        }
        if (memo[0].size() == c_1)
        {
            return c_1;
        }
        vector<vector<int>> memo_tmp;
        for(int i = 0; i < memo.size(); i++)
        {
            for(int j = 0; j < memo[i].size(); j++)
            {
                for(int kk = 0; kk < memo[i].size(); kk++)
                {
                    int new_mem = memo[i][j] + memo[i][kk];
                    if(new_mem <= k && new_mem > memo[i].back())
                    {
                        //if(!found_in_vec(memo[i], new_mem))
                        {
                            vector<int> tmp = memo[i];
                            tmp.push_back(new_mem);
                            //sort(tmp.begin(), tmp.end());
                            int curr_count = tmp.size() - 1;
                            if(tmp.back()*pow(2, c_1 - curr_count - 1) < k)
                            {
                                continue;
                            }
                            else
                            {
                                vector<int> tmp2 = tmp;
                                int new_mem_tmp = new_mem;
                                for (int p = 0; p < c_1 - curr_count - 2; p++)
                                {
                                    new_mem_tmp = new_mem_tmp * 2;
                                    tmp2.push_back(new_mem_tmp);
                                }
                                int remain = k - tmp2.back();
                                if (remain >= 1)
                                {
                                    if (!found_in_vec(tmp2, remain))
                                    {
                                        continue;
                                    }
                                }
                            }
                            if(!found_in_vec2(memo_tmp, tmp))
                            {
                                if(new_mem == k)
                                {
                                    return tmp.size()-1;
                                }
                                memo_tmp.push_back(tmp);
                            }
                        }
                    }
                }
            }
        }
        memo = memo_tmp;
    }
#endif
    return 0;
}

int problem_122(int K)
{
    int sum = 0;
    //int tmp = count_multiplications(15);
    //return sum;
    for(int i = 1; i <= K; i++)
    {
        int tmp = count_multiplications(i);
        cout << "k = " << i << " : "<< tmp << endl;
        sum += tmp;
    }
    return sum;
}
