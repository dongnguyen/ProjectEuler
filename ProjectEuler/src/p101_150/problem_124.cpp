//Ordered radicals
//Problem 124
//The radical of n, rad(n), is the product of distinct prime factors of n. For example, 504 = 23 � 32 � 7, so rad(504) = 2 � 3 � 7 = 42.
//
//If we calculate rad(n) for 1 ? n ? 10, then sort them on rad(n), and sorting on n if the radical values are equal, we get:
//
//Unsorted
//
//Sorted
//
//n
//
//rad(n)
//
//
//n
//
//rad(n)
//
//k
//1
//1
//
//1
//1
//1
//2
//2
//
//2
//2
//2
//3
//3
//
//4
//2
//3
//4
//2
//
//8
//2
//4
//5
//5
//
//3
//3
//5
//6
//6
//
//9
//3
//6
//7
//7
//
//5
//5
//7
//8
//2
//
//6
//6
//8
//9
//3
//
//7
//7
//9
//10
//10
//
//10
//10
//10
//Let E(k) be the kth element in the sorted n column; for example, E(4) = 8 and E(6) = 9.
//
//If rad(n) is sorted for 1 ? n ? 100000, find E(10000).

#include "template_class.h"

struct E_element
{
    int idx;
    uint64_t val;
    void operator*= (int n)
    {
        val *= n;
    }
    bool operator<(const E_element& ref) const
    {
        if(val == ref.val)
        {
            return idx < ref.idx;
        }
        return val < ref.val;
    }
};
int problem_124()
{
    int n = 100000;
    vector<int> prime;
    vector<E_element> co_prime_num;
    prime.resize(n+1);
    co_prime_num.resize(n+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    int limit = sqrt(n);
    for(int i = 4; i <= n; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 1; i <= n; i++)
    {
        co_prime_num[i].val = 1;
        co_prime_num[i].idx = i;
    }
    for(int j = 2; j <= n; j += 2)
    {
        co_prime_num[j] *= 2;
    }
    for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            for(int j = i; j <= n; j += i)
            {
                co_prime_num[j] *= i;
            }
            for(int j = i*i; j <= n; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    for(int i = limit + 1; i <= n; i++)
    {
        if(prime[i] == PRIME)
        {
            for(int j = i; j <= n; j += i)
            {
                co_prime_num[j] *= i;
            }
        }
    }
    sort(co_prime_num.begin() + 1, co_prime_num.end());
    return co_prime_num[10000].idx;
}
