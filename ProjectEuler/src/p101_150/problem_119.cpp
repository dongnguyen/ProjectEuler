//Digit power sum
//Problem 119
//The number 512 is interesting because it is equal to the sum of its digits raised to some power: 5 + 1 + 2 = 8, and 83 = 512. Another example of a number with this property is 614656 = 284.
//
//We shall define an to be the nth term of this sequence and insist that a number must contain at least two digits to have a sum.
//
//You are given that a2 = 512 and a10 = 614656.
//
//Find a30.

#include "template_class.h"
#include "BigIntegerLibrary.hh"

bool check_119(uint64_t x)
{
	if (x < 10)
	{
		return false;
	}
	uint64_t sum_digits = 0;
	uint64_t tmp = x;
	while (tmp)
	{
		sum_digits += tmp % 10;
		tmp /= 10;
	}
	uint64_t pow_n = (uint64_t)(log(x) / log(sum_digits) + 0.5);
	uint64_t new_x = (uint64_t)(pow(sum_digits, pow_n) + 0.5);
	if (new_x == x)
	{
		return true;
	}
	return false;
}

uint64_t problem_119()
{
	uint64_t limit = (uint64_t)1e15;
	uint32_t sqrt_limit = (uint32_t)sqrt(limit);
	vector<int> checked;
	checked.resize(sqrt_limit);
	set<uint64_t> ret;
	double log_limit = log(limit);
	for (uint32_t i = 2; i < sqrt_limit; i++)
	{
		if (!checked[i])
		{
			checked[i] = 1;
			uint32_t tmp = i;
			double log_i = log(i);
			double log_tmp = log_i;
			while (log_tmp < log_limit)
			{
				if (check_119(tmp))
				{
					ret.insert(tmp);
				}
				tmp *= i;
				log_tmp += log_i;
				if (tmp < sqrt_limit)
				{
					checked[tmp] = 1;
				}
			}
		}
	}
	uint64_t r = ret.size();
	vector<uint64_t> v_r;
	set<uint64_t>::iterator ite = ret.begin();
	for (; ite != ret.end(); ++ite)
	{
		v_r.push_back(*ite);
	}
	if (ret.size() >= 30)
	{
		r = v_r[29];
	}
	return r;
}
