//abc - hits
//Problem 127
//The radical of n, rad(n), is the product of distinct prime factors of n.For example, 504 = 23 � 32 � 7, so rad(504) = 2 � 3 � 7 = 42.
//
//We shall define the triplet of positive integers(a, b, c) to be an abc - hit if:
//
//GCD(a, b) = GCD(a, c) = GCD(b, c) = 1
//a < b
//a + b = c
//rad(abc) < c
//For example, (5, 27, 32) is an abc - hit, because:
//
//GCD(5, 27) = GCD(5, 32) = GCD(27, 32) = 1
//5 < 27
//5 + 27 = 32
//rad(4320) = 30 < 32
//It turns out that abc - hits are quite rare and there are only thirty - one abc - hits for c < 1000, with ?c = 12523.
//
//Find ?c for c < 120000.

#include "template_class.h"
#include <thread>         // std::thread
extern vector<int> prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

bool check_127(couple_int& tmp)
{
    bool ret = tmp.m_b < (tmp.m_a / 2);
    /*ret = ret && (gcd(tmp.m_b, tmp.m_a - tmp.m_b) == 1);
    ret = ret && (gcd(tmp.m_a, tmp.m_a - tmp.m_b) == 1);*/
    return ret;
}

vector< couple_int> big_memo;
vector< couple_int> big_memo1;
vector< couple_int> big_memo2;
vector< couple_int> big_memo3;
vector< couple_int> big_memo4;
vector<set<int>> rad_127;

int sum_127 = 0;
int sum_1_127 = 0;
int sum_2_127 = 0;
int sum_3_127 = 0;
int sum_4_127 = 0;

uint64_t compute_rad(int a, int b, int c)
{
    set<int> all = rad_127[a];
    for(set<int>::iterator ite = rad_127[b].begin(); ite != rad_127[b].end(); ++ite)
    {
        all.insert(*ite);
    }
    for(set<int>::iterator ite = rad_127[c].begin(); ite != rad_127[c].end(); ++ite)
    {
        all.insert(*ite);
    }
    uint64_t p = 1;
    for(set<int>::iterator ite = all.begin(); ite != all.end(); ++ite)
    {
        p *= *ite;
    }
    /*if(a == 32)
    {
        int d = 0;
        d++;
    }*/
    return p;
}

//void thread_127(vector<couple_int>& store_tmp, vector<couple_int>& bmemo, int b, int e, int n, int* sum)
//void thread_127(vector<couple_int>& store_tmp, int* sum, int b, int e, int n)

void thread_127_1(vector<couple_int>& store_tmp, int b, int e, int n)
{
    queue<couple_int> co_prime_queue;
    for (int i = b; i < e; i++)
    {
        couple_int start = store_tmp[i];

        co_prime_queue.push(start);
        while (!co_prime_queue.empty())
        {
            couple_int tmp = co_prime_queue.front();
            if (check_127(tmp))
            {
                if(compute_rad(tmp.m_a, tmp.m_b, tmp.m_a - tmp.m_b) < tmp.m_a)
                {
                    //big_memo1.push_back(tmp);
                    sum_1_127 += tmp.m_a;
                }
            }
            //coprime_memo[tmp.m_a].insert(tmp.m_b);
            co_prime_queue.pop();
            couple_int b1(2 * tmp.m_a - tmp.m_b, tmp.m_a);
            couple_int b2(2 * tmp.m_a + tmp.m_b, tmp.m_a);
            couple_int b3(tmp.m_a + 2 * tmp.m_b, tmp.m_b);
            if (b1.m_a < n)
            {
                co_prime_queue.push(b1);
            }
            if (b2.m_a < n)
            {
                co_prime_queue.push(b2);
            }
            if (b3.m_a < n)
            {
                co_prime_queue.push(b3);
            }
        }
    }
}

void thread_127_2(vector<couple_int>& store_tmp, int b, int e, int n)
{
    queue<couple_int> co_prime_queue;
    for (int i = b; i < e; i++)
    {
        couple_int start = store_tmp[i];

        co_prime_queue.push(start);
        while (!co_prime_queue.empty())
        {
            couple_int tmp = co_prime_queue.front();
            if (check_127(tmp))
            {
                if(compute_rad(tmp.m_a, tmp.m_b, tmp.m_a - tmp.m_b) < tmp.m_a)
                {
                    //big_memo2.push_back(tmp);
                    sum_2_127 += tmp.m_a;
                }
            }
            //coprime_memo[tmp.m_a].insert(tmp.m_b);
            co_prime_queue.pop();
            couple_int b1(2 * tmp.m_a - tmp.m_b, tmp.m_a);
            couple_int b2(2 * tmp.m_a + tmp.m_b, tmp.m_a);
            couple_int b3(tmp.m_a + 2 * tmp.m_b, tmp.m_b);
            if (b1.m_a < n)
            {
                co_prime_queue.push(b1);
            }
            if (b2.m_a < n)
            {
                co_prime_queue.push(b2);
            }
            if (b3.m_a < n)
            {
                co_prime_queue.push(b3);
            }
        }
    }
}

void thread_127_3(vector<couple_int>& store_tmp, int b, int e, int n)
{
    queue<couple_int> co_prime_queue;
    for (int i = b; i < e; i++)
    {
        couple_int start = store_tmp[i];

        co_prime_queue.push(start);
        while (!co_prime_queue.empty())
        {
            couple_int tmp = co_prime_queue.front();
            if (check_127(tmp))
            {
                if(compute_rad(tmp.m_a, tmp.m_b, tmp.m_a - tmp.m_b) < tmp.m_a)
                {
                    //big_memo3.push_back(tmp);
                    sum_3_127 += tmp.m_a;
                }
            }
            //coprime_memo[tmp.m_a].insert(tmp.m_b);
            co_prime_queue.pop();
            couple_int b1(2 * tmp.m_a - tmp.m_b, tmp.m_a);
            couple_int b2(2 * tmp.m_a + tmp.m_b, tmp.m_a);
            couple_int b3(tmp.m_a + 2 * tmp.m_b, tmp.m_b);
            if (b1.m_a < n)
            {
                co_prime_queue.push(b1);
            }
            if (b2.m_a < n)
            {
                co_prime_queue.push(b2);
            }
            if (b3.m_a < n)
            {
                co_prime_queue.push(b3);
            }
        }
    }
}

void thread_127_4(vector<couple_int>& store_tmp, int b, int e, int n)
{
    queue<couple_int> co_prime_queue;
    for (int i = b; i < e; i++)
    {
        couple_int start = store_tmp[i];

        co_prime_queue.push(start);
        while (!co_prime_queue.empty())
        {
            couple_int tmp = co_prime_queue.front();
            if (check_127(tmp))
            {
                if(compute_rad(tmp.m_a, tmp.m_b, tmp.m_a - tmp.m_b) < tmp.m_a)
                {
                    //big_memo4.push_back(tmp);
                    sum_4_127 += tmp.m_a;
                }
            }
            //coprime_memo[tmp.m_a].insert(tmp.m_b);
            co_prime_queue.pop();
            couple_int b1(2 * tmp.m_a - tmp.m_b, tmp.m_a);
            couple_int b2(2 * tmp.m_a + tmp.m_b, tmp.m_a);
            couple_int b3(tmp.m_a + 2 * tmp.m_b, tmp.m_b);
            if (b1.m_a < n)
            {
                co_prime_queue.push(b1);
            }
            if (b2.m_a < n)
            {
                co_prime_queue.push(b2);
            }
            if (b3.m_a < n)
            {
                co_prime_queue.push(b3);
            }
        }
    }
}


int problem_127(int n)
{
    //vector<set<int>> coprime_memo;
    //coprime_memo.resize(n);
    create_prime_filter_method(n);
#if 0
    rad_127.resize(n);
    for (int i = 0; i < prime_list.size(); i++)
    {
        int p = prime_list[i];
        for (int j = p; j < n; j += p)
        {
            rad_127[j].insert(p);
        }
    }
    couple_int start(2, 1);
    queue<couple_int> co_prime_queue;
    co_prime_queue.push(start);

    vector < couple_int> store_tmp;
    int sqrt_n = sqrt(n);
    while (!co_prime_queue.empty())
    {
        couple_int tmp = co_prime_queue.front();
        if (check_127(tmp))
        {
            if(compute_rad(tmp.m_a, tmp.m_b, tmp.m_a - tmp.m_b) < tmp.m_a)
            {
                //big_memo.push_back(tmp);
                sum_127 += tmp.m_a;
            }
        }
        //coprime_memo[tmp.m_a].insert(tmp.m_b);
        co_prime_queue.pop();
        couple_int b1(2 * tmp.m_a - tmp.m_b, tmp.m_a);
        couple_int b2(2 * tmp.m_a + tmp.m_b, tmp.m_a);
        couple_int b3(tmp.m_a + 2 * tmp.m_b, tmp.m_b);
        if (b1.m_a < sqrt_n)
        {
            co_prime_queue.push(b1);
        }
        else
        {
            store_tmp.push_back(b1);
        }
        if (b2.m_a < sqrt_n)
        {
            co_prime_queue.push(b2);
        }
        else
        {
            store_tmp.push_back(b2);
        }
        if (b3.m_a < sqrt_n)
        {
            co_prime_queue.push(b3);
        }
        else
        {
            store_tmp.push_back(b3);
        }
    }

    start = couple_int(3, 1);
    co_prime_queue.push(start);
    while (!co_prime_queue.empty())
    {
        couple_int tmp = co_prime_queue.front();
        if (check_127(tmp))
        {
            if(compute_rad(tmp.m_a, tmp.m_b, tmp.m_a - tmp.m_b) < tmp.m_a)
            {
                //big_memo.push_back(tmp);
                sum_127 += tmp.m_a;
            }
        }
        //coprime_memo[tmp.m_a].insert(tmp.m_b);
        co_prime_queue.pop();
        couple_int b1(2 * tmp.m_a - tmp.m_b, tmp.m_a);
        couple_int b2(2 * tmp.m_a + tmp.m_b, tmp.m_a);
        couple_int b3(tmp.m_a + 2 * tmp.m_b, tmp.m_b);
        if (b1.m_a < sqrt_n)
        {
            co_prime_queue.push(b1);
        }
        else
        {
            store_tmp.push_back(b1);
        }
        if (b2.m_a < sqrt_n)
        {
            co_prime_queue.push(b2);
        }
        else
        {
            store_tmp.push_back(b2);
        }
        if (b3.m_a < sqrt_n)
        {
            co_prime_queue.push(b3);
        }
        else
        {
            store_tmp.push_back(b3);
        }
    }

    int size = store_tmp.size();
    int b = 0;
    int e = size / 4;
    thread t1(thread_127_1, store_tmp, b, e, n);
    //thread t1(thread_127, store_tmp, &sum_1_127, b, e, n);
    b = e+1;
    e = size / 2;
    thread t2(thread_127_2, store_tmp, b, e, n);
    //thread t2(thread_127, store_tmp, &sum_2_127, b, e, n);
    b = e + 1;
    e = size / 4 * 3;
    thread t3(thread_127_3, store_tmp, b, e, n);
    //thread t3(thread_127, store_tmp, &sum_3_127, b, e, n);
    b = e + 1;
    e = size;
    thread t4(thread_127_4, store_tmp, b, e, n);
    //thread t4(thread_127, store_tmp, &sum_4_127, b, e, n);

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    /*for (int i = 0; i < big_memo.size(); i++)
    {
        couple_int tmp = big_memo[i];
        cout << "[ " << tmp.m_a << ", " << tmp.m_b << ", " << tmp.m_a - tmp.m_b << " ]" << endl;
    }
    for (int i = 0; i < big_memo1.size(); i++)
    {
        couple_int tmp = big_memo1[i];
        cout << "[ " << tmp.m_a << ", " << tmp.m_b << ", " << tmp.m_a - tmp.m_b << " ]" << endl;
    }
    for (int i = 0; i < big_memo2.size(); i++)
    {
        couple_int tmp = big_memo2[i];
        cout << "[ " << tmp.m_a << ", " << tmp.m_b << ", " << tmp.m_a - tmp.m_b << " ]" << endl;
    }
    for (int i = 0; i < big_memo3.size(); i++)
    {
        couple_int tmp = big_memo3[i];
        cout << "[ " << tmp.m_a << ", " << tmp.m_b << ", " << tmp.m_a - tmp.m_b << " ]" << endl;
    }
    for (int i = 0; i < big_memo4.size(); i++)
    {
        couple_int tmp = big_memo4[i];
        cout << "[ " << tmp.m_a << ", " << tmp.m_b << ", " << tmp.m_a - tmp.m_b << " ]" << endl;
    }

    return big_memo.size() +
        big_memo1.size() +
        big_memo2.size() +
        big_memo3.size() +
        big_memo4.size();*/
    return sum_127 +
           sum_1_127 +
           sum_2_127 +
           sum_3_127 +
           sum_4_127;
#else
    vector<uint64_t> rad_127_2;
    rad_127_2.resize(n);
    for (int i = 1; i < n; i++)
    {
        rad_127_2[i] = 1;
    }
    for (int i = 0; i < prime_list.size(); i++)
    {
        int p = prime_list[i];
        for (int j = p; j < n; j += p)
        {
            rad_127_2[j] *= p;
        }
    }
    int sum = 0;
    for (int a = 1; a < n / 2; a++)
    {
        for (int b = a + 1; b < n - a; b++)
        {
            if (gcd(b, a) == 1)
            {
                int c = a + b;
                if (rad_127_2[a] * rad_127_2[b] * rad_127_2[c] < c)
                {
                    sum += c;
                }
            }
        }
    }
    return sum;
#endif
}