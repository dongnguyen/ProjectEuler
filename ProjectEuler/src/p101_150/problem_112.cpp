//Bouncy numbers
//Problem 112
//Working from left-to-right if no digit is exceeded by the digit to its left it is called an increasing number; for example, 134468.
//
//Similarly if no digit is exceeded by the digit to its right it is called a decreasing number; for example, 66420.
//
//We shall call a positive integer that is neither increasing nor decreasing a "bouncy" number; for example, 155349.
//
//Clearly there cannot be any bouncy numbers below one-hundred, but just over half of the numbers below one-thousand (525) are bouncy. In fact, the least number for which the proportion of bouncy numbers first reaches 50% is 538.
//
//Surprisingly, bouncy numbers become more and more common and by the time we reach 21780 the proportion of bouncy numbers is equal to 90%.
//
//Find the least number for which the proportion of bouncy numbers is exactly 99%.

#include "template_class.h"

int count_increasing(int *digits, int start_digit, int size, bool unlimit)
{
    if(size == 1)
    {
        if(unlimit)
        {
            return (10 - start_digit);
        }
        else
        {
            return digits[0] >= start_digit ? (digits[0] - start_digit + 1) : 0;
        }
    }
    int sum = 0;
    if(unlimit)
    {
        for(int i = start_digit; i <= 10; i++)
        {
            sum += count_increasing(digits + 1, i, size - 1, true);
        }
    }
    else
    {
        for(int i = start_digit; i < digits[0]; i++)
        {
            sum += count_increasing(digits + 1, i, size - 1, true);
        }
        sum += count_increasing(digits + 1, digits[0], size - 1, false);
    }
    return sum;
}

#define INCREASING  0
#define DECREASING  1
#define BOTH        2
#define BOUNCY      3

int problem_112()
{
    /*int test[5];
    test[0] = 2;
    test[1] = 1;
    test[2] = 7;
    test[3] = 8;
    test[4] = 0;
    int d = count_increasing(test, 1, 5, false);*/
    vector<int> memo;
    memo.resize(100);
    int num_bouncy = 0;
    for(int i = 0; i < 10; i++)
    {
        memo[i] = BOTH;
    }
    for(int n = 10;; n++)
    {
        if(n == memo.size())
        {
            memo.resize(n*10);
        }
        int add = n % 10;
        int base = n / 10;
        int nearest_digit = base % 10;
        switch(memo[base])
        {
        case INCREASING:
            if(add >= nearest_digit)
            {
                memo[n] = INCREASING;
            }
            else
            {
                memo[n] = BOUNCY;
                num_bouncy++;
                if(num_bouncy * 100 == n*99)
                {
                    return n;
                }
            }
            break;
        case DECREASING:
            if(add <= nearest_digit)
            {
                memo[n] = DECREASING;
            }
            else
            {
                memo[n] = BOUNCY;
                num_bouncy++;
                if(num_bouncy * 100 == n*99)
                {
                    return n;
                }
            }
            break;
        case BOTH      :
            if(add > nearest_digit)
            {
                memo[n] = INCREASING;
            }
            else if(add < nearest_digit)
            {
                memo[n] = DECREASING;
            }
            else
            {
                memo[n] = BOTH;
            }
            break;
        case BOUNCY    :
            memo[n] = BOUNCY;
            num_bouncy++;
            if(num_bouncy * 100 == n*99)
            {
                return n;
            }
            break;
        default:
            exit(-1);
        }
    }

    return 0;
}
