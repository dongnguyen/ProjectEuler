//Digit factorial chains
//Problem 74
//The number 145 is well known for the property that the sum of the factorial of its digits is equal to 145:
//
//1! + 4! + 5! = 1 + 24 + 120 = 145
//
//Perhaps less well known is 169, in that it produces the longest chain of numbers that link back to 169; it turns out that there are only three such loops that exist:
//
//169 ? 363601 ? 1454 ? 169
//871 ? 45361 ? 871
//872 ? 45362 ? 872
//
//It is not difficult to prove that EVERY starting number will eventually get stuck in a loop. For example,
//
//69 ? 363600 ? 1454 ? 169 ? 363601 (? 1454)
//78 ? 45360 ? 871 ? 45361 (? 871)
//540 ? 145 (? 145)
//
//Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating chain with a starting number below one million is sixty terms.
//
//How many chains, with a starting number below one million, contain exactly sixty non-repeating terms?

#include "template_class.h"

#define LIMIT_74 1e6

uint64_t fact_lookup[10] =
{
    1,
    1,
    2,
    6,
    24,
    120,
    720,
    5040,
    40320,
    362880
};

uint64_t calc_sum_factor(uint64_t n)
{
    vector<int> digit;
    while(n)
    {
        digit.push_back(n % 10);
        n /= 10;
    }
    uint64_t sum = 0;
    for(int i = 0; i < digit.size(); i++)
    {
        sum += fact_lookup[digit[i]];
    }
    return sum;
}

int calc_loop_74(int n, vector<int>& num_non_loop)
{
    if(num_non_loop[n] != -1)
    {
        return num_non_loop[n];
    }
    vector<uint64_t> memo;
    memo.push_back(n);
    uint64_t next = n;
    while(1)
    {
        next = calc_sum_factor(next);
        if(next < LIMIT_74)
        {
            if(num_non_loop[next] != -1)
            {
                int tmp = num_non_loop[next] + 1;
                for(int i = memo.size() - 1; i >= 0; i--)
                {
                    if(memo[i] < LIMIT_74)
                    {
                        num_non_loop[memo[i]] = tmp;
                    }
                    tmp++;
                }
                goto EXIT;
            }
        }
        for(int i = 0; i < memo.size(); i++)
        {
            if(next == memo[i])
            {
                int tmp = memo.size() - i;
                for(int j = memo.size() - 1; j > i; j--)
                {
                    if(memo[j] < LIMIT_74)
                    {
                        num_non_loop[memo[j]] = tmp;
                    }
                    tmp++;
                }
                tmp = memo.size() - i;
                for(int j = i; j >= 0; j--)
                {
                    if(memo[j] < LIMIT_74)
                    {
                        num_non_loop[memo[j]] = tmp;
                    }
                    tmp++;
                }
                goto EXIT;
            }
        }
        memo.push_back(next);
    }
EXIT:
    return num_non_loop[n];
}

int problem_74()
{
    int n = LIMIT_74;
    vector<int> num_non_loop;
    num_non_loop.resize(n);
    for(int i = 0; i < n; i++)
    {
        num_non_loop[i] = -1;
    }
    int count = 0;
    for(int i = 1; i < n; i++)
    {
        if(60 == calc_loop_74(i, num_non_loop))
        {
            count++;
        }
    }
    return count;
}

/*vector<vector<int>> num_digit_form;
num_digit_form.resize(n);
vector<int> ite;
ite.resize(6);
for(int i = 0; i < n - 1; i++)
{
    num_digit_form[i] = ite;
    ite[0]++;
    int idx = 0;
    while(ite[idx] > 9)
    {
        ite[idx++] = 0;
        ite[idx]++;
    }
}
num_digit_form[n-1] = ite;*/