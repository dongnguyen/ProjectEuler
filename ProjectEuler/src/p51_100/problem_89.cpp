//Roman numerals
//Problem 89
//The rules for writing Roman numerals allow for many ways of writing each number (see About Roman Numerals...). However, there is always a "best" way of writing a particular number.
//
//For example, the following represent all of the legitimate ways of writing the number sixteen:
//
//IIIIIIIIIIIIIIII
//VIIIIIIIIIII
//VVIIIIII
//XIIIIII
//VVVI
//XVI
//
//The last example being considered the most efficient, as it uses the least number of numerals.
//
//The 11K text file, roman.txt (right click and 'Save Link/Target As...'), contains one thousand numbers written in valid, but not necessarily minimal, Roman numerals; that is, they are arranged in descending units and obey the subtractive pair rule (see About Roman Numerals... for the definitive rules for this problem).
//
//Find the number of characters saved by writing each of these in their minimal form.
//
//Note: You can assume that all the Roman numerals in the file contain no more than four consecutive identical units.

#include "template_class.h"

int roman_char_to_int(char roman)
{
    switch(roman)
    {
    case 'I':
        return 1;
        break;
    case 'V':
        return 5;
        break;
    case 'X':
        return 10;
        break;
    case 'L':
        return 50;
        break;
    case 'C':
        return 100;
        break;
    case 'D':
        return 500;
        break;
    case 'M':
        return 1000;
        break;
    default:
        return -1;
        break;
    }
    return -1;
}

//Only I, X, and C can be used as the leading numeral in part of a subtractive pair.
//I can only be placed before V and X.
//X can only be placed before L and C.
//C can only be placed before D and M.
int roman_to_int(string roman)
{
    int ret = 0;
    //int ref = 1000;
    int size = roman.size();
    for(int i = 0; i < size; i++)
    {
        char c = roman[i];
        if(c == 'I')
        {
            if((i < size - 1) && (roman[i+1] == 'V' || roman[i+1] == 'X'))
            {
                ret += roman_char_to_int(roman[i+1]) - roman_char_to_int(c);
                i++;
                continue;
            }
        }
        else if(c == 'X')
        {
            if((i < size - 1) && (roman[i+1] == 'L' || roman[i+1] == 'C'))
            {
                ret += roman_char_to_int(roman[i+1]) - roman_char_to_int(c);
                i++;
                continue;
            }
        }
        else if(c == 'C')
        {
            if((i < size - 1) && (roman[i+1] == 'D' || roman[i+1] == 'M'))
            {
                ret += roman_char_to_int(roman[i+1]) - roman_char_to_int(c);
                i++;
                continue;
            }
        }
        ret += roman_char_to_int(c);
    }
    return ret;
}

//I = 1
//V = 5
//X = 10
//L = 50
//C = 100
//D = 500
//M = 1000
//Only I, X, and C can be used as the leading numeral in part of a subtractive pair.
//I can only be placed before V and X.
//X can only be placed before L and C.
//C can only be placed before D and M.
int num_char_minimum_roman_form(int val)
{
    string tmp;
    int num_M = val / 1000;
    val = val % 1000;
    if(val >= 900)
    {
        num_M += 2;
        val -= 900;
    }
    int num_D = val / 500;
    val = val % 500;
    if(val >= 400)
    {
        num_D += 2;
        val -= 400;
    }
    int num_C = val / 100;
    val = val % 100;
    if(val >= 90)
    {
        num_C += 2;
        val -= 90;
    }
    int num_L = val / 50;
    val = val % 50;
    if(val >= 40)
    {
        num_L += 2;
        val -= 40;
    }
    int num_X = val / 10;
    val = val % 10;
    if(val >= 9)
    {
        num_X += 2;
        val -= 9;
    }
    int num_V = val / 5;
    val = val % 5;
    if(val >= 4)
    {
        num_V += 2;
        val -= 4;
    }
    int num_I = val;
    return num_M + num_D + num_C + num_L + num_X + num_V + num_I;
}

void read_roman(char* path, vector<string>& store)
{
    string line;
    ifstream infile;
    infile.open(path);
    while(!infile.eof())
    {
        getline(infile, line);
        store.push_back(line);
    }
    infile.close();
}

int problem_89(char* path)
{
    vector<string> store;
    read_roman(path, store);
    int save = 0;
    for(int i = 0; i < store.size(); i++)
    {
        int old_size = store[i].size();
        int new_size = num_char_minimum_roman_form(roman_to_int(store[i]));
        save += old_size - new_size;
        /*if(old_size < new_size)
        {
            cout << "Some error!!!" << endl;
            exit(0);
        }*/
    }
    return save;
}