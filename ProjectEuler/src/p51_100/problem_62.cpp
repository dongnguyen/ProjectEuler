//Cubic permutations
//Problem 62
//The cube, 41063625 (3453), can be permuted to produce two other cubes: 56623104 (3843) and 66430125 (4053). In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.
//
//Find the smallest cube for which exactly five permutations of its digits are cube.

#include "template_class.h"

extern void pow_a_b(int a, int b, vector<int>& result);

bool compare_two_vector(vector<int> a, vector<int> b)
{
    if(a.size() != b.size())
    {
        return false;
    }
    sort(a.begin(), a.end());
    sort(b.begin(), b.end());
    for(int i = 0; i < a.size(); i++)
    {
        if(a[i] != b[i])
        {
            return false;
        }
    }
    return true;
}

int found_permutation(vector<vector<int>>& result, int num)
{
    vector<int> num_per;
    int size = result.size();
    num_per.resize(size);
    for(int i = 0; i < size - 1; i++)
    {
        for(int j = i + 1; j < size; j++)
        {
            if(compare_two_vector(result[i], result[j]))
            {
                num_per[i]++;
                num_per[j]++;
                if(num_per[i] == num - 1)
                {
                    return i;
                }
            }
        }
    }
    return -1;
}

vector<int> problem_62()
{
    vector<vector<int>> result;
    int size = 1;
    for(int i = 1;; i++)
    {
        vector<int> tmp;
        pow_a_b(i, 3, tmp);
        if(tmp.size() == size)
        {
            result.push_back(tmp);
        }
        else
        {
            // found permutation here
            int ret = found_permutation(result, 5);
            if(ret != -1)
            {
                return result[ret];
            }
            // update size
            size = tmp.size();
            result.clear();
            result.push_back(tmp);
        }
    }
    vector<int> tmp;
    return tmp;
}
