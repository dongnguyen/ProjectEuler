//Counting summations
//Problem 76
//It is possible to write five as a sum in exactly six different ways:
//
//4 + 1
//3 + 2
//3 + 1 + 1
//2 + 2 + 1
//2 + 1 + 1 + 1
//1 + 1 + 1 + 1 + 1
//
//How many different ways can one hundred be written as a sum of at least two positive integers?

#include "template_class.h"

extern int calc_76(int n);
extern int calc_76_no_2(int n);
extern int calc_76_limit(int first, int n);

int calc_76_limit(int first, int n)
{
    if(n == 0)
    {
        return 1;
    }
    if(n == 1)
    {
        return 1;
    }
    if(first == 1)
    {
        return 1;
    }
    int sum = 0;
    for(int i = first; i >= 1; i--)
    {
        if(n - i > i)
        {
            sum += calc_76_limit(i, n - i);
        }
        else
        {
            sum += calc_76_no_2(n - i);
        }
    }
    return sum;
}

int calc_76_no_2(int n)
{
    if(n == 0)
    {
        return 1;
    }
    if(n == 1)
    {
        return 1;
    }
    if(n == 2)
    {
        return 2;
    }
    int sum = 0;
    for(int i = n; i > 0; i--)
    {
        if(i >= n - i)
        {
            sum += calc_76_no_2(n - i);
        }
        else
        {
            sum += calc_76_limit(i, n - i);
        }
    }
    return sum;
}

int calc_76(int n)
{
    if(n == 0)
    {
        return 0;
    }
    if(n == 1)
    {
        return 1;
    }
    int sum = 0;
    for(int i = n - 1; i > 0; i--)
    {
        if(i >= n - i)
        {
            sum += calc_76_no_2(n - i);
        }
        else
        {
            sum += calc_76_limit(i, n - i);
        }
    }
    return sum;
}


int problem_76(int n)
{
    int d = calc_76(n);
    return d;
}
