//Powerful digit counts
//Problem 63
//The 5-digit number, 16807=75, is also a fifth power. Similarly, the 9-digit number, 134217728=89, is a ninth power.
//
//How many n-digit positive integers exist which are also an nth power?

#include "template_class.h"

extern void pow_a_b(int a, int b, vector<int>& result);

int problem_63()
{
    int limit = log(9) / (log(10) - log(9)) + 1;
    int count = 0;
    for(int x = 1; x < 10; x++)
    {
        for(int n = 1; n <= limit; n++)
        {
            vector<int> result;
            pow_a_b(x, n, result);
            if(result.size() == n)
            {
                count++;
            }
        }
    }
    return count;
}
