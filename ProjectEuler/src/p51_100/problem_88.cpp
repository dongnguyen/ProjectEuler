// Product-sum numbers
// Problem 88
// A natural number, N, that can be written as the sum and product of a given set of at least two natural numbers, {a1, a2, ... , ak} is called a product-sum number: N = a1 + a2 + ... + ak = a1 � a2 � ... � ak.

// For example, 6 = 1 + 2 + 3 = 1 � 2 � 3.

// For a given set of size, k, we shall call the smallest N with this property a minimal product-sum number. The minimal product-sum numbers for sets of size, k = 2, 3, 4, 5, and 6 are as follows.

// k=2: 4 = 2 � 2 = 2 + 2
// k=3: 6 = 1 � 2 � 3 = 1 + 2 + 3
// k=4: 8 = 1 � 1 � 2 � 4 = 1 + 1 + 2 + 4
// k=5: 8 = 1 � 1 � 2 � 2 � 2 = 1 + 1 + 2 + 2 + 2
// k=6: 12 = 1 � 1 � 1 � 1 � 2 � 6 = 1 + 1 + 1 + 1 + 2 + 6

// Hence for 2=k=6, the sum of all the minimal product-sum numbers is 4+6+8+12 = 30; note that 8 is only counted once in the sum.

// In fact, as the complete set of minimal product-sum numbers for 2=k=12 is {4, 6, 8, 12, 15, 16}, the sum is 61.

// What is the sum of all the minimal product-sum numbers for 2=k=12000?

#include "template_class.h"

vector<vector<int>> memo_88;

vector<vector<int>> possible_represent(int n, int limit)
{
    vector<vector<int>> ret;
    if(memo_88[n].size() == 0)
    {
        return ret;
    }
    int idx = 0;
    for(int i = 0; i < memo_88[n].size(); i++)
    {
        int take_one = memo_88[n][i];
        if(take_one < limit)
        {
            continue;
        }
        int remain = n / take_one;
        vector<vector<int>> tmp = possible_represent(remain, take_one);
        ret.resize(ret.size() + tmp.size() + 1);
        ret[idx].push_back(take_one);
        ret[idx].push_back(remain);
        idx++;
        int size = tmp.size();
        //for(int i =
        for(int j = 0; j < size; j++)
        {
            ret[idx].push_back(take_one);
            for(int m = 0; m < tmp[j].size(); m++)
            {
                ret[idx].push_back(tmp[j][m]);
            }
            idx++;
        }
    }
    return ret;
}

int sum_vector(vector<int>& n)
{
    int ret = 0;
    for(int i = 0; i < n.size(); i++)
    {
        ret += n[i];
    }
    return ret;
}

uint64_t problem_88(int n)
{
    int size = 1e5;
    vector<int> k;
    memo_88.resize(size);
    k.resize(n+1);
    for(int i = 2; i <= n; i++)
    {
        k[i] = INT_MAX;
    }
    int limit = sqrt(size);
    for(int i = 2; i <= limit; i++)
    {
        for(int j = i*i; j < size; j += i)
        {
            memo_88[j].push_back(i);
        }
    }

    set<int> tmp;

    int stop = n - 1;
    int count = 0;
    for(int i = 4; ; i++)
    {
        vector<vector<int>> all_represent = possible_represent(i, 1);
        for(int j = 0; j < all_represent.size(); j++)
        {
            int tk = all_represent[j].size();
            int tsum = sum_vector(all_represent[j]);
            tk += (i - tsum);
            if((tk <= n) && (tk >= 2))
            {
                if(k[tk] == INT_MAX)
                {
                    count++;
                    k[tk] = i;
                    tmp.insert(i);
                    if(count == stop)
                    {
                        goto END;
                    }
                }
            }
        }
    }
END:
    uint64_t ret = 0;
    for(set<int>::iterator ite = tmp.begin(); ite != tmp.end(); ++ite)
    {
        ret += *ite;
    }
    return ret;
}