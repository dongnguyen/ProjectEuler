//Spiral primes
//Problem 58
//Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.
//
//37 36 35 34 33 32 31
//38 17 16 15 14 13 30
//39 18  5  4  3 12 29
//40 19  6  1  2 11 28
//41 20  7  8  9 10 27
//42 21 22 23 24 25 26
//43 44 45 46 47 48 49
//
//It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ? 62%.
//
//If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued, what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?

#include "template_class.h"

extern void create_prime_filter_method(int n, bool need_set = false);
extern set<int> set_prime_list;
extern void create_prime_list(int x);
extern vector<int> prime_list;
extern bool prime_test3(int x);

int problem_58()
{
    /*prime_list.push_back(2);
    prime_list.push_back(3);
    set_prime_list.insert(2);
    set_prime_list.insert(3);
    create_prime_list(1e6);*/
    create_prime_filter_method(3e4, false);
    int side_len = 1;
    int count_prime = 0;
    int count_total = 1;
    int step = 2;
    int start = 1;
    while(1)
    {
        side_len += 2;
        start += step;
        //if(set_prime_list.find(start) != set_prime_list.end())
        if(prime_test3(start))
        {
            count_prime++;
        }
        start += step;
        //if(set_prime_list.find(start) != set_prime_list.end())
        if(prime_test3(start))
        {
            count_prime++;
        }
        start += step;
        //if(set_prime_list.find(start) != set_prime_list.end())
        if(prime_test3(start))
        {
            count_prime++;
        }
        start += step;
        //if(set_prime_list.find(start) != set_prime_list.end())
        if(prime_test3(start))
        {
            count_prime++;
        }
        step += 2;
        count_total += 4;
        double ratio = (double)count_prime / count_total * 100;
        if(ratio <= 10)
        {
            return side_len;
            //return sqrt(start);
        }
    }
    return side_len;
}
