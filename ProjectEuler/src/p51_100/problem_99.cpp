//Largest exponential
//Problem 99
//Comparing two numbers written in index form like 211 and 37 is not difficult, as any calculator would confirm that 211 = 2048 < 37 = 2187.
//
//However, confirming that 632382518061 > 519432525806 would be much more difficult, as both numbers contain over three million digits.
//
//Using base_exp.txt (right click and 'Save Link/Target As...'), a 22K text file containing one thousand lines with a base/exponent pair on each line, determine which line number has the greatest numerical value.
//
//NOTE: The first two lines in the file represent the numbers in the example given above.

#include "template_class.h"

#define LIMIT_99 1000

struct int2
{
    int m;
    int n;
    int2(int a, int b)
    {
        m = a;
        n = b;
    }
    int2()
    {
        m = 0;
        n = 0;
    }
};

vector<int2> input;
void read_99(char* path)
{
    FILE* fp = fopen(path, "r");
    for(int i = 0; i < LIMIT_99; i++)
    {
        int2 tmp(0, 0);
        fscanf(fp, "%d,%d", &tmp.m, &tmp.n);
        input[i] = tmp;
    }
    fclose(fp);
}

int problem_99(char* path)
{
    int n = LIMIT_99;
    input.resize(n);
    read_99(path);
    int line = 0;
    double max = 0.0;
    for(int i = 0; i < LIMIT_99; i++)
    {
        double tmp = input[i].n*log(input[i].m);
        if(max < tmp)
        {
            line = i;
            max = tmp;
        }
    }
    return line+1;
}
