//Amicable chains
//Problem 95
//The proper divisors of a number are all the divisors excluding the number itself. For example, the proper divisors of 28 are 1, 2, 4, 7, and 14. As the sum of these divisors is equal to 28, we call it a perfect number.
//
//Interestingly the sum of the proper divisors of 220 is 284 and the sum of the proper divisors of 284 is 220, forming a chain of two numbers. For this reason, 220 and 284 are called an amicable pair.
//
//Perhaps less well known are longer chains. For example, starting with 12496, we form a chain of five numbers:
//
//12496 ? 14288 ? 15472 ? 14536 ? 14264 (? 12496 ? ...)
//
//Since this chain returns to its starting point, it is called an amicable chain.
//
//Find the smallest member of the longest amicable chain with no element exceeding one million.

#include "template_class.h"

#define NOT_CHECK 0
#define VALID_CHAIN 1
#define BROKEN_CHAIN 2

int problem_95()
{
    int n = 1e6;
    vector<int> amicable_memo;
    amicable_memo.resize(n);
    for(int i = 1; i < n / 2; i++)
    {
        for(int j = 2*i; j < n; j += i)
        {
            amicable_memo[j] += i;
        }
    }
    // Debug
    /*int count = 0;
    for(int i = 0; i < n; i++)
    {
        if(amicable_memo[i] > n)
        {
            count++;
        }
    }
    return count;*/


    vector<vector<int>> pre_compute;
    pre_compute.resize(n);

    pre_compute[0].push_back(BROKEN_CHAIN);
    for(int i = 1; i < n; i++)
    {
        pre_compute[i].push_back(NOT_CHECK);
    }

    for(int i = 1; i < n; i++)
    {
        switch(pre_compute[i][0])
        {
        case NOT_CHECK:
        {
            int starting = i;
            int next = starting;
            vector<int> tmp_memo;
            tmp_memo.push_back(starting);
            while(1)
            {
                next = amicable_memo[next];
                if((next == 0) || (next >= n))
                {
                    for(int i = 0; i < tmp_memo.size(); i++)
                    {
                        pre_compute[tmp_memo[i]][0] = BROKEN_CHAIN;
                    }
                    break;
                }
                if(pre_compute[next][0] == VALID_CHAIN)
                {
                    for(int i = 0; i < tmp_memo.size(); i++)
                    {
                        pre_compute[tmp_memo[i]][0] = BROKEN_CHAIN;
                    }
                    break;
                }
                int found_idx = -1;
                for(int i = 0; i < tmp_memo.size(); i++)
                {
                    if(next == tmp_memo[i])
                    {
                        found_idx = i;
                    }
                }
                if(found_idx != -1)
                {
                    for(int i = 0; i < found_idx; i++)
                    {
                        pre_compute[tmp_memo[i]][0] = BROKEN_CHAIN;
                    }
                    pre_compute[next][0] = VALID_CHAIN;
                    for(int i = found_idx + 1; i < tmp_memo.size(); i++)
                    {
                        pre_compute[next].push_back(tmp_memo[i]);
                        pre_compute[tmp_memo[i]][0] = BROKEN_CHAIN;
                    }
                    break;
                }
                tmp_memo.push_back(next);
            }
        }
        break;
        case VALID_CHAIN:
        case BROKEN_CHAIN:
            break;
        default:
            cout << "assert error" << endl;
            exit(-1);
        }
    }
    //cout << "here" << endl;
    int max_len = 0;
    int start = 0;
    for(int i = 0; i < pre_compute.size(); i++)
    {
        if(pre_compute[i][0] == VALID_CHAIN)
        {
            if(max_len < pre_compute[i].size())
            {
                max_len = pre_compute[i].size();
                start = i;
            }
        }
    }
    //cout << "here2" << endl;
    int min_element = start;
    //cout << start << " ";
    for(int i = 2; i < pre_compute[start].size(); i++)
    {
        //cout << pre_compute[start][i] << " ";
        if(min_element > pre_compute[start][i])
        {
            min_element = pre_compute[start][i];
        }
    }
    return min_element;
}
