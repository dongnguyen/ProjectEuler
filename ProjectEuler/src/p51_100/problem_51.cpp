//Prime digit replacements
//Problem 51
//By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
//
//By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.
//
//Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_list(int x);
extern bool check_in_list(int n, vector<int>& list, int idx = 0);
extern void combination(int offset, int k, vector<int>& set, vector<int>& com, vector<vector<int>>& outset);

int count_num_family(int n, vector<int>& list, vector<int>& out_sequence)
{
    /*if(n < 10)
    {
        int count = 0;
        for(int i = 0; i < 10; i++)
        {
            if(check_in_list(i, list))
            {
                count++;
            }
        }
        return count;
    }
    if(n < 100)
    {
        int max = 0;
        for(int i = 0; i < 10; i++)
        {
            int tmp = i * 10 + (n % 10);
            if(check_in_list(tmp, list))
            {
                max++;
            }
        }
        int count = 0;
        for(int i = 0; i < 10; i++)
        {
            int tmp = i + (n / 10) * 10;
            if(check_in_list(tmp, list))
            {
                count++;
            }
        }
        if(max < count)
        {
            max = count;
        }
        count = 0;
        for(int i = 0; i < 10; i++)
        {
            int tmp = i + i * 10;
            if(check_in_list(tmp, list))
            {
                count++;
            }
        }
        if(max < count)
        {
            max = count;
        }
        return max;
    }
    if(n < 1000)*/
    {
        vector<int> digits;
        int tmp = n;
        while(tmp)
        {
            digits.push_back(tmp % 10);
            tmp /= 10;
        }
        int max = 0;
        for(int k = 1; k < digits.size(); k++)
        {
            vector<int> com;
            vector<vector<int>> outset;
            vector<int> set;
            set.resize(digits.size());
            for(int i = 0; i < digits.size(); i++)
            {
                set[i] = i;
            }
            combination(1, k, set, com, outset);
            for(int i = 0; i < outset.size(); i++)
            {
                /*if(k == 3 && i == 14)
                {
                    int d = 0;
                    d++;
                }*/
                int count = 0;
                vector<int> tmp_out_sequence;
                for(int m = 0; m < 10; m++)
                {
                    vector<int> tmp_digits;
                    tmp_digits.assign(digits.begin(), digits.end());
                    for(int j = 0; j < k; j++)
                    {
                        tmp_digits[outset[i][j]] = m;
                    }
                    int tmp = 0;
                    for(int j = tmp_digits.size() - 1; j >= 0; j--)
                    {
                        tmp = tmp * 10 + tmp_digits[j];
                    }
                    if(check_in_list(tmp, list))
                    {
                        tmp_out_sequence.push_back(tmp);
                        count++;
                    }
                }
                if(max < count)
                {
                    out_sequence.assign(tmp_out_sequence.begin(), tmp_out_sequence.end());
                    max = count;
                }
            }
        }
        return max;
    }
    return 0;
}

int problem_51()
{
    /*vector<int> set;
    vector<int> com;
    vector<vector<int>> outset;
    set.resize(5);
    set[0] = 0;
    set[1] = 1;
    set[2] = 2;
    set[3] = 3;
    set[4] = 4;
    combination(0, 5, set, com, outset);*/

    create_prime_list(1e6);
    int found_idx = 0;
    for(; found_idx < prime_list.size(); found_idx++)
    {
        if(prime_list[found_idx] > 1e5)
        {
            break;
        }
    }
    vector<int> prime_list_n_digits;
    prime_list_n_digits.assign(prime_list.begin() + found_idx, prime_list.end());

    vector<int> out_sequence;
    //count_num_family(121313, prime_list_n_digits, out_sequence);


    for(int i = 0; i < prime_list_n_digits.size(); i++)
    {
        if(8 == count_num_family(prime_list_n_digits[i], prime_list_n_digits, out_sequence))
        {
            return out_sequence[0];
        }
    }
    return -1;
}
