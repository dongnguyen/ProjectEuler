//Coin partitions
//Problem 78
//Let p(n) represent the number of different ways in which n coins can be separated into piles. For example, five coins can separated into piles in exactly seven different ways, so p(5)=7.
//
//OOOOO
//OOOO   O
//OOO   OO
//OOO   O   O
//OO   OO   O
//OO   O   O   O
//O   O   O   O   O
//Find the least value of n for which p(n) is divisible by one million.

#include "template_class.h"

extern uint64_t calc_78(uint64_t n);
extern uint64_t calc_78_limit(uint64_t first, uint64_t n);

vector<uint64_t> memo;
vector<vector<uint64_t>> memo_limit;

uint64_t calc_78_limit(uint64_t first, uint64_t n)
{
    /*if(n >= memo_limit.size())
    {
        int d = 0;
        d++;
    }
    if(first >= memo_limit[n].size())
    {
        int d = 0;
        d++;
    }*/
    if(memo_limit[n][first])
    {
        return memo_limit[n][first];
    }
    if(n == 0)
    {
        return 1;
    }
    if(n == 1)
    {
        return 1;
    }
    if(first == 1)
    {
        return 1;
    }
    uint64_t sum = 0;
    for(uint64_t i = first; i >= 1; i--)
    {
        if(n - i > i)
        {
            sum += calc_78_limit(i, n - i);
            //sum += calc_78(n % i);
        }
        else
        {
            sum += calc_78(n - i);
        }
    }
    sum %= 1000000;
    memo_limit[n][first] = sum;
    return sum;
}

uint64_t calc_78(uint64_t n)
{
    if(memo[n])
    {
        return memo[n];
    }
    if(n == 0)
    {
        return 1;
    }
    if(n == 1)
    {
        return 1;
    }
    if(n == 2)
    {
        return 2;
    }
    uint64_t sum = 0;
    for(uint64_t i = n; i > 0; i--)
    {
        if(i >= n - i)
        {
            sum += calc_78(n - i);
        }
        else
        {
            sum += calc_78_limit(i, n - i);
            //sum += calc_78(n % i);
        }
    }
    sum %= 1000000;
    memo[n] = sum;
    return sum;
}

// http://en.wikipedia.org/wiki/Partition_(number_theory)#Exact_formula

extern int32_t pentagonal_number(int32_t n);
extern int64_t pentagonal_number(int64_t n);

uint64_t problem_78()
{
    vector<int> pentagonal_numbers;
    pentagonal_numbers.push_back(0);
    for(int i = 1;; i++)
    {
        int tmp = pentagonal_number(i);
        if(tmp < 4e6)
        {
            pentagonal_numbers.push_back(tmp);
        }
        else
        {
            break;
        }
        tmp = pentagonal_number(-i);
        if(tmp < 4e6)
        {
            pentagonal_numbers.push_back(tmp);
        }
        else
        {
            break;
        }
    }
    vector<int> partition_numbers;
    partition_numbers.resize(4e6);
    partition_numbers[0] = 1;
    partition_numbers[1] = 1;
    for(int n = 2;; n++)
    {
        int sign = 1;
        int sum = 0;
        for(int k = 1;; k++)
        {
            if(n - pentagonal_numbers[k] >= 0)
            {
                sum += sign*partition_numbers[n - pentagonal_numbers[k]];
            }
            else
            {
                partition_numbers[n] = sum % 1000000;
                if(partition_numbers[n] == 0)
                {
                    return n;
                }
                break;
            }
            if((k & 1) == 0)
            {
                sign = -sign;
            }
        }
    }
    return 0;
#if 0
    memo.resize(1000);
    memo_limit.resize(1000);
    for(int i = 0; i < 1000; i++)
    {
        memo_limit[i].resize(1000);
    }
    for(int i = 1; ; i++)
    {
        if(i >= memo.size())
        {
            memo.resize(i*10);
            memo_limit.resize(i*10);
            for(int j = 0; j < i*10; j++)
            {
                memo_limit[j].resize(i*10);
            }
        }
        uint64_t tmp = calc_78(i);
        if(tmp == 0)
        {
            return i;
        }
    }
    return memo[100];
#endif
}
