//Singular integer right triangles
//Problem 75
//It turns out that 12 cm is the smallest length of wire that can be bent to form an integer sided right angle triangle in exactly one way, but there are many more examples.
//
//12 cm: (3,4,5)
//24 cm: (6,8,10)
//30 cm: (5,12,13)
//36 cm: (9,12,15)
//40 cm: (8,15,17)
//48 cm: (12,16,20)
//
//In contrast, some lengths of wire, like 20 cm, cannot be bent to form an integer sided right angle triangle, and other lengths allow more than one solution to be found; for example, using 120 cm it is possible to form exactly three different integer sided right angle triangles.
//
//120 cm: (30,40,50), (20,48,52), (24,45,51)
//
//Given that L is the length of the wire, for how many values of L ? 1,500,000 can exactly one integer sided right angle triangle be formed?

#include "template_class.h"

struct pythago
{
    int a;
    int b;
    int c;
    pythago()
    {
        a = 0;
        b = 0;
        c = 0;
    }
    pythago(int m, int n, int p)
    {
        a = m;
        b = n;
        c = p;
    }
    int sum()
    {
        return a+b+c;
    }
};

int problem_75()
{
    int limit = 1.5e6;
    vector<int> count_L;
    count_L.resize(limit + 1);
    pythago start(3,4,5);
    queue<pythago> all_primitive;
    all_primitive.push(start);
    while(!all_primitive.empty())
    {
        pythago tmp = all_primitive.front();
        all_primitive.pop();
        int sum = tmp.sum();
        for(int i = sum; i <= limit; i += sum)
        {
            count_L[i]++;
        }
        pythago T1(tmp.a - 2*tmp.b + 2*tmp.c, 2*tmp.a - tmp.b + 2*tmp.c, 2*tmp.a - 2*tmp.b + 3*tmp.c);
        pythago T2(tmp.a + 2*tmp.b + 2*tmp.c, 2*tmp.a + tmp.b + 2*tmp.c, 2*tmp.a + 2*tmp.b + 3*tmp.c);
        pythago T3(-tmp.a + 2*tmp.b + 2*tmp.c, -2*tmp.a + tmp.b + 2*tmp.c, -2*tmp.a + 2*tmp.b + 3*tmp.c);
        if(T1.sum() <= limit)
        {
            all_primitive.push(T1);
        }
        if(T2.sum() <= limit)
        {
            all_primitive.push(T2);
        }
        if(T3.sum() <= limit)
        {
            all_primitive.push(T3);
        }
    }
    int count = 0;
    for(int i = 0; i <= limit; i++)
    {
        if(count_L[i] == 1)
        {
            count++;
        }
    }
    return count;
}

#if 0

extern bool is_square(uint64_t n);

int problem_75(int n)
{
    /*int limit = sqrt(n);
    vector<int> square;
    set<int> square_set;
    vector<int> num_count;
    square.resize(limit + 1);
    num_count.resize(n + 1);
    for(int i = 0; i <= limit; i++)
    {
        square[i] = i*i;
        square_set.insert(i*i);
    }
    for(int i = 1; i < limit - 1; i++)
    {
        for(int j = i + 1; j < limit; j++)
        {
            int tmp = square[i] + square[j];
            if(square_set.find(tmp) != square_set.end())
            {
                int L = sqrt(tmp) + i + j;
                if(L <= n)
                {
                    num_count[L]++;
                }
            }
        }
    }*/
    vector<int> L_count;
    L_count.resize(n + 1);

    for(uint64_t i = 3; i < n/2; i++)
    {
        int limit = i * i;
        for(uint64_t j = i + 1;; j++)
        {
            int inc = 2*j + 1;
            if(limit < inc)
            {
                break;
            }
            uint64_t tmp = limit + j * j;
            if(i + j >= n)
            {
                goto EXIT;
            }
            if(is_square(tmp))
            {
                int L = sqrt(tmp) + i + j;
                if(L <= n)
                {
                    L_count[L]++;
                }
                else
                {
                    break;
                }
            }
        }
    }
EXIT:
    int count = 0;
    for(int i = 0; i <= n; i++)
    {
        if(L_count[i] == 1)
        {
            count++;
        }
    }
    return count;
}

#endif
