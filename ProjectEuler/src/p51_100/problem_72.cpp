//Counting fractions
//Problem 72
//Consider the fraction, n/d, where n and d are positive integers. If n<d and HCF(n,d)=1, it is called a reduced proper fraction.
//
//If we list the set of reduced proper fractions for d ? 8 in ascending order of size, we get:
//
//1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
//
//It can be seen that there are 21 elements in this set.
//
//How many elements would be contained in the set of reduced proper fractions for d ? 1,000,000?

#include "template_class.h"


uint64_t problem_72(int n)
{
    vector<int> prime;
    vector<int> co_prime_num;
    prime.resize(n+1);
    co_prime_num.resize(n+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    int limit = sqrt(n);
    for(int i = 4; i <= n; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 1; i <= n; i++)
    {
        co_prime_num[i] = i;
    }
    double tmp = 1.0 - 1.0/2;
    for(int j = 2; j <= n; j += 2)
    {
        co_prime_num[j] *= tmp;
    }
    for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            int nume = i - 1;
            for(int j = i; j <= n; j += i)
            {
                co_prime_num[j] = co_prime_num[j] / i * nume;
            }
            for(int j = i*i; j <= n; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    for(int i = limit + 1; i <= n; i++)
    {
        if(prime[i] == PRIME)
        {
            int nume = i - 1;
            for(int j = i; j <= n; j += i)
            {
                co_prime_num[j] = co_prime_num[j] / i * nume;
            }
        }
    }
    uint64_t ret = 0;
    for(int i = 2; i <= n; i++)
    {
        ret += co_prime_num[i];
    }
    return ret;
}
