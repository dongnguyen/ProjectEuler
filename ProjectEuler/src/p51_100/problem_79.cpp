//Passcode derivation
//Problem 79
//A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.
//
//The text file, keylog.txt, contains fifty successful login attempts.
//
//Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.

#include "template_class.h"

void read_input_79(char* path, vector<vector<int>>& input)
{
    FILE* fp = fopen(path, "r");
    while(!feof(fp))
    {
        int tmp;
        fscanf(fp, "%d,", &tmp);
        vector<int> vtmp;
        while(tmp)
        {
            vtmp.push_back(tmp % 10);
            tmp /= 10;
        }
        for(int i = 0; i < vtmp.size()/2; i++)
        {
            swap(vtmp[i], vtmp[vtmp.size() - 1 - i]);
        }
        input.push_back(vtmp);
    }
    input.resize(input.size() - 1);
    fclose(fp);
}

struct num_79
{
    num_79()
    {
        num = 0;
    }
    int num;
    vector<int> left;
    vector<int> right;
};

#define INVALID  0
#define LARGER   1
#define SMALLER  2
#define HAVE_TWO 3

int compare(int a, int b, vector<vector<int>>& input)
{
    vector<int> result;
    for(int i = 0; i < input.size(); i++)
    {
        int found_a = -1;
        int found_b = -1;
        for(int j = 0; j < input[i].size(); j++)
        {
            if(input[i][j] == a)
            {
                found_a = j;
            }
            else if(input[i][j] == b)
            {
                found_b = j;
            }
            if(found_a != -1 && found_b != -1)
            {
                result.push_back(found_a > found_b ? LARGER : SMALLER);
            }
        }
    }
    if(result.size() == 0)
    {
        return INVALID;
    }
    for(int i = 1; i < result.size(); i++)
    {
        if(result[i] != result[0])
        {
            return HAVE_TWO;
        }
    }
    return result[0];
}

int problem_79(char* path)
{
    vector<vector<int>> input;
    //vector<int> passcode;
    read_input_79(path, input);
    for(int i = 0; i < input.size() - 1; i++)
    {
        for(int j = i + 1; j < input.size(); j++)
        {
            int equal = true;
            for(int k = 0; k < input[i].size(); k++)
            {
                if(input[i][k] != input[j][k])
                {
                    equal = false;
                    break;
                }
            }
            if(equal)
            {
                input.erase(input.begin() + j);
                j--;
            }
        }
    }
    /*passcode = input.back();
    input.resize(input.size() - 1);
    for(int i = 0; i < input.size(); i++)
    {
        if(passcode[passcode.size() - 2] == input[i][0] &&
           passcode[passcode.size() - 1] == input[i][1])
        {
            for(int k = 2; k < input[i].size(); k++)
            {
                passcode.push_back(input[i][k]);
            }
            input.erase(input.begin() + i);
            i--;
        }
        else if(passcode[0] == input[i][input[i].size() - 2] &&
                passcode[1] == input[i][input[i].size() - 1])
        {
            for(int k = input[i].size() - 3; k >= 0; k--)
            {
                passcode.insert(passcode.begin(), input[i][k]);
            }
            input.erase(input.begin() + i);
            i--;
        }
    }*/
    num_79 result[10];
    for(int i = 0; i < 10; i++)
    {
        result[i].num = 0;
        for(int j = 0; j < input.size(); j++)
        {
            int count = 0;
            for(int k = 0; k < input[j].size(); k++)
            {
                if(input[j][k] == i)
                {
                    count++;
                }
            }
            if(result[i].num < count)
            {
                result[i].num = count;
            }
        }
    }
    int have_ambiguous = 0;
    for(int i = 0; i < 9; i++)
    {
        if(result[i].num)
        {
            for(int j = i + 1; j < 10; j++)
            {
                if(result[j].num)
                {
                    int tmp = compare(i, j, input);
                    if(LARGER == tmp)
                    {
                        result[i].left  .push_back(j);
                        result[j].right .push_back(i);
                    }
                    else if(SMALLER == tmp)
                    {
                        result[j].left  .push_back(i);
                        result[i].right .push_back(j);
                    }
                    else
                    {
                        have_ambiguous++;
                    }
                }
            }
        }
    }
    int ret = 0;
    if(have_ambiguous == 0)
    {
        int right_max = 0;
        int val = 0;
        for(int i = 0; i < 10; i++)
        {
            if(result[i].num)
            {
                if(right_max < result[i].right.size())
                {
                    right_max = result[i].right.size();
                    val = i;
                }
            }
        }
        ret = ret*10 + val;
        right_max--;
        while(right_max >= 0)
        {
            for(int i = 0; i < 10; i++)
            {
                if(result[i].num)
                {
                    if(right_max == result[i].right.size())
                    {
                        val = i;
                    }
                }
            }
            ret = ret*10 + val;
            right_max--;
        }
    }
    return ret;
}