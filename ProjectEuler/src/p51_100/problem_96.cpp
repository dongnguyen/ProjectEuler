//Su Doku
//Problem 96
//Su Doku(Japanese meaning number place) is the name given to a popular puzzle concept.Its origin is unclear, but credit must be attributed to Leonhard Euler who invented a similar, and much more difficult, puzzle idea called Latin Squares.The objective of Su Doku puzzles, however, is to replace the blanks(or zeros) in a 9 by 9 grid in such that each row, column, and 3 by 3 box contains each of the digits 1 to 9. Below is an example of a typical starting puzzle grid and its solution grid.
//
//0 0 3
//9 0 0
//0 0 1	0 2 0
//3 0 5
//8 0 6	6 0 0
//0 0 1
//4 0 0
//0 0 8
//7 0 0
//0 0 6	1 0 2
//0 0 0
//7 0 8	9 0 0
//0 0 8
//2 0 0
//0 0 2
//8 0 0
//0 0 5	6 0 9
//2 0 3
//0 1 0	5 0 0
//0 0 9
//3 0 0
//
//4 8 3
//9 6 7
//2 5 1	9 2 1
//3 4 5
//8 7 6	6 5 7
//8 2 1
//4 9 3
//5 4 8
//7 2 9
//1 3 6	1 3 2
//5 6 4
//7 9 8	9 7 6
//1 3 8
//2 4 5
//3 7 2
//8 1 4
//6 9 5	6 8 9
//2 5 3
//4 1 7	5 1 4
//7 6 9
//3 8 2
//A well constructed Su Doku puzzle has a unique solution and can be solved by logic, although it may be necessary to employ "guess and test" methods in order to eliminate options(there is much contested opinion over this).The complexity of the search determines the difficulty of the puzzle; the example above is considered easy because it can be solved by straight forward direct deduction.
//
//The 6K text file, sudoku.txt(right click and 'Save Link/Target As...'), contains fifty different Su Doku puzzles ranging in difficulty, but all with unique solutions(the first puzzle in the file is the example above).
//
//By solving all fifty puzzles find the sum of the 3 - digit numbers found in the top left corner of each solution grid; for example, 483 is the 3 - digit number found in the top left corner of the solution grid above.

#include "template_class.h"

set<int> default_pro;

class stage_sudoku
{
public:
    set<int> stage[9][9];
    int count_remain;
    bool complete;
    bool processed[9][9];
    bool is_valid;
    stage_sudoku()
    {
        count_remain = 81;
        complete = false;
        is_valid = true;
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                processed[i][j] = false;
            }
        }
    }
    void init(int input[9][9])
    {
        // pre
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                int t_i = input[i][j];
                if (t_i)
                {
                    stage[i][j].insert(t_i);
                }
                else
                {
                    stage[i][j] = default_pro;
                }
            }
        }
        // process
        process();
    }
    void process()
    {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if ((stage[i][j].size() == 1) && (processed[i][j] == false))
                {
                    process(i, j);
                    if (complete)
                    {
                        return;
                    }
                    if (!is_valid)
                    {
                        return;
                    }
                }
            }
        }
    }
    void process(int i, int j)
    {
        processed[i][j] = true;
        count_remain--;
        if (count_remain == 0)
        {
            complete = true;
            return;
        }
        int t_i = *stage[i][j].begin();
        // col
        for (int r = 0; r < 9; r++)
        {
            if (r != i)
            {
                int pre_size = stage[r][j].size();
                stage[r][j].erase(t_i);
                int new_size = stage[r][j].size();
                if (pre_size != new_size)
                {
                    if (stage[r][j].size() == 1)
                    {
                        process(r, j);
                    }
                    else if (stage[r][j].size() == 0)
                    {
                        is_valid = false;
                        return;
                    }
                }
            }
        }
        // row
        for (int c = 0; c < 9; c++)
        {
            if (c != j)
            {
                int pre_size = stage[i][c].size();
                stage[i][c].erase(t_i);
                int new_size = stage[i][c].size();
                if (pre_size != new_size)
                {
                    if (stage[i][c].size() == 1)
                    {
                        process(i, c);
                    }
                    else if (stage[i][c].size() == 0)
                    {
                        is_valid = false;
                        return;
                    }
                }
            }
        }
        // block
        int start_i = (i / 3) * 3;
        int start_j = (j / 3) * 3;
        for (int r = start_i; r < start_i + 3; r++)
        {
            for (int c = start_j; c < start_j + 3; c++)
            {
                if ((r != i) && (c != j))
                {
                    int pre_size = stage[r][c].size();
                    stage[r][c].erase(t_i);
                    int new_size = stage[r][c].size();
                    if (pre_size != new_size)
                    {
                        if (stage[r][c].size() == 1)
                        {
                            process(r, c);
                        }
                        else if (stage[r][c].size() == 0)
                        {
                            is_valid = false;
                            return;
                        }
                    }
                }
            }
        }
    }
};

vector<stage_sudoku> sudokus;

void read_input_96(char* path)
{
    string line;
    ifstream infile;
    infile.open(path);
    while (!infile.eof())
    {
        getline(infile, line);
        if (line[0] == 'G')
        {
            int input[9][9];
            for (int i = 0; i < 9; i++)
            {
                getline(infile, line);
                for (int j = 0; j < 9; j++)
                {
                    input[i][j] = line[j] - '0';
                }
            }
            stage_sudoku stage;
            stage.init(input);
            sudokus.push_back(stage);
        }
    }
    infile.close();
}

bool solve_sudoku(stage_sudoku& stage)
{
    if (stage.complete == true)
    {
        return true;
    }
    if (stage.is_valid == false)
    {
        return false;
    }
    // brute-force
    int min_size = 9;
    int r = 0;
    int c = 0;
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            if (stage.stage[i][j].size() != 1)
            {
                if (min_size > stage.stage[i][j].size())
                {
                    min_size = stage.stage[i][j].size();
                    r = i;
                    c = j;
                }
            }
        }
    }
    bool ret = false;
    for (set<int>::iterator ite = stage.stage[r][c].begin(); ite != stage.stage[r][c].end(); ++ite)
    {
        int guess = *ite;
        stage_sudoku new_stage = stage;
        new_stage.stage[r][c].clear();
        new_stage.stage[r][c].insert(guess);
        new_stage.process(r, c);
        if (new_stage.is_valid == false)
        {
            continue;
        }
        if (new_stage.complete)
        {
            stage = new_stage;
            return true;
        }
        else
        {
            if (solve_sudoku(new_stage))
            {
                stage = new_stage;
                return true;
            }
            else
            {
                continue;
            }
        }
    }
    return ret;
}

int problem_96(char* path)
{
    for (int i = 1; i < 10; i++)
    {
        default_pro.insert(i);
    }
    read_input_96(path);
    int ret = 0;
    for (int i = 0; i < sudokus.size(); i++)
    {
        if (solve_sudoku(sudokus[i]))
        {
            ret += *sudokus[i].stage[0][0].begin() * 100 +
                   *sudokus[i].stage[0][1].begin() * 10 +
                   *sudokus[i].stage[0][2].begin();
        }
        else
        {
            cout << "some thing is wrong!!" << endl;
        }
    }
    return ret;
}