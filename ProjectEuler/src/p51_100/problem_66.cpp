//Diophantine equation
//Problem 66
//Consider quadratic Diophantine equations of the form:
//
//x2 � Dy2 = 1
//
//For example, when D=13, the minimal solution in x is 6492 � 13�1802 = 1.
//
//It can be assumed that there are no solutions in positive integers when D is square.
//
//By finding minimal solutions in x for D = {2, 3, 5, 6, 7}, we obtain the following:
//
//32 � 2�22 = 1
//22 � 3�12 = 1
//92 � 5�42 = 1
//52 � 6�22 = 1
//82 � 7�32 = 1
//
//Hence, by considering minimal solutions in x for D ? 7, the largest x is obtained when D=5.
//
//Find the value of D ? 1000 in minimal solutions of x for which the largest value of x is obtained.

// http://en.wikipedia.org/wiki/Pell%27s_equation

#include "template_class.h"
#include "BigIntegerLibrary.hh"

bool is_square(uint64_t n)
{
    uint64_t tmp = sqrt(n);
    if(n == tmp * tmp)
    {
        return true;
    }
    return false;
}

#define TEST_LIMIT 200000

uint64_t calc_x_66(uint64_t d)
{
    if(is_square(d))
    {
        return 0;
    }
    /*for(uint64_t y = 1;; y++)
    {
        uint64_t x = 1 + d*y*y;
        if(is_square(x))
        {
            return sqrt(x);
        }
    }*/
    for(int n = 1; n < TEST_LIMIT; n++)
    {
        if(is_square(n*(n*d+2)))
        {
            return n*d+1;
        }
        if(is_square(n*(n*d-2)))
        {
            return n*d-1;
        }
    }
    return TEST_LIMIT;
}

uint64_t calc_x_66_2(uint64_t d)
{
    if(is_square(d))
    {
        return 0;
    }
    double sqrt_d = sqrt(d);
    for(uint64_t i = 1;; i++)
    {
        uint64_t h = ceil(sqrt_d*i);
        if(h*h - d*i*i == 1)
        {
            return h;
        }
    }
    return 0;
}


extern vector<int> continued_fractions(int n);
extern void mul_two_vector(vector<int>& a, vector<int>& b, vector<int>& result);

bool compare_two_vector(vector<int>& a, vector<int>& b)
{
    if(a.size() != b.size())
    {
        return false;
    }
    int size = a.size();
    for(int i = 0; i < size; i++)
    {
        if(a[i] != b[i])
        {
            return false;
        }
    }
    return true;
}

bool check_condition_66(vector<int>& h_digits, vector<int>& k_digits, int d)
{
    vector<int> h_square;
    vector<int> k_square;
    vector<int> val_1;
    vector<int> kd_1_square;
    val_1.push_back(1);
    mul_two_vector(h_digits, h_digits, h_square);
    mul_two_vector(k_digits, k_digits, k_square);
    mul_string_with_n(d, k_square);
    add_two_vector(k_square, val_1, kd_1_square);
    return compare_two_vector(h_square, kd_1_square);
}

struct factor_66
{
    vector<int> num;
    vector<int> denum;
    factor_66() {};
    void revert()
    {
        swap(num, denum);
    }
    factor_66& operator+=(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
    factor_66& operator+(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
};

vector<int> calc_x_66_3(int d)
{
    static uint64_t compare = INT64_MAX - 1000;
    if(d == 26)
    {
        int aa = 0;
        aa++;
    }
    if(is_square(d))
    {
        vector<int> ret;
        ret.push_back(0);
        //return 0;
        return ret;
    }
    vector<int> cont_frac = continued_fractions(d);
    int period = cont_frac.size() - 1;
    uint64_t h = cont_frac[0];
    uint64_t k = 1;
    if(h*h - d*k*k == 1)
    {
        //return h;
        if(h < 10)
        {
            vector<int> ret;
            ret.push_back(h);
            //return 1;
            return ret;
        }
        vector<int> ret;
        ret.push_back(h % 10);
        ret.push_back(h / 10);
        //return 2;
        return ret;
    }
    for(int i = 2;; i++)
    {
        int idx = ((i - 2) % period) + 1;
        int count = i;
        factor_66 fraction;
        fraction.num.push_back(1);
        //h = 1;
        k = cont_frac[idx--];
        while(k)
        {
            fraction.denum.push_back(k % 10);
            k /= 10;
        }
        if(idx < 1)
        {
            idx = period;
        }
        for(; count > 2; count--)
        {
            //h = h + k*cont_frac[idx--];
            fraction += cont_frac[idx--];
            if(idx < 1)
            {
                idx = period;
            }
            //swap(h, k);
            fraction.revert();
        }
        //h = cont_frac[0]*k + h;
        fraction += cont_frac[0];
        //if(h*h - d*k*k == 1)
        /*if(h > INT64_MAX/2)
        {
            return compare++;
        }*/
        if(fraction.num.size() > 1000)
        {
            //return fraction.num.size();
            return fraction.num;
        }
        if(check_condition_66(fraction.num, fraction.denum, d))
        {
            //return h;
            //return fraction.num.size();
            return fraction.num;
        }
    }
}

BigInteger calc_x_66_4(int d)
{
    if(is_square(d))
    {
        return 0;
    }
    vector<int> cont_frac = continued_fractions(d);
    int period = cont_frac.size() - 1;
    BigInteger h_2;
    BigInteger h_1;
    BigInteger h;
    BigInteger k_2;
    BigInteger k_1;
    BigInteger k;
    BigInteger D = d;
    h_2 = 1;
    h_1 = cont_frac[0];
    k_2 = 0;
    k_1 = 1;
    if(h_1*h_1 == D*k_1*k_1+1)
    {
        return h_1;
    }
    for(int i = 0; ; i++)
    {
        BigInteger a = cont_frac[(i % period) + 1];
        h = a*h_1 + h_2;
        k = a*k_1 + k_2;
        if(h*h == D*k*k+1)
        {
            return h;
        }
        h_2 = h_1;
        h_1 = h;
        k_2 = k_1;
        k_1 = k;
    }
    return 0;
}

int problem_66()
{
    BigInteger max = 0;
    int ret = 0;
    for(int d = 0; d <= 1000; d++)
        //for(int d = 0; d <= 26; d++)
    {
        //vector<int> tmp = calc_x_66_3(d);
        BigInteger tmp = calc_x_66_4(d);
        //cout << d << " --> " << tmp << endl;
        /*cout << d << " --> ";
        for(int i = tmp.size() - 1; i >= 0; i--)
        {
            cout << tmp[i];
        }
        cout << endl;*/
        if(max < tmp)
        {
            max = tmp;
            ret = d;
        }
    }
    return ret;
}
