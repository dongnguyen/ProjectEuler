//Almost equilateral triangles
//Problem 94
//It is easily proved that no equilateral triangle exists with integral length sides and integral area.However, the almost equilateral triangle 5 - 5 - 6 has an area of 12 square units.
//
//We shall define an almost equilateral triangle to be a triangle for which two sides are equal and the third differs by no more than one unit.
//
//Find the sum of the perimeters of all almost equilateral triangles with integral side lengths and area and whose perimeters do not exceed one billion(1, 000, 000, 000).

//http://en.wikipedia.org/wiki/Heronian_triangle#Exact_formula_for_Heronian_triangles

#include "template_class.h"
#include "BigIntegerLibrary.hh"

#if 0
uint64_t problem_94(int x)
{
    uint64_t ret = 0;
    uint64_t limit = x/3 + 1;
    for(uint64_t n = 3; n < limit; n+=2)
    {
        uint64_t test = (3*n + 1)*(n-1);
        uint64_t sqrt_test = sqrt(test);
        if(sqrt_test*sqrt_test == test)
        {
            uint64_t tmp = 3*n + 1;
            ret += tmp;
        }
        test = (3*n - 1)*(n+1);
        sqrt_test = sqrt(test);
        if(sqrt_test*sqrt_test == test)
        {
            uint64_t tmp = 3*n - 1;
            ret += tmp;
        }
    }
    return ret;
}
#else
BigInteger problem_94(int x)
{
    BigInteger ret = 0;
    uint64_t limit = x/3 + 1;
    for(uint64_t n = 3; n < limit; n+=2)
    {
        uint64_t test = (3*n + 1)*(n-1);
        uint64_t sqrt_test = sqrt(test);
        if(sqrt_test*sqrt_test == test)
        {
            BigInteger tmp = uint32_t(3*n + 1);
            ret += tmp;
        }
        test = (3*n - 1)*(n+1);
        sqrt_test = sqrt(test);
        if(sqrt_test*sqrt_test == test)
        {
            BigInteger tmp = uint32_t(3*n - 1);
            ret += tmp;
        }
    }
    return ret;
}
#endif

#if 0

BigInteger problem_94(int limit)
{
    BigInteger ret = 0;
    int limit_3 = limit / 3 + 1;
    //int count = 0;
    for (uint64_t a = 3; a < limit_3; a += 2)
        //for (int a = 3; a < limit_3; a++)
    {
        uint64_t test = ((3 * a + 1))*((a - 1)) >> 0;
        uint64_t sqrt_t = sqrt(test);
        if (sqrt_t * sqrt_t == test)
        {
            //cout << a << ", " << a << ", " << a + 1 << endl;
            ret += uint32_t(3 * a + 1);
            /*if (3 * a + 1 >= limit)
            {
                cout << "exit" << endl;
                exit(0);
            }*/
            //count++;
        }
        test = ((3 * a - 1))*((a + 1)) >> 0;
        sqrt_t = sqrt(test);
        if (sqrt_t * sqrt_t == test)
        {
            //cout << a << ", " << a << ", " << a - 1 << endl;
            ret += uint32_t(3 * a - 1);
            /*if (3 * a - 1 >= limit)
            {
                cout << "exit" << endl;
                exit(0);
            }*/
            //count++;
        }
        //if(count >= 1000)
        {
            //break;
        }
    }
    return ret;
#if 0
    int sqrt_limit = sqrt(limit / 6);
    int count = 0;
    for(int n = 1; n < limit - 1; n++)
    {
        for(int m = n + 1;; m += 2)
        {
            if(gcd(n, m) == 1)
            {
                int a = m*m - n*n;
                int b = 2*m*n;
                int c = m*m + n*n;

                if ((2 * a + 2 * c) > limit && (2 * b + 2 * c) > limit)
                {
                    break;
                }

                if (abs(2 * a - c) == 1)
                {
                    cout << "[" << 2 * a << ", " << c << ", " << c << "]" << endl;
                    break;
                }

                if (abs(2 * b - c) == 1)
                {
                    cout << "[" << 2 * b << ", " << c << ", " << c << "]" << endl;
                    break;
                }

                if (2 * a > (c + 1) && 2 * b > (c + 1))
                {
                    break;
                }
            }
        }
    }
    return count;
#endif
#if 0
    int sqrt3_limit = ceil(pow(limit / 3.0, 1.0 / 3));
    int count = 0;
    int k = 1;
    int m = 1;
    int n = 1;
    for (int k = 1; k < sqrt3_limit; k++)
        for (int n = 1; n < sqrt3_limit; n++)
            for (int m = n; m < sqrt3_limit; m++)
            {
                if ((gcd(gcd(k, m), n) == 1) && (m*n > k*k) && (k*k >= (double(m*m*n)/(2*m+n))))
                {
                    int a = n*(m*m + k*k);
                    int b = m*(n*n + k*k);
                    int c = (m + n)*(m*n - k*k);
                    int gcd_abc = gcd(gcd(a, b), c);
                    a /= gcd_abc;
                    b /= gcd_abc;
                    c /= gcd_abc;
                    if ((a == b && abs(c - a) == 1) || (b == c && abs(a - b) == 1))
                    {
                        cout << a << " " << b << " " << c << endl;
                        cout << m << " " << n << " " << k << endl;
                        cout << gcd_abc << endl << endl;
                    }
                    //cout << " " << m << " " << n << " " << k << endl;
                    count++;
                    if (count == 10)
                    {
                        //return 0;
                    }
                }
            }
#endif
    return 0;
}
#endif