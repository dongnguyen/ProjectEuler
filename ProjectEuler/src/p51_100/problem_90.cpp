// Cube digit pairs
// Problem 90
// Each of the six faces on a cube has a different digit (0 to 9) written on it; the same is done to a second cube. By placing the two cubes side-by-side in different positions we can form a variety of 2-digit numbers.

// For example, the square number 64 could be formed:

// In fact, by carefully choosing the digits on both cubes it is possible to display all of the square numbers below one-hundred: 01, 04, 09, 16, 25, 36, 49, 64, and 81.

// For example, one way this can be achieved is by placing {0, 5, 6, 7, 8, 9} on one cube and {1, 2, 3, 4, 8, 9} on the other cube.

// However, for this problem we shall allow the 6 or 9 to be turned upside-down so that an arrangement like {0, 5, 6, 7, 8, 9} and {1, 2, 3, 4, 6, 7} allows for all nine square numbers to be displayed; otherwise it would be impossible to obtain 09.

// In determining a distinct arrangement we are interested in the digits on each cube, not the order.

// {1, 2, 3, 4, 5, 6} is equivalent to {3, 6, 4, 1, 2, 5}
// {1, 2, 3, 4, 5, 6} is distinct from {1, 2, 3, 4, 5, 9}

// But because we are allowing 6 and 9 to be reversed, the two distinct sets in the last example both represent the extended set {1, 2, 3, 4, 5, 6, 9} for the purpose of forming 2-digit numbers.

// How many distinct arrangements of the two cubes allow for all of the square numbers to be displayed?

//#include "template_class.h"

#include <stdio.h>
#include <vector>
#include <map>

using namespace std;

extern void combination(int offset, int k, vector<int>& set, vector<int>& com, vector < vector < int >> &outset);

bool twosetareqeual(vector<int>& seta, vector<int>& setb)
{
	int size = seta.size();
	if (size != setb.size())
	{
		return false;
	}
	for (int i = 0; i < size; i++)
	{
		if (seta[i] != setb[i])
		{
			return false;
		}
	}
	return true;
}

bool isNumInSet90(int n, vector<int>& set)
{
	int size = set.size();
	for (int i = 0; i < size; i++)
	{
		if (n == set[i])
		{
			return true;
		}
		if (n == 6)
		{
			if (set[i] == 9)
			{
				return true;
			}
		}
		if (n == 9)
		{
			if (set[i] == 6)
			{
				return true;
			}
		}
	}
	return false;
}

bool isValidTwoDigits(int a, int b, vector<int>& seta, vector<int>& setb)
{
	if (isNumInSet90(a, seta))
	{
		if (isNumInSet90(b, setb))
		{
			return true;
		}
	}
	if (isNumInSet90(b, seta))
	{
		if (isNumInSet90(a, setb))
		{
			return true;
		}
	}
	return false;
}

//vector < map<vector<int>, vector<int>>> collections;
int cnt = 0;
bool verify_90(vector<int>& seta, vector<int>& setb)
{
	bool result = true;
	//01
	if (!isValidTwoDigits(0, 1, seta, setb))
	{
		return false;
	}
	//04
	if (!isValidTwoDigits(0, 4, seta, setb))
	{
		return false;
	}
	//09
	if (!isValidTwoDigits(0, 9, seta, setb))
	{
		return false;
	}
	//16
	if (!isValidTwoDigits(1, 6, seta, setb))
	{
		return false;
	}
	//25
	if (!isValidTwoDigits(2, 5, seta, setb))
	{
		return false;
	}
	//36
	if (!isValidTwoDigits(3, 6, seta, setb))
	{
		return false;
	}
	//49
	if (!isValidTwoDigits(4, 9, seta, setb))
	{
		return false;
	}
	//64
	if (!isValidTwoDigits(6, 4, seta, setb))
	{
		return false;
	}
	//81
	if (!isValidTwoDigits(8, 1, seta, setb))
	{
		return false;
	}
	//map<vector<int>, vector<int>> tmpmap; (seta, setb)
	cnt++;
	return true;
}

int problem_90()
{
	int ret = 0;
	vector<int> digits;
	vector < vector < int >> outsets;
	vector<int> com;
	digits.resize(10);
	for (int i = 0; i < 10; i++)
	{
		digits[i] = i;
	}
	combination(0, 6, digits, com, outsets);
	ret = outsets.size();
	for (int i = 0; i < ret; i++)
	{
		for (int j = i; j < ret; j++)
		{
			verify_90(outsets[i], outsets[j]);
		}
	}
	ret = cnt;
	return ret;
}
