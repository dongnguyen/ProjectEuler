//Arranged probability
//Problem 100
//If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs, and two discs were taken at random, it can be seen that the probability of taking two blue discs, P(BB) = (15/21)�(14/20) = 1/2.
//
//The next such arrangement, for which there is exactly 50% chance of taking two blue discs at random, is a box containing eighty-five blue discs and thirty-five red discs.
//
//By finding the first arrangement to contain over 1012 = 1,000,000,000,000 discs in total, determine the number of blue discs that the box would contain.

#include "template_class.h"

extern vector<int> continued_fractions(int n);
#include "BigIntegerLibrary.hh"

struct factor_100
{
    vector<int> num;
    vector<int> denum;
    factor_100() {};
    bool check_100()
    {
        vector<int> tmp;
        add_two_vector(num, denum, tmp);
        if(tmp.size() > 12)
        {
            return true;
        }
        return false;
    }
    void revert()
    {
        swap(num, denum);
    }
    factor_100& operator+=(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
    factor_100& operator+(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
};

BigInteger problem_100()
{
    BigInteger h_2;
    BigInteger h_1;
    BigInteger h;
    BigInteger k_2;
    BigInteger k_1;
    BigInteger k;
    BigInteger D = 2;
    h_2 = 1;
    h_1 = 1;
    k_2 = 0;
    k_1 = 1;
    if(h_1*h_1 == D*k_1*k_1+1)
    {
        return h_1;
    }
    BigInteger a = 2;
    BigInteger limit(2*1000000);
    limit *= 1000000;
    for(int i = 1;; i++)
    {
        h = a*h_1 + h_2;
        k = a*k_1 + k_2;
        if(h > limit)
        {
            if(h*h == D*k*k-1)
            {
                return (k-1)/2 + 1;
            }
        }
        h_2 = h_1;
        h_1 = h;
        k_2 = k_1;
        k_1 = k;
    }
    return h;
#if 0
    factor_100 ret;
    ret.num.push_back(1);
    ret.denum.push_back(2);
    for(int i = 0; ; i++)
    {
        ret += 2;

        for(int m = ret.num.size() - 1; m >= 0; m--)
        {
            cout << ret.num[m];
        }
        cout << "\/";
        for(int m = ret.denum.size() - 1; m >= 0; m--)
        {
            cout << ret.denum[m];
        }
        cout << endl;
        if(ret.check_100())
        {
            return ret.num;
        }
        ret.revert();
    }

    return ret.num;
#endif
}
