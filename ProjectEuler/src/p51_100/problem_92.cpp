//Square digit chains
//Problem 92
//A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.
//
//For example,
//
//44 ? 32 ? 13 ? 10 ? 1 ? 1
//85 ? 89 ? 145 ? 42 ? 20 ? 4 ? 16 ? 37 ? 58 ? 89
//
//Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.
//
//How many starting numbers below ten million will arrive at 89?

#include "template_class.h"

int calc_square_digits(int n)
{
    vector<int> digits;
    while(n)
    {
        int tmp = n % 10;
        digits.push_back(tmp * tmp);
        n /= 10;
    }
    int sum = 0;
    for(int i = 0; i < digits.size(); i++)
    {
        sum += digits[i];
    }
    return sum;
}

int init_list(int i, vector<int>& list)
{
    vector<int> _queue;
    int tmp = i;
    int arrive = 0;
    _queue.push_back(tmp);
    while(1)
    {
        tmp = calc_square_digits(tmp);
        if(tmp != 89 && tmp != 1)
        {
            if(list[tmp] == -1)
            {
                _queue.push_back(tmp);
            }
            else
            {
                arrive = list[tmp];
                break;
            }
        }
        else
        {
            arrive = tmp;
            break;
        }
    }
    for(int i = 0; i < _queue.size(); i++)
    {
        list[_queue[i]] = arrive;
    }
    return arrive;
}

int problem_92()
{
    vector<int> list;
    list.resize(568);
    for(int i = 0; i < 568; i++)
    {
        list[i] = -1;
    }
    int count = 0;
    for(int i = 1; i < 568; i++)
    {
        if(list[i] == -1)
        {
            init_list(i, list);
        }
        if(list[i] == 89)
        {
            count++;
        }
    }
    for(int i = 568; i < 1e7; i++)
    {
        int tmp = calc_square_digits(i);
        if(list[tmp] == 89)
        {
            count++;
        }
    }
    return count;
}
