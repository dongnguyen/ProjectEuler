//Prime summations
//Problem 77
//It is possible to write ten as the sum of primes in exactly five different ways:
//
//7 + 3
//5 + 5
//5 + 3 + 2
//3 + 3 + 2 + 2
//2 + 2 + 2 + 2 + 2
//
//What is the first value which can be written as the sum of primes in over five thousand different ways?

#include "template_class.h"

extern int count_77(int n);
extern int count_77_2(int n);
extern int count_77_limit(int first, int n);

extern vector<int> prime_list;
extern set<int> set_prime_list;

int count_77_limit(int first, int n)
{
    if(n < 2)
    {
        return 0;
    }

    int found_idx = 0;
    for(int i = 0; i < prime_list.size(); i++)
    {
        if(prime_list[i] == first)
        {
            found_idx = i;
            break;
        }
    }

    int sum = 0;
    for(int i = found_idx; i >= 0; i--)
    {
        if(n - prime_list[i] > prime_list[i])
        {
            sum += count_77_limit(prime_list[i], n - prime_list[i]);
        }
        else
        {
            sum += count_77_2(n - prime_list[i]);
        }
    }
    return sum;
}

int count_77_2(int n)
{
    if(n == 0)
    {
        return 1;
    }
    if(n == 1)
    {
        return 0;
    }
    if(n < 4)
    {
        return 1;
    }

    int found_idx = 0;
    for(int i = 0; i < prime_list.size(); i++)
    {
        if(prime_list[i] > n)
        {
            found_idx = i - 1;
            break;
        }
    }

    int sum = 0;
    for(int i = found_idx; i >= 0; i--)
    {
        if(prime_list[i] >= n - prime_list[i])
        {
            sum += count_77_2(n - prime_list[i]);
        }
        else
        {
            sum += count_77_limit(prime_list[i], n - prime_list[i]);
        }
    }

    return sum;
}

int count_77(int n)
{
    if(n < 4)
    {
        return 0;
    }

    int found_idx = 0;
    for(int i = 0; i < prime_list.size(); i++)
    {
        if(prime_list[i] >= n)
        {
            found_idx = i - 1;
            break;
        }
    }

    int sum = 0;
    for(int i = found_idx; i >= 0; i--)
    {
        if(prime_list[i] >= n - prime_list[i])
        {
            sum += count_77_2(n - prime_list[i]);
        }
        else
        {
            sum += count_77_limit(prime_list[i], n - prime_list[i]);
        }
    }
    return sum;
}

int problem_77()
{
    int limit = 1e3;
    vector<int> prime;
    prime.resize(limit+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    prime_list.push_back(2);
    int sqrt_limit = sqrt(limit);
    for(int i = 4; i <= limit; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 3; i <= sqrt_limit; i++)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
            for(int j = i*i; j <= limit; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    for(int i = sqrt_limit + 1; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
        }
    }
    set_prime_list.insert(prime_list.begin(), prime_list.end());
    for(int i = 10;; i++)
    {
        int c = count_77(i);
        if(c > 5000)
        {
            return i;
        }
    }
    return 0;
}
