//Convergents of e
//Problem 65
//The square root of 2 can be written as an infinite continued fraction.
//
//?2 = 1 +
//1
//
// 	2 +
//1
//
// 	 	2 +
//1
//
// 	 	 	2 +
//1
//
// 	 	 	 	2 + ...
//The infinite continued fraction can be written, ?2 = [1;(2)], (2) indicates that 2 repeats ad infinitum. In a similar way, ?23 = [4;(1,3,1,8)].
//
//It turns out that the sequence of partial values of continued fractions for square roots provide the best rational approximations. Let us consider the convergents for ?2.
//
//1 +
//1
//
//= 3/2
//
//2
//
//1 +
//1
//
//= 7/5
// 	2 +
//1
//
//
//2
//
//1 +
//1
//
//= 17/12
// 	2 +
//1
//
//
// 	 	2 +
//1
//
//
//
//2
//
//1 +
//1
//
//= 41/29
// 	2 +
//1
//
// 	 	2 +
//1
//
//
// 	 	 	2 +
//1
//
//
//
//2
//
//Hence the sequence of the first ten convergents for ?2 are:
//
//1, 3/2, 7/5, 17/12, 41/29, 99/70, 239/169, 577/408, 1393/985, 3363/2378, ...
//What is most surprising is that the important mathematical constant,
//e = [2; 1,2,1, 1,4,1, 1,6,1 , ... , 1,2k,1, ...].
//
//The first ten terms in the sequence of convergents for e are:
//
//2, 3, 8/3, 11/4, 19/7, 87/32, 106/39, 193/71, 1264/465, 1457/536, ...
//The sum of digits in the numerator of the 10th convergent is 1+4+5+7=17.
//
//Find the sum of digits in the numerator of the 100th convergent of the continued fraction for e.

#include "template_class.h"

struct factor_65
{
    vector<int> num;
    vector<int> denum;
    factor_65() {};
    int sum_num()
    {
        int sum = 0;
        for(int i = 0; i < num.size(); i++)
        {
            sum += num[i];
        }
        return sum;
    }
    void revert()
    {
        swap(num, denum);
    }
    factor_65& operator+=(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
    factor_65& operator+(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
};

int problem_65(int n)
{
    factor_65 ret;
    factor_65 tmp;
    int idx = (n + 1) % 3;
    int k   = (n + 1) / 3;
    if(idx == 1)
    {
        ret.num.push_back(1);
        ret.denum.push_back(2*k);
        k--;
    }
    else
    {
        ret.num.push_back(1);
        ret.denum.push_back(1);
    }
    idx--;
    if(idx == -1)
    {
        idx = 2;
    }
    for(int i = n; i > 2; i--)
    {
        if(idx == 1)
        {
            ret += 2*k;
            k--;
        }
        else
        {
            ret += 1;
        }
        idx--;
        if(idx == -1)
        {
            idx = 2;
        }
        ret.revert();
    }

    ret += 2;

    int sum = ret.sum_num();

    return sum;
}
