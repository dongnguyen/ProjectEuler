//XOR decryption
//Problem 59
//Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
//
//A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.
//
//For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.
//
//Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.
//
//Your task has been made easy, as the encryption key consists of three lower case characters. Using cipher1.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.

#include "template_class.h"

void read_input_59(char* path, vector<char>& input)
{
    FILE* fp = fopen(path, "r");
    while(!feof(fp))
    {
        int tmp;
        fscanf(fp, "%d,", &tmp);
        input.push_back(tmp);
    }
    input.resize(input.size() - 1);
    fclose(fp);
}

void decrypt_text(vector<char>& input, vector<char>& password, vector<char>& decrypted)
{
    int tmp = password.size();
    for(int i = 0; i < input.size(); i++)
    {
        decrypted[i] = input[i] ^ password[i % tmp];
    }
}

int find_num_common_word(vector<char>& input, vector<char>& common_word)
{
    int count = 0;
    for(int i = 0; i < input.size(); i++)
    {
        if(input[i] == common_word[0])
        {
            int tmp = i+1;
            bool match = true;
            for(int j = 1; j < common_word.size(); j++)
            {
                if(tmp == input.size())
                {
                    return count;
                }
                if(input[tmp++] != common_word[j])
                {
                    match = false;
                    break;
                }
            }
            if(match)
            {
                i = tmp;
                count++;
            }
        }
    }
    return count;
}

int problem_59(char* path)
{
    vector<char> input;
    vector<char> decrypted;
    vector<char> found_decrypted;
    vector<char> common_word;
    common_word.resize(3);
    common_word[0] = 't';
    common_word[1] = 'h';
    common_word[2] = 'e';
    read_input_59(path, input);
    decrypted.resize(input.size());
    found_decrypted.resize(input.size());
    /*vector<vector<int>> count;
    count.resize(3);
    count[0].resize(256);
    count[1].resize(256);
    count[2].resize(256);
    for(int i = 0; i < input.size(); i++)
    {
        count[i%3][input[i]]++;
    }*/
    vector<char> password;
    password.resize(3);
    vector<char> found_password;
    found_password.resize(3);
    int max = 0;
    for(char i = 'a'; i <= 'z'; i++)
    {
        password[0] = i;
        for(char j = 'a'; j <= 'z'; j++)
        {
            password[1] = j;
            for(char k = 'a'; k <= 'z'; k++)
            {
                password[2] = k;
                decrypt_text(input, password, decrypted);
                int tmp = find_num_common_word(decrypted, common_word);
                if(max < tmp)
                {
                    max = tmp;
                    found_password = password;
                    found_decrypted = decrypted;
                }
            }
        }
    }
    int sum = 0;
    for(int i = 0; i < found_decrypted.size(); i++)
    {
        sum += found_decrypted[i];
    }

    return sum;
}
