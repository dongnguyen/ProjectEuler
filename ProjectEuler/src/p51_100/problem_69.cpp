//Totient maximum
//Problem 69
//Euler's Totient function, ?(n) [sometimes called the phi function], is used to determine the number of numbers less than n which are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, ?(9)=6.
//
//n	Relatively Prime	?(n)	n/?(n)
//2	1	1	2
//3	1,2	2	1.5
//4	1,3	2	2
//5	1,2,3,4	4	1.25
//6	1,5	2	3
//7	1,2,3,4,5,6	6	1.1666...
//8	1,3,5,7	4	2
//9	1,2,4,5,7,8	6	1.5
//10	1,3,7,9	4	2.5
//It can be seen that n=6 produces a maximum n/?(n) for n ? 10.
//
//Find the value of n ? 1,000,000 for which n/?(n) is a maximum.

// http://en.wikipedia.org/wiki/Euler%27s_totient_function#Computing_Euler.27s_function
// http://en.wikipedia.org/wiki/Coprime_integers

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_list(int x);
extern int gcd(int a, int b);


#define METHOD_1 1

#if METHOD_1

int problem_69(int n)
{
    //create_prime_list(1e6);
    //FILE *fp = fopen("debug.txt", "w");
    vector<int> prime;
    //vector<int> co_prime_num;
    vector<double> co_prime_num;
    prime.resize(n+1);
    co_prime_num.resize(n+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    int limit = sqrt(n);
    for(int i = 4; i <= n; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 1; i <= n; i++)
    {
        co_prime_num[i] = i;
    }
    // 2 is prime add co-prime to all 2k+1 (k >= 1)
    /*for(int i = 3; i <= n; i += 2)
    {
        co_prime_num[i]++;
    }*/
    /*int idx = 2;
    double ratio = (double)idx / (co_prime_num[idx] + 1);*/
    double tmp = 1.0 - 1.0/2;
    for(int j = 2; j <= n; j += 2)
    {
        co_prime_num[j] *= tmp;
    }
    int idx = 2;
    double ratio = (double)idx / co_prime_num[idx];
    for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            double tmp = 1.0 - 1.0/i;
            for(int j = i; j <= n; j += i)
            {
                co_prime_num[j] *= tmp;
            }
            // i is prime add co-prime to all i*k+c (c = 1 --> i-1) (k >= 1)
            /*for(int c = 1; c < i; c++)
            {
                for(int j = i + c; j <= n; j += i)
                {
                    co_prime_num[j]++;
                }
            }*/
            //fprintf(fp, "%d\n", i);
            for(int j = i*i; j <= n; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
        //else
        {
            /*double tmp = (double)i / (co_prime_num[i] + 1);
            if(ratio < tmp)
            {
                ratio = tmp;
                idx = i;
            }*/
        }
        double tmp = (double)i / co_prime_num[i];
        if(ratio < tmp)
        {
            ratio = tmp;
            idx = i;
        }
    }
    for(int i = limit + 1; i <= n; i++)
    {
        if(prime[i] == PRIME)
        {
            double tmp = 1.0 - 1.0/i;
            for(int j = i; j <= n; j += i)
            {
                co_prime_num[j] *= tmp;
            }
        }
        double tmp = (double)i / co_prime_num[i];
        if(ratio < tmp)
        {
            ratio = tmp;
            idx = i;
        }
    }
    /*for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == NO_PRIME)
        {
            for(int j = i + 1; j <= n; j++)
            {
                if(prime[j] == NO_PRIME)
                {
                    if(1 == gcd(i, j))
                    {
                        co_prime_num[j]++;
                    }
                }
            }
            double tmp = (double)i / (co_prime_num[i] + 1);
            if(ratio < tmp)
            {
                ratio = tmp;
                idx = i;
            }
        }
    }*/
    //fclose(fp);
    return idx;
}
#else
struct int2
{
    int m;
    int n;
    int2(int a, int b)
    {
        m = a;
        n = b;
    }
    int2()
    {
        m = 0;
        n = 0;
    }
};
int problem_69(int n)
{
    queue<int2> co_prime_queue;
    vector<int> co_prime_num;
    co_prime_num.resize(n+1);
    int2 start(2, 1);
    co_prime_queue.push(start);
    while(!co_prime_queue.empty())
    {
        int size = co_prime_queue.size();
        for(int i = 0; i < size; i++)
        {
            int2 tmp = co_prime_queue.front();
            co_prime_queue.pop();
            int2 b1(2*tmp.m-tmp.n, tmp.m);
            int2 b2(2*tmp.m+tmp.n, tmp.m);
            int2 b3(tmp.m+2*tmp.n, tmp.n);
            if(b1.m <= n)
            {
                co_prime_queue.push(b1);
            }
            if(b2.m <= n)
            {
                co_prime_queue.push(b2);
            }
            if(b3.m <= n)
            {
                co_prime_queue.push(b3);
            }
            co_prime_num[tmp.m]++;
        }
    }
    start = int2(3, 1);
    co_prime_queue.push(start);
    while(!co_prime_queue.empty())
    {
        int size = co_prime_queue.size();
        for(int i = 0; i < size; i++)
        {
            int2 tmp = co_prime_queue.front();
            co_prime_queue.pop();
            int2 b1(2*tmp.m-tmp.n, tmp.m);
            int2 b2(2*tmp.m+tmp.n, tmp.m);
            int2 b3(tmp.m+2*tmp.n, tmp.n);
            if(b1.m <= n)
            {
                co_prime_queue.push(b1);
            }
            if(b2.m <= n)
            {
                co_prime_queue.push(b2);
            }
            if(b3.m <= n)
            {
                co_prime_queue.push(b3);
            }
            co_prime_num[tmp.m]++;
        }
    }
    int idx = 2;
    double ratio = (double)idx / co_prime_num[idx];
    for(int i = 3; i <= n; i++)
    {
        double tmp = (double)i / co_prime_num[i];
        if(ratio < tmp)
        {
            ratio = tmp;
            idx = i;
        }
    }
    return idx;
}
#endif
// backup
#if 0

int problem_69()
{
    create_prime_filter_method(1e6, true);
    vector<int> num_co_prime;
    num_co_prime.resize(1e6 + 1);
    for(int i = 2; i <= 1000000; i++)
    {
        if(num_co_prime[i])
        {
            continue;
        }
        num_co_prime[i]++;
        vector<int> memo;
        for(int j = 2; j < i; j++)
        {
            if(1 == gcd(i, j))
            {
                num_co_prime[i]++;
                memo.push_back(j);
            }
        }
        for(int j = 0; j < memo.size(); j++)
        {
            if(i*memo[j] <= 1000000)
            {
                num_co_prime[i*memo[j]] = num_co_prime[i]*num_co_prime[memo[j]];
            }
        }
    }
    double ratio = 0.0;
    int ret = 0;
    for(int i = 2; i <= 1000000; i++)
    {
        if(ratio < (double)i/num_co_prime[i])
        {
            ratio = (double)i/num_co_prime[i];
            ret = i;
        }
    }
    return ret;
}
#endif