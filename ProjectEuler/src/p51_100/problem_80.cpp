//Square root digital expansion
//Problem 80
//It is well known that if the square root of a natural number is not an integer, then it is irrational. The decimal expansion of such square roots is infinite without any repeating pattern at all.
//
//The square root of two is 1.41421356237309504880..., and the digital sum of the first one hundred decimal digits is 475.
//
//For the first one hundred natural numbers, find the total of the digital sums of the first one hundred decimal digits for all the irrational square roots.

#include "template_class.h"
#include "BigIntegerLibrary.hh"

extern vector<int> continued_fractions(int n);
extern bool is_square(uint64_t n);

int sum_100_digits(BigInteger h, BigInteger k)
{
    int sum = (h / k).toInt();
    //cout << sum << ".";
    for(int i = 1; i < 100; i++)
    {
        h = (h % k) * 10;
        int tmp = (h / k).toInt();
        //cout << tmp;
        sum += tmp;
    }
    //cout << endl;
    return sum;
}

int sum_80(int n)
{
    if(is_square(n))
    {
        return 0;
    }
    vector<int> cont_frac = continued_fractions(n);
    int period = cont_frac.size() - 1;
    BigInteger h_2;
    BigInteger h_1;
    BigInteger h;
    BigInteger k_2;
    BigInteger k_1;
    BigInteger k;
    h_2 = 1;
    h_1 = cont_frac[0];
    k_2 = 0;
    k_1 = 1;
    for(int i = 0; i < 180; i++)
    {
        BigInteger a = cont_frac[(i % period) + 1];
        h = a*h_1 + h_2;
        k = a*k_1 + k_2;
        h_2 = h_1;
        h_1 = h;
        k_2 = k_1;
        k_1 = k;
    }
    return sum_100_digits(h, k);
}

int problem_80()
{
    int sum = 0;
    for(int i = 2; i < 100; i++)
    {
        sum += sum_80(i);
    }
    return sum;
}
