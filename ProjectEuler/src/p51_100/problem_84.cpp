/*Monopoly odds
 Problem 84
In the game, Monopoly, the standard board is set up in the following way:

GO	A1	CC1	A2	T1	R1	B1	CH1	B2	B3	JAIL
H2	 	C1
T2	 	U1
H1	 	C2
CH3	 	C3
R4	 	R2
G3	 	D1
CC3	 	CC2
G2	 	D2
G1	 	D3
G2J	F3	U2	F2	F1	R3	E3	E2	CH2	E1	FP
A player starts on the GO square and adds the scores on two 6-sided dice to determine the number of squares they advance in a clockwise direction. Without any further rules we would expect to visit each square with equal probability: 2.5%. However, landing on G2J (Go To Jail), CC (community chest), and CH (chance) changes this distribution.

In addition to G2J, and one card from each of CC and CH, that orders the player to go directly to jail, if a player rolls three consecutive doubles, they do not advance the result of their 3rd roll. Instead they proceed directly to jail.

At the beginning of the game, the CC and CH cards are shuffled. When a player lands on CC or CH they take a card from the top of the respective pile and, after following the instructions, it is returned to the bottom of the pile. There are sixteen cards in each pile, but for the purpose of this problem we are only concerned with cards that order a movement; any instruction not concerned with movement will be ignored and the player will remain on the CC/CH square.

Community Chest (2/16 cards):
Advance to GO
Go to JAIL
Chance (10/16 cards):
Advance to GO
Go to JAIL
Go to C1
Go to E3
Go to H2
Go to R1
Go to next R (railway company)
Go to next R
Go to next U (utility company)
Go back 3 squares.
The heart of this problem concerns the likelihood of visiting a particular square. That is, the probability of finishing at that square after a roll. For this reason it should be clear that, with the exception of G2J for which the probability of finishing on it is zero, the CH squares will have the lowest probabilities, as 5/8 request a movement to another square, and it is the final square that the player finishes at on each roll that we are interested in. We shall make no distinction between "Just Visiting" and being sent to JAIL, and we shall also ignore the rule about requiring a double to "get out of jail", assuming that they pay to get out on their next turn.

By starting at GO and numbering the squares sequentially from 00 to 39 we can concatenate these two-digit numbers to produce strings that correspond with sets of squares.

Statistically it can be shown that the three most popular squares, in order, are JAIL (6.24%) = Square 10, E3 (3.18%) = Square 24, and GO (3.09%) = Square 00. So these three most popular squares can be listed with the six-digit modal string: 102400.

If, instead of using two 6-sided dice, two 4-sided dice are used, find the six-digit modal string. */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "template_class.h"

#define PLAY_TIMES 10000
#define SHUFFLE_NUM 1000
#define ROLLS_PER_GAME (40*1000)

enum {
	ADVANCETOGO,
	GOTOJAIL,
	GOTOC1,
	GOTOE3,
	GOTOH2,
	GOTOR1,
	GOTONEXTR,
	GOTONEXTU,
	GOBACK3SQUARES,
	UNDEFINED
};

enum {
	GO,
	A1,
	CC1,
	A2,
	T1,
	R1,
	B1,
	CH1,
	B2,
	B3,
	JAIL,
	C1,
	U1,
	C2,
	C3,
	R2,
	D1,
	CC2,
	D2,
	D3,
	FP,
	E1,
	CH2,
	E2,
	E3,
	R3,
	F1,
	F2,
	U2,
	F3,
	G2J,
	G1,
	G2,
	CC3,
	G3,
	R4,
	CH3,
	H1,
	T2,
	H2
};

char strings[40][200] = {
	"GO",
	"A1",
	"CC1",
	"A2",
	"T1",
	"R1",
	"B1",
	"CH1",
	"B2",
	"B3",
	"JAIL",
	"C1",
	"U1",
	"C2",
	"C3",
	"R2",
	"D1",
	"CC2",
	"D2",
	"D3",
	"FP",
	"E1",
	"CH2",
	"E2",
	"E3",
	"R3",
	"F1",
	"F2",
	"U2",
	"F3",
	"G2J",
	"G1",
	"G2",
	"CC3",
	"G3",
	"R4",
	"CH3",
	"H1",
	"T2",
	"H2"
};

void swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void swapu(unsigned int* a, unsigned int* b)
{
	unsigned tmp = *a;
	*a = *b;
	*b = tmp;
}

void shuffle(int* array, int size)
{
	for (int i = 0; i < SHUFFLE_NUM; i++)
	{
		int r = rand() % size;
		int d = rand() % size;
		swap(array + r, array + d);
	}
}

void initCommunity_Chest(int* array)
{
	array[0] = ADVANCETOGO;
	array[1] = GOTOJAIL;
	array[2] = UNDEFINED;
	array[3] = UNDEFINED;
	array[4] = UNDEFINED;
	array[5] = UNDEFINED;
	array[6] = UNDEFINED;
	array[7] = UNDEFINED;
	array[8] = UNDEFINED;
	array[9] = UNDEFINED;
	array[10] = UNDEFINED;
	array[11] = UNDEFINED;
	array[12] = UNDEFINED;
	array[13] = UNDEFINED;
	array[14] = UNDEFINED;
	array[15] = UNDEFINED;
	shuffle(array, 16);
}

void initChance(int* array)
{
	array[0] = ADVANCETOGO;
	array[1] = GOTOJAIL;
	array[2] = GOTOC1;
	array[3] = GOTOE3;
	array[4] = GOTOH2;
	array[5] = GOTOR1;
	array[6] = GOTONEXTR;
	array[7] = GOTONEXTR;
	array[8] = GOTONEXTU;
	array[9] = GOBACK3SQUARES;
	array[10] = UNDEFINED;
	array[11] = UNDEFINED;
	array[12] = UNDEFINED;
	array[13] = UNDEFINED;
	array[14] = UNDEFINED;
	array[15] = UNDEFINED;
	shuffle(array, 16);
}

void printArray(char * name, int* array, int size)
{
	printf("Print %s:\n", name);
	for (int i = 0; i < size; i++)
	{
		printf("  %s[%d]: %d\n", name, i, array[i]);
	}
}

int pickCard(int* array)
{
	int ret = array[0];
	for (int i = 0; i < 15; i++)
	{
		array[i] = array[i + 1];
	}
	array[15] = ret;
	return ret;
}

int problem_84()
{
	unsigned int squares[40] = { 0 };
	unsigned int dice_max_number = 4;
	int Community_Chest1[16] = { 0 };
	int Community_Chest2[16] = { 0 };
	int Community_Chest3[16] = { 0 };
	int Chance1[16] = { 0 };
	int Chance2[16] = { 0 };
	int Chance3[16] = { 0 };
	//printArray("Community_Chest", Community_Chest, 16);
	//printArray("Chance", Chance, 16);
	for (int time_play = 0; time_play < PLAY_TIMES; time_play++)
	{
		int current_pos = GO;
		int next_pos = GO;
		unsigned int double_cnt = 0;
		srand(time(NULL));
		initCommunity_Chest(Community_Chest1);
		initCommunity_Chest(Community_Chest2);
		initCommunity_Chest(Community_Chest3);
		initChance(Chance1);
		initChance(Chance2);
		initChance(Chance3);
		for (int roll = 0; roll < ROLLS_PER_GAME; roll++)
		{
			int dice1 = (rand() % dice_max_number) + 1;
			int dice2 = (rand() % dice_max_number) + 1;
			current_pos = next_pos;
			if (dice1 == dice2)
			{
				double_cnt++;
			}
			else
			{
				double_cnt = 0;
			}
			if (double_cnt == 3)
			{
				next_pos = JAIL;
				double_cnt = 0;
			}
			else
			{
				int moves = dice1 + dice2;
				next_pos = (current_pos + moves) % 40;
				int card = UNDEFINED;
				switch (next_pos)
				{
				case CH1:
					card = pickCard(Chance1);
					if (card == ADVANCETOGO)
					{
						next_pos = GO;
					}
					else if (card == GOTOJAIL)
					{
						next_pos = JAIL;
					}
					else if (card == GOTOC1)
					{
						next_pos = C1;
					}
					else if (card == GOTOE3)
					{
						next_pos = E3;
					}
					else if (card == GOTOH2)
					{
						next_pos = H2;
					}
					else if (card == GOTOR1)
					{
						next_pos = R1;
					}
					else if (card == GOTONEXTR)
					{
						next_pos = R2;
					}
					else if (card == GOTONEXTU)
					{
						next_pos = U1;
					}
					else if (card == GOBACK3SQUARES)
					{
						next_pos = (next_pos + 40 - 3) % 40;
					}
					else // stay
					{
					}
					break;
				case CH2:
					card = pickCard(Chance2);
					if (card == ADVANCETOGO)
					{
						next_pos = GO;
					}
					else if (card == GOTOJAIL)
					{
						next_pos = JAIL;
					}
					else if (card == GOTOC1)
					{
						next_pos = C1;
					}
					else if (card == GOTOE3)
					{
						next_pos = E3;
					}
					else if (card == GOTOH2)
					{
						next_pos = H2;
					}
					else if (card == GOTOR1)
					{
						next_pos = R1;
					}
					else if (card == GOTONEXTR)
					{
						next_pos = R3;
					}
					else if (card == GOTONEXTU)
					{
						next_pos = U2;
					}
					else if (card == GOBACK3SQUARES)
					{
						next_pos = (next_pos + 40 - 3) % 40;
					}
					else // stay
					{
					}
					break;
				case CH3:
					card = pickCard(Chance3);
					if (card == ADVANCETOGO)
					{
						next_pos = GO;
					}
					else if (card == GOTOJAIL)
					{
						next_pos = JAIL;
					}
					else if (card == GOTOC1)
					{
						next_pos = C1;
					}
					else if (card == GOTOE3)
					{
						next_pos = E3;
					}
					else if (card == GOTOH2)
					{
						next_pos = H2;
					}
					else if (card == GOTOR1)
					{
						next_pos = R1;
					}
					else if (card == GOTONEXTR)
					{
						next_pos = R1;
					}
					else if (card == GOTONEXTU)
					{
						next_pos = U1;
					}
					else if (card == GOBACK3SQUARES)
					{
						next_pos = (next_pos + 40 - 3) % 40;
					}
					else // stay
					{
					}
					break;
				case CC1:
					card = pickCard(Community_Chest1);
					if (card == ADVANCETOGO)
					{
						next_pos = GO;
					}
					else if (card == GOTOJAIL)
					{
						next_pos = JAIL;
					}
					else // stay
					{
					}
					break;
				case CC2:
					card = pickCard(Community_Chest2);
					if (card == ADVANCETOGO)
					{
						next_pos = GO;
					}
					else if (card == GOTOJAIL)
					{
						next_pos = JAIL;
					}
					else // stay
					{
					}
					break;
				case CC3:
					card = pickCard(Community_Chest3);
					if (card == ADVANCETOGO)
					{
						next_pos = GO;
					}
					else if (card == GOTOJAIL)
					{
						next_pos = JAIL;
					}
					else // stay
					{
					}
					break;
				case G2J:
					next_pos = JAIL;
					break;
				}
			}
			squares[next_pos]++;
		}
	}

	// sort squares
	unsigned int max[3];
	int pos[3];
	max[0] = squares[0];
	max[1] = squares[1];
	max[2] = squares[2];
	pos[0] = 0;
	pos[1] = 1;
	pos[2] = 2;

	if (max[0] < max[1])
	{
		swapu(max, max + 1);
		swap(pos, pos + 1);
	}
	if (max[0] < max[2])
	{
		swapu(max, max + 2);
		swap(pos, pos + 2);
	}
	if (max[1] < max[2])
	{
		swapu(max + 1, max + 2);
		swap(pos + 1, pos + 2);
	}

	for (int i = 4; i < 40; i++)
	{
		if (max[2] < squares[i])
		{
			max[2] = squares[i];
			pos[2] = i;
			if (max[0] < max[1])
			{
				swapu(max, max + 1);
				swap(pos, pos + 1);
			}
			if (max[0] < max[2])
			{
				swapu(max, max + 2);
				swap(pos, pos + 2);
			}
			if (max[1] < max[2])
			{
				swapu(max + 1, max + 2);
				swap(pos + 1, pos + 2);
			}
		}
	}
	printf("%s %s %s\n", strings[pos[0]], strings[pos[1]], strings[pos[2]]);
	printf("%d %d %d\n", pos[0], pos[1], pos[2]);
	printf("%d %d %d\n", max[0], max[1], max[2]);

	unsigned int totalsum = 0;
	for (int i = 0; i < 40; i++)
	{
		totalsum += squares[i];
	}

	printf("%lf %lf %lf\n", double(max[0]) / totalsum * 100,
		double(max[1]) / totalsum * 100,
		double(max[2]) / totalsum * 100);

	/* 	printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest));
		printf("pickCard: %d\n", pickCard(Community_Chest)); */

	return 0;
}
