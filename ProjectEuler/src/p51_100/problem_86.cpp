
#include "template_class.h"

// guessing
int limit_86 = 1818;

int count_86(triple<int> s)
{
    int count = 0;
    for (int k = 1;; k++)
    {
        uint64_t a = s.a*k;
        uint64_t b = s.b*k;
        uint64_t c = s.c*k;
        if ((a <= limit_86 && b <= 2 * limit_86) || (b <= limit_86 && a <= 2 * limit_86))
        {
            uint64_t c2 = c*c;
            if (b <= limit_86)
            {
                for (uint64_t i = 1; i <= a / 2; i++)
                {
                    uint64_t j = a - i;
                    if (j <= limit_86)
                    {
                        uint64_t path1 = i*i + (j + b)*(j + b);
                        uint64_t path2 = j*j + (i + b)*(i + b);
                        if (c2 <= path1 && c2 <= path2)
                        {
                            count++;
                        }
                    }
                }
            }
            if (a <= limit_86)
            {
                for (uint64_t i = 1; i <= b / 2; i++)
                {
                    uint64_t j = b - i;
                    if (j <= limit_86)
                    {
                        uint64_t path1 = i*i + (j + a)*(j + a);
                        uint64_t path2 = j*j + (i + a)*(i + a);
                        if (c2 <= path1 && c2 <= path2)
                        {
                            count++;
                        }
                    }
                }
            }
        }
        else
        {
            break;
        }
    }
    return count;
}

int problem_86()
{
    for (limit_86 = 100;; limit_86++)
    {
        queue<triple<int>> memo;
        vector<triple<int>> store_seeds;
        triple<int> seed(3, 4, 5);
        memo.push(seed);
        int count = 0;
        while (!memo.empty())
        {
            triple<int> s = memo.front();
            memo.pop();
            count += count_86(s);
            int a = s.a;
            int b = s.b;
            int c = s.c;

            triple<int> p1(a - 2 * b + 2 * c, 2 * a - b + 2 * c, 2 * a - 2 * b + 3 * c);
            if ((p1.a <= limit_86 && p1.b <= 2 * limit_86) || (p1.a <= 2 * limit_86 && p1.b <= limit_86))
            {
                memo.push(p1);
            }
            else
            {
                //store_seeds.push_back(p1);
            }

            triple<int> p2(a + 2 * b + 2 * c, 2 * a + b + 2 * c, 2 * a + 2 * b + 3 * c);
            if ((p2.a <= limit_86 && p2.b <= 2 * limit_86) || (p2.a <= 2 * limit_86 && p2.b <= limit_86))
            {
                memo.push(p2);
            }
            else
            {
                //store_seeds.push_back(p2);
            }

            triple<int> p3(-a + 2 * b + 2 * c, -2 * a + b + 2 * c, -2 * a + 2 * b + 3 * c);
            if ((p3.a <= limit_86 && p3.b <= 2 * limit_86) || (p3.a <= 2 * limit_86 && p3.b <= limit_86))
            {
                memo.push(p3);
            }
            else
            {
                //store_seeds.push_back(p3);
            }
        }
        if (count > 1e6)
        {
            break;
        }
    }
    return limit_86;
}
