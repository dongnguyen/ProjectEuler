//Powerful digit sum
//Problem 56
//A googol (10100) is a massive number: one followed by one-hundred zeros; 100100 is almost unimaginably large: one followed by two-hundred zeros. Despite their size, the sum of the digits in each number is only 1.
//
//Considering natural numbers of the form, ab, where a, b < 100, what is the maximum digital sum?

#include "template_class.h"

extern void pow_a_b(int a, int b, vector<int>& result);

int sum_digits_pow_a_b(int a, int b)
{
    int sum = 0;
    vector<int> result;
    pow_a_b(a, b, result);
    for(int i = 0; i < result.size(); i++)
    {
        sum += result[i];
    }
    return sum;
}

int problem_56()
{
    int max = 0;
    for(int a = 2; a < 100; a++)
    {
        for(int b = 2; b < 100; b++)
        {
            int tmp = sum_digits_pow_a_b(a, b);
            if(max < tmp)
            {
                max = tmp;
            }
        }
    }
    return max;
}
