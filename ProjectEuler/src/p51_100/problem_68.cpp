//Magic 5-gon ring
//Problem 68
//Consider the following "magic" 3-gon ring, filled with the numbers 1 to 6, and each line adding to nine.
//
//
//Working clockwise, and starting from the group of three with the numerically lowest external node (4,3,2 in this example), each solution can be described uniquely. For example, the above solution can be described by the set: 4,3,2; 6,2,1; 5,1,3.
//
//It is possible to complete the ring with four different totals: 9, 10, 11, and 12. There are eight solutions in total.
//
//Total	Solution Set
//9	4,2,3; 5,3,1; 6,1,2
//9	4,3,2; 6,2,1; 5,1,3
//10	2,3,5; 4,5,1; 6,1,3
//10	2,5,3; 6,3,1; 4,1,5
//11	1,4,6; 3,6,2; 5,2,4
//11	1,6,4; 5,4,2; 3,2,6
//12	1,5,6; 2,6,4; 3,4,5
//12	1,6,5; 3,5,4; 2,4,6
//By concatenating each group it is possible to form 9-digit strings; the maximum string for a 3-gon ring is 432621513.
//
//Using the numbers 1 to 10, and depending on arrangements, it is possible to form 16- and 17-digit strings. What is the maximum 16-digit string for a "magic" 5-gon ring?

#include "template_class.h"

bool verify_68(vector<int>& ext_nodes, vector<int>& int_nodes)
{
#if 1
    int sum = ext_nodes[0] + int_nodes[0] + int_nodes[1];
    if(sum != ext_nodes[1] + int_nodes[1] + int_nodes[2]) return false;
    if(sum != ext_nodes[2] + int_nodes[2] + int_nodes[3]) return false;
    if(sum != ext_nodes[3] + int_nodes[3] + int_nodes[4]) return false;
    if(sum != ext_nodes[4] + int_nodes[4] + int_nodes[0]) return false;
#else
    int sum = ext_nodes[0] + int_nodes[0] + int_nodes[1];
    if(sum != ext_nodes[1] + int_nodes[1] + int_nodes[2]) return false;
    if(sum != ext_nodes[2] + int_nodes[2] + int_nodes[0]) return false;
#endif
    return true;
}

vector<int> problem_68()
{
    vector<int> ret;
    vector<int> ext_nodes;
    vector<int> int_nodes;
    ret.resize(15);
    ext_nodes.resize(5);
    int_nodes.resize(5);
    set<int> set_num;
    for(int i = 1; i <= 10; i++)
    {
        set_num.insert(i);
    }
    for(ext_nodes[0] = 1; ext_nodes[0] < 7; ext_nodes[0]++)
    {
        set_num.erase(ext_nodes[0]);
        for(ext_nodes[1] = ext_nodes[0] + 1; ext_nodes[1] < 8; ext_nodes[1]++)
        {
            set_num.erase(ext_nodes[1]);
            for(ext_nodes[2] = ext_nodes[1] + 1; ext_nodes[2] < 9; ext_nodes[2]++)
            {
                set_num.erase(ext_nodes[2]);
                for(ext_nodes[3] = ext_nodes[2] + 1; ext_nodes[3] < 10; ext_nodes[3]++)
                {
                    set_num.erase(ext_nodes[3]);
                    for(ext_nodes[4] = ext_nodes[3] + 1; ext_nodes[4] < 11; ext_nodes[4]++)
                    {
                        set_num.erase(ext_nodes[4]);

                        set<int>::iterator ite;
                        int idx = 0;
                        for(ite = set_num.begin(); ite != set_num.end(); ++ite)
                        {
                            int_nodes[idx++] = *ite;
                        }
                        do
                        {
                            do
                            {
                                if(verify_68(ext_nodes, int_nodes))
                                {
                                    /*cout << ext_nodes[0] << int_nodes[0] << int_nodes[1];
                                    cout << ext_nodes[1] << int_nodes[1] << int_nodes[2];
                                    cout << ext_nodes[2] << int_nodes[2] << int_nodes[3];
                                    cout << ext_nodes[3] << int_nodes[3] << int_nodes[4];
                                    cout << ext_nodes[4] << int_nodes[4] << int_nodes[0];
                                    cout << endl;*/
                                    ret[0]  = ext_nodes[0];
                                    ret[1]  = int_nodes[0];
                                    ret[2]  = int_nodes[1];
                                    ret[3]  = ext_nodes[1];
                                    ret[4]  = int_nodes[1];
                                    ret[5]  = int_nodes[2];
                                    ret[6]  = ext_nodes[2];
                                    ret[7]  = int_nodes[2];
                                    ret[8]  = int_nodes[3];
                                    ret[9]  = ext_nodes[3];
                                    ret[10] = int_nodes[3];
                                    ret[11] = int_nodes[4];
                                    ret[12] = ext_nodes[4];
                                    ret[13] = int_nodes[4];
                                    ret[14] = int_nodes[0];
                                }
                            }
                            while(next_permutation(int_nodes.begin(), int_nodes.end()));
                        }
                        while(next_permutation(ext_nodes.begin() + 1, ext_nodes.end()));

                        set_num.insert(ext_nodes[4]);
                    }
                    set_num.insert(ext_nodes[3]);
                }
                set_num.insert(ext_nodes[2]);
            }
            set_num.insert(ext_nodes[1]);
        }
        set_num.insert(ext_nodes[0]);
    }
#if 0
    ret.resize(10);
    ext_nodes.resize(5);
    int_nodes.resize(5);
    set<int> set_num;
    for(int i = 1; i < 10; i++)
    {
        set_num.insert(i);
    }
    for(ext_nodes[0] = 1; ext_nodes[0] <= 6; ext_nodes[0]++)
    {
        set_num.erase(ext_nodes[0]);
        for(int i = 1; i < 8; i++)
        {
            if(i != ext_nodes[0])
            {
                set_num.erase(i);
                for(int j = i + 1; j < 9; j++)
                {
                    if(j != ext_nodes[0])
                    {
                        set_num.erase(j);
                        for(int k = j + 1; k < 10; k++)
                        {
                            if(k != ext_nodes[0])
                            {
                                set_num.erase(k);
                                vector<int> per;
                                per.resize(3);
                                per[0] = i;
                                per[1] = j;
                                per[2] = k;
                                do
                                {
                                    set<int>::iterator ite;
                                    int idx = 0;
                                    for(ite = set_num.begin(); ite != set_num.end(); ite++)
                                    {
                                        int_nodes[idx++] = *ite;
                                    }

                                    do
                                    {
                                        ext_nodes[1] = 10;
                                        ext_nodes[2] = per[0];
                                        ext_nodes[3] = per[1];
                                        ext_nodes[4] = per[2];
                                        if(verify_68(ext_nodes, int_nodes))
                                        {
                                            cout << ext_nodes[0] << "," << int_nodes[0] << "," << int_nodes[1] << ";";
                                            cout << ext_nodes[1] << "," << int_nodes[1] << "," << int_nodes[2] << ";";
                                            cout << ext_nodes[2] << "," << int_nodes[2] << "," << int_nodes[3] << ";";
                                            cout << ext_nodes[3] << "," << int_nodes[3] << "," << int_nodes[4] << ";";
                                            cout << ext_nodes[4] << "," << int_nodes[4] << "," << int_nodes[0] << ";";
                                            cout << endl;
                                        }

                                        ext_nodes[1] = per[0];
                                        ext_nodes[2] = 10;
                                        ext_nodes[3] = per[1];
                                        ext_nodes[4] = per[2];
                                        if(verify_68(ext_nodes, int_nodes))
                                        {
                                            cout << ext_nodes[0] << "," << int_nodes[0] << "," << int_nodes[1] << ";";
                                            cout << ext_nodes[1] << "," << int_nodes[1] << "," << int_nodes[2] << ";";
                                            cout << ext_nodes[2] << "," << int_nodes[2] << "," << int_nodes[3] << ";";
                                            cout << ext_nodes[3] << "," << int_nodes[3] << "," << int_nodes[4] << ";";
                                            cout << ext_nodes[4] << "," << int_nodes[4] << "," << int_nodes[0] << ";";
                                            cout << endl;
                                        }

                                        ext_nodes[1] = per[0];
                                        ext_nodes[2] = per[1];
                                        ext_nodes[3] = 10;
                                        ext_nodes[4] = per[2];
                                        if(verify_68(ext_nodes, int_nodes))
                                        {
                                            cout << ext_nodes[0] << "," << int_nodes[0] << "," << int_nodes[1] << ";";
                                            cout << ext_nodes[1] << "," << int_nodes[1] << "," << int_nodes[2] << ";";
                                            cout << ext_nodes[2] << "," << int_nodes[2] << "," << int_nodes[3] << ";";
                                            cout << ext_nodes[3] << "," << int_nodes[3] << "," << int_nodes[4] << ";";
                                            cout << ext_nodes[4] << "," << int_nodes[4] << "," << int_nodes[0] << ";";
                                            cout << endl;
                                        }

                                        ext_nodes[1] = per[0];
                                        ext_nodes[2] = per[1];
                                        ext_nodes[3] = per[2];
                                        ext_nodes[4] = 10;
                                        if(verify_68(ext_nodes, int_nodes))
                                        {
                                            cout << ext_nodes[0] << "," << int_nodes[0] << "," << int_nodes[1] << ";";
                                            cout << ext_nodes[1] << "," << int_nodes[1] << "," << int_nodes[2] << ";";
                                            cout << ext_nodes[2] << "," << int_nodes[2] << "," << int_nodes[3] << ";";
                                            cout << ext_nodes[3] << "," << int_nodes[3] << "," << int_nodes[4] << ";";
                                            cout << ext_nodes[4] << "," << int_nodes[4] << "," << int_nodes[0] << ";";
                                            cout << endl;
                                        }
                                    }
                                    while(next_permutation(int_nodes.begin(), int_nodes.end()));
                                }
                                while(next_permutation(per.begin(), per.end()));
                                set_num.insert(k);
                            }
                        }
                        set_num.insert(j);
                    }
                }
                set_num.insert(i);
            }
        }
        set_num.insert(ext_nodes[0]);
    }
#endif
    return ret;
}
