//Totient permutation
//Problem 70
//Euler's Totient function, ?(n) [sometimes called the phi function], is used to determine the number of positive numbers less than or equal to n which are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, ?(9)=6.
//The number 1 is considered to be relatively prime to every positive number, so ?(1)=1.
//
//Interestingly, ?(87109)=79180, and it can be seen that 87109 is a permutation of 79180.
//
//Find the value of n, 1 < n < 107, for which ?(n) is a permutation of n and the ratio n/?(n) produces a minimum.

#include "template_class.h"
#ifdef _LINUX
#include <cfloat>
#endif

bool check_permutation(int a, int b)
{
    vector<int> d_a;
    vector<int> d_b;
    while(a)
    {
        d_a.push_back(a % 10);
        a /= 10;
    }
    while(b)
    {
        d_b.push_back(b % 10);
        b /= 10;
    }
    if(d_a.size() != d_b.size())
    {
        return false;
    }
    sort(d_a.begin(), d_a.end());
    sort(d_b.begin(), d_b.end());
    int size = d_a.size();
    for(int i = 0; i < size; i++)
    {
        if(d_a[i] != d_b[i])
        {
            return false;
        }
    }
    return true;
}

int problem_70(int n)
{
    vector<int> prime;
    vector<int> Totient;
    prime.resize(n+1);
    Totient.resize(n+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    int limit = sqrt(n);
    for(int i = 4; i <= n; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 1; i <= n; i++)
    {
        Totient[i] = i;
    }
    //double tmp = 1.0 - 1.0/2;
    for(int j = 2; j <= n; j += 2)
    {
        Totient[j] >>= 1;
    }
    for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            int nume = i - 1;
            for(int j = i; j <= n; j += i)
            {
                Totient[j] = Totient[j] / i * nume;
            }
            for(int j = i*i; j <= n; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    for(int i = limit + 1; i <= n; i++)
    {
        if(prime[i] == PRIME)
        {
            int nume = i - 1;
            for(int j = i; j <= n; j += i)
            {
                Totient[j] = Totient[j] / i * nume;
            }
        }
    }
    int ret = 0;
    double ratio = FLT_MAX;
    for(int i = 2; i < n; i++)
    {
        if(prime[i] == NO_PRIME)
        {
            if(check_permutation(i, Totient[i]))
            {
                double tmp = double(i) / Totient[i];
                if(tmp < ratio)
                {
                    ratio = tmp;
                    ret = i;
                }
            }
        }
    }
    return ret;
}
