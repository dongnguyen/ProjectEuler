//Permuted multiples
//Problem 52
//It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.
//
//Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.

#include "template_class.h"

bool compare_digits(vector<int>& base, int n)
{
    vector<int> digits;
    while(n)
    {
        digits.push_back(n % 10);
        n /= 10;
    }
    if(digits.size() != base.size())
    {
        return false;
    }
    sort(digits.begin(), digits.end());
    for(int i = 0; i < digits.size(); i++)
    {
        if(digits[i] != base[i])
        {
            return false;
        }
    }
    return true;
}

int problem_52()
{
    vector<int> base;
    for(int num = 2; ; num++)
    {
        int start = 10*(pow(10, num - 2));
        int end = 17*(pow(10, num - 2));
        for(int i = start; i < end; i++)
        {
            base.resize(0);
            base.reserve(num);
            int tmp = i*2;
            while(tmp)
            {
                base.push_back(tmp % 10);
                tmp /= 10;
            }
            sort(base.begin(), base.end());
            bool result = true;
            for(int j = 3; j <= 6; j++)
            {
                tmp = i * j;
                if(!compare_digits(base, tmp))
                {
                    result = false;
                    break;
                }
            }
            if(result)
            {
                return i;
            }
        }
    }
    return -1;
}
