//Large non-Mersenne prime
//Problem 97
//The first known prime found to exceed one million digits was discovered in 1999, and is a Mersenne prime of the form 26972593?1; it contains exactly 2,098,960 digits. Subsequently other Mersenne primes, of the form 2p?1, have been found which contain more digits.
//
//However, in 2004 there was found a massive non-Mersenne prime which contains 2,357,207 digits: 28433�2^7830457+1.
//
//Find the last ten digits of this prime number.

#include "template_class.h"

void mul_two_vector_limit(vector<int>& a, vector<int>& b, vector<int>& p)
{
    if(a.size() != b.size())
    {
        printf("assert error");
        exit(-1);
    }
    int size = a.size();
    p.clear();
    p.resize(a.size());
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            if(i + j < size)
            {
                p[i+j] += a[i] * b[j];
            }
        }
    }
    for(int i = 0; i < size - 1; i++)
    {
        int remain = p[i] / 10;
        p[i] %= 10;
        p[i+1] += remain;
    }
    p[size - 1] %= 10;
}

uint64_t problem_97()
{
    /*double log2 = log(7830457) / log(2);
    int pow2 = pow(2, (int)log2);
    vector<int> a;
    vector<int> b;
    vector<int> p;
    a.resize(10);
    b.resize(10);
    a[0] = 4;
    a[1] = 2;
    a[2] = 0;
    a[3] = 1;
    b[0] = 4;
    b[1] = 2;
    b[2] = 0;
    b[3] = 1;
    mul_two_vector(a, b, p);*/
    vector<int> tmp;
    vector<int> tmp2;
    vector<int> b;
    vector<int> p;
    p.resize(10);
    p[0] = 2;
    vector<vector<int>> pow2_list;
    pow2_list.resize(sizeof(int)*8);
    pow2_list[0] = p;
    for(int i = 1; i < sizeof(int)*8; i++)
    {
        mul_two_vector_limit(pow2_list[i-1], pow2_list[i-1], p);
        pow2_list[i] = p;
    }
    int power = 7830457;
    int base = 0;
    p.clear();
    p.resize(10);
    p[0] = 1;
    // 2^7830457
    while(power)
    {
        if(power & 1)
        {
            tmp.assign(p.begin(), p.end());
            mul_two_vector_limit(pow2_list[base], tmp, p);
        }
        base++;
        power >>= 1;
    }
    // 28433
    tmp.clear();
    tmp.resize(10);
    tmp[0] = 3;
    tmp[1] = 3;
    tmp[2] = 4;
    tmp[3] = 8;
    tmp[4] = 2;
    tmp2.assign(p.begin(), p.end());
    mul_two_vector_limit(tmp2, tmp, p);
    uint64_t ret = p[0] + 1 +
                   p[1]*1e1 +
                   p[2]*1e2 +
                   p[3]*1e3 +
                   p[4]*1e4 +
                   p[5]*1e5 +
                   p[6]*1e6 +
                   p[7]*1e7 +
                   p[8]*1e8 +
                   p[9]*1e9;
    return ret;
}
