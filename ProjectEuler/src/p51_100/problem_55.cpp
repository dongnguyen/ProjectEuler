//Lychrel numbers
//Problem 55
//If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.
//
//Not all numbers produce palindromes so quickly. For example,
//
//349 + 943 = 1292,
//1292 + 2921 = 4213
//4213 + 3124 = 7337
//
//That is, 349 took three iterations to arrive at a palindrome.
//
//Although no one has proved it yet, it is thought that some numbers, like 196, never produce a palindrome. A number that never forms a palindrome through the reverse and add process is called a Lychrel number. Due to the theoretical nature of these numbers, and for the purpose of this problem, we shall assume that a number is Lychrel until proven otherwise. In addition you are given that for every number below ten-thousand, it will either (i) become a palindrome in less than fifty iterations, or, (ii) no one, with all the computing power that exists, has managed so far to map it to a palindrome. In fact, 10677 is the first number to be shown to require over fifty iterations before producing a palindrome: 4668731596684224866951378664 (53 iterations, 28-digits).
//
//Surprisingly, there are palindromic numbers that are themselves Lychrel numbers; the first example is 4994.
//
//How many Lychrel numbers are there below ten-thousand?
//
//NOTE: Wording was modified slightly on 24 April 2007 to emphasise the theoretical nature of Lychrel numbers.

#include "template_class.h"

extern bool check_palindromic(vector<int> digits);

//vector<int> Lychrel_set;
int limit55 = 1e4;

bool is_Lychrel(int n)
{
    /*if(Lychrel_set[n])
    {
        return Lychrel_set[n] == 1 ? true : false;
    }*/
    vector<int> digits;
    vector<int> digits_rev;
    vector<int> sum;
    bool ret = true;
    while(n)
    {
        digits.push_back(n % 10);
        digits_rev.insert(digits_rev.begin(), n % 10);
        n /= 10;
    }
    /*vector<int> memory;
    bool still_small = true;*/
    for(int i = 0; i < 50; i++)
    {
        add_two_vector(digits, digits_rev, sum);
        /*if(still_small)
        {
            int candidate_1 = 0;
            for(int j = 0; j < digits.size(); j++)
            {
                candidate_1 = candidate_1*10 + digits[j];
            }
            int candidate_2 = 0;
            for(int j = 0; j < digits_rev.size(); j++)
            {
                candidate_2 = candidate_2*10 + digits_rev[j];
            }
            if(candidate_1 < limit55)
            {
                memory.push_back(candidate_1);
            }
            if(candidate_2 < limit55)
            {
                memory.push_back(candidate_2);
            }
            still_small = (candidate_1 < limit55) && (candidate_2 < limit55);
        }*/
        if(check_palindromic(sum))
        {
            ret = false;
            break;
        }
        digits.assign(sum.begin(), sum.end());
        digits_rev.resize(digits.size());
        reverse_copy(digits.data(), digits.data() + digits.size(), digits_rev.begin());
    }
    /*int ret_int = ret ? 1 : 2;
    for(int i = 0; i < memory.size(); i++)
    {
        Lychrel_set[memory[i]] = ret_int;
    }*/
    return ret;
}

int problem_55()
{
    //Lychrel_set.resize(limit55);
    //bool d = is_Lychrel(4213);
    int count = 0;
    for(int i = 1; i < limit55; i++)
    {
        if(is_Lychrel(i))
        {
            count++;
        }
    }
    return count;
}