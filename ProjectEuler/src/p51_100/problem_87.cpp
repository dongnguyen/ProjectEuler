//Prime power triples
//Problem 87
//The smallest number expressible as the sum of a prime square, prime cube, and prime fourth power is 28. In fact, there are exactly four numbers below fifty that can be expressed in such a way:
//
//28 = 22 + 23 + 24
//33 = 32 + 23 + 24
//49 = 52 + 23 + 24
//47 = 22 + 33 + 24
//
//How many numbers below fifty million can be expressed as the sum of a prime square, prime cube, and prime fourth power?

#include "template_class.h"

extern void create_prime_list(int x);
extern vector<int> prime_list;

int problem_87()
{
    int LIMIT = 50e6;
    int limit = sqrt(LIMIT);
    prime_list.push_back(2);
    prime_list.push_back(3);
    create_prime_list(limit);
    int idx[3] = {0};
    //int count = 0;
    set<int> result;
    while(1)
    {
        if(idx[0] >= prime_list.size())
        {
            int d = 0;
            d++;
        }
        int a = prime_list[idx[0]];
        int b = prime_list[idx[1]];
        int c = prime_list[idx[2]];
        a = a*a;
        b = b*b*b;
        c = c*c*c*c;
        int tmp = a + b + c;
        if(tmp < LIMIT)
        {
            result.insert(tmp);
            idx[0]++;
            if(idx[0] >= prime_list.size())
            {
                idx[0] = 0;
                idx[1]++;
            }
        }
        else
        {
            if(idx[0] == 0 && idx[1] == 0 && idx[2])
            {
                break;
            }
            if(idx[0] == 0 && idx[1])
            {
                idx[2]++;
                idx[1] = 0;
            }
            else if(idx[0])
            {
                idx[1]++;
                idx[0] = 0;
            }
        }
    }
    return result.size();
}
