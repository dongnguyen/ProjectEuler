//Right triangles with integer coordinates
//Problem 91
//The points P(x1, y1) and Q(x2, y2) are plotted at integer co - ordinates and are joined to the origin, O(0, 0), to form ?OPQ.
//
//
//There are exactly fourteen triangles containing a right angle that can be formed when each co - ordinate lies between 0 and 2 inclusive; that is,
//0 ? x1, y1, x2, y2 ? 2.
//
//
//Given that 0 ? x1, y1, x2, y2 ? 50, how many right triangles can be formed ?

#include "template_class.h"

int problem_91(int n)
{
    int ret = 0;
    // O is right angle
    ret += n*n;
    // right angle lays on diagonal line (i-j = 0)
    for (int i = 1; i < n; i++)
    {
        ret += 2 * min(n - i, i);
    }
    // A (or B) is right angle and lay on one side of square
    ret += 2 * n*n;
    // Remain (?), looping for A, A is not belong to side, diagonal
    // j > 0, i > j
    for (int j = 1; j < n; j++)
    {
        for (int i = j + 1; i <= n; i++)
        {
            int gcd_ij = gcd(i, j);
            int r_x = j / gcd_ij;
            int c_x = i / gcd_ij;
            int r = i - r_x;
            int c = j + c_x;
            while ((r >= 0) && (c <= n))
            {
                ret += 2;
                r -= r_x;
                c += c_x;
            }
            r = i + r_x;
            c = j - c_x;
            while ((c >= 0) && (r <= n))
            {
                ret += 2;
                r += r_x;
                c -= c_x;
            }
        }
    }
    return ret;
}