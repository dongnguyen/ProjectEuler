//Poker hands
//Problem 54
//In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:
//
//High Card: Highest value card.
//One Pair: Two cards of the same value.
//Two Pairs: Two different pairs.
//Three of a Kind: Three cards of the same value.
//Straight: All cards are consecutive values.
//Flush: All cards of the same suit.
//Full House: Three of a kind and a pair.
//Four of a Kind: Four cards of the same value.
//Straight Flush: All cards are consecutive values of same suit.
//Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
//The cards are valued in the order:
//2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.
//
//If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.
//
//Consider the following five hands dealt to two players:
//
//Hand	 	Player 1	 	Player 2	 	Winner
//1	 	5H 5C 6S 7S KD
//Pair of Fives
// 	2C 3S 8S 8D TD
//Pair of Eights
// 	Player 2
//2	 	5D 8C 9S JS AC
//Highest card Ace
// 	2C 5C 7D 8S QH
//Highest card Queen
// 	Player 1
//3	 	2D 9C AS AH AC
//Three Aces
// 	3D 6D 7D TD QD
//Flush with Diamonds
// 	Player 2
//4	 	4D 6S 9H QH QC
//Pair of Queens
//Highest card Nine
// 	3D 6D 7H QD QS
//Pair of Queens
//Highest card Seven
// 	Player 1
//5	 	2H 2D 4C 4D 4S
//Full House
//With Three Fours
// 	3C 3D 3S 9S 9D
//Full House
//with Three Threes
// 	Player 1
//The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.
//
//How many hands does Player 1 win?

#include "template_class.h"

#define NUM_OF_HANDS 1000
//#define NUM_OF_HANDS 5

vector<vector<uint32_t>> player_1;
vector<vector<uint32_t>> player_2;

uint32_t convert_card(const char* card)
{
    uint32_t val = 0;
    uint32_t suite = 0;
    switch(card[0])
    {
    case '2':
        val = 2;
        break;
    case '3':
        val = 3;
        break;
    case '4':
        val = 4;
        break;
    case '5':
        val = 5;
        break;
    case '6':
        val = 6;
        break;
    case '7':
        val = 7;
        break;
    case '8':
        val = 8;
        break;
    case '9':
        val = 9;
        break;
    case 'T':
        val = 10;
        break;
    case 'J':
        val = 11;
        break;
    case 'Q':
        val = 12;
        break;
    case 'K':
        val = 13;
        break;
    case 'A':
        val = 14;
        break;
    default:
        cout << "error card" << endl;
        exit(-1);
    }
    switch(card[1])
    {
    case 'S':
        break;
    case 'H':
        suite = 1;
        break;
    case 'D':
        suite = 2;
        break;
    case 'C':
        suite = 3;
        break;
    default:
        cout << "error card" << endl;
        exit(-1);
    }
    return (val << 16) + suite;
}

uint32_t check_same_suite(vector<uint32_t>& hand, uint64_t& high_card)
{
    for(uint32_t i = 0; i < 4; i++)
    {
        if((hand[i] & 0x0000ffff) != (hand[i+1] & 0x0000ffff))
        {
            return false;
        }
    }
    high_card = hand[4] >> 16;
    return true;
}

bool check_all_consecutive(vector<uint32_t>& hand, uint64_t& val_all_cons)
{
    for(uint32_t i = 0; i < 4; i++)
    {
        if((hand[i] >> 16) + 1 != (hand[i+1] >> 16))
        {
            return false;
        }
    }
    val_all_cons = hand[0] >> 16;
    return true;
}

uint64_t check_four_of_a_kind(vector<uint32_t>& hand)
{
    uint64_t ret = hand[0] >> 16;
    for(int i = 1; i < 4; i++)
    {
        if((hand[0] >> 16) != (hand[i] >> 16))
        {
            ret = 0;
            break;
        }
    }
    if(ret)
    {
        return ret;
    }
    ret = hand[1] >> 16;
    for(int i = 2; i < 5; i++)
    {
        if((hand[1] >> 16) != (hand[i] >> 16))
        {
            ret = 0;
            break;
        }
    }
    if(ret)
    {
        return ret;
    }
    return ret;
}

bool check_full_house(vector<uint32_t>& hand, uint64_t& val_triple, uint64_t& val_pair)
{
    if((hand[0] >> 16) == (hand[1] >> 16) && (hand[0] >> 16) == (hand[2] >> 16) && (hand[3] >> 16) == (hand[4] >> 16))
    {
        val_triple = hand[0] >> 16;
        val_pair   = hand[3] >> 16;
        return true;
    }
    if((hand[0] >> 16) == (hand[1] >> 16) && (hand[2] >> 16) == (hand[3] >> 16) && (hand[3] >> 16) == (hand[4] >> 16))
    {
        val_triple = hand[2] >> 16;
        val_pair   = hand[0] >> 16;
        return true;
    }
    return false;
}

bool check_two_pair(vector<uint32_t>& hand, uint64_t& val_pair_1, uint64_t& val_pair_2, uint64_t& remain)
{
    if((hand[0] >> 16) == (hand[1] >> 16) && (hand[2] >> 16) == (hand[3] >> 16))
    {
        val_pair_1 = hand[2] >> 16;
        val_pair_2 = hand[0] >> 16;
        remain     = hand[4] >> 16;
        return true;
    }
    if((hand[0] >> 16) == (hand[1] >> 16) && (hand[4] >> 16) == (hand[3] >> 16))
    {
        val_pair_1 = hand[3] >> 16;
        val_pair_2 = hand[0] >> 16;
        remain     = hand[2] >> 16;
        return true;
    }
    if((hand[1] >> 16) == (hand[2] >> 16) && (hand[3] >> 16) == (hand[4] >> 16))
    {
        val_pair_1 = hand[3] >> 16;
        val_pair_2 = hand[1] >> 16;
        remain     = hand[0] >> 16;
        return true;
    }
    return false;
}

bool check_one_pair(vector<uint32_t>& hand, uint64_t& val_pair, uint64_t& remain)
{
    if((hand[0] >> 16) == (hand[1] >> 16))
    {
        val_pair = hand[0] >> 16;
        remain = hand[4] >> 16;
        return true;
    }
    if((hand[2] >> 16) == (hand[1] >> 16))
    {
        val_pair = hand[1] >> 16;
        remain = hand[4] >> 16;
        return true;
    }
    if((hand[2] >> 16) == (hand[3] >> 16))
    {
        val_pair = hand[2] >> 16;
        remain = hand[4] >> 16;
        return true;
    }
    if((hand[4] >> 16) == (hand[3] >> 16))
    {
        val_pair = hand[3] >> 16;
        remain = hand[2] >> 16;
        return true;
    }
    return false;
}

uint64_t check_three_of_a_kind(vector<uint32_t>& hand)
{
    uint64_t ret = hand[0] >> 16;
    for(int i = 1; i < 3; i++)
    {
        if((hand[0] >> 16) != (hand[i] >> 16))
        {
            ret = 0;
            break;
        }
    }
    if(ret)
    {
        return ret;
    }
    ret = hand[1] >> 16;
    for(int i = 2; i < 4; i++)
    {
        if((hand[1] >> 16) != (hand[i] >> 16))
        {
            ret = 0;
            break;
        }
    }
    if(ret)
    {
        return ret;
    }
    ret = hand[2] >> 16;
    for(int i = 3; i < 5; i++)
    {
        if((hand[2] >> 16) != (hand[i] >> 16))
        {
            ret = 0;
            break;
        }
    }
    if(ret)
    {
        return ret;
    }
    return ret;
}

#define High_Card       0  //Highest value card.
#define One_Pair        4  //Two cards of the same value.
#define Two_Pairs       8  //Two different pairs.
#define Three_of_a_Kind 16 //Three cards of the same value.
#define Straight        20 //All cards are consecutive values.
#define Flush           24 //All cards of the same suit.
#define Full_House      28 //Three of a kind and a pair.
#define Four_of_a_Kind  36 //Four cards of the same value.
#define Straight_Flush  40 //All cards are consecutive values of same suit.
#define Royal_Flush     44 //Ten, Jack, Queen, King, Ace, in same suit.

uint64_t value_hand(vector<uint32_t>& hand)
{
    uint64_t level = 0;
    uint64_t val = 0;

    uint64_t val_all_cons = 0;
    bool all_conse = check_all_consecutive(hand, val_all_cons);
    uint64_t high_card = 0;
    uint32_t same_suite = check_same_suite(hand, high_card);

    if(same_suite && all_conse)
    {
        if((hand[0] >> 16) == 10)
        {
            return 1LL << Royal_Flush;
        }
        return (uint64_t(hand[0]) >> 16) << Straight_Flush;
    }

    uint64_t four_a_kind = check_four_of_a_kind(hand);

    if(four_a_kind)
    {
        return (four_a_kind << Four_of_a_Kind);
    }

    uint64_t val_triple = 0;
    uint64_t val_pair = 0;
    bool full_house = check_full_house(hand, val_triple, val_pair);

    if(full_house)
    {
        return (val_triple << (Full_House + 4)) + (val_pair << Full_House);
    }

    if(same_suite)
    {
        return high_card << Flush;
    }

    if(all_conse)
    {
        return val_all_cons << Straight;
    }

    uint64_t three_a_kind = check_three_of_a_kind(hand);

    if(three_a_kind)
    {
        return three_a_kind << Three_of_a_Kind;
    }

    uint64_t val_pair_1 = 0;
    uint64_t val_pair_2 = 0;
    uint64_t remain = 0;
    bool two_pair = check_two_pair(hand, val_pair_1, val_pair_2, remain);

    if(two_pair)
    {
        return (val_pair_1 << (Two_Pairs + 4)) + (val_pair_2 << Two_Pairs) + remain;
    }

    val_pair = 0;
    bool one_pair = check_one_pair(hand, val_pair, remain);

    if(one_pair)
    {
        return (val_pair << One_Pair) + remain;
    }

    return (hand[4] >> 16);
}

void read_cards(char* path)
{
    string line;
    ifstream infile;
    infile.open(path);
    for(int idx = 0; idx < NUM_OF_HANDS; idx++)
    {
        getline(infile, line);
        const char* ptr = line.c_str();
        for(uint32_t i = 0; i < 15; i += 3)
        {
            player_1[idx][i / 3] = convert_card(ptr + i);
        }
        for(uint32_t i = 15; i < 30; i += 3)
        {
            player_2[idx][(i - 15) / 3] = convert_card(ptr + i);
        }
        sort(player_1[idx].begin(), player_1[idx].end());
        sort(player_2[idx].begin(), player_2[idx].end());
    }
    infile.close();
}

int problem_54(char* path)
{
    uint32_t num = NUM_OF_HANDS;
    player_1.resize(num);
    player_2.resize(num);
    for(uint32_t i = 0; i < num; i++)
    {
        player_1[i].resize(5);
        player_2[i].resize(5);
    }
    read_cards(path);

    int count = 0;
    for(uint32_t i = 0; i < num; i++)
    {
        if(value_hand(player_1[i]) > value_hand(player_2[i]))
        {
            count++;
        }
    }
    return count;
}
