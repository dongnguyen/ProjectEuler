//Combinatoric selections
//Problem 53
//There are exactly ten ways of selecting three from five, 12345:
//
//123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
//
//In combinatorics, we use the notation, 5C3 = 10.
//
//In general,
//
//nCr =
//n!
//r!(n?r)!
//,where r ? n, n! = n�(n?1)�...�3�2�1, and 0! = 1.
//It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.
//
//How many, not necessarily distinct, values of  nCr, for 1 ? n ? 100, are greater than one-million?

#include "template_class.h"

extern uint64_t factorial(int n);
extern double ln_factorial(int n);

double ln_combination(int n, int r)
{
    if(r == 0)
    {
        return 0;
    }
    if(r == n)
    {
        return 0;
    }
    return ln_factorial(n) - ln_factorial(r) - ln_factorial(n - r);
}

int problem_53()
{
    double log_1m = log(1e6);
    int count = 0;
    for(int n = 1; n <= 100; n++)
    {
        for(int r = 1; r < n; r++)
        {
            if(ln_combination(n, r) > log_1m)
            {
                count++;
            }
        }
    }
    return count;
}
