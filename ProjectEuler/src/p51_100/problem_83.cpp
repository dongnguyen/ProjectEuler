//Path sum: four ways
//Problem 83
//NOTE: This problem is a significantly more challenging version of Problem 81.
//
//In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by moving left, right, up, and down, is indicated in bold red and is equal to 2297.
//
//
//131	673	234	103	18
//201	96	342	965	150
//630	803	746	422	111
//537	699	497	121	956
//805	732	524	37	331
//
//Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by moving left, right, up, and down.

#include "template_class.h"

extern void read_input_81(char* path, vector<vector<int>>& input);

uint32_t min_path_four_way(vector<vector<int>>& input)
{
    vector<vector<uint32_t>> cost;
    uint32_t n = input.size();
    cost.resize(n);
    for(uint32_t i = 0; i < n; i++)
    {
        cost[i].resize(n);
        for(uint32_t j = 0; j < n; j++)
        {
            cost[i][j] = INT_MAX;
        }
    }
    cost[0][0] = input[0][0];
    vector<vector<uint32_t>> unvisited;
    unvisited.resize(n);
    for(uint32_t i = 0; i < n; i++)
    {
        unvisited[i].resize(n);
        for(uint32_t j = 0; j < n; j++)
        {
            unvisited[i][j] = 1;
        }
    }
    int start_x = 0;
    int start_y = 0;
    while(unvisited[n-1][n-1])
    {
        int tmp_x = start_x + 1;
        int tmp_y = start_y;
        if(tmp_x < n)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        tmp_x = start_x;
        tmp_y = start_y - 1;
        if(tmp_y >= 0)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        tmp_x = start_x;
        tmp_y = start_y + 1;
        if(tmp_y < n)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        tmp_x = start_x - 1;
        tmp_y = start_y;
        if(tmp_x >= 0)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        unvisited[start_y][start_x] = 0;
        uint32_t min_tentative_cost = INT_MAX;
        for(uint32_t i = 0; i < n; i++)
        {
            for(uint32_t j = 0; j < n; j++)
            {
                if(unvisited[i][j])
                {
                    if(min_tentative_cost > cost[i][j])
                    {
                        min_tentative_cost = cost[i][j];
                        start_x = j;
                        start_y = i;
                    }
                }
            }
        }
    }
    return cost[n-1][n-1];
}

int problem_83(char* path)
{
    vector<vector<int>> input;
    read_input_81(path, input);
    return min_path_four_way(input);
}
