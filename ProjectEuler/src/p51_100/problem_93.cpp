//Arithmetic expressions
//Problem 93
//By using each of the digits from the set, { 1, 2, 3, 4 }, exactly once, and making use of the four arithmetic operations(+, ?, *, / ) and brackets / parentheses, it is possible to form different positive integer targets.
//
//For example,
//
//8 = (4 * (1 + 3)) / 2
//14 = 4 * (3 + 1 / 2)
//19 = 4 * (2 + 3) ? 1
//36 = 3 * 4 * (2 + 1)
//
//Note that concatenations of the digits, like 12 + 34, are not allowed.
//
//Using the set, { 1, 2, 3, 4 }, it is possible to obtain thirty - one different target numbers of which 36 is the maximum, and each of the numbers 1 to 28 can be obtained before encountering the first non - expressible number.
//
//Find the set of four distinct digits, a < b < c < d, for which the longest set of consecutive positive integers, 1 to n, can be obtained, giving your answer as a string : abcd.

#include "template_class.h"

#define PLUS_OP  0
#define MINUS_OP 1
#define MUL_OP   2
#define DIV_OP   3

#define INVALID 0xDEADBEEF

factor<int> process_op(int op, factor<int> a, factor<int> b)
{
    if (a.is_valid == false)
    {
        return a;
    }
    if (b.is_valid == false)
    {
        return b;
    }
    switch (op)
    {
    case PLUS_OP:
        return a + b;
        break;
    case MINUS_OP:
        return a - b;
        break;
    case MUL_OP:
        return a * b;
        break;
    case DIV_OP:
        if (b.num == 0)
        {
            factor<int> ret;
            ret.is_valid = false;
            return ret;
        }
        b.swap();
        return a * b;
        break;
    default:
        factor<int> ret;
        ret.is_valid = false;
        return ret;
        break;
    }
    factor<int> ret;
    ret.is_valid = false;
    return ret;
}

int count_93(vector<int>& set_digits)
{
    set<int> res;
    do
    {
        for (int op1 = PLUS_OP; op1 <= DIV_OP; op1++)
        {
            for (int op2 = PLUS_OP; op2 <= DIV_OP; op2++)
            {
                for (int op3 = PLUS_OP; op3 <= DIV_OP; op3++)
                {
                    factor<int> tmp;
                    factor<int> tmp2;
                    // 1 2 3
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                    tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                    tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // 1 3 2
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                    tmp2 = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                    tmp = process_op(op2, tmp, tmp2);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // 2 1 3
                    tmp2 = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp2);
                    tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // 2 3 1
                    tmp2 = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                    tmp2 = process_op(op3, tmp2, factor<int>(set_digits[3], 1));
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp2);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // 3 1 2
                    tmp = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                    tmp2 = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                    tmp = process_op(op2, tmp2, tmp);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // 3 2 1
                    tmp = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                    tmp = process_op(op2, factor<int>(set_digits[1], 1), tmp);
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
#if 0
                    // no ()
                    int tmp = 0;
                    int tmp2 = 0;
                    if (op3 > MINUS_OP)
                    {
                        if (op2 > MINUS_OP)
                        {
                            if (op1 > MINUS_OP)
                            {
                                tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                                tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                                tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                            }
                            else
                            {
                                tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                                tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                                tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                            }
                        }
                        else
                        {
                            tmp = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                            tmp2 = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                            tmp = process_op(op2, tmp2, tmp);
                        }
                    }
                    else
                    {
                        if (op2 > MINUS_OP)
                        {
                            if (op1 > MINUS_OP)
                            {
                                tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                                tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                                tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                            }
                            else
                            {
                                tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                                tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                                tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                            }
                        }
                        else
                        {
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                            tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                        }
                    }
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // ( a b ) c d
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                    if (op3 > MINUS_OP)
                    {
                        if (op2 > MINUS_OP)
                        {
                            tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                        }
                        else
                        {
                            tmp2 = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                            tmp = process_op(op2, tmp, tmp2);
                        }
                    }
                    else
                    {
                        tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                        tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    }
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // ( a b ) (c d)
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                    tmp2 = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                    tmp = process_op(op2, tmp, tmp2);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    //  a b  (c d)
                    tmp = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                    if (op2 > MINUS_OP)
                    {
                        if (op1 > MINUS_OP)
                        {
                            tmp2 = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                            tmp = process_op(op2, tmp2, tmp);
                        }
                        else
                        {
                            tmp = process_op(op2, factor<int>(set_digits[1], 1), tmp);
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                        }
                    }
                    else
                    {
                        tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                        tmp2 = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                        tmp = process_op(op2, tmp, tmp2);
                    }
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    //  a (b c) d
                    tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                    if (op3 > MINUS_OP)
                    {
                        if (op1 > MINUS_OP)
                        {
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                        }
                        else
                        {
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                        }
                    }
                    else
                    {
                        tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                        tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    }
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // (a b c) d
                    if (op2 > MINUS_OP)
                    {
                        if (op1 > MINUS_OP)
                        {
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                            tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                        }
                        else
                        {
                            tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                        }
                    }
                    else
                    {
                        tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                        tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                        tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    }
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // ((a b) c) d
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), factor<int>(set_digits[1], 1));
                    tmp = process_op(op2, tmp, factor<int>(set_digits[2], 1));
                    tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // (a (b c)) d
                    tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                    tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // a (b c d)
                    if (op3 > MINUS_OP)
                    {
                        if (op2 > MINUS_OP)
                        {
                            tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                            tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                        }
                        else
                        {
                            tmp = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                            tmp = process_op(op2, factor<int>(set_digits[1], 1), tmp);
                            tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                        }
                    }
                    else
                    {
                        tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                        tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                        tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                    }
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // a ((b c) d)
                    tmp = process_op(op2, factor<int>(set_digits[1], 1), factor<int>(set_digits[2], 1));
                    tmp = process_op(op3, tmp, factor<int>(set_digits[3], 1));
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
                    // a (b (c d))
                    tmp = process_op(op3, factor<int>(set_digits[2], 1), factor<int>(set_digits[3], 1));
                    tmp = process_op(op2, factor<int>(set_digits[1], 1), tmp);
                    tmp = process_op(op1, factor<int>(set_digits[0], 1), tmp);
                    if ((tmp.is_valid) && (tmp.num > 0) && (tmp.denum == 1))
                    {
                        res.insert(tmp.num);
                    }
#endif
                }
            }
        }
    }
    while (next_permutation(set_digits.begin(), set_digits.end()));
    int cmp = 1;
    for (set<int>::iterator ite = res.begin(); ite != res.end(); ++ite)
    {
        if (*ite != cmp)
        {
            return cmp - 1;
        }
        cmp++;
    }
    return 0;
}
int problem_93()
{
    vector<int> set_digits;
    vector<int> ret;
    set_digits.resize(4);

    /*set_digits[0] = 1;
    set_digits[1] = 2;
    set_digits[2] = 3;
    set_digits[3] = 4;
    count_93(set_digits);*/

    int max = 0;
    for (int a = 1; a < 7; a++)
    {
        set_digits[0] = a;
        for (int b = a + 1; b < 8; b++)
        {
            set_digits[1] = b;
            for (int c = b + 1; c < 9; c++)
            {
                set_digits[2] = c;
                for (int d = c + 1; d < 10; d++)
                {
                    set_digits[3] = d;
                    int tmp = count_93(set_digits);
                    if (max < tmp)
                    {
                        max = tmp;
                        ret = set_digits;
                    }
                }
            }
        }
    }
    return  ret[0] * 1000 +
            ret[1] * 100 +
            ret[2] * 10 +
            ret[3];
}
