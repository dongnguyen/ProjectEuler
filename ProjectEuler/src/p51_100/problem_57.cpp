//Square root convergents
//Problem 57
//It is possible to show that the square root of two can be expressed as an infinite continued fraction.
//
//? 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...
//
//By expanding this for the first four iterations, we get:
//
//1 + 1/2 = 3/2 = 1.5
//1 + 1/(2 + 1/2) = 7/5 = 1.4
//1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
//1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...
//
//The next three expansions are 99/70, 239/169, and 577/408, but the eighth expansion, 1393/985, is the first example where the number of digits in the numerator exceeds the number of digits in the denominator.
//
//In the first one-thousand expansions, how many fractions contain a numerator with more digits than denominator?

#include "template_class.h"

struct factor_57
{
    vector<int> num;
    vector<int> denum;
    factor_57() {};
    bool is_satisfy_57()
    {
        return num.size() > denum.size();
    }
    void revert()
    {
        swap(num, denum);
    }
    factor_57& operator+=(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
    factor_57& operator+(const int rhs)
    {
        // actual addition of rhs to *this
        vector<int> tmp = denum;
        mul_string_with_n(rhs, tmp);
        vector<int> tmp2 = num;
        add_two_vector(tmp, tmp2, num);
        return *this;
    }
};

factor_57 calc_57(int num_ite)
{
    factor_57 ret;
    if(num_ite == 1)
    {
        ret.num.push_back(1);
        ret.denum.push_back(2);
        ret += 1;
        return ret;
    }
    ret.num.push_back(1);
    ret.denum.push_back(2);
    for(int i = 1; i < num_ite; i++)
    {
        ret += 2;
        ret.revert();
    }
    ret += 1;
    return ret;
}

int problem_57()
{
    /*factor_57 a;
    factor_57 b;
    a.num.push_back(1);
    a.denum.push_back(2);
    a += 2;*/
    /*b.num.resize(10);
    b.num[0] = 10;
    b.denum.resize(8);
    a = b;
    swap(a.num, a.denum);*/
    int count = 0;
    /*for(int i = 1; i <= 1000; i++)
    {
        factor_57 ret = calc_57(i);
        if(ret.is_satisfy_57())
        {
            count++;
        }
    }*/
    factor_57 ret;
    factor_57 tmp;
    ret.num.push_back(1);
    ret.denum.push_back(2);
    tmp = ret;
    tmp += 1;
    if(tmp.is_satisfy_57())
    {
        count++;
    }
    for(int i = 2; i <= 1000; i++)
    {
        ret += 2;
        ret.revert();
        tmp = ret;
        tmp += 1;
        if(tmp.is_satisfy_57())
        {
            count++;
        }
    }
    return count;
}
