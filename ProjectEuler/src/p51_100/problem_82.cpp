//Path sum: three ways
//Problem 82
//NOTE: This problem is a more challenging version of Problem 81.
//
//The minimal path sum in the 5 by 5 matrix below, by starting in any cell in the left column and finishing in any cell in the right column, and only moving up, down, and right, is indicated in red and bold; the sum is equal to 994.
//
//
//131	673	234	103	18
//201	96	342	965	150
//630	803	746	422	111
//537	699	497	121	956
//805	732	524	37	331
//
//Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), a 31K text file containing a 80 by 80 matrix, from the left column to the right column.

#include "template_class.h"

extern void read_input_81(char* path, vector<vector<int>>& input);

uint32_t min_path_at_one_start_node(vector<vector<int>>& input, int x, int y)
{
    vector<vector<uint32_t>> cost;
    uint32_t n = input.size();
    cost.resize(n);
    for(uint32_t i = 0; i < n; i++)
    {
        cost[i].resize(n);
        for(uint32_t j = 0; j < n; j++)
        {
            cost[i][j] = INT_MAX;
        }
    }
    cost[y][x] = input[y][x];
    vector<vector<uint32_t>> unvisited;
    unvisited.resize(n);
    for(uint32_t i = 0; i < n; i++)
    {
        unvisited[i].resize(n);
        for(uint32_t j = 0; j < n; j++)
        {
            unvisited[i][j] = 1;
        }
    }
    uint32_t num_unvisited = n * n;
    int start_x = x;
    int start_y = y;
    while(num_unvisited)
    {
        int tmp_x = start_x + 1;
        int tmp_y = start_y;
        if(tmp_x < n)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        tmp_x = start_x;
        tmp_y = start_y - 1;
        if(tmp_y >= 0)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        tmp_x = start_x;
        tmp_y = start_y + 1;
        if(tmp_y < n)
        {
            if(unvisited[tmp_y][tmp_x])
            {
                uint32_t tmp_cost = cost[start_y][start_x] + input[tmp_y][tmp_x];
                if(tmp_cost < cost[tmp_y][tmp_x])
                {
                    cost[tmp_y][tmp_x] = tmp_cost;
                }
            }
        }
        unvisited[start_y][start_x] = 0;
        num_unvisited--;
        uint32_t min_tentative_cost = INT_MAX;
        for(uint32_t i = 0; i < n; i++)
        {
            for(uint32_t j = 0; j < n; j++)
            {
                if(unvisited[i][j])
                {
                    if(min_tentative_cost > cost[i][j])
                    {
                        min_tentative_cost = cost[i][j];
                        start_x = j;
                        start_y = i;
                    }
                }
            }
        }
    }
    uint32_t ret = INT_MAX;
    for(uint32_t i = 0; i < n; i++)
    {
        if(ret > cost[i][n-1])
        {
            ret = cost[i][n-1];
        }
    }
    return ret;
}

uint32_t problem_82(char* path)
{
    vector<vector<int>> input;
    read_input_81(path, input);
    uint32_t n = input.size();
    uint32_t ret = INT_MAX;
    for(uint32_t i = 0; i < n; i++)
    {
        uint32_t tmp = min_path_at_one_start_node(input, 0, i);
        if(ret > tmp)
        {
            ret = tmp;
        }
    }


    return ret;
}
