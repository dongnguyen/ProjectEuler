//Counting rectangles
//Problem 85
//By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles:
//
//
//Although there exists no rectangular grid that contains exactly two million rectangles, find the area of the grid with the nearest solution.

#include "template_class.h"

int count_rect(int a, int b)
{
    int sum = 0;
    for(int i = 1; i <= a; i++)
    {
        for(int j = 1; j <= b; j++)
        {
            int m = a - i + 1;
            int n = b - j + 1;
            sum += m*n;
        }
    }
    return sum;
}

int problem_85()
{
    int target = 2e6;
    int min = target;
    int ret = 0;
    for(int i = 1;; i++)
    {
        for(int j = i;; j++)
        {
            int count = count_rect(i, j);
            if(abs(count - target) < min)
            {
                min = abs(count - target);
                ret = i*j;
            }
            if(count > target)
            {
                if(i == j)
                {
                    goto EXIT;
                }
                break;
            }
        }
    }
EXIT:
    return ret;
}
