
#include "template_class.h"

extern vector<int> prime_list;
extern set<int> set_prime_list;
extern bool prime_test3(uint64_t x);

int n_60 = 1e8;

int pow_10[] =
{
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
};

int base_mul_10(int n)
{
    int compare_level = 10;
    while(1)
    {
        if(n < compare_level)
        {
            return compare_level;
        }
        compare_level *= 10;
    }
    return compare_level;
}

int count_num_digit(int n)
{
    int compare_level = 10;
    int count = 1;
    while(1)
    {
        if(n < compare_level)
        {
            return count;
        }
        compare_level *= 10;
        count++;
    }
    return count;
}

bool check_60(int a, int b)
{
    int new_num = a*base_mul_10(b) + b;
    if(new_num > n_60)
    {
        return false;
    }
    if(set_prime_list.find(new_num) == set_prime_list.end())
    {
        return false;
    }
    new_num = a + b*base_mul_10(a);
    if(new_num > n_60)
    {
        return false;
    }
    if(set_prime_list.find(new_num) == set_prime_list.end())
    {
        return false;
    }
    return true;
}

int problem_60()
{
#if 1
    // debug
    vector<int> prime;
    prime.resize(n_60+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    prime_list.push_back(2);
    int limit = sqrt(n_60);
    for(int i = 4; i <= n_60; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 3; i <= limit; i += 2)
    {
        if(prime[i] == PRIME)
        {
            //prime_list.push_back(i);
            for(int j = i*i; j <= n_60; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    for(int i = 3; i <= n_60; i += 2)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
        }
    }
    prime.clear();
    set_prime_list.insert(prime_list.begin(), prime_list.end());

    int ret = INT_MAX;
    limit = n_60 / 10;
    for(int i = 0; i < prime_list.size(); i++)
    {
        int start = prime_list[i];
        if(start > limit)
        {
            break;
        }
        for(int j = i + 1; j < prime_list.size(); j++)
        {
            int tmp = prime_list[j];
            if(tmp > limit)
            {
                break;
            }
            int64_t new1 = tmp * base_mul_10(start) + start;
            int64_t new2 = tmp + start * base_mul_10(tmp);
            if(new1 > n_60)
            {
                break;
            }
            if(new2 > n_60)
            {
                break;
            }
            if(set_prime_list.find(new1) != set_prime_list.end())
                if(set_prime_list.find(new2) != set_prime_list.end())
                {
                    int start2 = tmp;
                    for(int k = j + 1; k < prime_list.size(); k++)
                    {
                        int tmp = prime_list[k];
                        if(tmp > limit)
                        {
                            break;
                        }
                        int64_t new1 = tmp * base_mul_10(start) + start;
                        int64_t new2 = tmp + start * base_mul_10(tmp);
                        int64_t new3 = tmp * base_mul_10(start2) + start2;
                        int64_t new4 = tmp + start2 * base_mul_10(tmp);
                        if(new1 > n_60)
                        {
                            break;
                        }
                        if(new2 > n_60)
                        {
                            break;
                        }
                        if(new3 > n_60)
                        {
                            break;
                        }
                        if(new4 > n_60)
                        {
                            break;
                        }
                        if(set_prime_list.find(new1) != set_prime_list.end())
                            if(set_prime_list.find(new2) != set_prime_list.end())
                                if(set_prime_list.find(new3) != set_prime_list.end())
                                    if(set_prime_list.find(new4) != set_prime_list.end())
                                    {
                                        int start3 = tmp;
                                        for(int l = k + 1; l < prime_list.size(); l++)
                                        {
                                            int tmp = prime_list[l];
                                            if(tmp > limit)
                                            {
                                                break;
                                            }
                                            int64_t new1 = tmp * base_mul_10(start) + start;
                                            int64_t new2 = tmp + start * base_mul_10(tmp);
                                            int64_t new3 = tmp * base_mul_10(start2) + start2;
                                            int64_t new4 = tmp + start2 * base_mul_10(tmp);
                                            int64_t new5 = tmp * base_mul_10(start3) + start3;
                                            int64_t new6 = tmp + start3 * base_mul_10(tmp);
                                            if(new1 > n_60)
                                            {
                                                break;
                                            }
                                            if(new2 > n_60)
                                            {
                                                break;
                                            }
                                            if(new3 > n_60)
                                            {
                                                break;
                                            }
                                            if(new4 > n_60)
                                            {
                                                break;
                                            }
                                            if(new5 > n_60)
                                            {
                                                break;
                                            }
                                            if(new6 > n_60)
                                            {
                                                break;
                                            }
                                            if(set_prime_list.find(new1) != set_prime_list.end())
                                                if(set_prime_list.find(new2) != set_prime_list.end())
                                                    if(set_prime_list.find(new3) != set_prime_list.end())
                                                        if(set_prime_list.find(new4) != set_prime_list.end())
                                                            if(set_prime_list.find(new5) != set_prime_list.end())
                                                                if(set_prime_list.find(new6) != set_prime_list.end())
                                                                {
                                                                    int start4 = tmp;
                                                                    for(int m = l + 1; m < prime_list.size(); m++)
                                                                    {
                                                                        int tmp = prime_list[m];
                                                                        if(tmp > limit)
                                                                        {
                                                                            break;
                                                                        }
                                                                        int64_t new1 = tmp * base_mul_10(start) + start;
                                                                        int64_t new2 = tmp + start * base_mul_10(tmp);
                                                                        int64_t new3 = tmp * base_mul_10(start2) + start2;
                                                                        int64_t new4 = tmp + start2 * base_mul_10(tmp);
                                                                        int64_t new5 = tmp * base_mul_10(start3) + start3;
                                                                        int64_t new6 = tmp + start3 * base_mul_10(tmp);
                                                                        int64_t new7 = tmp * base_mul_10(start4) + start4;
                                                                        int64_t new8 = tmp + start4 * base_mul_10(tmp);
                                                                        if(new1 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new2 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new3 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new4 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new5 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new6 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new7 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(new8 > n_60)
                                                                        {
                                                                            break;
                                                                        }
                                                                        if(set_prime_list.find(new1) != set_prime_list.end())
                                                                            if(set_prime_list.find(new2) != set_prime_list.end())
                                                                                if(set_prime_list.find(new3) != set_prime_list.end())
                                                                                    if(set_prime_list.find(new4) != set_prime_list.end())
                                                                                        if(set_prime_list.find(new5) != set_prime_list.end())
                                                                                            if(set_prime_list.find(new6) != set_prime_list.end())
                                                                                                if(set_prime_list.find(new7) != set_prime_list.end())
                                                                                                    if(set_prime_list.find(new8) != set_prime_list.end())
                                                                                                    {
                                                                                                        int sum = start + start2 + start3 + start4 + tmp;
                                                                                                        // Debug
                                                                                                        cout << start << " + "
                                                                                                             << start2 << " + "
                                                                                                             << start3 << " + "
                                                                                                             << start4 << " + "
                                                                                                             << tmp << " = "
                                                                                                             << sum << endl;
                                                                                                        if(sum < ret)
                                                                                                        {
                                                                                                            ret = sum;
                                                                                                            return ret;
                                                                                                        }
                                                                                                    }
                                                                    }
                                                                }
                                        }
                                    }
                    }
                }
        }
    }
    return ret;
#endif
#if 0
    vector<int> prime;
    prime.resize(n_60+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    prime_list.push_back(2);
    int limit = sqrt(n_60);
    for(int i = 4; i <= n_60; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
            for(int j = i*i; j <= n_60; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    for(int i = limit + 1; i <= n_60; i++)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
        }
    }
    set_prime_list.insert(prime_list.begin(), prime_list.end());
    vector<int64_t> result;
    result.resize(5);
    result[0] = 3;
    result[1] = 7;
    int idx = 4;
    int num_found = 2;
    for(; idx < prime_list.size(); idx++)
    {
        int64_t tmp = prime_list[idx];
        bool test = true;
        for(int i = 0; i < num_found; i++)
        {
            int64_t new1 = tmp * base_mul_10(result[i]) + result[i];
            int64_t new2 = tmp + result[i] * base_mul_10(tmp);

            if(new1 < 0)
            {
                cout << "overflow";
                exit(-1);
            }
            if(new2 < 0)
            {
                cout << "overflow";
                exit(-1);
            }

            if(new1 <= n_60)
            {
                if(set_prime_list.find(new1) == set_prime_list.end())
                {
                    test = false;
                    break;
                }
            }
            else
            {
                if(prime_test3(new1) == false)
                {
                    test = false;
                    break;
                }
            }
            if(new2 <= n_60)
            {
                if(set_prime_list.find(new2) == set_prime_list.end())
                {
                    test = false;
                    break;
                }
            }
            else
            {
                if(prime_test3(new2) == false)
                {
                    test = false;
                    break;
                }
            }
        }
        if(test)
        {
            result[num_found++] = tmp;
            if(num_found == 5)
            {
                goto EXIT;
            }
        }
    }
    for(int tmp = (n_60 / 2 * 2 + 1);; tmp += 2)
    {
        if(prime_test3(tmp))
        {
            bool test = true;
            for(int i = 0; i < num_found; i++)
            {
                int new1 = tmp * base_mul_10(result[i]) + result[i];
                int new2 = tmp + result[i] * base_mul_10(tmp);
                if(new1 <= n_60)
                {
                    if(set_prime_list.find(new1) == set_prime_list.end())
                    {
                        test = false;
                        break;
                    }
                }
                else
                {
                    if(prime_test3(new1) == false)
                    {
                        test = false;
                        break;
                    }
                }
                if(new2 <= n_60)
                {
                    if(set_prime_list.find(new2) == set_prime_list.end())
                    {
                        test = false;
                        break;
                    }
                }
                else
                {
                    if(prime_test3(new2) == false)
                    {
                        test = false;
                        break;
                    }
                }
            }
            if(test)
            {
                result[num_found++] = tmp;
                if(num_found == 5)
                {
                    goto EXIT;
                }
            }
        }
    }
EXIT:

    return result[0] +
           result[1] +
           result[2] +
           result[3] +
           result[4];
#endif
#if 0
    int size = prime_list.size();
    vector<vector<int>> pre_compute;
    pre_compute.resize(size);
    for(int i = 1; i < size; i++)
    {
        int prime = prime_list[i];
        if(prime < 10)
        {
            continue;
        }
        int num_digit = count_num_digit(prime);
        for(int sep = 1; sep < num_digit; sep++)
        {
            int part_1 = prime / pow_10[sep];
            int part_2 = prime % pow_10[sep];
            set<int>::iterator ite_1 = set_prime_list.find(part_1);
            if(ite_1 == set_prime_list.end())
            {
                break;
            }
            set<int>::iterator ite_2 = set_prime_list.find(part_2);
            if(ite_2 == set_prime_list.end())
            {
                break;
            }
            int new_prime = part_2 * pow_10[num_digit - sep] + part_1;
            if(set_prime_list.find(new_prime) == set_prime_list.end())
            {
                break;
            }
            int idx_1 = distance(set_prime_list.begin(), ite_1);
            int idx_2 = distance(set_prime_list.begin(), ite_2);
            if(idx_1 < idx_2)
            {
                pre_compute[idx_1].push_back(idx_2);
            }
            else
            {
                pre_compute[idx_2].push_back(idx_1);
            }
        }
    }
    vector<int> idx_result;
    vector<int> result;
    idx_result.resize(5);
    result.resize(5);
    int ret = INT_MAX;
    for(int i = 0; i < size - 4; i++)
    {
        if(pre_compute[i].size())
        {
            idx_result[0] = i;
            result[0] = prime_list[i];
            for(int j = 0; j < pre_compute[i].size(); j++)
            {
                int new_idx = pre_compute[i][j];
                if(pre_compute[new_idx].size())
                {
                    idx_result[1] = new_idx;
                    result[1] = prime_list[new_idx];
                    for(int k = 0; k < pre_compute[new_idx].size(); k++)
                    {
                        int new_idx1 = pre_compute[new_idx][k];
                        if(pre_compute[new_idx1].size())
                        {
                            // find new_idx1 in set i
                            bool find = false;
                            for(int tmp = 0; tmp < pre_compute[i].size(); tmp++)
                            {
                                if(new_idx1 == pre_compute[i][tmp])
                                {
                                    find = true;
                                    break;
                                }
                            }
                            if(find)
                            {
                                idx_result[2] = new_idx1;
                                result[2] = prime_list[new_idx1];
                                for(int l = 0; l < pre_compute[new_idx1].size(); l++)
                                {
                                    int new_idx2 = pre_compute[new_idx1][l];
                                    if(pre_compute[new_idx2].size())
                                    {
                                        // find new_idx2 in set i
                                        bool find = false;
                                        for(int tmp = 0; tmp < pre_compute[i].size(); tmp++)
                                        {
                                            if(new_idx2 == pre_compute[i][tmp])
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                        if(find == false)
                                        {
                                            break;
                                        }
                                        find = false;
                                        // find new_idx2 in set new_idx
                                        for(int tmp = 0; tmp < pre_compute[new_idx].size(); tmp++)
                                        {
                                            if(new_idx2 == pre_compute[new_idx][tmp])
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                        if(find == false)
                                        {
                                            break;
                                        }
                                        idx_result[3] = new_idx2;
                                        result[3] = prime_list[new_idx2];
                                        for(int m = 0; m < pre_compute[new_idx2].size(); m++)
                                        {
                                            int new_idx3 = pre_compute[new_idx2][m];

                                            // debug
                                            int sum = prime_list[idx_result[0]] +
                                                      prime_list[idx_result[1]] +
                                                      prime_list[idx_result[2]] +
                                                      prime_list[idx_result[3]];
                                            return sum;

                                            if(pre_compute[new_idx3].size())
                                            {
                                                // find new_idx3 in set i
                                                bool find = false;
                                                for(int tmp = 0; tmp < pre_compute[i].size(); tmp++)
                                                {
                                                    if(new_idx3 == pre_compute[i][tmp])
                                                    {
                                                        find = true;
                                                        break;
                                                    }
                                                }
                                                if(find == false)
                                                {
                                                    break;
                                                }
                                                // find new_idx3 in set new_idx
                                                find = false;
                                                for(int tmp = 0; tmp < pre_compute[new_idx].size(); tmp++)
                                                {
                                                    if(new_idx3 == pre_compute[new_idx][tmp])
                                                    {
                                                        find = true;
                                                        break;
                                                    }
                                                }
                                                if(find == false)
                                                {
                                                    break;
                                                }
                                                // find new_idx3 in set new_idx1
                                                find = false;
                                                for(int tmp = 0; tmp < pre_compute[new_idx1].size(); tmp++)
                                                {
                                                    if(new_idx3 == pre_compute[new_idx1][tmp])
                                                    {
                                                        find = true;
                                                        break;
                                                    }
                                                }
                                                if(find == false)
                                                {
                                                    break;
                                                }
                                                idx_result[4] = new_idx3;
                                                int sum = prime_list[idx_result[0]] +
                                                          prime_list[idx_result[1]] +
                                                          prime_list[idx_result[2]] +
                                                          prime_list[idx_result[3]] +
                                                          prime_list[idx_result[4]];
                                                if(sum < ret)
                                                {
                                                    ret = sum;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return ret;
#endif
}
