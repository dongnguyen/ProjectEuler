//Path sum: two ways
//Problem 81
//In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by only moving to the right and down, is indicated in bold red and is equal to 2427.
//
//
//131	673	234	103	18
//201	96	342	965	150
//630	803	746	422	111
//537	699	497	121	956
//805	732	524	37	331
//
//Find the minimal path sum, in matrix.txt (right click and 'Save Link/Target As...'), a 31K text file containing a 80 by 80 matrix, from the top left to the bottom right by only moving right and down.

#include "template_class.h"

void read_input_81(char* path, vector<vector<int>>& input)
{
    vector<int> tmp;
    FILE* fp = fopen(path, "r");
    while(!feof(fp))
    {
        int t = 0;
        fscanf(fp, "%d,", &t);
        tmp.push_back(t);
    }
    tmp.resize(tmp.size() - 1);
    int dim = (int)sqrt(tmp.size());
    input.resize(dim);
    int idx = 0;
    for(int i = 0; i < dim; i++)
    {
        input[i].resize(dim);
        for(int j = 0; j < dim; j++)
        {
            input[i][j] = tmp[idx++];
        }
    }
    fclose(fp);
}

int problem_81(char* path)
{
    vector<vector<int>> input;
    vector<vector<int>> cost;
    read_input_81(path, input);
    int n = input.size();
    cost.resize(n);
    for(int i = 0; i < n; i++)
    {
        cost[i].resize(n);
        for(int j = 0; j < n; j++)
        {
            cost[i][j] = INT_MAX;
        }
    }
    cost[0][0] = input[0][0];
    for(int d = 0; d < 2*n - 2; d++)
    {
        for(int i = 0; i <= d; i++)
        {
            int j = d - i;
            if(j + 1 < n && i < n)
            {
                int cost_nei = cost[i][j] + input[i][j + 1];
                if(cost_nei < cost[i][j + 1])
                {
                    cost[i][j + 1] = cost_nei;
                }
            }
            if(i + 1 < n && j < n)
            {
                int cost_nei = cost[i][j] + input[i + 1][j];
                if(cost_nei < cost[i + 1][j])
                {
                    cost[i + 1][j] = cost_nei;
                }
            }
        }
    }

    return cost[n-1][n-1];
}
