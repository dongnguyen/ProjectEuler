//Concealed Square
//Problem 206
//Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
//where each �_� is a single digit.

#include "template_class.h"

void list_element()
{
    vector<string> num;
    vector<string> num2;
    int n = 9;
    num.resize(n);
    num2.resize(2*n - 1);
    for(int i = 0; i < n; i++)
    {
        num[i] += "n[";
        num[i] += to_string(i);
        num[i] += "]";
    }
    for(int i = 0; i < n; i++)
    {
        num2[i*2] = num[i] + "*" + num[i];
    }
    for(int i = 0; i < n - 1; i++)
    {
        for(int j = i + 1; j < n; j++)
        {
            if(num2[i+j] == "")
            {
                num2[i+j] += "2*";
            }
            else
            {
                num2[i+j] += " + 2*";
            }
            num2[i+j] += num[i] + "*" + num[j];
        }
    }
    vector<string> num2_X;
    num2_X.resize(2*n - 1);
    num2_X[ 0] = "9";
    num2_X[ 1] = "_";
    num2_X[ 2] = "8";
    num2_X[ 3] = "_";
    num2_X[ 4] = "7";
    num2_X[ 5] = "_";
    num2_X[ 6] = "6";
    num2_X[ 7] = "_";
    num2_X[ 8] = "5";
    num2_X[ 9] = "_";
    num2_X[10] = "4";
    num2_X[11] = "_";
    num2_X[12] = "3";
    num2_X[13] = "_";
    num2_X[14] = "2";
    num2_X[15] = "_";
    num2_X[16] = "1";
    for(int i = 0; i < 2*n - 1; i++)
    {
        cout << num2_X[i] << " = " << num2[i] << " + C" << i << endl;
    }
    return;
}

uint64_t problem_206()
{
    //list_element();
    //exit(0);
    vector<uint64_t> n;
    n.resize(9);
    n[8] = 1;
    for(n[0] = 3; n[0] < 8; n[0] += 4)
    {
        for(n[1] = 0; n[1] < 10; n[1]++)
        {
            uint64_t C2 = (2*n[0]*n[1] + (n[0]*n[0] / 10)) / 10;
            for(n[2] = 0; n[2] < 10; n[2]++)
            {
                uint64_t tmp = n[1]*n[1] + 2*n[0]*n[2] + C2;
                if(tmp % 10 != 8)
                {
                    continue;
                }
                uint64_t C3 = tmp / 10;
                for(n[3] = 0; n[3] < 10; n[3]++)
                {
                    uint64_t C4 = (2*n[0]*n[3] + 2*n[1]*n[2] + C3) / 10;
                    for(n[4] = 0; n[4] < 10; n[4]++)
                    {
                        uint64_t tmp = n[2]*n[2] + 2*n[0]*n[4] + 2*n[1]*n[3] + C4;
                        if(tmp % 10 != 7)
                        {
                            continue;
                        }
                        uint64_t C5 = tmp / 10;
                        for(n[5] = 0; n[5] < 10; n[5]++)
                        {
                            uint64_t C6 = (2*n[0]*n[5] + 2*n[1]*n[4] + 2*n[2]*n[3] + C5) / 10;
                            for(n[6] = 0; n[6] < 10; n[6]++)
                            {
                                uint64_t tmp = n[3]*n[3] + 2*n[0]*n[6] + 2*n[1]*n[5] + 2*n[2]*n[4] + C6;
                                if(tmp % 10 != 6)
                                {
                                    continue;
                                }
                                uint64_t C7 = tmp / 10;
                                for(n[7] = 0; n[7] < 4; n[7]++)
                                {
                                    uint64_t C8 = (2*n[0]*n[7] + 2*n[1]*n[6] + 2*n[2]*n[5] + 2*n[3]*n[4] + C7) / 10;
                                    uint64_t tmp = n[4]*n[4] + 2*n[0]*n[8] + 2*n[1]*n[7] + 2*n[2]*n[6] + 2*n[3]*n[5] + C8;
                                    if(tmp % 10 != 5)
                                    {
                                        continue;
                                    }
                                    uint64_t C9 = tmp / 10;
                                    uint64_t C10 = (2*n[1]*n[8] + 2*n[2]*n[7] + 2*n[3]*n[6] + 2*n[4]*n[5] + C9) / 10;
                                    tmp = n[5]*n[5] + 2*n[2]*n[8] + 2*n[3]*n[7] + 2*n[4]*n[6] + C10;
                                    if(tmp % 10 != 4)
                                    {
                                        continue;
                                    }
                                    uint64_t C11 = tmp / 10;
                                    uint64_t C12 = (2*n[3]*n[8] + 2*n[4]*n[7] + 2*n[5]*n[6] + C11) / 10;
                                    tmp = n[6]*n[6] + 2*n[4]*n[8] + 2*n[5]*n[7] + C12;
                                    if(tmp % 10 != 3)
                                    {
                                        continue;
                                    }
                                    uint64_t C13 = tmp / 10;
                                    uint64_t C14 = (2*n[5]*n[8] + 2*n[6]*n[7] + C13) / 10;
                                    tmp = n[7]*n[7] + 2*n[6]*n[8] + C14;
                                    /*cout << n[8] << n[7] << n[6] << n[5] << n[4] << n[3] << n[2] << n[1] << n[0] << endl;
                                    cout << (n[0]*1e1 +
                                           n[1]*1e2 +
                                           n[2]*1e3 +
                                           n[3]*1e4 +
                                           n[4]*1e5 +
                                           n[5]*1e6 +
                                           n[6]*1e7 +
                                           n[7]*1e8 +
                                           n[8]*1e9) * (n[0]*1e1 +
                                           n[1]*1e2 +
                                           n[2]*1e3 +
                                           n[3]*1e4 +
                                           n[4]*1e5 +
                                           n[5]*1e6 +
                                           n[6]*1e7 +
                                           n[7]*1e8 +
                                           n[8]*1e9) << endl << endl;*/
                                    if(tmp % 10 != 2)
                                    {
                                        continue;
                                    }
                                    return n[0]*1e1 +
                                           n[1]*1e2 +
                                           n[2]*1e3 +
                                           n[3]*1e4 +
                                           n[4]*1e5 +
                                           n[5]*1e6 +
                                           n[6]*1e7 +
                                           n[7]*1e8 +
                                           n[8]*1e9;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return -1;
}
