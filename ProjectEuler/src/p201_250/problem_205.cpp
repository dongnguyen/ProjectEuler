//Dice Game
//Problem 205
//Peter has nine four-sided (pyramidal) dice, each with faces numbered 1, 2, 3, 4.
//Colin has six six-sided (cubic) dice, each with faces numbered 1, 2, 3, 4, 5, 6.
//
//Peter and Colin roll their dice and compare totals: the highest total wins. The result is a draw if the totals are equal.
//
//What is the probability that Pyramidal Pete beats Cubic Colin? Give your answer rounded to seven decimal places in the form 0.abcdefg

#include "template_class.h"

template <typename T>
void mul_factor_probably(vector<T>& a, vector<T>& b, vector<T>& result)
{
    int size_a = a.size();
    int size_b = b.size();
    result.resize(size_a + size_b - 1);
    for(int i = 0; i < size_a; i++)
    {
        for(int j = 0; j < size_b; j++)
        {
            result[i + j] += a[i] * b[j];
        }
    }
}

double problem_205()
{
    vector<factor<BigInteger>> Peter;
    vector<factor<BigInteger>> Colin;
    vector<factor<BigInteger>> Peter_pro;
    vector<factor<BigInteger>> Colin_pro;
    Peter.resize(4+1);
    Colin.resize(6+1);
    for(int i = 1; i <= 4; i++)
    {
        Peter[i].Init(1, 4);
    }
    for(int i = 1; i <= 6; i++)
    {
        Colin[i].Init(1, 6);
    }
    vector<factor<BigInteger>> tmp;
    tmp = Peter;
    for(int i = 0; i < 8; i++)
    {
        mul_factor_probably(tmp, Peter, Peter_pro);
        tmp = Peter_pro;
        Peter_pro.clear();
    }
    Peter_pro = tmp;

    tmp = Colin;
    for(int i = 0; i < 5; i++)
    {
        mul_factor_probably(tmp, Colin, Colin_pro);
        tmp = Colin_pro;
        Colin_pro.clear();
    }
    Colin_pro = tmp;
    factor<BigInteger> prob;
    for(int i = 0; i <= 36; i++)
    {
        for(int j = 0; j < i; j++)
        {
            prob += Peter_pro[i]*Colin_pro[j];
        }
    }
    //cout << prob.num << endl;
    //cout << prob.denum << endl;
    double num = prob.num.toInt();
    double denum = prob.denum.toInt();
    return num/denum;
}
