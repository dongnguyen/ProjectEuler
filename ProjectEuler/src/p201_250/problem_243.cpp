//Totient Chains
//Problem 214
//Let ? be Euler's totient function, i.e. for a natural number n, ?(n) is the number of k, 1 ? k ? n, for which gcd(k,n) = 1.
//
//By iterating ?, each positive integer generates a decreasing chain of numbers ending in 1.
//E.g. if we start with 5 the sequence 5,4,2,1 is generated.
//Here is a listing of all chains with length 4:
//
//5,4,2,1
//7,6,2,1
//8,4,2,1
//9,6,2,1
//10,4,2,1
//12,4,2,1
//14,6,2,1
//18,6,2,1
//Only two of these chains start with a prime, their sum is 12.
//
//What is the sum of all primes less than 40000000 which generate a chain of length 25?

#include "template_class.h"

extern vector<int> prime_list;

uint64_t problem_243(int n, int len)
{
    vector<int> prime;
    vector<int> Totient;
    vector<int> memo;
    memo.resize(n+1);
    prime.resize(n+1);
    Totient.resize(n+1);
    prime[0] = NO_PRIME;
    prime[1] = NO_PRIME;
    prime[2] = PRIME;
    prime_list.push_back(2);
    int limit = sqrt(n);
    for(int i = 4; i <= n; i += 2)
    {
        prime[i] = NO_PRIME;
    }
    for(int i = 1; i <= n; i++)
    {
        Totient[i] = i;
    }
    for(int j = 2; j <= n; j += 2)
    {
        Totient[j] >>= 1;
    }
    for(int i = 3; i <= limit; i++)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
            int nume = i - 1;
            for(int j = i; j <= n; j += i)
            {
                Totient[j] = Totient[j] / i * nume;
            }
            for(int j = i*i; j <= n; j += i)
            {
                prime[j] = NO_PRIME;
            }
        }
    }
    int new_limit = limit + 1;
    if((new_limit & 1) == 0)
    {
        new_limit++;
    }
    for(int i = new_limit; i <= n; i += 2)
    {
        if(prime[i] == PRIME)
        {
            prime_list.push_back(i);
            int nume = i - 1;
            for(int j = i; j <= n; j += i)
            {
                Totient[j] = Totient[j] / i * nume;
            }
        }
    }
    prime.clear();
    int size = prime_list.size();
    uint64_t sum = 0;
    for(int i = 0; i < size; i++)
    {
        vector<int> tmp;
        int start = prime_list[i];
        int next = start;
        tmp.push_back(start);
        while(1)
        {
            next = Totient[next];
            if(memo[next])
            {
                int c = memo[next] + 1;
                for(int j = tmp.size() - 1; j >= 0; j--)
                {
                    memo[tmp[j]] = c++;
                }
                break;
            }
            if(next == 1)
            {
                int c = 2;
                for(int j = tmp.size() - 1; j >= 0; j--)
                {
                    memo[tmp[j]] = c++;
                }
                break;
            }
            tmp.push_back(next);
        }
        if(memo[start] == len)
        {
            sum += start;
        }
    }
    return sum;
}
