
#include "template_class.h"

extern vector<int> prime_list;
extern set<int> set_prime_list;

extern void create_prime_filter_method(int n, bool need_set = false);
void create_prime_filter_method(vector<uint64_t> list, uint64_t start, uint64_t end);

int problem_216()
{
    vector<uint64_t> list;
    uint64_t n = 50e6;
    uint64_t step = n / 10000;
    uint64_t count = 0;
    {
        uint64_t start = 1;
        uint64_t end = step;
        start = 2 * start*start - 1;
        end = 2 * end*end - 1;
        create_prime_filter_method(list, start, end);
        count += list.size();
    }
    for (uint64_t i = 1; i < 10000; i++)
    {
        cout << i << endl;
        uint64_t start = i*step;
        uint64_t end = (i+1)*step;
        start = 2 * start*start - 1;
        end = 2 * end*end - 1;
        create_prime_filter_method(list, start, end);
        count += list.size();
    }
    cout << count << endl;
    return 0;
}