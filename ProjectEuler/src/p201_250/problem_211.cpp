//Divisor Square Sum
//Problem 211
//For a positive integer n, let ?2(n) be the sum of the squares of its divisors. For example,
//
//?2(10) = 1 + 4 + 25 + 100 = 130.
//Find the sum of all n, 0 < n < 64,000,000 such that ?2(n) is a perfect square.

#include "template_class.h"
#include "BigIntegerLibrary.hh"
#include "mpirxx.h"

extern void create_prime_filter_method(int n, bool need_set = false);
extern vector<int> prime_list;

BigInteger pow_big(int a, int b)
{
    BigInteger ret = a;
    for(int i = 1; i <= b; i++)
    {
        ret *= a;
    }
    return ret;
}

mpz_class pow_mpz(int a, int b)
{
    mpz_class ret = a;
    for(int i = 1; i < b; i++)
    {
        ret *= a;
    }
    return ret;
}

mpz_class problem_211(int n)
{
    mpz_class ret = 0;

    int MAX = 64000000;
#if 0
    long long int i, j, sum, x, *a;
    a = (long long int *) malloc(sizeof(long long int) * (MAX+1));
    for(i=1; i<MAX; i++)
    {
        for(j=i; j<MAX; j+=i)
        {
            a[j] += (i*i);
        }
    }

    sum = 0;
    for(x=1; x<MAX; x++)
    {
        j = floor(sqrt(a[x]));
        if(j*j == a[x]) sum += x;
    }
    delete []a;
    cout << sum << endl;    // 1922364685
    //return 0;
#endif
#if 0
    n = 1e6;
    cout << 1 << endl;
    for(int i = 2; i < n; i++)
        //uint64_t i = 66223;

        //cout << 1 << " " << i << " " << endl;
    {
        int si = sqrt(i);
        mpz_class tmp = 1LL;
        mpz_class t2 = i;
        tmp += t2*t2;
        //cout << i*i << endl;
        for(int j = 2; j <= si; j++)
        {
            if(i % j == 0)
            {
                int t = i / j;
                if(t != j)
                {
                    mpz_class t3 = j;
                    tmp += t3*t3;
                    t3 = t;
                    tmp += t3*t3;
                    //cout << j << " " << t << " " << endl;
                }
                else
                {
                    mpz_class t3 = j;
                    tmp += t3*t3;
                    //cout << j << endl;
                }
            }
        }
        mpz_class sqrt_tmp = sqrt(tmp);
        if((sqrt_tmp * sqrt_tmp) == tmp)
        {
            cout << i << endl;
            //cout << tmp << endl;
            //cout << sqrt_tmp << endl;
            ret += i;
        }
    }
    return ret + 1;
#endif

    n = MAX;
    create_prime_filter_method(n);

    //cout << 1 << endl;
    for(int range = 0; range < 64; range++)
    {
        cout << range << endl;
        vector<vector<int>> memo;
        memo.resize(1e6);

        int MIN = 1e6 * range;
        int MAX = MIN + 1e6;

        for(int i = 0; ; i++)
        {
            if(i >= prime_list.size())
            {
                break;
            }
            if(prime_list[i] >= MAX)
            {
                break;
            }
            int p = prime_list[i];

            int tmp_min = MIN / p * p;
            if(tmp_min < MIN)
            {
                tmp_min += p;
            }
            if(tmp_min == 0)
            {
                tmp_min = p;
            }

            for(int j = tmp_min; j < MAX; j += p)
            {
                memo[j - MIN].push_back(p);
            }
        }
        int start = MIN;
        if(MIN == 0)
        {
            start = 2;
        }
        for(int idx = start; idx < MAX; idx++)
        {
            mpz_class tmp = 1;
            //stringstream ss;
            int tidx = idx;
            for(int j = 0; j < memo[idx - MIN].size(); j++)
            {
                int tp = memo[idx - MIN][j];
                int count = 0;
                /*if(tidx % tp)
                {
                    cout << idx << " error" << endl;
                    exit(0);
                }*/
                while(tidx % tp == 0)
                {
                    count++;
                    tidx /= tp;
                }
                //ss << tp << "^" << count << " * ";
                mpz_class om = pow_mpz(tp, 2*(count + 1));
                mpz_class dm = tp;
                dm *= dm;
                tmp *= (om - 1) / (dm - 1);
            }
            /*if(tidx != 1)
            {
                cout << "error" << endl;
                exit(0);
            }*/

            mpz_class sqrt_tmp = 0;

            sqrt_tmp = sqrt(tmp);

            if(sqrt_tmp * sqrt_tmp == tmp)
            {
                //cout << idx << ": " << sqrt_tmp << " * " << sqrt_tmp << " = " << tmp << endl;
                //cout << idx << " = " << ss.str() << endl << endl;
                //cout << idx << endl;
                ret += idx;
            }
        }
    }
#if 0
    vector<vector<int>> memo;
    memo.resize(n);
    int size = prime_list.size();
    int idx = 2;
    for(int i = 0; i < size; i++)
    {
        int p = prime_list[i];
        for(int j = p; j < n; j += p)
        {
            memo[j].push_back(p);
        }
        for(; idx <= p; idx++)
        {
            mpz_class tmp = 1;
            for(int j = 0; j < memo[idx].size(); j++)
            {
                uint64_t tp = memo[idx][j];
                int count = 0;
                int tidx = idx;
                while(tidx % tp == 0)
                {
                    count++;
                    tidx /= tp;
                }
                tmp *= (pow(tp, 2*(count + 1)) - 1) / (tp*tp - 1);
            }
            mpz_class sqrt_tmp = sqrt(tmp);
            if(sqrt_tmp * sqrt_tmp == tmp)
            {
                ret += idx;
            }
            memo[idx].~vector();
        }
    }
    for(; idx < n; idx++)
    {
        mpz_class tmp = 1;
        for(int j = 0; j < memo[idx].size(); j++)
        {
            uint64_t tp = memo[idx][j];
            int count = 0;
            int tidx = idx;
            while(tidx % tp == 0)
            {
                count++;
                tidx /= tp;
            }
            tmp *= (pow(tp, 2*(count + 1)) - 1) / (tp*tp - 1);
        }
        mpz_class sqrt_tmp = sqrt(tmp);
        if(sqrt_tmp * sqrt_tmp == tmp)
        {
            ret += idx;
        }
        memo[idx].~vector();
    }
#endif
#if 0
    vector<BigInteger> memo;
    memo.resize(n);
    cout << "here" << endl;
    return ret;
    for(int i = 0; i < n; i++)
    {
        memo[i] = 1;
    }
    cout << "here" << endl;
    return ret;
    int size = prime_list.size();
    int size_1000 = size / 1000;
    for(int i = 0; i < size; i++)
    {
        if((i % size_1000) == 0)
        {
            cout << i / size_1000 << "%0" << endl;
        }
        uint64_t op = prime_list[i];
        uint64_t p = op;
        uint64_t ln_p = log(n) / log(p);
        for(uint64_t pw = 1; pw <= ln_p; pw++)
        {
            //uint64_t tmp = (pow(op, 2) - 1);
            BigInteger tmp = (pow_big(op, 2) - 1);
            for(int j = 1; p*j < n; j++)
            {
                if(j % op)
                {
                    memo[p*j] *= (pow_big(op, 2*(pw+1)) - 1) / tmp;
                }
            }
            p *= op;
        }
    }
#endif
    return ret+1;
}
