//Generalised Hamming Numbers
//Problem 204
//A Hamming number is a positive number which has no prime factor larger than 5.
//So the first few Hamming numbers are 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15.
//There are 1105 Hamming numbers not exceeding 108.
//
//We will call a positive number a generalised Hamming number of type n, if it has no prime factor larger than n.
//Hence the Hamming numbers are the generalised Hamming numbers of type 5.
//
//How many generalised Hamming numbers of type 100 are there which don't exceed 109?

#include "template_class.h"

extern vector<int> prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);
vector<double> ln_pime;

int count_204(int idx, double limit)
{
    if(idx == 0)
    {
        int ret = limit / ln_pime[0];
        return ret + 1;
    }
    int range = limit / ln_pime[idx];
    int sum = 0;
    for(int i = 0; i <= range; i++)
    {
        sum += count_204(idx - 1, limit - i*ln_pime[idx]);
    }
    return sum;
}

int problem_204(int type, int limit)
{
    create_prime_filter_method(type+1);
    vector<int> count_exponent;
    int size = prime_list.size();
    ln_pime.resize(size);
    for(int i = 0; i < size; i++)
    {
        ln_pime[i] = log(prime_list[i]);
    }
    double ln_limit = log(limit);
    return count_204(size - 1, ln_limit);

    /*count_exponent.resize(size);
    int count = 0;
    while(1)
    {
        uint64_t tmp = 1;
        for(int i = 0; i < size; i++)
        {
            tmp *= pow(prime_list[i], count_exponent[i]);
        }
        if(tmp <= limit)
        {
            count++;
        }
        else
        {
            if(count_exponent.back())
            {
                bool test_zero = true;
                for(int i = 0; i < size - 1; i++)
                {
                    if(count_exponent[i])
                    {
                        test_zero = false;
                        break;
                    }
                }
                if(test_zero)
                {
                    break;
                }
            }
            int found_idx = -1;
            for(int i = 0; i < size - 1; i++)
            {
                if(count_exponent[i])
                {
                    found_idx = i;
                    break;
                }
            }
            if(found_idx != -1)
            {
                count_exponent[found_idx] = 0;
                count_exponent[found_idx+1]++;
                continue;
            }
            else
            {
                break;
            }
        }
        count_exponent[0]++;
    }
    return count;*/
}
