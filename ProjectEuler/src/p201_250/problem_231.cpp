//The prime factorisation of binomial coefficients
//Problem 231
//The binomial coefficient 10C3 = 120.
//120 = 23 � 3 � 5 = 2 � 2 � 2 � 3 � 5, and 2 + 2 + 2 + 3 + 5 = 14.
//So the sum of the terms in the prime factorisation of 10C3 is 14.
//
//Find the sum of the terms in the prime factorisation of 20000000C15000000.

#include "template_class.h"

extern vector<int> prime_list;
extern set<int> set_prime_list;
extern void create_prime_filter_method(int n, bool need_set = false);

uint64_t problem_231(int k, int n)
{
    /*k = 3;
    n = 10;*/
    create_prime_filter_method(n);
    //vector<vector<int>> memo;
    /*vector<int> memo;
    int size = n - k;
    memo.resize(size);

    for (int i = 0; i < size; i++)
    {
        memo[i] = i + k;
    }*/

    uint64_t sum = 0;
    for (int i = 0; i < prime_list.size(); i++)
    {
        int p = prime_list[i];
        int start = (k / p) * p;
        if (start <= k)
        {
            start += p;
        }
        for (int j = start; j <= n; j += p)
        {
            int tmp = j;
            while (tmp % p == 0)
            {
                tmp /= p;
                sum += p;
            }
            //memo[j - k].push_back(p);
        }
        for (int j = p; j <= n-k; j += p)
        {
            int tmp = j;
            while (tmp % p == 0)
            {
                tmp /= p;
                sum -= p;
            }
            //memo[j - k].push_back(p);
        }
    }
    return sum;
}