#include "template_class.h"
#include "BigIntegerLibrary.hh"
#include "InfInt.h"
#include "mpirxx.h"
#ifdef _MSC_VER
#if _MSC_VER < 1900
#include "ParallelPrimeSieve.h"
#else
#include "primesieve.hpp"
using namespace primesieve;
#endif
#else
#include "primesieve.hpp"
using namespace primesieve;
#endif

#ifdef WIN32
#pragma warning(disable:4018)
#endif

using std::set;
using std::min;
using std::max;

extern vector<int> triangle_list;
extern vector<uint64_t> fact_list;
extern vector<BigInteger> fact_bigint_list;
extern vector<int> abundant_list;
extern vector<int> sum_two_abundant_list;
extern vector<int> prime_list;
extern set<int> set_prime_list;

extern void create_prime_filter_method(int n, bool need_set = false);
void create_prime_filter_method(vector<uint64_t> list, uint64_t start, uint64_t end);

extern int triangle_number(int n);
extern uint64_t factorial(int n);
extern int find_num_divisor(int x);
extern void sum_num_string(uint16_t *a, uint16_t *b, uint16_t *s, int size);
extern uint64_t count_collatz_sequence(uint64_t x);
extern void compute_2_pow_n(int n, vector<char>& result);
extern double golden_ratio(double esp);
extern int sum_of_divisors(int n);
extern int count_way(int val, int* coin, int size);
extern bool check_palindromic(int x);
extern bool check_palindromic(vector<int> digits);
extern int make_palindromic(int x);
extern int gcd(int a, int b);
extern uint64_t gcd(uint64_t a, uint64_t b);
extern BigInteger gcd(BigInteger a, BigInteger b);
extern bool prime_test(int x);
extern bool prime_test2(int x);
extern bool prime_test3(int x);
extern bool prime_test3(uint64_t x);
extern bool abundant_test(int x);
extern void create_abundant_list();
extern void create_sum_two_abundant_list();
extern int first_num(vector<int>& list, int& order);
extern bool test_problem_30(vector<int>& list, int val);
extern int BitReverse32(int x);
extern int32_t FoundNumLeadingZero(int32_t x);
extern void create_prime_list(int x);
extern bool check_triangle_number(int num);
extern bool check_pentagonal_number(uint64_t num);
extern bool check_hexagonal_number(uint64_t num);
extern int32_t pentagonal_number(int32_t n);
extern int64_t pentagonal_number(int64_t n);
extern uint64_t pentagonal_number(uint64_t n);
extern uint64_t triangle_number(uint64_t n);
extern uint64_t hexagonal_number(uint64_t n);
extern uint64_t square_number(uint64_t n);
extern uint64_t heptagonal_number(uint64_t n);
extern uint64_t octagonal_number(uint64_t n);
extern bool check_square(uint32_t n);
extern void sum_num_string(vector<int>& a, vector<int>& b, vector<int>& s);
extern bool check_in_list(int n, vector<int>& list, int idx = 0);
extern void combination(int offset, int k, vector<int>& set, vector<int>& com, vector<vector<int>>& outset);
extern double ln_factorial(int n);
extern void pow_a_b(int a, int b, vector<int>& result);
extern BigInteger pow_a_b(int a, int b);
extern InfInt pow_a_b_infi(int a, int b);
extern mpz_class pow_a_b_mpz(int a, int b);
extern int64_t pow_a_b(int64_t a, int64_t b);
extern void mul_two_vector(vector<int>& a, vector<int>& b, vector<int>& result);
extern int count_bit_1(int n);

int triangle_number(int n)
{
	if (triangle_list.size() == 0)
	{
		triangle_list.push_back(0);
	}

	for (int i = triangle_list.size(); i <= n; i++)
	{
		triangle_list.push_back(triangle_list.back() + i);
	}

	return triangle_list[n];
}

uint64_t factorial(int n)
{
	if (fact_list.size() == 0)
	{
		fact_list.push_back(1);
		fact_list.push_back(1);
	}

	if (n < fact_list.size())
	{
		return fact_list[n];
	}

	for (int i = fact_list.size(); i <= n; i++)
	{
		fact_list.push_back(fact_list.back() * i);
	}

	return fact_list.back();
}

BigInteger factorial_bigint(int n)
{
	if (fact_bigint_list.size() == 0)
	{
		fact_bigint_list.push_back(1);
		fact_bigint_list.push_back(1);
	}

	if (n < fact_bigint_list.size())
	{
		return fact_bigint_list[n];
	}

	for (int i = fact_bigint_list.size(); i <= n; i++)
	{
		fact_bigint_list.push_back(fact_bigint_list.back() * i);
	}

	return fact_bigint_list.back();
}

int find_num_divisor(int x)
{
	if (x == 1)
	{
		return 1;
	}

	if (x == 2)
	{
		return 2;
	}

	if (x > prime_list.back() + 2)
	{
		for (int i = prime_list.back() + 2; i < x; i += 2)
		{
			prime_test(i);
		}
	}

	//int count_type = 0;
	vector<int> num_each_type;
	for (int i = 0; i < prime_list.size(); i++)
	{
		int tmp = 0;
		while (x % prime_list[i] == 0)
		{
			x /= prime_list[i];
			tmp++;
		}
		if (tmp)
		{
			//count_type++;
			num_each_type.push_back(tmp);
		}
	}

	/*int num = 0;
	for(int i = 1; i < count_type; i++)
	{
		num += factorial(count_type) /
			   factorial(count_type - i) /
			   factorial(i);
	}

	for(int i = 0; i < num_each_type.size(); i++)
	{
		num *= num_each_type[i];
	}*/
	int num = 1;
	for (int i = 0; i < num_each_type.size(); i++)
	{
		num *= (num_each_type[i] + 1);
	}

	return num;
}

void sum_num_string(uint16_t *a, uint16_t *b, uint16_t *s, int size)
{
	uint16_t remainder = 0;
	for (int i = size - 1; i > 0; i--)
	{
		uint16_t tmp = a[i] + b[i] + remainder;
		remainder = tmp / 10;
		s[i] = tmp % 10;
	}
	s[0] = a[0] + b[0] + remainder;
}

void sum_num_string(vector<int>& a, vector<int>& b, vector<int>& s)
{
	int remainder = 0;
	int size_min = min(a.size(), b.size());
	int size_max = max(a.size(), b.size());
	vector<int>& ref = a.size() > b.size() ? a : b;
	if (s.size() == 0)
	{
		s.resize(max(a.size(), b.size()) + 1);
	}
	int i = 0;
	for (; i < size_min; i++)
	{
		int tmp = a[i] + b[i] + remainder;
		remainder = tmp / 10;
		s[i] = (tmp % 10);
	}
	for (; i < size_max; i++)
	{
		int tmp = ref[i] + remainder;
		remainder = tmp / 10;
		s[i] = (tmp % 10);
	}
	/*if(remainder)
	{
		s[i] = (remainder);
	}*/
}

uint64_t count_collatz_sequence(uint64_t x)
{
	if (x == 1)
	{
		return 1;
	}
	uint64_t count = 1;
	while (x != 1)
	{
		if (x & 1)
		{
			x = 3 * x + 1;
		}
		else
		{
			x >>= 1;
		}
		count++;
	}
	return count;
}

void compute_2_pow_n(int n, vector<char>& result)
{
	if (n == 0)
	{
		result.resize(1);
		result[0] = 1;
		return;
	}

	if (result.size() == 0)
	{
		result.push_back(1);
	}

	for (int i = 0; i < n; i++)
	{
		mul_string_with_n(2, result);
	}
}

double golden_ratio(double esp)
{
	double n = 1;
	double n1 = 1;
	double n2 = n + n1;
	double pre_ratio = n1 / n;
	double ratio = n2 / n1;
	while (abs(ratio - pre_ratio) > esp)
	{
		n = n1;
		n1 = n2;
		n2 = n + n1;
		pre_ratio = n1 / n;
		ratio = n2 / n1;
	}
	return ratio;
}

int sum_of_divisors(int n)
{
	if (n <= 1)
	{
		return 0;
	}

	if (n == 2)
	{
		return 1;
	}

	if (n > prime_list.back())
	{
		for (int i = prime_list.back() + 2; i <= n; i += 2)
		{
			prime_test(i);
		}
	}

	//int count_type = 0;
	vector<int> num_each_prime;
	vector<int> divisor_prime;
	int tmp_n = n;
	for (int i = 0; i < prime_list.size(); i++)
	{
		if (tmp_n == 1)
		{
			break;
		}
		int tmp = 0;
		while (tmp_n % prime_list[i] == 0)
		{
			tmp_n /= prime_list[i];
			tmp++;
		}
		if (tmp)
		{
			//count_type++;
			num_each_prime.push_back(tmp);
			divisor_prime.push_back(prime_list[i]);
		}
	}
	int sum = 0;
	vector<int> iterator;
	iterator.resize(num_each_prime.size());
	iterator.back() = 1;
	while (1)
	{
		int p = 1;
		for (int i = 0; i < num_each_prime.size(); i++)
		{
			for (int j = 0; j < iterator[i]; j++)
			{
				p *= divisor_prime[i];
			}
		}
		if (p < n)
		{
			sum += p;
		}

		iterator.back()++;

		for (int i = iterator.size() - 1; i > 0; i--)
		{
			if (iterator[i] > num_each_prime[i])
			{
				iterator[i] = 0;
				iterator[i - 1]++;
			}
		}
		// break condition
		if (iterator[0] == num_each_prime[0] + 1)
		{
			break;
		}
	}

	return sum + 1;
}

int count_way(int val, int* coin, int size)
{
	if (val == 0)
	{
		return 0;
	}

	if (size == 1)
	{
		if (val % coin[0] == 0)
		{
			return val / coin[0];
		}
		else
		{
			return 0;
		}
	}

	// Loop all coins
	int big_sum = 0;
	for (int i = 0; i < size - 1; i++)
	{
		for (int k = i + 1; k < size; k++)
		{
			int *new_set_coin = new int[size - k];
			for (int j = 0; j < size - k; j++)
			{
				new_set_coin[j] = coin[j + k];
			}
			for (int div = 0; div <= val / coin[i]; div++)
			{
				big_sum += count_way(val - div*coin[i], new_set_coin, size - k);
			}
			delete[]new_set_coin;
		}
	}
	return big_sum;
}

bool check_palindromic(int x)
{
	/*if(x < 10)
	{
		return true;
	}
	if(x < 100)
	{
		if((x / 10) == (x % 10))
		{
			return true;
		}
		return false;
	}
	if(x < 1e3)
	{
		if((x / 100) == (x % 10))
		{
			return true;
		}
		return false;
	}
	if(x < 1e4)
	{
		int d[4];
		for(int i = 0; i < 4; i++)
		{
			d[i] = x % 10;
			x /= 10;
		}
		if(d[3] == d[0] && d[2] == d[1])
		{
			return true;
		}
		return false;
	}
	if(x < 1e5)
	{
		int d[5];
		for(int i = 0; i < 5; i++)
		{
			d[i] = x % 10;
			x /= 10;
		}
		if(d[4] == d[0] && d[3] == d[1])
		{
			return true;
		}
		return false;
	}
	int d[6];
	for(int i = 0; i < 6; i++)
	{
		d[i] = x % 10;
		x /= 10;
	}
	if(d[5] == d[0] && d[4] == d[1] && d[2] == d[3])
	{
		return true;
	}*/
	if (x < 10)
	{
		return true;
	}
	vector<int> digits;
	while (x)
	{
		digits.push_back(x % 10);
		x /= 10;
	}
	int n = digits.size();
	for (int i = 0; i < n / 2; i++)
	{
		if (digits[i] != digits[n - i - 1])
		{
			return false;
		}
	}
	return true;
}

int make_palindromic(int x)
{
	int d[3];
	int tmp = x;
	for (int i = 0; i < 3; i++)
	{
		d[i] = tmp % 10;
		tmp /= 10;
	}
	return x * 1000 + d[0] * 100 + d[1] * 10 + d[2];
}

int gcd(int a, int b)
{
	if (a > b)
	{
		int tmp = a;
		a = b;
		b = tmp;
	}
	if (b % a == 0)
	{
		return a;
	}
	return gcd(b % a, a);
}

uint64_t gcd(uint64_t a, uint64_t b)
{
	if (a == 0 || b == 0)
	{
		return 1;
	}
	if (a > b)
	{
		uint64_t tmp = a;
		a = b;
		b = tmp;
	}
	if (b % a == 0)
	{
		return a;
	}
	return gcd(b % a, a);
}

BigInteger gcd(BigInteger a, BigInteger b)
{
	if (a == 0 || b == 0)
	{
		return 1;
	}
	if (a > b)
	{
		BigInteger tmp = a;
		a = b;
		b = tmp;
	}
	if (b % a == 0)
	{
		return a;
	}
	return gcd(b % a, a);
}

bool prime_test(int x)
{
	if (x < 2)
	{
		return false;
	}
	if (x <= prime_list.back())
	{
		for (int i = 0; i < prime_list.size(); i++)
		{
			if (x == prime_list[i])
			{
				return true;
			}
			else if (x < prime_list[i])
			{
				return false;
			}
		}
		return false;
	}
	float sqrt_x = sqrt((float)x);
	for (unsigned int i = 1; i < prime_list.size(); i++)
	{
		if (prime_list[i] > sqrt_x)
		{
			break;
		}

		if (x % prime_list[i] == 0)
		{
			return false;
		}
	}
	prime_list.push_back(x);
	return true;
}

bool prime_test3(int x)
{
	if (x < 2)
	{
		return false;
	}
	if (x <= prime_list.back())
	{
		for (int i = 0; i < prime_list.size(); i++)
		{
			if (x == prime_list[i])
			{
				return true;
			}
			else if (x < prime_list[i])
			{
				return false;
			}
		}
		return false;
	}
	float sqrt_x = sqrt((float)x);
	for (unsigned int i = 0; i < prime_list.size(); i++)
	{
		if (prime_list[i] > sqrt_x)
		{
			break;
		}

		if (x % prime_list[i] == 0)
		{
			return false;
		}
	}
	return true;
}

bool prime_test3(uint64_t x)
{
	if (x < 2)
	{
		return false;
	}
	if (x <= prime_list.back())
	{
		/*for(int i = 0; i < prime_list.size(); i++)
		{
			if(x == prime_list[i])
			{
				return true;
			}
			else if(x < prime_list[i])
			{
				return false;
			}
		}*/
		if (set_prime_list.find(x) == set_prime_list.end())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	uint32_t sqrt_x = sqrt(x);
	for (unsigned int i = 0; i < prime_list.size(); i++)
	{
		if (prime_list[i] > sqrt_x)
		{
			break;
		}

		if (x % prime_list[i] == 0)
		{
			return false;
		}
	}
	return true;
}

bool prime_test2(int x)
{
	int sqrt_x = (int)sqrt((float)x);
	for (unsigned int i = 0; i < prime_list.size(); i++)
	{
		if (prime_list[i] > sqrt_x)
		{
			break;
		}

		if (x % prime_list[i] == 0)
		{
			return false;
		}
	}
	return true;
}

void create_prime_list(int x)
{
	for (int i = 5; i < x; i += 2)
	{
		if (prime_test2(i))
		{
			prime_list.push_back(i);
			set_prime_list.insert(i);
		}
	}
}

bool abundant_test(int x)
{
	if (sum_of_divisors(x) > x)
	{
		return true;
	}
	return false;
}

void create_abundant_list()
{
	for (int i = 0; i <= 28123; i++)
	{
		if (abundant_test(i))
		{
			abundant_list.push_back(i);
		}
	}
}

void create_sum_two_abundant_list()
{
	create_abundant_list();
	sum_two_abundant_list.resize(28123 + 1);
	for (int i = 0; i < abundant_list.size(); i++)
	{
		for (int j = i; j < abundant_list.size(); j++)
		{
			if (abundant_list[i] + abundant_list[j] <= 28123)
			{
				sum_two_abundant_list[abundant_list[i] + abundant_list[j]] = 1;
			}
		}
	}
}

int first_num(vector<int>& list, int& order)
{
	int64_t total = factorial(list.size());
	if (order >= total)
	{
		return -1;
	}
	int64_t remains = total / list.size();
	int l_order = order / remains;
	order = order % remains;
	int ret = list[l_order];
	for (int i = l_order; i < list.size() - 1; i++)
	{
		list[i] = list[i + 1];
	}
	list.resize(list.size() - 1);
	return ret;
}

bool test_problem_30(vector<int>& list, int val)
{
	if (val <= 0)
	{
		return false;
	}
	vector<int> tmp;
	while (val != 0)
	{
		tmp.push_back(val % 10);
		val /= 10;
	}
	if (list.size() != tmp.size())
	{
		return false;
	}
	sort(list.begin(), list.end());
	sort(tmp.begin(), tmp.end());
	for (int i = 0; i < list.size(); i++)
	{
		if (list[i] != tmp[i])
		{
			return false;
		}
	}
	return true;
}

static const unsigned char BitReverseTable256[] =
{
	0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
	0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
	0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
	0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
	0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
	0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
	0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
	0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
	0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
	0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
	0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
	0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
	0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
	0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
	0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
	0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};

int BitReverse32(int x)
{
	return (BitReverseTable256[x & 0xff] << 24) |
		(BitReverseTable256[(x >> 8) & 0xff] << 16) |
		(BitReverseTable256[(x >> 16) & 0xff] << 8) |
		(BitReverseTable256[(x >> 24) & 0xff]);
}

int32_t FoundNumLeadingZero(int32_t x)
{
	int32_t mask = -1;
	int count = 32;
	while (x & mask)
	{
		count--;
		mask <<= 1;
	}
	return count;
}

bool check_triangle_number(int num)
{
	int delta = 1 + 8 * num;
	int sqrt_delta = (int)sqrt((float)delta);
	if (delta != sqrt_delta*sqrt_delta)
	{
		return false;
	}
	if (sqrt_delta & 1)
	{
		return true;
	}
	return false;
}

bool check_pentagonal_number(uint64_t num)
{
	uint64_t delta = 1 + 24 * num;
	uint64_t sqrt_delta = (uint64_t)sqrt((double)delta);
	if (delta != sqrt_delta*sqrt_delta)
	{
		return false;
	}
	if (sqrt_delta % 6 == 5)
	{
		return true;
	}
	return false;
}

bool check_hexagonal_number(uint64_t num)
{
	uint64_t delta = 1 + 8 * num;
	uint64_t sqrt_delta = (uint64_t)sqrt((double)delta);
	if (delta != sqrt_delta*sqrt_delta)
	{
		return false;
	}
	if (sqrt_delta % 4 == 3)
	{
		return true;
	}
	return false;
}

uint64_t pentagonal_number(uint64_t n)
{
	return (3 * n - 1)*n / 2;
}

int64_t pentagonal_number(int64_t n)
{
	return (3 * n - 1)*n / 2;
}

int32_t pentagonal_number(int32_t n)
{
	return (3 * n - 1)*n / 2;
}

uint64_t triangle_number(uint64_t n)
{
	return (n + 1)*n / 2;
}

uint64_t hexagonal_number(uint64_t n)
{
	return (2 * n - 1)*n;
}

bool check_square(uint32_t n)
{
	uint32_t sqrt_n = (uint32_t)sqrt((float)n);
	if (n != sqrt_n*sqrt_n)
	{
		return false;
	}
	return true;
}

bool check_in_list(int n, vector<int>& list, int idx)
{
	for (int i = idx; i < list.size(); i++)
	{
		if (n == list[i])
		{
			return true;
		}
	}
	return false;
}

void combination(int offset, int k, vector<int>& set, vector<int>& com, vector<vector<int>>& outset)
{
	if (k == 0)
	{
		outset.push_back(com);
		return;
	}
	for (int i = offset; i <= set.size() - k; ++i)
	{
		com.push_back(set[i]);
		combination(i + 1, k - 1, set, com, outset);
		com.pop_back();
	}
}

double ln_factorial(int n)
{
	double ret = 0;
	if (n < 1)
	{
		return 0;
	}
	for (double i = 1; i <= n; i += 1)
	{
		ret += log(i);
	}
	return ret;
}

void pow_a_b(int a, int b, vector<int>& result)
{
	if (a < 1)
	{
		result.resize(1);
		result[0] = 0;
		return;
	}
	if (b == 0)
	{
		result.resize(1);
		result[0] = 1;
		return;
	}
	result.resize(1);
	result[0] = a;
	for (int i = 1; i < b; i++)
	{
		mul_string_with_n(a, result);
	}
}

BigInteger pow_a_b(int a, int b)
{
	if (a == 0)
	{
		return 0;
	}
	if (b == 0)
	{
		return 1;
	}
	BigInteger p = a;
	for (int i = 1; i < b; i++)
	{
		p *= a;
	}
	return p;
}

InfInt pow_a_b_infi(int a, int b)
{
	if (a == 0)
	{
		return 0;
	}
	if (b == 0)
	{
		return 1;
	}
	InfInt p = a;
	for (int i = 1; i < b; i++)
	{
		p *= a;
	}
	return p;
}

mpz_class pow_a_b_mpz(int a, int b)
{
	if (a == 0)
	{
		return 0;
	}
	if (b == 0)
	{
		return 1;
	}
	mpz_class p = a;
	for (int i = 1; i < b; i++)
	{
		p *= a;
	}
	return p;
}

int64_t pow_a_b(int64_t a, int64_t b)
{
	if (a == 0)
	{
		return 0;
	}
	if (b == 0)
	{
		return 1;
	}
	int64_t p = a;
	for (int i = 1; i < b; i++)
	{
		p *= a;
	}
	return p;
}

bool check_palindromic(vector<int> digits)
{
	for (int i = 0; i < digits.size() / 2; i++)
	{
		if (digits[i] != digits[digits.size() - i - 1])
		{
			return false;
		}
	}
	return true;
}

void mul_two_vector(vector<int>& a, vector<int>& b, vector<int>& result)
{
	int size_a = a.size();
	int size_b = b.size();
	result.resize(size_a + size_b);
	for (int i = 0; i < size_a; i++)
	{
		for (int j = 0; j < size_b; j++)
		{
			result[i + j] += a[i] * b[j];
		}
	}
	int remain = result[0] / 10;
	result[0] %= 10;
	int idx = 1;
	while (remain)
	{
		result[idx] += remain;
		remain = result[idx] / 10;
		if (remain)
		{
			result[idx] %= 10;
			idx++;
		}
	}
}

uint64_t square_number(uint64_t n)
{
	return n*n;
}

uint64_t heptagonal_number(uint64_t n)
{
	return n*(5 * n - 3) / 2;
}

uint64_t octagonal_number(uint64_t n)
{
	return n*(3 * n - 2);
}

int count_bit_1(int n)
{
	int mask = 1;
	int count = 0;
	for (int i = 0; i < sizeof(int) * 8; i++)
	{
		if (n & mask)
		{
			count++;
		}
		mask <<= 1;
	}
	return count;
}

uint8_t count_bit_1(uint8_t n)
{
	uint8_t mask = 1;
	uint8_t count = 0;
	for (uint8_t i = 0; i < 8; i++)
	{
		if (n & mask)
		{
			count++;
		}
		mask <<= 1;
	}
	return count;
}

#pragma warning(disable:4996)
#define PRIME    0
#define NO_PRIME 1

void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end)
{
#ifdef _MSC_VER
#if _MSC_VER < 1900
	ParallelPrimeSieve pp;
	pp.setNumThreads(4);
	pp.generatePrimes(start, end, list);
#else
	set_num_threads(4);
	generate_primes(start, end, list);
#endif
#else
	set_num_threads(4);
	generate_primes(start, end, list);
#endif
}

void create_prime_filter_method(vector<uint64_t> list, uint64_t start, uint64_t end)
{
#ifdef _MSC_VER
#if _MSC_VER < 1900
	ParallelPrimeSieve pp;
	pp.setNumThreads(4);
	pp.generatePrimes(start, end, &list);
#else
	set_num_threads(4);
	generate_primes(start, end, &list);
#endif
#else
	set_num_threads(4);
	generate_primes(start, end, &list);
#endif
}

void create_prime_filter_method(int n, bool need_set)
{
#if 1
#ifdef _MSC_VER
#if _MSC_VER < 1900
	ParallelPrimeSieve pp;
	pp.setNumThreads(4);
	pp.generatePrimes(uint64_t(0), uint64_t(n), &prime_list);
#else
	set_num_threads(4);
	generate_primes(uint64_t(0), uint64_t(n), &prime_list);
#endif
#else
	set_num_threads(4);
	generate_primes(uint64_t(0), uint64_t(n), &prime_list);
#endif

	if (need_set)
	{
		//set_prime_list.insert(prime_list.data(), prime_list.data() + prime_list.size());
		set_prime_list.insert(prime_list.begin(), prime_list.end());
	}
#else
	vector<int> all;
	all.resize(n);
	all[0] = NO_PRIME;
	all[1] = NO_PRIME;
	all[2] = PRIME;
	int limit = sqrt(n);
	for (int i = 4; i < n; i += 2)
	{
		all[i] = NO_PRIME;
	}
	for (int i = 3; i <= limit; i += 2)
	{
		if (all[i] == PRIME)
		{
			for (int j = i*i; j < n; j += i)
			{
				all[j] = NO_PRIME;
			}
		}
	}
	prime_list.push_back(2);
	for (int i = 3; i < n; i += 2)
	{
		if (all[i] == PRIME)
		{
			prime_list.push_back(i);
		}
	}
	if (need_set)
	{
		set_prime_list.insert(prime_list.data(), prime_list.data() + prime_list.size());
	}
	//FILE* fp = fopen("prime.txt", "w");
	/*for(int i = 0; i < prime_list.size(); i++)
	{
	if(i % 8 == 0)
	{
	fprintf(fp, "\n");
	}
	fprintf(fp, "%d, ", prime_list[i]);
	}*/
	//fprintf(fp, "%d\n", prime_list.size());
	//fclose(fp);
#endif
}

class uint64x2_t
{
public:
	uint64_t hi;
	uint64_t lo;
	uint64x2_t()
	{
		hi = 0;
		lo = 0;
	}
	uint64x2_t(uint64_t h, uint64_t l)
	{
		hi = h;
		lo = l;
	}
};

uint64x2_t plus128(uint64_t a, uint64_t b)
{
	uint64_t lo = (a & 0xffffffff) + (b & 0xffffffff);
	uint64_t hi = (a >> 32) + (b >> 32) + (lo >> 32);
	uint64x2_t ret(hi >> 32, (hi << 32) | (lo & 0xffffffff));
	return ret;
}

class uint128_t : public uint64x2_t
{
private:
	uint128_t(const uint64x2_t& ref)
	{
		hi = ref.hi;
		lo = ref.lo;
	}
	uint128_t operator+ (const uint128_t& ref)
	{
		uint64x2_t n_hi = plus128(hi, ref.hi);
		uint64x2_t n_lo = plus128(lo, ref.lo);
		n_lo.hi += n_hi.lo;
		return n_lo;
	}
};
