//Prime generating integers
//Problem 357
//Consider the divisors of 30: 1, 2, 3, 5, 6, 10, 15, 30.
//It can be seen that for every divisor d of 30, d + 30 / d is prime.
//
//Find the sum of all positive integers n not exceeding 100 000 000
//such that for every divisor d of n, d + n / d is prime.

#include "template_class.h"
#include "BigIntegerLibrary.hh"

extern void create_prime_filter_method(int n, bool need_set = false);
extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);

vector<uint64_t> HarshadNumber;
vector<uint64_t> strongHarshadNumber;
static vector<uint64_t> prime_list;
extern set<int> set_prime_list;

static bool prime_test(uint64_t x)
{
	if (x < 2)
	{
		return false;
	}
	uint32_t sqrt_x = sqrt(x);
	for (unsigned int i = 0; i < prime_list.size(); i++)
	{
		if (prime_list[i] > sqrt_x)
		{
			break;
		}

		if (x % prime_list[i] == 0)
		{
			return false;
		}
	}
	return true;
}

uint64_t sum_digits(uint64_t n)
{
	uint64_t sum = 0;
	while (n)
	{
		sum += n % 10;
		n /= 10;
	}
	return sum;
}

bool testHarshadNumber(uint64_t n)
{
	if (sum_digits(n) == 0)
	{
		return false;
	}
	bool ret = false;
	if (n % sum_digits(n) == 0)
	{
		ret = true;
		if (prime_test(n / sum_digits(n)))
		{
			strongHarshadNumber.push_back(n);
		}
	}
	return ret;
}

void genrateStrongRightTruncHarshadNumber(uint64_t upper)
{
	uint64_t start = 0;
	uint64_t end = 10;
	for (uint64_t i = start; i < end; i++)
	{
		HarshadNumber.push_back(i);
	}
	while (end <= upper)
	{
		int size = HarshadNumber.size();
		for (int i = 0; i < size; i++)
		{
			if (HarshadNumber[i] >= start && HarshadNumber[i] <= end)
			{
				for (uint64_t newDigit = 0; newDigit < 10; newDigit++)
				{
					uint64_t test = HarshadNumber[i] * 10 + newDigit;
					if (testHarshadNumber(test))
					{
						HarshadNumber.push_back(test);
					}
				}
			}
		}
		start = end;
		end *= 10;
	}
}

uint64_t problem_387()
{
	create_prime_filter_method(&prime_list, 0, 1e7);
	genrateStrongRightTruncHarshadNumber(1e14 / 10);
	uint64_t sum = 0;
	for (int i = 0; i < strongHarshadNumber.size(); i++)
	{
		for (int j = 0; j < 10; j++)
		{
			uint64_t test = strongHarshadNumber[i] * 10 + j;
			if (prime_test(test) && test < 1e14)
			{
				//cout << test << endl;
				sum += test;
			}
		}
	}
	return sum;
}
