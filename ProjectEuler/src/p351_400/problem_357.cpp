//Prime generating integers
//Problem 357
//Consider the divisors of 30: 1, 2, 3, 5, 6, 10, 15, 30.
//It can be seen that for every divisor d of 30, d + 30 / d is prime.
//
//Find the sum of all positive integers n not exceeding 100 000 000
//such that for every divisor d of n, d + n / d is prime.

#include "template_class.h"
#include "BigIntegerLibrary.hh"

extern void create_prime_filter_method(int n, bool need_set = false);

extern vector<int> prime_list;
extern set<int> set_prime_list;
extern void combination(int offset, int k, vector<int>& set, vector<int>& com, vector < vector < int >> &outset);

BigInteger problem_357(int n)
{
    create_prime_filter_method(n+2, true);
    vector<int> candidate;
    int size = prime_list.size();
    for (int i = 1; i < size; i++)
    {
        int tmp = prime_list[i] - 3;
        if (tmp % 4 == 0)
        {
            candidate.push_back(tmp + 2);
        }
    }
    size = candidate.size();
    //int count = 0;

    BigInteger ret = 0;
    for (int i = 0; i < size; i++)
    {
        int limit = sqrt(candidate[i]);
        int candidate_test = candidate[i];
        //stringstream ss;
        //ss << candidate_test << " = 2 * ";
        bool test = true;
        if (set_prime_list.find(2 + candidate_test / 2) == set_prime_list.end())
        {
            continue;
        }
        vector<int> set;
        set.push_back(2);
        for (int j = 1; prime_list[j] <= limit; j++)
        {
            if ((candidate_test % prime_list[j]) == 0)
            {
                //ss << prime_list[j] << " * ";
                if ((candidate_test % (prime_list[j] * prime_list[j])) == 0)
                {
                    test = false;
                    goto EXIT;
                }
                int p_can = prime_list[j] + candidate_test / prime_list[j];
                if (set_prime_list.find(p_can) == set_prime_list.end())
                {
                    test = false;
                    goto EXIT;
                }
                set.push_back(prime_list[j]);
            }
        }
        for (int k = 2; k <= set.size(); k++)
        {
            vector<int> com;
            vector<vector<int>> outset;
            combination(0, k, set, com, outset);
            for (int m = 0; m < outset.size(); m++)
            {
                int can = 1;
                for (int n = 0; n < k; n++)
                {
                    can *= outset[m][n];
                }
                if (can <= limit)
                {
                    if (set_prime_list.find(can + candidate_test / can) == set_prime_list.end())
                    {
                        test = false;
                        goto EXIT;
                    }
                }
            }
        }
EXIT:
        if (test)
        {
            //ss << endl;
            //cout << ss.str();
            ret += candidate_test;
            //cout << candidate_test << endl;
            //count++;
            /*if (count == 100)
            {
                break;
            }*/
        }
    }

    return ret+1;
}