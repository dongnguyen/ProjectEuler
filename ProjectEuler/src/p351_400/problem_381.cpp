// (prime-k) factorial
// Problem 381
// For a prime p let S(p) = (∑(p-k)!) mod(p) for 1 ≤ k ≤ 5.

// For example, if p=7,
// (7-1)! + (7-2)! + (7-3)! + (7-4)! + (7-5)! = 6! + 5! + 4! + 3! + 2! = 720+120+24+6+2 = 872.
// As 872 mod(7) = 4, S(7) = 4.

// It can be verified that ∑S(p) = 480 for 5 ≤ p < 100.

// Find ∑S(p) for 5 ≤ p < 108.

#include "template_class.h"
#include "BigIntegerLibrary.hh"

extern void create_prime_filter_method(int n, bool need_set = false);
extern void create_prime_filter_method(vector<uint64_t>* list, uint64_t start, uint64_t end);

static vector<uint64_t> prime_list;
#define N 1e8

//inverse_modulo
uint64_t calc381(int64_t p)
{
	if(p < 5)
	{
		return 0;
	}
	int64_t p1 = p - 1;
	int64_t p_2 = inverse_modulo(p-1, p);
	int64_t p_3 = inverse_modulo(p-2, p);
	int64_t p_4 = inverse_modulo(p-3, p);
	int64_t p_5 = inverse_modulo(p-4, p);
	int64_t p2 = ((p - 1)*p_2) % p;
	int64_t p3 = (p2 * p_3)% p;
	int64_t p4 = (p3 * p_4)% p;
	int64_t p5 = (p4 * p_5)% p;
	return (p1+p2+p3+p4+p5)%p;
}

uint64_t problem_381()
{
	create_prime_filter_method(&prime_list, 0, N);
	uint64_t sum = 0;
	for(int i = 0; i < prime_list.size(); i++)
	{
		sum += calc381(prime_list[i]);
	}
	return sum;
}
