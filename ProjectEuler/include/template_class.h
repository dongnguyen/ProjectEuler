#ifndef TEMPLATE_CLASS_H
#define TEMPLATE_CLASS_H

#include <vector>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdint>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <forward_list>
#include <set>
#include <queue>
#include <sstream>
#include <limits>
#include <map>

#ifdef _LINUX
#include <limits.h>
#endif

#ifdef WIN32
#include "Profiler.h"
#pragma warning(disable:4996) // fopen unsafe
//#pragma warning(disable:4244) // conversion loss data
//#pragma warning(disable:4800)
#pragma warning(disable:4018) // < signed & unsigned mismatch
#pragma warning(disable:4146) // minus for unsigned
#endif

#include "BigIntegerLibrary.hh"

#define PRIME 0
#define NO_PRIME 1

using std::vector;
using std::set;
using std::string;
using std::swap;
using std::cout;
using std::endl;
using std::ifstream;
using std::getline;
using std::stringstream;
using std::queue;
using std::to_string;
using std::min;
using std::max;
using std::numeric_limits;
using std::map;

template <typename T>
T gcd(T a, T b)
{
	if (a == 0)
	{
		return 0;
	}
	if (b == 0)
	{
		return 0;
	}
	if (numeric_limits<T>::is_signed && a < 0)
	{
		a = -a;
	}
	if (numeric_limits<T>::is_signed && b < 0)
	{
		b = -b;
	}
	if (a > b)
	{
		T tmp = a;
		a = b;
		b = tmp;
	}
	if (b % a == 0)
	{
		return a;
	}
	return gcd(b % a, a);
}

template <typename T>
struct factor
{
	T num;
	T denum;
	bool is_valid;
	factor()
	{
		num = 0;
		denum = 1;
		is_valid = true;
	}
	factor(T a, T b)
	{
		Init(a, b);
	}
	void Init(T a, T b)
	{
		num = a;
		denum = b;
		if (denum == 0)
		{
			is_valid = false;
			return;
		}
		is_valid = true;
		if (denum != 1)
		{
			reduce();
		}
	}
	void swap()
	{
		std::swap(num, denum);
	}
	void reduce()
	{
		if (is_valid)
		{
			if (num == 0)
			{
				denum = 1;
				return;
			}
			T c = gcd(num, denum);
			if (c > 1)
			{
				num /= c;
				denum /= c;
			}
		}
	}
	factor operator+ (const factor& ref)
	{
		if (num == 0)
		{
			factor ret(ref.num, ref.denum);
			return ret;
		}
		if (ref.num == 0)
		{
			return *this;
		}
		factor ret(num, denum);
		ret.num = ret.num * ref.denum + ref.num * ret.denum;
		ret.denum *= ref.denum;
		T c = gcd(ret.num, ret.denum);
		if (c > 1)
		{
			ret.num /= c;
			ret.denum /= c;
		}
		if (ret.denum == 0)
		{
			ret.is_valid = false;
		}
		return ret;
	}
	factor operator- (const factor& ref)
	{
		if (num == 0)
		{
			factor ret(ref.num, ref.denum);
			return ret;
		}
		if (ref.num == 0)
		{
			return *this;
		}
		factor ret(num, denum);
		ret.num = ret.num * ref.denum - ref.num * ret.denum;
		ret.denum *= ref.denum;
		T c = gcd(ret.num, ret.denum);
		if (c > 1)
		{
			ret.num /= c;
			ret.denum /= c;
		}
		if (ret.denum == 0)
		{
			ret.is_valid = false;
		}
		return ret;
	}
	factor operator* (const factor& ref)
	{
		factor ret(num, denum);
		ret.num *= ref.num;
		ret.denum *= ref.denum;
		T c = gcd(ret.num, ret.denum);
		if (c > 1)
		{
			ret.num /= c;
			ret.denum /= c;
		}
		if (ret.denum == 0)
		{
			ret.is_valid = false;
		}
		return ret;
	}
	void operator+= (const factor& ref)
	{
		*this = *this + ref;
	}
	void operator-= (const factor& ref)
	{
		*this = *this - ref;
	}
	void operator*= (const factor& ref)
	{
		*this = *this * ref;
	}
	bool operator< (const factor& ref) const
	{
		T num1 = num*ref.denum;
		T num2 = ref.num*denum;
		return (num1 < num2);
	}
};

template <typename T>
void mul_string_with_n(int n, vector<T>& result)
{
	if (result.size() == 0)
	{
		return;
	}
	for (int i = 0; i < result.size(); i++)
	{
		result[i] *= n;
	}
	for (int i = 0; i < result.size() - 1; i++)
	{
		T tmp = result[i] / 10;
		result[i + 1] += tmp;
		result[i] = result[i] % 10;
	}
	T tmp;
	do
	{
		tmp = result[result.size() - 1] / 10;
		if (tmp)
		{
			result[result.size() - 1] = result[result.size() - 1] % 10;
			result.push_back(tmp);
		}
		else
		{
			break;
		}
	} while (1);
}

template <typename T>
void factorial_string(int n, vector<T>& result)
{
	result.resize(1);
	result[0] = 1;
	for (int i = 1; i <= n; i++)
	{
		mul_string_with_n(i, result);
	}
}

template <typename T>
void add_two_vector(vector<T>& a, vector<T>& b, vector<T>& s)
{
	s.resize(0);
	T remain = 0;
	int i = 0;
	for (; i < min(a.size(), b.size()); i++)
	{
		T tmp = a[i] + b[i] + remain;
		s.push_back(tmp % 10);
		remain = tmp / 10;
	}
	vector<T>& l_tmp = a.size() > b.size() ? a : b;
	for (; i < l_tmp.size(); i++)
	{
		T tmp = l_tmp[i] + remain;
		s.push_back(tmp % 10);
		remain = tmp / 10;
	}
	if (remain)
	{
		s.push_back(remain);
	}
}

class couple_int
{
public:
	couple_int()
	{
		m_a = 0;
		m_b = 0;
	}
	couple_int(int a, int b)
	{
		m_a = a;
		m_b = b;
	}
	int m_a;
	int m_b;
};

template <typename T>
class triple
{
public:
	triple()
	{
		a = 0;
		b = 0;
		c = 0;
	}
	triple(T _a, T _b, T _c)
	{
		a = _a;
		b = _b;
		c = _c;
	}
	T a;
	T b;
	T c;
};

class fraction
{
public:
	fraction()
	{
		numerator = 0;
		denominator = 0;
	}
	fraction(int n, int d)
	{
		numerator = n;
		denominator = d;
	}
	bool operator==(const fraction &other) const
	{
		return (numerator == other.numerator && denominator == other.denominator);
	}
	void reduce_form()
	{
		int gcd_num = gcd(numerator, denominator);
		if (gcd_num)
		{
			numerator /= gcd_num;
			denominator /= gcd_num;
		}
	}
	int numerator;
	int denominator;
};

struct int_3
{
	int_3()
	{
		a = 0;
		b = 0;
		a_pow_b = 0;
	}
	int_3(int pa, int pb, uint64_t ptmp)
	{
		a = pa;
		b = pb;
		a_pow_b = ptmp;
	}
	bool operator==(const int_3 &other) const
	{
		return (a_pow_b == other.a_pow_b);
	}
	bool operator<(const int_3 &other) const
	{
		return (a_pow_b < other.a_pow_b);
	}
	int a;
	int b;
	uint64_t a_pow_b;
};

template <typename T>
void sort(vector<T>& set)
{
	int size = set.size();
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (set[i] > set[j])
			{
				T tmp = set[i];
				set[i] = set[j];
				set[j] = tmp;
			}
		}
	}
}

// TODO: improve performance
template <typename T>
vector<T> divisors_of_n(T n, vector<T>& prime_list, set<T>& prime_set)
{
	vector<T> num_each_prime;
	vector<T> divisor_prime;
	vector<T> ret;
	if (n <= (T)1)
	{
		ret.push_back((T)1);
		return ret;
	}
	T tmp_n = n;
	int pSize = prime_list.size();
	if (prime_set.size())
	{
		if (prime_set.find(n) != prime_set.end())
		{
			ret.push_back((T)1);
			ret.push_back(n);
			return ret;
		}
	}
	for (int i = 0; i < pSize; i++)
	{
		if (tmp_n == 1)
		{
			break;
		}
		T tmp = 0;
		while (tmp_n % prime_list[i] == 0)
		{
			tmp_n /= prime_list[i];
			tmp++;
		}
		if (tmp)
		{
			//count_type++;
			num_each_prime.push_back(tmp);
			divisor_prime.push_back(prime_list[i]);
		}
	}
	vector<T> iterator;
	iterator.resize(num_each_prime.size());
	iterator.back() = 1;
	T limit = (T)(sqrt(n) + 0.5);
	while (1)
	{
		T p = 1;
		for (T i = 0; i < num_each_prime.size(); i++)
		{
			for (T j = 0; j < iterator[i]; j++)
			{
				p *= divisor_prime[i];
			}
		}
		if (p < n)
		{
			ret.push_back(p);
			//ret.push_back(n/p);
		}

		iterator.back()++;

		for (T i = iterator.size() - 1; i > 0; i--)
		{
			T once = false;
			if ((iterator[i] > num_each_prime[i]))
			{
				iterator[i] = 0;
				iterator[i - 1]++;
			}
			/*else if(p > limit)
			{
				if(once == false)
				{
					iterator[i] = 0;
					iterator[i - 1]++;
					once = true;
				}
			}*/
		}
		// break condition
		if (iterator[0] == num_each_prime[0] + 1)
		{
			break;
		}
	}
	sort(ret);
	return ret;
}

template <typename T>
T inverse_modulo(T a, T m)
{
	T t = 0;
	T newt = 1;
	T r = m;
	T newr = a;
	while (newr != 0)
	{
		T quotient = r / newr;
		T tmpt = t;
		T tmpnewt = newt;
		T tmpr = r;
		T tmpnewr = newr;
		t = tmpnewt;
		newt = tmpt - quotient * tmpnewt;
		r = tmpnewr;
		newr = tmpr - quotient * tmpnewr;
	}
	if (r > 1)
	{
		return 0;
	}
	if (t < 0)
	{
		t = t + m;
	}
	return t;
}

#endif
