///
/// @file  stop_primesieve.h
///
/// Copyright (C) 2013 Kim Walisch, <kim.walisch@gmail.com>
///
/// This file is distributed under the BSD License. See the COPYING
/// file in the top level directory.
///

#ifndef STOP_PRIMESIEVE_H
#define STOP_PRIMESIEVE_H

#include <exception>

class stop_primesieve : public std::exception { };

#endif
