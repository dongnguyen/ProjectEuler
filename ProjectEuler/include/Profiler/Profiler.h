// MAIN HEADER FILE FOR CProfiler CLASS
//
// A small profiling class using the Win32 API
//
// v.1.0
// Written by Hernan Di Pietro (May 2003)
// mailto:hernandp@speedy.com.ar
///////////////////////////////////////////////////
// See Profiler.cpp for implementation
//////////////////////////////////////////////////
#ifndef __PROFILER_H__
#define __PROFILER_H__

#ifdef WIN32
#include <windows.h>
#else
typedef long long __int64;
#endif

// enumeration for Debug Logging Output Types (or "styles")
enum LOGTYPE { LOGNONE, LOGTICKS, LOGSECS, LOGMILISECS, LOGMICROSECS, LOGALL, LOGMSGBOX };

class CProfiler
{
public:

    CProfiler(void);
    ~CProfiler(void);
    void		ProfileStart(LOGTYPE logtype = LOGNONE); // starts profiling
    __int64		ProfileEnd  (char* TraceStr = "");       // end profiling
    double		SecsFromTicks ( __int64 ticks);
    double		miliSecsFromTicks ( __int64 ticks);
    double		microSecsFromTicks ( __int64 ticks);
    DWORD		GetLastRetVal(void);	// returns the last error, if any.

private:

    LARGE_INTEGER	m_QPFrequency;		// ticks/sec resolution
    LARGE_INTEGER	m_StartCounter;		// start time
    LARGE_INTEGER	m_EndCounter;		// finish time
    __int64			m_ElapsedTime;		// elapsed time
    DWORD			m_Retval;			// return value for API functions
    LOGTYPE			m_LogType;			// logging type

};

#endif // __PROFILER_H__ 
