// IMPLEMENTATION FOR THE CProfiler CLASS
// (c) Hernan Di Pietro 2003.
/////////////////////////////////////////////////////////////////
#include <cstdio>
#include <iostream>
#include "profiler.h"
#pragma warning(disable:4996)

CProfiler::CProfiler(void)
{
    // constructor zeroes all LARGE_INTEGER data
    // members and gets the correct counter frequency
    // for the system.
    ZeroMemory(&m_QPFrequency, sizeof(m_QPFrequency));
    ZeroMemory(&m_ElapsedTime, sizeof(m_ElapsedTime));
    ZeroMemory(&m_StartCounter,sizeof(m_StartCounter));
    m_Retval = 0;

    // Query Frecuency
    m_Retval = QueryPerformanceFrequency(&m_QPFrequency);
}

CProfiler::~CProfiler(void)
{
    // destructor does nothing specific...
}

/////////////////////////////////////////////////////////////
// GetLastRetVal()
// returns error code for the latest API function called
/////////////////////////////////////////////////////////////
inline DWORD CProfiler::GetLastRetVal()
{
    return static_cast<DWORD>(m_Retval);
}

/////////////////////////////////////////////////////////////
// ProfileStart
// Starts time count, specifying the debug log type
// which is written when ProfileEnd is called.
// May be LOGNONE (default), LOGTICKS, LOGSECS, LOGALL
////////////////////////////////////////////////////////////
void CProfiler::ProfileStart(LOGTYPE logtype)
{
    // get and store start time
    m_Retval = QueryPerformanceCounter (&m_StartCounter);
    // store logging type
    m_LogType = logtype;
}

/////////////////////////////////////////////////////////////
// ProfileEnd
// End profiling and optionally write a string to the
// debug window or to a message box, depending on the
// LOGTYPE specified on ProfileStart(...)
////////////////////////////////////////////////////////////
__int64 CProfiler::ProfileEnd(char* TraceStr)
{
    // get and store finishing time and calc elapsed time(ticks)
    m_Retval = QueryPerformanceCounter (&m_EndCounter);
    m_ElapsedTime = (m_EndCounter.QuadPart  - m_StartCounter.QuadPart );

    // output debugging log?
    if (m_LogType != LOGNONE)
    {
        // variables for output
        char* MsgOut;
        char tmpbuf[300];

        if (m_LogType == LOGTICKS)	// output in ticks
            sprintf(tmpbuf, "** ProfileEnd: %I64d clock ticks elapsed.\n", m_ElapsedTime);

        if (m_LogType == LOGSECS)  // output in secs
            sprintf(tmpbuf, "** ProfileEnd: %.3fsecs. elapsed.\n", SecsFromTicks(m_ElapsedTime));

        if (m_LogType == LOGMILISECS)  // output in secs
            sprintf(tmpbuf, "** ProfileEnd: %.3fmilisecs. elapsed.\n", miliSecsFromTicks(m_ElapsedTime));

        if (m_LogType == LOGMICROSECS)  // output in secs
            sprintf(tmpbuf, "** ProfileEnd: %.3fmicrosecs. elapsed.\n", microSecsFromTicks(m_ElapsedTime));

        if (m_LogType == LOGMSGBOX)
            sprintf(tmpbuf, "\n\n%I64d clock ticks (%.3fsecs.) elapsed.\n",
                    m_ElapsedTime, SecsFromTicks(m_ElapsedTime));

        if (m_LogType == LOGALL)
            sprintf(tmpbuf, "** Profile End **\n[CPU Hires counter freq is: %I64d clock ticks"
                    "per second.\n%I64d clock ticks (%.3fsecs.) elapsed.\n",
                    m_QPFrequency.QuadPart ,m_ElapsedTime, SecsFromTicks(m_ElapsedTime));

        MsgOut = new char[strlen(tmpbuf)+strlen(TraceStr)+1];
        strcpy (MsgOut, TraceStr);
        strcat (MsgOut, tmpbuf);

        // select output to msgbox or debug-results window
        if (m_LogType == LOGMSGBOX)
        {
            MessageBox (NULL, MsgOut, "CProfiler::ProfileEnd Timing",
                        MB_ICONINFORMATION | MB_OK);
        }
        else
        {
            //OutputDebugString(MsgOut);
            std::cout << MsgOut;
        }

        delete [] MsgOut;
    }
    return m_ElapsedTime;
}

// The following function convert ticks to seconds
double CProfiler::SecsFromTicks (__int64 ticks)
{
    return static_cast<double>(ticks) / static_cast<double>(m_QPFrequency.QuadPart);
}

// The following function convert ticks to microseconds
double CProfiler::miliSecsFromTicks (__int64 ticks)
{
    return static_cast<double>(ticks) / static_cast<double>(m_QPFrequency.QuadPart) * 1e3;
}

// The following function convert ticks to microseconds
double CProfiler::microSecsFromTicks (__int64 ticks)
{
    return static_cast<double>(ticks) / static_cast<double>(m_QPFrequency.QuadPart) * 1e6;
}